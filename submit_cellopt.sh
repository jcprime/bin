#!/bin/bash
# submit_cellopt.sh (from submitnum.sh)
# Submits every .inp file in each of the path's subdirectories' "cellopt" subdirectories to CP2K on 4 slots
# NOTE: Only recurses one level deep

if [[ "$#" -eq "0" ]]; then
    echo "Please provide the path for traversal as first argument!"
    exit
fi

# Need to source file with bash function for Calling CP2K My Way (to be able to use cp2k call below)
source ~/.bash_functions/codes

path="$1"
cd "$path" || (echo "Cannot move into $path; exiting..." && exit)

for dir in *; do
    if [ -d "$dir" ]; then
        echo "$dir"
        cd "$dir/cellopt" || (echo "Cannot move into $dir/cellopt; trying next one..." && continue)
        for file in *.inp; do
            echo "Submitting $file to CP2K (4 slots)..."
            cp2k "$file" 4
            cd ../..
        done
    fi
done
