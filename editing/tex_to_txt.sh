#!/bin/bash
# tex_to_txt.sh
# Changes .tex extensions to .txt for ease of editing and/or amalgamating into larger files! 

cd /home/cirrusprime/Desktop/tex/

for i in *.txt; do
 cp "$i" /home/cirrusprime/Desktop/tex/"${i%.txt}.tex"
done
