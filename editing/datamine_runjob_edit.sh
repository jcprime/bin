#!/bin/bash
# datamine_runjob_edit.sh
# Edits dmol/datamine details to replace atoms accordingly for, erm, datamining purposes

path="james_p_rsync/dmol/datamine"
path2="james_p_rsync/dmol"

M=("Li" "Na" "K" "Rb")
X=("F" "Cl" "Br" "I")

# Defined existing structures to be used for datamining, to allow the later if-elif conditions to edit the respective run.job files correctly
MX=("LiF" "NaCl" "KF" "RbBr")

a="$1"
b="$2"
c="$3"
i="$4"
rank="$5"

      cp ~/"$path"/"${MX[c]}"_"$i"_lm"$rank".xyz ~/"$path2"/"${M[a]X[b]}"_"$i"/restart/"${M[a]X[b]}"_"$i"_lm"$rank"_"${MX[c]}".xyz
# Edits run.job file to make the default "DM_REPLACE_ATOMS:'Na-Cs''Na-K'" line read "DM_REPLACE_ATOMS:'M-M[a]''X-X[b]'"
      if [[ "$c" -eq "0" ]]; then
       echo '%s/DM_REPLACE_ATOMS:'Na-Cs''Na-K'/DM_REPLACE_ATOMS:'Li-"${M[a]"''F-"${X[b]}"/g
       w
       q
       ' | ex ~/"path2"/"${M[a]}${X[b]}_$i"/data/run.job
      elif [[ "$c" -eq "1" ]]; then
       echo '%s/DM_REPLACE_ATOMS:'Na-Cs''Na-K'/DM_REPLACE_ATOMS:'Na-"${M[a]"''Cl-"${X[b]}"/g
       w
       q
       ' | ex ~/"path2"/"${M[a]}${X[b]}_$i"/data/run.job
      elif [[ "$c" -eq "2" ]]; then
       echo '%s/DM_REPLACE_ATOMS:'Na-Cs''Na-K'/DM_REPLACE_ATOMS:'K-"${M[a]"''F-"${X[b]}"/g
       w
       q
       ' | ex ~/"path2"/"${M[a]}${X[b]}_$i"/data/run.job
      elif [[ "$c" -eq "3" ]]; then
       echo '%s/DM_REPLACE_ATOMS:'Na-Cs''Na-K'/DM_REPLACE_ATOMS:'Rb-"${M[a]"''Br-"${X[b]}"/g
       w
       q
       ' | ex ~/"path2"/"${M[a]}${X[b]}_$i"/data/run.job
      else
       echo "Conversion of run.job failed for ${MX[c]} to ${M[a]X[b]}_$i_lm$rank_${MX[c]}" >> ~/"$path2"/datamine_error.txt
      fi
echo "Hopefully the replacements for ${MX[c]} with ${M[a]}${X[b]} have worked."
