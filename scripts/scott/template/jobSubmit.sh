#!/bin/bash
#$ -cwd -V
#$ -q all.q
#$ -N Pacman
#$ -e job.error
#$ -o job.output
#$ -l msi_dmol=1

# setting up the environment
#setenv DMOL_TMP /scr/tomas/$JOB_ID
export DMOL3_DATA="/usr/local/msi/MaterialsStudio/Data/Resources/Quantum/DMol3/"

source /usr/local/msi/MaterialsStudio/Licensing/Setup/matStudioNewLicPackPath

/home/james/binX/klmc.exe
