/**********************************************************************/
/* generate_neighbours.cpp: Search molecule for neighbour list        */
/* NOTE: neighb is referenced from ZERO                               */
/* Started Dave and Dewi 23/03/1995                                   */
/* Updated James (Prime) 01/02/2017                                   */
/**********************************************************************/

#include <cstdio>
#include <cstring>
#include <cmath>

#include "structures.h"
#include "maxima.h"
#include "own_maths.h"

#define BOND_TOL 0.1

double standard_bond(char *atom1, char *atom2);
double atom_separation_squared(atom *p_A, atom *p_B, int pbc);

void generate_neighbours(atom *p_molecule, int num_atoms,
                            atom_number *p_types, int *p_num_types,
                            int use_pbc)
{
    #include "header.h"
    int iatom1, iatom2;
    int idummy2, idummy1;
    int itype;

    double standard, actual_dist;
    atom *p_atom1, *p_atom2;
    atom_number *p_this_type;

    /********* Work out how many of each atom are present in this molecule *****/

    *p_num_types = 0;
    strcpy(&(p_types->atom_type[0]), &(p_molecule->elem[0]));

    iatom1 = 0;
    p_atom1 = p_molecule;

    for (iatom1 = 1; iatom1 <= num_atoms; iatom1++)
    {
        p_atom1++;
        p_this_type = p_types - 1;

        for (itype=0; itype<= *p_num_types; itype++)
        {
            p_this_type++;
            if (!strcmp(&(p_this_type->atom_type[0]), &(p_atom1->elem[0])))
            {
                break;
            }
        }

        /**** Test to see if that was a known element *****/

        if (itype <= *p_num_types)
        {
            (p_this_type->num)++;
        }
        else
        {
            (*p_num_types)++;
            p_this_type++;
            strcpy( &(p_this_type->atom_type[0]), &(p_atom1->elem[0]));
            p_this_type->num = 0;
        }
    }

    /**********************************************************************/
    /***************** Loop over pairs of atoms in molecule ***************/
    /*****************         looking for bonds            ***************/
    /**********************************************************************/

    for (iatom1 = 0; iatom1 <= num_atoms; iatom1++)
    {
        p_atom1 = p_molecule + iatom1;

        for (iatom2 = iatom1 + 1; iatom2 <= num_atoms; iatom2++)
        {
            p_atom2 = p_molecule + iatom2;
            standard = standard_bond( &(p_atom1->elem[0]), &(p_atom2->elem[0]));

            if (standard != -1)
            {
                actual_dist = atom_separation_squared(p_atom1, p_atom2, use_pbc);
                actual_dist = sqrt(actual_dist);

                /* Test bond length against standard */

                if (actual_dist <= standard + BOND_TOL)
                {
                    idummy1 = (p_atom1->num_neigh)++;
                    p_atom1->neighb[idummy1] = iatom2;

                    idummy2 = (p_atom2->num_neigh)++;
                    p_atom2->neighb[idummy2] = iatom1;
                }
            } /* End of if (Standard) */
        }
    }
    return;
}
