#!/bin/bash
# tabulate_v2.sh
# Grabs the important bits out of the ground state .gout files
# and squidges them all into a table-y format

for zeo in nat sco mes; do
	working_dir="/home/james/data/test/$zeo/gs"
	cd $working_dir
	# Squishes tab-separated titles into new file called tablings.txt
	echo "File,Energy/eV,Gnorm,A/Ang,B/Ang,C/Ang,Alpha/Deg,Beta/Deg,Gamma/Deg,Volume/Ang^3,Primitive volume/Ang^3" > tablings_v2.csv
	for file in *.gout; do
		# Grabs values in order of headers and assigns them to variables
		energy=$(grep 'Final ene' "$file" | awk '{print $4}')
		gnorm=$(grep 'Final Gnorm' "$file" | awk '{print $4}')
		A=$(grep -A3 'Final cell par' "$file" | awk '/\sa/ && NR>1 {print $2}')
		B=$(grep -A4 'Final cell par' "$file" | awk '/\sb/ && NR>1 {print $2}')
		C=$(grep -A5 'Final cell par' "$file" | awk '/\sc/ && NR>1 {print $2}')
                alpha=$(grep -A6 'Final cell par' "$file" | awk '/\salpha/ && NR>1 {print $2}')
                beta=$(grep -A7 'Final cell par' "$file" | awk '/\sbeta/ && NR>1 {print $2}')
                gamma=$(grep -A8 'Final cell par' "$file" | awk '/\sgamma/ && NR>1 {print $2}')
                vol=$(grep 'Volume' "$file" | awk '/\sVolume/ {print $3}') #$2 would be initial volume, and $4 would be the calculated difference between the two
                prim_vol=$(grep 'Primitive cell volume' "$file" | awk '/\sPrimitive/ {print $5}') # Actually I may have incorrectly assumed "PV" to mean primitive volume rather than PV=pressure*volume, but have yet to have that clarified, so...
		# Tabulates new line in tablings_v2.csv with each tab-separated variable
		echo "$file,$energy,$gnorm,$A,$B,$C,$alpha,$beta,$gamma,$vol,$prim_vol" >> tablings_v2.csv
	done
done
