def unique_in_order(iterable):
    if len(iterable) == 0:
        return []
    else:
        uniques = [iterable[0]]
        for i in range(1,len(iterable)):
            if iterable[i] != iterable[i-1]:
                uniques.append(iterable[i])
        return uniques
