#!/usr/bin/env python
# Takes DNA strand as input and outputs the complement strand as string

def DNA_strand(dna):
    complement = ""
        for symbol in dna:
            if symbol == "A":
                partner = "T"
            if symbol == "T":
                partner = "A"
            if symbol == "C":
                partner = "G"
            if symbol == "G":
                partner = "C"
            complement = complement + partner
    return complement
