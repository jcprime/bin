#!/bin/bash
# pathway_find.sh
#   - Takes the second record in the given assumed-.ssv file
#       - Assuming that it has been written by energy_extract.sh, which will include
#         the starting file --- the one without the underscore --- from the previous step,
#         which will be the lowest-energy
#   - Appends it to the given output file
# To be called using, e.g. for file in *.ssv; do pathway_find.sh "$file" "pathway.ssv"; done

function print_usage() {
    echo "Usage: pathway_find.sh <ssv_file> <output_file>"
}

# If "-h" or "--help" is given when running the script as only argument, print usage information
if [[ ( "$#" -le "1" ) || ( ( "$1" == "-h" ) || ( "$1" == "--help" ) ) ]]; then
    print_usage && exit
elif [[ "$#" -eq "2" ]]; then
    ssv_file="$1"
    output_file="$2"
else
    # Just in case!
    print_usage && exit
fi

inpath="$(realpath "$ssv_file")"
outpath="$(realpath "$output_file")"

# echo "Input path: $inpath"
# echo "Output path: $outpath"

# echo >> "$output_file"
awk 'NR==2 {min=$0} END{print min}' "$ssv_file" >> "$output_file"
