#!/bin/bash
# sortinpfiles.sh
# Takes input files from path provided as first argument and copies them into their respective intended directories
# (Untested generalised version, originally from ~/test/coords/nat)

if [[ "$#" -eq "0" ]]; then
    echo "Please provide path to input files as first argument!"
    exit
fi

path="$1" # Should have zeolite abbreviation as $(dirname), ideally
cd "$path" || exit

for file in *[0-9][0-9]*.inp; do
    num="${file##[a-z]*}" && num="${num%%[_.]*inp}"
    name="${file%%.inp}"
    cp "$file" "$num/$name/"
done
