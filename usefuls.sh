#!/bin/bash
# usefuls.sh
# Making functions out of random useful things I've come across and want to remember!

function check_os() {
    # If zsh isn't installed, get the platform of the current machine
    platform=$(uname);
    # If the platform is Linux, tell the user which one if possible!
    if [[ $platform == 'Linux' ]]; then
        if [[ -f /etc/redhat-release ]]; then
            echo "You're running a Redhat-y version of Linux"
            #sudo yum install stuff
        fi
        if [[ -f /etc/debian_version ]]; then
            echo "You're running a Debian-y version of Linux"
            #sudo apt-get install stuff
        fi
    # If the platform is OS X, tell the user that!
    elif [[ $platform == 'Darwin' ]]; then
        echo "You're running OS X"
        exit
    fi
}

# Useful AWK and Jmol commands in here
function from_export_images() {
    num="$1"
    blah="$2"
    file="$3"

    path="$HOME" # Edit as necessary!
    to_find=$(awk -v find_me="$blah" 'NR==$find_me { print; exit }' "$file") #Sets find_me variable to blah, prints find_me-th line

    # Sets suffix to the result of to_find with the longest match of "*_" removed from the front of it
    suffix=${to_find##*_}

    dump_to_find=$(find ./"$num" ./* | grep "./^$to_find.*dump")
    cp "$path"/"$num"/"$dump_to_find" "$path"/structures
    cd "$path"/structures || exit
    echo "Exporting" "$blah$num\_$suffix\_lm$blah".dump "structure in" "$num" "directory, via Jmol, as" "$blah$num\_$suffix\_lm$blah".jpg "image of optimised rank $blah structure, in current directory."

    # Exports JPEG of visualised structure
    jmol -i -g 700x657 "$path/$num/$blah$num\_$suffix\_lm$blah".dump -s "$HOME"/bin/jmol_export.spt -w JPG:"$blah$num\_$suffix\_lm$blah".jpg -x #Call Jmol to export the structure associated with the given rank to a .jpg file of the same name in the current directory, and then exit when finished
}

function recursive_grep() {
    pattern="$1"
    start_dir="$2"
    grep -i -d recurse "$pattern" "$start_dir"
}
