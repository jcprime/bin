#!/bin/bash
# link_syreport.sh

src="/Users/jcprime/Google Drive/bin"
dst="/Users/jcprime/bin"
cd -P "$src"
pax -rwlpe . "$dst"

#dir="$HOME/Google\ Drive/bin"
#shopt -s globstar
#for file in "$dir"/*; do
	#path="$(dirname $file)"
	#ln -f "$path"/"$file" "$HOME/${path#*Drive/}/$file"
	#echo "Want to move $path$file to $HOME/${path#*Drive\/}/$file"
	#chmod go-w "$HOME/bin/"$path"/$file"
#done

# Alternatives suggested by Stack Overflow/Unix StackExchange
# 1:
#for file in `find /my/path -name "*.ext"`
#do
    #path="$(dirname $file)"
    #onlyfile="$(basename $file)"
    # ...
#done

# 2:
#src="$HOME/Google Drive/bin"
#dst="$HOME/bin"
#if [ -d "$dst" ]; then
	#rm -r "$dst/[a-zA-Z]*"
#fi
#absolute_dst=$(umask 077 && mkdir -p -- "$dst" && cd -P -- "$dst" && pwd -P) && (cd -P -- "$src" && pax -rwlpe . "$absolute_dst")
