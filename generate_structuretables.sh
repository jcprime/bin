#!/bin/bash
# generate_structuretables.sh
# Requires presence of tex_preamble.tex in same directory

if [ "$#" -eq "1" ]; then
    newfile="$1"
else
    newfile="$HOME/words/structure_tables_sco.tex"
fi

preamble_file="$HOME/bin/tex_preamble.tex"
graphicspath="$HOME/words/graphics"

cp "$preamble_file" "$newfile" || exit

{   echo '\begin{longtabu}[c]{m{1cm} m{6cm} m{6cm}}';
	echo '\caption{Ground-state structures for scolecite dehydration} \\';
	echo 'Step & GULP Structure \\';
	echo '\toprule';
	echo '\toprule';
	echo '\endfirsthead';
	echo '\toprule';
    echo 'Step & GULP Structure \\';
	echo '\toprule';
	echo '\endhead';
} >> "$newfile"

for num in {00..23}; do
    gulpfile="$(find "$graphicspath/scol_gulp/" -name "scol$num*.png")"
    #cp2kfile="$(find "$graphicspath/sco_cp2k/" -name "sco$num*.png")"
    {   echo "$((10#$num+1))"' &';
        echo '\includegraphics[width=0.5\textwidth]{'"$gulpfile"'} \\';
        #echo '\includegraphics[width=0.5\textwidth]{'"$cp2kfile"'} \\';
    } >> "$newfile"
    if [ "$num" -lt "23" ]; then
        echo '\midrule' >> "$newfile"
    else
        echo '\bottomrule' >> "$newfile"
    fi
done

{
    echo '\end{longtabu}';
    echo '\end{document}';
} >> "$newfile"
