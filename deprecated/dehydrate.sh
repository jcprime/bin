#!/bin/bash
# From ~/Tanya/Mesolite/16/meso_16.csh
# Need to also link to a script to set off the calculations ONLY WHEN the previous set have been sent AND RETURNED WITHOUT ERROR, and to notify me somehow if it did come back with error codes!

# Argument check
argc="$#"
if [[ "$argc" -eq "0" ]]; then
	echo "Usage: ./dehydrate.sh <path to file> <total H2O in this run>"
elif [[ "$argc" -gt "2" ]]; then
	echo "Usage: ./dehydrate.sh <path to file> <total H2O in this run>"
# Submits the jobs to GULP, for the given path and the given number of water molecules in this run
else
	path="$1"
	water="$2"
	while [[ "$water" -ge "0" ]]; do
		/usr/local/gulp < "$path"_n"$water".gin > "$path"_n"$water".gout
		water-=1
	done
fi
