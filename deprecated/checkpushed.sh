#!/bin/bash
# checkpushed.sh
# Check all commits have actually been pushed!  Since this has scuppered me more than once now, I'm scripting it
# DEPRECATED: HAVE SINCE ADDED FUNCTIONALITY TO ~/BIN/CHECKSTATUS.SH

for directory in "bin" ".dotfiles" "words" "test" "writings" "dotfiles-universal" "ice" "webstuff" "mfarmer" "flp" "wallpapers" ".config/sublime-text-3/Packages/User" "Library/Application Support/Sublime Text 3/Packages/User"; do
    cd "$HOME/$directory" || continue
    echo
    echo "$directory":
    git log origin/master..HEAD
    #echo
    cd "$HOME"
done
