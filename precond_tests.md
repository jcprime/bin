# precond_FULL_ALL
   971 OT CG       0.39E-14   12.1     0.00913106     -1738.8488723167  0.00E+00
   972 OT LS       0.78E-14    6.1                    -1738.8488723167
   973 OT CG       0.78E-14   12.1     0.00913106     -1738.8488723167  9.09E-13
   974 OT LS       0.16E-13    6.1                    -1738.8488723167
   975 OT CG       0.16E-13   12.1     0.00913106     -1738.8488723167  2.27E-12
   976 OT LS       0.37E-14    6.1                    -1738.8488723167
   977 OT CG       0.37E-14   12.1     0.00913106     -1738.8488723167  4.55E-13
   978 OT LS       0.74E-14    6.1                    -1738.8488723167
   979 OT CG       0.74E-14   12.1     0.00913106     -1738.8488723167  9.09E-13
   980 OT LS       0.15E-13    6.1                    -1738.8488723167
   981 OT CG       0.15E-13   12.1     0.00913106     -1738.8488723167  6.82E-13
   982 OT LS       0.30E-13    6.1                    -1738.8488723167
   983 OT CG       0.30E-13   12.1     0.00913106     -1738.8488723167  3.41E-12
   984 OT LS       0.66E-14    6.1                    -1738.8488723167
   985 OT CG       0.66E-14   12.1     0.00913106     -1738.8488723167  4.55E-13
   986 OT LS       0.13E-13    6.1                    -1738.8488723167
   987 OT CG       0.13E-13   12.1     0.00913106     -1738.8488723167  1.59E-12
   988 OT LS       0.32E-14    6.1                    -1738.8488723167
   989 OT CG       0.32E-14   12.1     0.00913106     -1738.8488723167  1.14E-12
   990 OT LS       0.13E-13    6.1                    -1738.8488723167
   991 OT CG       0.13E-13   12.1     0.00913106     -1738.8488723167  9.09E-13
   992 OT LS       0.34E-14    6.1                    -1738.8488723167
   993 OT CG       0.34E-14   12.1     0.00913106     -1738.8488723167  0.00E+00
   994 OT LS       0.68E-14    6.1                    -1738.8488723167
   995 OT CG       0.68E-14   12.1     0.00913106     -1738.8488723167  9.09E-13
   996 OT LS       0.14E-13    6.1                    -1738.8488723167
   997 OT CG       0.14E-13   12.1     0.00913106     -1738.8488723167  1.36E-12
   998 OT LS       0.34E-14    6.1                    -1738.8488723167
   999 OT CG       0.34E-14   12.1     0.00913106     -1738.8488723167  4.55E-13
  1000 OT LS       0.68E-14    6.1                    -1738.8488723167

  *** SCF run NOT converged ***


  Electronic density on regular grids:       -734.3069779240       89.6930220760
  Core density on regular grids:              823.9993803002       -0.0006196998
  Total charge density on r-space grids:       89.6924023762
  Total charge density g-space grids:          89.6924023762

  Overlap energy of the core charge distribution:               1.30028796218487
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1316.73529426008872
  Hartree energy:                                            1953.81681615361367
  Exchange-correlation energy:                               -373.84343456845761

  Total energy:                                             -1738.84887231667790

  outer SCF iter =   36 RMS gradient =   0.91E-02 energy =      -1738.8488723167

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_ALL            : diagonalization, state selective
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------

 *************************************************************************
 *** 03:36:31 ERRORL2 in cp_dbcsr_cholesky:cp_dbcsr_cholesky_decompose ***
 *** processor 0  :: err=-300 condition FAILED at line 126             ***
 *************************************************************************


 ===== Routine Calling Stack ===== 

           14 cp_dbcsr_cholesky_decompose
           13 qs_ot_get_derivative
           12 ot_mini
           11 ot_scf_mini
           10 qs_scf_loop_do_ot
            9 scf_env_do_scf_inner_loop
            8 scf_env_do_scf
            7 qs_energies_scf
            6 qs_forces
            5 cp_eval_at
            4 cp_cg_main
            3 geoopt_cg
            2 cp_geo_opt
            1 CP2K
 CP2K| condition FAILED at line 126
 CP2K| Abnormal program termination, stopped by process number 0
 CP2K| condition FAILED at line 126
 CP2K| Abnormal program termination, stopped by process number 2
 CP2K| condition FAILED at line 126
 CP2K| Abnormal program termination, stopped by process number 1
 CP2K| condition FAILED at line 126
 CP2K| Abnormal program termination, stopped by process number 3

# precond_FULL_KINETIC
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00  122.3     0.00052092     -1813.7620676843  2.48E-03

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987388855        0.0012611145
  Core density on regular grids:              823.9993806001       -0.0006193999
  Total charge density on r-space grids:        0.0006417146
  Total charge density g-space grids:           0.0006417146

  Overlap energy of the core charge distribution:               1.20303690008450
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.99926355634443
  Hartree energy:                                            1921.21451626065073
  Exchange-correlation energy:                               -420.32104827728477

  Total energy:                                             -1813.76206768431257

  outer SCF iter =   41 RMS gradient =   0.52E-03 energy =      -1813.7620676843

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_KINETIC        : inversion of T + eS
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00  135.0     0.00093571     -1813.7492726828  1.28E-02

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987389043        0.0012610957
  Core density on regular grids:              823.9993806001       -0.0006193999
  Total charge density on r-space grids:        0.0006416958
  Total charge density g-space grids:           0.0006416958

  Overlap energy of the core charge distribution:               1.20303690008450
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.98902423418212
  Hartree energy:                                            1921.23319400264654
  Exchange-correlation energy:                               -420.31669169560081

  Total energy:                                             -1813.74927268279498

  outer SCF iter =   42 RMS gradient =   0.94E-03 energy =      -1813.7492726828

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_KINETIC        : inversion of T + eS
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00  148.7     0.00168833     -1813.7022713664  4.70E-02
     2 OT LS       0.54E-01    6.0                    -1813.5468299754
     3 OT CG       0.54E-01   11.7     0.00016094     -1813.7710514672 -6.88E-02

  *** SCF run converged in     3 steps ***


  Electronic density on regular grids:       -823.9987389278        0.0012610722
  Core density on regular grids:              823.9993806001       -0.0006193999
  Total charge density on r-space grids:        0.0006416723
  Total charge density g-space grids:           0.0006416723

  Overlap energy of the core charge distribution:               1.20303690008450
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.96602863134467
  Hartree energy:                                            1921.23829434167283
  Exchange-correlation energy:                               -420.32057521617708

  Total energy:                                             -1813.77105146718259

# precond_FULL_SINGLE
    18 OT LS       0.47E-03    6.0                    -1780.6935558305
    19 OT CG       0.47E-03   12.0     0.04083692     -1782.9651341734 -1.46E+00
    20 OT LS       0.13E-02    6.0                    -1783.4195425301
    21 OT CG       0.13E-02   12.0     0.05683979     -1783.7334476783 -7.68E-01
    22 OT LS       0.12E-02    6.0                    -1785.5990735414
    23 OT CG       0.12E-02   11.9     0.07813594     -1785.5942763091 -1.86E+00
    24 OT LS       0.17E-02    6.0                    -1790.2224298402
    25 OT CG       0.17E-02   11.8     0.08309022     -1790.8575500316 -5.26E+00
    26 OT LS       0.15E-02    6.0                    -1796.1617795566
    27 OT CG       0.15E-02   11.9     0.06517762     -1796.2810308455 -5.42E+00
    28 OT LS       0.14E-02    6.1                    -1798.8346259901
    29 OT CG       0.14E-02   12.0     0.04094707     -1798.8285729265 -2.55E+00
    30 OT LS       0.90E-03    6.1                    -1799.2939920574
    31 OT CG       0.90E-03   11.9     0.02804283     -1799.6011415854 -7.73E-01
    32 OT LS       0.36E-02    6.0                    -1800.1810860303
    33 OT CG       0.36E-02   11.9     0.04698601     -1800.6173561771 -1.02E+00
    34 OT LS       0.22E-02    6.0                    -1801.6693643247
    35 OT CG       0.22E-02   12.0     0.06474613     -1802.4386862732 -1.82E+00
    36 OT LS       0.13E-02    6.0                    -1803.3164423148
    37 OT CG       0.13E-02   12.0     0.04737067     -1803.5706679871 -1.13E+00
    38 OT LS       0.95E-03    6.0                    -1804.4266581965
    39 OT CG       0.95E-03   11.9     0.05691780     -1804.5110252886 -9.40E-01
    40 OT LS       0.43E-03    6.0                    -1804.2744102928
    41 OT CG       0.43E-03   11.9     0.04570310     -1805.2717009298 -7.61E-01
    42 OT LS       0.12E-02    6.0                    -1805.9469808946
    43 OT CG       0.12E-02   11.9     0.04799860     -1805.6893553674 -4.18E-01
    44 OT LS       0.54E-03    6.0                    -1805.4279436795
    45 OT CG       0.54E-03   11.9     0.01275806     -1806.2868947157 -5.98E-01
    46 OT LS       0.22E-02    6.0                    -1806.3582319960
    47 OT CG       0.22E-02   12.1     0.01363905     -1806.5509603593 -2.64E-01
    48 OT LS       0.12E-02    6.5                    -1806.5976679359
    49 OT CG       0.12E-02   12.9     0.01937726     -1806.6675700621 -1.17E-01
    50 OT LS       0.57E-03    6.6                    -1806.6348310373
    51 OT CG       0.57E-03   12.9     0.01184855     -1806.7622551856 -9.47E-02
    52 OT LS       0.23E-02    6.5                    -1806.8317111110
    53 OT CG       0.23E-02   13.0     0.01171816     -1807.0303183124 -2.68E-01
    54 OT LS       0.11E-02    6.5                    -1807.0271288467
    55 OT CG       0.11E-02   13.0     0.01534351     -1807.1005168286 -7.02E-02
    56 OT LS       0.45E-02    6.5                    -1807.3262267986
    57 OT CG       0.45E-02   13.0     0.02044886     -1807.8894634595 -7.89E-01
    58 OT LS       0.12E-02    6.6                    -1805.9684018564
    59 OT CG       0.12E-02   13.0     0.02829122     -1808.0440041568 -1.55E-01
    60 OT LS       0.48E-03    6.3                    -1807.8401604029
    61 OT CG       0.48E-03   12.0     0.00913036     -1808.2190908410 -1.75E-01
    62 OT LS       0.19E-02    6.0                    -1808.2520771962
    63 OT CG       0.19E-02   12.0     0.01356402     -1808.3161786487 -9.71E-02
    64 OT LS       0.34E-02    6.1                    -1808.6135418681
    65 OT CG       0.34E-02   12.2     0.02916382     -1808.6795593511 -3.63E-01
    66 OT LS       0.11E-02    6.1                    -1807.3505655204
    67 OT CG       0.11E-02   12.1     0.03502559     -1808.6389135537  4.06E-02
    68 OT LS       0.51E-03    6.0                    -1808.5163015005
    69 OT CG       0.51E-03   12.0     0.00979742     -1808.9399688615 -3.01E-01
    70 OT LS       0.96E-03    6.0                    -1808.9729394406
    71 OT CG       0.96E-03   14.9     0.01094336     -1808.9819814875 -4.20E-02
    72 OT LS       0.38E-02    6.4                    -1809.0812341315
    73 OT CG       0.38E-02   12.1     0.01444662     -1809.3324252137 -3.50E-01
    74 OT LS       0.15E-02    6.2                    -1809.0222107375
    75 OT CG       0.15E-02   12.4     0.02166269     -1809.5106439896 -1.78E-01
    76 OT LS       0.47E-03    6.9                    -1809.1459878826
    77 OT CG       0.47E-03   13.6     0.00783716     -1809.6043727052 -9.37E-02
    78 OT LS       0.19E-02    6.9                    -1809.6268849263
    79 OT CG       0.19E-02   13.5     0.00873668     -1809.6856146192 -8.12E-02
    80 OT LS       0.15E-02    9.4                    -1809.7429460110
    81 OT CG       0.15E-02   12.3     0.01404910     -1809.7470286699 -6.14E-02
    82 OT LS       0.51E-03    5.9                    -1809.6265545767
    83 OT CG       0.51E-03   11.7     0.00749352     -1809.7913898435 -4.44E-02
    84 OT LS       0.20E-02    5.9                    -1809.8160235142
    85 OT CG       0.20E-02   11.9     0.00840180     -1809.8843192974 -9.29E-02
    86 OT LS       0.15E-02    6.6                    -1809.9369191757
    87 OT CG       0.15E-02   13.1     0.01330801     -1809.9436580253 -5.93E-02
    88 OT LS       0.55E-03    6.6                    -1809.8520103455
    89 OT CG       0.55E-03   12.7     0.00733985     -1809.9874501206 -4.38E-02
    90 OT LS       0.22E-02   10.3                    -1810.0127645005
    91 OT CG       0.22E-02   12.4     0.01044589     -1810.0738711026 -8.64E-02
    92 OT LS       0.21E-02    6.1                    -1810.2204995714
    93 OT CG       0.21E-02   11.6     0.01808710     -1810.2204948621 -1.47E-01
    94 OT LS       0.59E-03    5.9                    -1809.7424504401
    95 OT CG       0.59E-03   12.3     0.00701197     -1810.3029355608 -8.24E-02
    96 OT LS       0.24E-02    6.7                    -1810.3254588982
    97 OT CG       0.24E-02   13.4     0.00997904     -1810.3839904297 -8.11E-02
    98 OT LS       0.13E-02    6.9                    -1810.4182351808
    99 OT CG       0.13E-02   16.2     0.01368858     -1810.4653818552 -8.14E-02
   100 OT LS       0.52E-03    8.3                    -1810.4019815563
   101 OT CG       0.52E-03   12.6     0.00661386     -1810.5095479395 -4.42E-02
   102 OT LS       0.21E-02    6.2                    -1810.5290947587
   103 OT CG       0.21E-02   12.5     0.00959315     -1810.5755554301 -6.60E-02
   104 OT LS       0.23E-02    6.4                    -1810.7107646684
   105 OT CG       0.23E-02   14.2     0.01791008     -1810.7122186239 -1.37E-01
   106 OT LS       0.57E-03    7.2                    -1810.0292318045
   107 OT CG       0.57E-03   14.4     0.00679493     -1810.7885161874 -7.63E-02
   108 OT LS       0.23E-02    6.9                    -1810.8071070160
   109 OT CG       0.23E-02   17.8     0.01238582     -1810.8422831961 -5.38E-02
   110 OT LS       0.27E-02    6.4                    -1811.0645623347
   111 OT CG       0.27E-02   12.6     0.02189895     -1811.0703280549 -2.28E-01
   112 OT LS       0.59E-03    6.4                    -1809.6173947479
   113 OT CG       0.59E-03   12.6     0.00842876     -1811.1739815802 -1.04E-01
   114 OT LS       0.12E-02    6.8                    -1811.1886592276
   115 OT CG       0.12E-02   13.9     0.01212139     -1811.1938914596 -1.99E-02
   116 OT LS       0.12E-02    7.0                    -1811.2747874879
   117 OT CG       0.12E-02   13.9     0.01085426     -1811.2747918701 -8.09E-02

# precond_FULL_SINGLE_INVERSE
  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_SINGLE_INVERSE : inversion of H + eS - 2*(Sc)(c^T*H*c+const)(Sc)^T
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.08000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.80E-01   25.5     0.00016618     -1814.3546435207 -4.56E-04

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987366674        0.0012633326
  Core density on regular grids:              823.9993806423       -0.0006193577
  Total charge density on r-space grids:        0.0006439749
  Total charge density g-space grids:           0.0006439749

  Overlap energy of the core charge distribution:               1.20648606984002
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.98808219837429
  Hartree energy:                                            1920.84626283060197
  Exchange-correlation energy:                               -420.53763849545095

  Total energy:                                             -1814.35464352074177

  outer SCF iter =   13 RMS gradient =   0.17E-03 energy =      -1814.3546435207

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_SINGLE_INVERSE : inversion of H + eS - 2*(Sc)(c^T*H*c+const)(Sc)^T
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.08000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.80E-01   39.6     0.00041840     -1814.3544481214  1.95E-04

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987367142        0.0012632858
  Core density on regular grids:              823.9993806423       -0.0006193577
  Total charge density on r-space grids:        0.0006439281
  Total charge density g-space grids:           0.0006439281

  Overlap energy of the core charge distribution:               1.20648606984002
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.93965336478823
  Hartree energy:                                            1920.88701001378399
  Exchange-correlation energy:                               -420.52976144568561

  Total energy:                                             -1814.35444812138076

  outer SCF iter =   14 RMS gradient =   0.42E-03 energy =      -1814.3544481214

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_SINGLE_INVERSE : inversion of H + eS - 2*(Sc)(c^T*H*c+const)(Sc)^T
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.08000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.80E-01   53.8     0.00124622     -1814.3385708064  1.59E-02
     2 OT LS       0.24E-01    5.9                    -1814.2613741174

# precond_FULL_S_INVERSE
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00   50.7     0.00268112     -1813.6516775256  7.90E-02
     2 OT LS       0.27E-01    6.2                    -1811.8979567268
     3 OT CG       0.27E-01   12.2     0.00015337     -1813.7378064569 -8.61E-02

  *** SCF run converged in     3 steps ***


  Electronic density on regular grids:       -823.9987391840        0.0012608160
  Core density on regular grids:              823.9993805001       -0.0006194999
  Total charge density on r-space grids:        0.0006413161
  Total charge density g-space grids:           0.0006413161

  Overlap energy of the core charge distribution:               1.11573840906692
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.87213506781927
  Hartree energy:                                            1921.39693580976336
  Exchange-correlation energy:                               -420.26477961947251

  Total energy:                                             -1813.73780645693023

  outer SCF iter =   16 RMS gradient =   0.15E-03 energy =      -1813.7378064569

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_S_INVERSE      : cholesky inversion of S
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00   25.2     0.00014976     -1813.7383035646 -4.97E-04

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987391852        0.0012608148
  Core density on regular grids:              823.9993805001       -0.0006194999
  Total charge density on r-space grids:        0.0006413148
  Total charge density g-space grids:           0.0006413148

  Overlap energy of the core charge distribution:               1.11573840906692
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.87180256877991
  Hartree energy:                                            1921.39700589960717
  Exchange-correlation energy:                               -420.26501431794497

  Total energy:                                             -1813.73830356459803

  outer SCF iter =   17 RMS gradient =   0.15E-03 energy =      -1813.7383035646

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : FULL_S_INVERSE      : cholesky inversion of S
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00   38.3     0.00029724     -1813.7400794422 -1.78E-03

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987391803        0.0012608197
  Core density on regular grids:              823.9993805001       -0.0006194999
  Total charge density on r-space grids:        0.0006413197
  Total charge density g-space grids:           0.0006413197

  Overlap energy of the core charge distribution:               1.11573840906692
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1320.86848244408338
  Hartree energy:                                            1921.39953557067042
  Exchange-correlation energy:                               -420.26599974195801

  Total energy:                                             -1813.74007944224468

# precond_NONE

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : NONE
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00   84.8     0.00040551     -1813.3374648855  1.24E-03

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987343657        0.0012656343
  Core density on regular grids:              823.9993805577       -0.0006194423
  Total charge density on r-space grids:        0.0006461920
  Total charge density g-space grids:           0.0006461920

  Overlap energy of the core charge distribution:               1.18604230995313
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1321.24576260192953
  Hartree energy:                                            1921.38682915464710
  Exchange-correlation energy:                               -420.29826282796927

  Total energy:                                             -1813.33746488554652

  outer SCF iter =  649 RMS gradient =   0.41E-03 energy =      -1813.3374648855

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : NONE
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00   97.0     0.00073322     -1813.3305423250  6.92E-03

  *** SCF run converged in     1 steps ***


  Electronic density on regular grids:       -823.9987344679        0.0012655321
  Core density on regular grids:              823.9993805577       -0.0006194423
  Total charge density on r-space grids:        0.0006460898
  Total charge density g-space grids:           0.0006460898

  Overlap energy of the core charge distribution:               1.18604230995313
  Self energy of the core charge distribution:              -4636.85783612410705
  Core Hamiltonian energy:                                   1321.12554956055942
  Hartree energy:                                            1921.47237055631240
  Exchange-correlation energy:                               -420.25666862773420

  Total energy:                                             -1813.33054232501604

  outer SCF iter =  650 RMS gradient =   0.73E-03 energy =      -1813.3305423250

  ----------------------------------- OT ---------------------------------------

  Allowing for rotations:  F
  Optimizing orbital energies:  F
  Minimizer      : CG                  : conjugate gradient
  Preconditioner : NONE
  Precond_solver : DEFAULT
  Line search    : 2PNT                : 2 energies, one gradient
  stepsize       :    0.15000000
  energy_gap     :    0.00100000

  eps_taylor     :   0.10000E-15
  max_taylor     :             4

  mixed_precision    : F

  ----------------------------------- OT ---------------------------------------

  Step     Update method      Time    Convergence         Total energy    Change
  ------------------------------------------------------------------------------
     1 OT CG       0.15E+00  109.3     0.00137451     -1813.2970018283  3.35E-02

