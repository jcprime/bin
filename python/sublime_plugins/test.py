#!/usr/bin/env python3

import sublime, sublime_plugin

class TestCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.insert(edit, 0, "Hello, World!")
