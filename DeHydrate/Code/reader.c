/*****************************************************************************/ 
/******  reader.c                                                *************/ 
/******  Input file reader. started 23/11/94 DWL                 *************/ 
/******  from Tim Bush's reader.c                                *************/ 
/******                                                          *************/ 
/******  IMPORTANT CHANGE: dwl 10.4                              *************/
/******  interaction keyword now cumulative                      *************/
/******  i.e. if charges specificied then steric+nonbond automatic ***********/
/******  if nonbonded specified then steric automatic              ***********/
/******  Bomb out if steric = 0                                    ***********/
/******                                                            ***********/
/******  22/2/96 DWL new keywords                                  ***********/
/******  initial_minimize                                          ***********/
/******  flag whether to do template min and inpore min before we  ***********/
/******                                                            ***********/
/******  21/10/96 DWL                                              ***********/
/******  Logical flagging now included for most things:            ***********/
/******     keyword [on/off]                                       ***********/
/******        replacing earlier effort with yes/no which was cack ***********/
/******  Keywords implemented in this way are:                     ***********/
/******  steric, non_bonded, charges, initial_minimize             ***********/
/******  centre, check                                             ***********/
/******  NEW KEYWORD : random [on/off]                             ***********/
/******  NEW KEYWORD : determines if real_random is random or not! ***********/
/******  21/10/96 DWL                                              ***********/
/******  NEW KEYWORD : cost_function [steric/non_bond/energy]      ***********/
/******  NEW KEYWORD : determines which cost_function to use       ***********/
/******  7th March 1997 Dave Willock                               ***********/
/******  Decided that cost_function directive can set steric,      ***********/ 
/******  non_bond and charges logicals now so the explicit input   ***********/ 
/******  to set them has been removed!!                            ***********/
/******  Dewi and Dave June 97                                     ***********/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include "maxima.h"
#include "global_values.h"
#include "structures.h"
#include "data.h"
#include "reader.h"

int  parse_line(void);

int locate_string( char *p_key, char *p_char, int num_of_chars );

int reader(char *file_to_read)
{
#include "header.h"
  char *fullstop;
  int abandon_ship, error;


printf("\n\nENTERING READER\n\n");
  if (!(input_fp = fopen(file_to_read, "r")))
     {
       fprintf(output_fp,"ERROR: Problem opening file %s for input\n", file_to_read);
       exit(1);
     }

  for(;;)   /* get the info from the file */
    {
      abandon_ship = parse_line();

      fclose(input_fp);
      return(abandon_ship);
    }
}

/*****************************************************************************
  Parse_line, chops an input line from the input file and chops it into
  tokens, each of which is tested against the list of allowed directives
  in reader.h.  The correct function to read subsequent program parameters
  is then called..
*****************************************************************************/

int check_element(char *p_element);
int find_kind(char *token, int level);
char * tok_get(FILE *input_fp, int skip_lines);
int get_dehydrate(void);
int get_ow_label(void);
int get_hw_label(void);


int    parse_line(void)
{
#include "header.h"
  int   l;
  int   bail_out;
  int   token, second_token, third_token;
  char  *tok, *last_tok;
  char  *inform;
  

  l = 0;
  bail_out= FALSE;
  
  for (;;)
    {

      if (!(tok = tok_get(input_fp, TRUE)))
    	{
	    if (++l > 3) break; else continue;
	    }
      l = 0;
/*****************************************************************/
/**** If required print out the token you are about to process ***/
/*****************************************************************/

      token = find_kind(tok, PRIME_DIRECTIVE);

/****DEBUG ****/
printf("Token = >>%s<<\n", tok);
/****DEBUG ****/

/*****************************************************************/
/**** If the last command was not found let the user know ********/
/**** and then bail out after processing the entire file! ********/
/**** Added 4/3/97 Dave Willock **********************************/
/*****************************************************************/
      if (token == BLANK_DIRECT)
         {
            fprintf(output_fp,"ERROR: The command >>%s<< was not recognised\n",tok);
            bail_out= TRUE;
         }

      last_tok = tok;
      switch (token)
	   {
           case DEHYDRATE: bail_out= get_dehydrate() || bail_out; break;
                case OXYGEN: bail_out= get_ow_label()||bail_out;break;
                case HYDROGEN:  bail_out=get_hw_label()||bail_out;break;
                break;


           }
}

/*******************************************************************************/
/* errors will have been reported and the code should wind up if bail_out TRUE */  
/*******************************************************************************/

  return (bail_out); 
}

/******************************************************************************
  tok_get.c : parses command lines in a semi intelligent fashion
IMportant change 22/6/95 DWL Now ignores EVERYTHING after a #
******************************************************************************/


char * tok_get(FILE *input_fp, int skip_lines)
{
  int          i;
  char         *tok;
  static char  target[BUFFER];
  static char  buf[BUFFER];
  static char  *line, *last_tok;
  extern int   read_new_line, line_no;
  char         *comment;	

  i = 0;

/**************************************************************************/
/*** If the last token read was a NULL character last_tok will act ********/
/*** like a FALSE logical and force us to read the next line **************/
/**************************************************************************/

  if (!last_tok && skip_lines)
    {

/**************************************************************************/
/**** Read in the next line from input_fp, if it is NULL return NULL ******/
/**************************************************************************/

      if (!(line = fgets(target, BUFFER, input_fp))) return(line);

/**************************************************************************/
/**** Remove commented lines or parts of lines that are comments **********/
/**************************************************************************/

      if ((comment = strchr(line,'#')) != NULL) 
        {

/**************************************************************************/
/****** chop off the bits after the hash **********************************/
/**************************************************************************/

           *(comment) = '\0';

/**************************************************************************/
/****** if this is a simple comment line it will now have length 0 ********/
/****** so simply return a single # character                      ********/
/**************************************************************************/

           if (strlen(line) < 1) 
             {
               *line= '#';
               *(line+1)= '\0';
               return(line);
             }
         }

/**************************************************************************/
/***** read_new_line says we have just read a new line in !! **************/
/**************************************************************************/

       read_new_line = TRUE;
       line_no++;
    }
  else if (!last_tok && !skip_lines)
    {
       return(NULL);
    }

/**************************************************************************/
/****** If we have just read a new line copy it to the buffer *************/
/**************************************************************************/

  if (read_new_line) strncpy(buf, line, BUFFER);

/**************************************************************************/
/***** Now process the latest string of information ***********************/
/**************************************************************************/

  for(;;)
    {

/**************************************************************************/
/***** tok is read from the last line if some tokens are left *************/
/**************************************************************************/

      tok = (read_new_line) ? strtok(buf, " :;,\n\r\t") : strtok(NULL, " :;,\n\r\t");

      read_new_line = (!tok) ? TRUE : FALSE; 
      last_tok = tok;
      if (!tok || !isalpha(*tok)) break;

    /* DO NOT LOWER CASE CONVERT */
    /*  for (i = 0; tok[i] != '\0'; i++) tok[i] = tolower(tok[i]);*/
      if (find_kind(tok, REMAINING_DIRECTIVE) != UNIT) break;
      continue;
    }
  return(tok);
}
  
/******************************************************************************
  Find_kind takes a parsed token, and matches it against the directive list
  contained in reader.h, returns a BLANK_DIRECT value if token is unrecognised,
  and a UNIT value if the token is extraneous clutter..
******************************************************************************/

list null_namelist[]   = {NULL_DIRECTIVE_LIST};
list first_namelist[]  = {FIRST_DIRECTIVE_LIST};
list second_namelist[] = {SECOND_DIRECTIVE_LIST};
list third_namelist[] = {THIRD_DIRECTIVE_LIST}; 

/*****************************************************************************/
/*** find_kind altered to be context aware by knowing the level from *********/
/*** which the token was read. Dave Willock March 1997 ***********************/
/*****************************************************************************/

int find_kind(char *token, int level)
{
  int i, j,k,  l;
  
  i = 0;
  j = 0;
  k = 0;
  l = 0;

  if (!token) return(-1);

if (level == PRIME_DIRECTIVE || level == REMAINING_DIRECTIVE)
  {
    do
      {
        if (!strncmp(token, first_namelist[j].directive,4))
          {
	     return(first_namelist[j].token_index);
	  }
      }
  while(first_namelist[j++].token_index != BLANK_DIRECT);
  }
else if (level == SECONDARY_DIRECTIVE)
  {
    do
      {
        if (!strncmp(token,second_namelist[k].directive,4))
  	  {
	    return(second_namelist[k].token_index);
	  }
      }
    while(second_namelist[k++].token_index != BLANK_DIRECT); 
  }
else if (level == TERTIARY_DIRECTIVE)
  {
    do 
     { 
/**********************************************************/
/**** For 3rd directive compare only to 2nd character *****/
/**********************************************************/
       if (!strncmp(token,third_namelist[l].directive,2))
	 { 
	  return(third_namelist[l].token_index); 
	 } 
     } 
    while(third_namelist[l++].token_index != BLANK_DIRECT);
  }

    do
      {
        if (!strncmp(token, null_namelist[i].directive,2))
	  {
	    return(UNIT);
	  }
      }
    while(null_namelist[i++].token_index != BLANK_DIRECT);

  return(BLANK_DIRECT);
}
  
/******************************************************************************
  Get token routines for individual datatypes, token is taken from line and
  cast to apprpriate type...
******************************************************************************/
 
int get_integer(int *p_error, int skip_lines)
{
#include "header.h"
  int answer;
  char *tok;

  tok= tok_get(input_fp, skip_lines);

/*******************************************************************/
/**** Check all is well, tok will give FALSE if it is NULL *********/
/**** If there is a string there check the first character *********/
/**** to see if it is a valid decimal number               *********/
/**** Dave Willock March 1997 **************************************/
/*******************************************************************/

  if (tok)
    {
       if ((*tok >= '0' && *tok <= '9') || *tok == '-')
          {
             answer = atoi(tok);
             *p_error = FALSE;
          }
       else
          {
             answer= 0;
             *p_error = TRUE;
          }
    }
  else
    {
       answer= 0;
       *p_error = TRUE;
    }
  return (answer);

}

char * get_string(void)
{
#include "header.h"
  char *dummy;

  dummy = tok_get(input_fp,FALSE);
  return (dummy);
}

double get_double(int *p_error, int skip_lines)
{
#include "header.h"
double answer;
int itsanum;
char *tok;
  
  tok= tok_get(input_fp, skip_lines);

/*******************************************************************/
/**** Check all is well, tok will give FALSE if it is NULL *********/
/**** If there is a string there check the first character *********/
/**** to see if it is a valid decimal number               *********/
/**** Dave Willock March 1997 **************************************/
/*******************************************************************/

  if (tok)
    {
       if ((*tok >= '0' && *tok <= '9') || *tok == '-' || *tok == '.')
          {
             answer = atof(tok);
             *p_error = FALSE;
          }
       else
          {
             answer= 0;
             *p_error = TRUE;
          }
    }
  else
    {
       answer= 0;
       *p_error = TRUE;
    }
  return (answer);
}
  
/******************************************************************************
  Contains all the files used in parse_line to read specific data into the 
  correct variables.  Also contains error function returning error parsing
  file message, including error line and lasdt token read
******************************************************************************/

void parse_error(char *last_tok)
{
#include "header.h"
  if (!(output_fp =fopen(output_file, "w")))
    {
      fprintf(output_fp,"\nERROR: opening file \"%s\"\n", output_file);
      exit(1);
    }
  fprintf(output_fp,"\n\terror parsing input file: %s", output_file);
  fprintf(output_fp,"\n\t\tlast token read: %s", last_tok);
  fprintf(output_fp,"\t at or near line: %d\n\n", line_no);
  fclose(output_fp);
  exit(1);
} 


int get_dehydrate(void)
{
#include "header.h"
int error;
int i;

printf("get_dehydrate\n");
    num_dehydrate_labels = get_integer(&error, FALSE);
    if (num_dehydrate_labels <1)
           {
            fprintf(output_fp,"Error, number of dehydrate labels not valid\n");
            fprintf(output_fp,"Exiting....\n");
            exit(-1);
           }

     for (i=0;i<num_dehydrate_labels;i++)
        {
        fgets(buffer,BUFFER,input_fp);
        sscanf(buffer, "%s", &dehydrate_this[i]);
        }
    for (i=0;i<num_dehydrate_labels;i++)
        {
        printf("%s \n", dehydrate_this[i]);
        }
    


     return;
    
}


int get_ow_label(void)
{
#include "header.h"
int error;
int i;

printf("get_oxygen_label\n");
    
     num_ow_labels = get_integer(&error, FALSE);
    if (num_ow_labels <1)
           {
            fprintf(output_fp,"Error, number of ow labels not valid\n");
            fprintf(output_fp,"Exiting....\n");
            exit(-1);
           }

    
     for (i=0;i<num_ow_labels;i++)
        {
        fgets(buffer,BUFFER,input_fp);
        sscanf(buffer, "%s", &ow_label[i]);
        }
     return;
}


int get_hw_label(void)
{
#include "header.h"
int error;
int i;

printf("get_hydrogelabel\n");

     num_hw_labels = get_integer(&error, FALSE);
    if (num_hw_labels <1)
           {
            fprintf(output_fp,"Error, number of hw labels not valid\n");
            fprintf(output_fp,"Exiting....\n");
            exit(-1);
           }


     for (i=0;i<num_hw_labels;i++)
        {
        fgets(buffer,BUFFER,input_fp);
        sscanf(buffer, "%s", &hw_label[i]);
        }
     return;
}




