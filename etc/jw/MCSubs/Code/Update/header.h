/************************************/
/* THIS IS MY STUFF                 */
/************************************/

#ifdef MAIN
#define EXTERNAL
#else
#define EXTERNAL extern
#endif

/********************** read variables********************/

EXTERNAL int read_new_line;
EXTERNAL int line_no;

EXTERNAL FILE *input_fp;
EXTERNAL FILE *output_fp;
EXTERNAL char gulpfile[81];
EXTERNAL char gulpfile_out[81];
EXTERNAL char gulpfile_in[81];
EXTERNAL char gulp_command_line[256];
EXTERNAL char gulp_path[256];
EXTERNAL char analyse_path[256];
EXTERNAL char generate_path[256];
EXTERNAL char regenerate_path[256];
EXTERNAL char run_gulp_command[256];
EXTERNAL char coordinate_file[81];
EXTERNAL char pots_file[81];
EXTERNAL char output_file[81];

EXTERNAL int num_fixed_cations;
EXTERNAL double ratio;
EXTERNAL int keep;
EXTERNAL int cation_insert_run;
EXTERNAL double fixed_cation_cutoff;
EXTERNAL int peek_freq;
EXTERNAL int max_cycles;
EXTERNAL int max_subs;
EXTERNAL int gulp_max_cycles;
EXTERNAL int lowenstein;
EXTERNAL int generate_only;
EXTERNAL int regenerate_run;
EXTERNAL int configurations_flag;
EXTERNAL int randomisation;
EXTERNAL char title[81];
EXTERNAL char cell_volume_flags[20];
EXTERNAL char cation[8];
EXTERNAL char insert_ele[3];
EXTERNAL char replace_ele[3];
EXTERNAL double cation_charge;
EXTERNAL double insert_charge;
EXTERNAL double replace_charge;
EXTERNAL double gulp_time_limit;

EXTERNAL int script_type;
EXTERNAL int analyse_type;


/*********************periodic structure info ***********************/

/********************************************************************/
/***** pbc    : flags that periodic boundary conditions are *********/
/*****          to be used                                  *********/
/***** abc    : holds a b c alpha beta gamma Angstroms      *********/
/*****                                     and degrees      *********/
/***** latt_vec : holds cartessian vectors for a b c        *********/
/***** recip_latt_vec : holds recip. space a* b* c*         *********/
/********************************************************************/

EXTERNAL int pbc;
EXTERNAL double abc[6];
EXTERNAL double latt_vec[9];
EXTERNAL double real_latt_sizes[3];
EXTERNAL double recip_latt_vec[9];
EXTERNAL double recip_latt_sizes[3];
EXTERNAL double cell_volume;



#undef EXTERNAL
