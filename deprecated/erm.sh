#!/bin/bash
# erm.sh

# Get x (stage of dehydration - ie. number of water molecules missing)


# Find lowest energy structure for the last dehydration step
# (where -k5 means sort by the value in the 5th column, with
# whitespace as the default delimiter, and n is numerical sort)
grep 'Final ene' na"$x"*out | sort -n -k5
top=$(grep 'Final ene' na"$x"*out | sort -n -k5 | cut -f1)
echo "$top"

# Below is gulp_batch.sh, which will be called by this script
argc="$#"
water_total="$1"
prefix="$2"

for i in {0.."$water_total"}; do
	/usr/local/gulp/gulp-4.2.mpi < "$prefix"_N"$i".gin > "$prefix"_N"$i".gout
done
