/****************************************************************/
/* DeHydrate : Utility to scan Gulp input file for water and    */
/*             generate input files for a dehydratiion.         */
/* DWL 07/12/00                                                 */
/****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "maxima.h"
#include "global_values.h"
#include "structures.h"

#define MAIN 0
#include "header.h"
#include "data.h"
#include "ewald.h"
#include "own_maths.h"
#undef MAIN

#define MAXATOMS 8000
#define BUFFER    256
#define TRUE        1
#define FALSE       0

#define FIND_MIN "/home/dwl22/DeHydrate/Code/find_min.csh"
int reader(char *file_to_read);
double atom_separation_squared(atom *p_A, atom *p_B, int pbc);
simple_atom h_pos(simple_atom *si1, simple_atom *si2, simple_atom *o);
simple_atom X_pos(simple_atom *si1, simple_atom *si2, simple_atom *o);
void save_config(FILE *file_fp, int config, int num_replace_atoms, 
                 double final_energy,
                 int *p_replaceable_index,int *p_replaceable_done);


void generate_neighbours( atom *p_molecule, int num_atoms,
                          atom_number *p_types, int *p_num_types,
                          int use_pbc);
void print_neighbours( atom *p_molecule, int num_atoms, FILE *fp);

void print_molecule(atom *p_molecule, int num_atoms, FILE *output_fp);

void set_elem(atom *p_molecule, int num_atoms);
void steric_setup(atom *p_molecule, int num_atoms);
double real_random(int done);
void index_sort(int n, double *p_arrin  ,int *p_indx);
void save_xtlfile(FILE *file_fp, atom *p_mol, 
                   int num_atoms, char *p_spacegroup);
                   
void save_gulpfile(FILE *file_fp, char *p_dump_name, atom *p_mol, 
                   int num_atoms, char *p_spacegroup,
                   char *p_xtlname, char *p_pots_file);

void main(argc, argv)
	int   argc;
	char  **argv;
{
        int finish, line_before,line_count;
        int clash;
        int found_shell, done_this,got_one;
        int first_shell;
        double core_shell_sep;
        double rdummy;
        double actual_dist;
	int i, j;
	int iloop, jloop;
        int ivec;
        int max_trials, this_trial;
        int  subs_done;
	int skip_this_solvent;
	int skip_this_flag;
	int num_atoms, num_solvent_molecules;
        int solvent_mol_no;
	int ineigh,jneigh;
	int num_mol_types;
	char dummy[BUFFER];
	char lines[1000][85];
	atom mol[MAXATOMS];
	atom framework[MAXATOMS];
	atom solvent[MAXATOMS];
        char input_file[BUFFER], output_file[BUFFER];
        char control_file[BUFFER];
        char script_file[BUFFER];
        simple_atom cation_pos, O1, O2, T1, T2, bridgeO;
	int replaceable_index[MAXATOMS];
	int replaceable_done[MAXATOMS];
	atom_number mol_types[10];
        int random_seed;
	FILE *input_fp;
	FILE *script_fp;
	FILE *gulpfp;
	FILE *dumpfp;
	FILE *gulpoutputfp;
	FILE *coordinate_fp;
	FILE *config_fp;
	char *fullstop;
	char gulpfile_root[BUFFER];
	char dumpfile[BUFFER];
	char config_file[BUFFER];
	char pot_file[BUFFER];
        char spacegroup[20];
        char append_pots_command[256];
	int num_ow, num_hw;
        int num_framework_atoms;
        int num_solvent_atoms;
	char this_output_file[BUFFER];
        double a,b,c, alpha, beta, gamma;
        atom temp_atom;

/***** labels of water molecules and how many***/
/*now external in header.h*/
        /*char ow_label[MAX_LABELS][4];*/
        /*char hw_label[MAX_LABELS][4];*/
        /*int  num_ow_labels;*/
        /*int  num_hw_labels;*/
        /*int  is_water_flag;*/
        /*int  is_ow;*/
        /*int  is_water; */
/***** labels of water molecules that can be dehydrated *****/
        /*char dehydrate_this[MAX_LABELS][4];*/
        /*int  num_dehydrate_labels;*/
        /*int  num_to_dehydrate;*/
        /*int  dehydrate_this_flag;*/
	
    if (argc == 1)
		{
		fprintf(stderr, "Usage: %s  file [-c control]\n", 
                                 argv[0]);
                fprintf(stderr, "       Default: water is O1 and H, all molecules dehydrate\n");
                fprintf(stderr, "       Optional control file has labels of water oxygen and hydrogen\n");
                fprintf(stderr, "       and of molecules to remove\n");
                fprintf(stderr, "       e.g\n");
                fprintf(stderr, "       dehydrate 1\n");
                fprintf(stderr, "       O4\n");
                fprintf(stderr, "       oxygen 2\n");
                fprintf(stderr, "       O4\n");
                fprintf(stderr, "       O3\n");
                fprintf(stderr, "       hydrogen 2\n");
                fprintf(stderr, "       H\n");
                fprintf(stderr, "       H\n");
          	exit(1);
    	        }
	else
		{
                 strcpy(output_file, argv[1]);
                 strcpy(input_file, argv[1]);
                 strcat(input_file, INPUT_EXTENSION);
		if ((input_fp = fopen(input_file, "r")) == NULL) 
			{
			fprintf(stderr, "Unable to open file %s for input.\n", argv[1]);
			exit(1);
			}
		}
	

/******add reader for ocntrol file here*****/
printf ("DB>> argc %d\n", argc);
if (argc > 3)
   {
        strcpy(control_file,argv[3]);
        printf ("control_file %s\n",control_file);
        reader(control_file);
        printf ("...read\n");
        
   }
else
   {
   /*default water labels */
        printf ("DB>> no extra flags; using defaults\n");
        strcpy(ow_label[0],OW_SPECIES);
        strcpy(hw_label[0],HW_SPECIES);
        num_ow_labels=1;
        num_hw_labels=1;
        strcpy(dehydrate_this[0],OW_SPECIES);
        num_dehydrate_labels = 1;

   }



	/****** read in file ******/

	/** read lines and store until "frac" **/
	/** skip any comment lines!!!!!!!!!!! **/
	/** skip any dump / output!!!!!!!!!!! **/
        finish=0;
        line_count=1;
        
        while (finish==0)
          {
           fgets(buffer,BUFFER,input_fp);
           if ((strstr(buffer, HASH) ==NULL) 
                && (strstr(buffer, DUMP) ==NULL) 
                && (strstr(buffer, OUTPUT) ==NULL))
             {
              strcpy(lines[line_count], buffer);
printf("R: %d: %s\n", line_count, lines[line_count]);
              line_count++;
               if (strstr(buffer, CELL) !=NULL)
                   {
               
                    pbc=1;
                    fgets(buffer,BUFFER,input_fp);
           	    sscanf(buffer,"%lf%lf%lf%lf%lf%lf", 
    
			    &abc[0],
                         &abc[1], &abc[2], &abc[3], &abc[4],
                         &abc[5]);
                    strcpy(lines[line_count], buffer);
printf("R: %d: %s\n", line_count, lines[line_count]);
                    line_count++;
                   }
               else if (strstr(buffer, FRAC) !=NULL) 
                   {
                    finish=1;
                   }
             }
          }
	/** read atoms until core/shel not present **/
        line_before=line_count;
        finish=0;
        num_atoms=0;
        num_framework_atoms=0;
        num_solvent_atoms=0;
        num_solvent_molecules=0;
        while ((fgets(buffer,BUFFER,input_fp) != NULL)&&(finish==0))
          {
           if ((strstr(buffer, CORE) !=NULL) || (strstr(buffer, SHELL) != NULL))
              {
              /*coord coming*/
               /***determine if atom has label of *any* water type ***/
               is_water_flag=FALSE;
               for (is_water=0;is_water<num_ow_labels;is_water++)
                {
                 if (strstr(buffer, ow_label[is_water]) != NULL) 
                                               is_water_flag = IS_O;
                }
               for (is_water=0;is_water<num_hw_labels;is_water++)
                {
                 if (strstr(buffer, hw_label[is_water]) != NULL) 
                                               is_water_flag = IS_H;
                }

               if (is_water_flag == FALSE)
                {
                /*not solvent*/
                sscanf(buffer,"%s%s%lf%lf%lf%lf", &framework[num_framework_atoms].label,
                                &framework[num_framework_atoms].type,
                                &framework[num_framework_atoms].x, &framework[num_framework_atoms].y, &framework[num_framework_atoms].z,
                                &framework[num_framework_atoms].charge);

                /***init opti flag info ****/
                framework[num_framework_atoms].flag_x = 0;
                framework[num_framework_atoms].flag_y = 0;
                framework[num_framework_atoms].flag_z = 0;

                /***init general flag (not used) */
                framework[num_framework_atoms].general_flag = 0;

                /***init neighbour info ***/
                framework[num_framework_atoms].num_neigh=0;
                for (ineigh=0;ineigh<4;ineigh++) framework[num_framework_atoms].neighb[ineigh]=-1;


                num_framework_atoms++;
                }
               else
                {
                /*solvent*/
                sscanf(buffer,"%s%s%lf%lf%lf%lf", &solvent[num_solvent_atoms].label,
                                &solvent[num_solvent_atoms].type,
                                &solvent[num_solvent_atoms].x, &solvent[num_solvent_atoms].y, &solvent[num_solvent_atoms].z,
                                &solvent[num_solvent_atoms].charge);

                /***init opti flag info ****/
                solvent[num_solvent_atoms].flag_x = 0;
                solvent[num_solvent_atoms].flag_y = 0;
                solvent[num_solvent_atoms].flag_z = 0;
                /***init general flag (used to signify if dehydratable) */
                solvent[num_solvent_atoms].general_flag = 0;
                /***init neighbour info ***/
                solvent[num_solvent_atoms].num_neigh=0;
                for (ineigh=0;ineigh<4;ineigh++) solvent[num_solvent_atoms].neighb[ineigh]=-1;

                num_solvent_atoms++;
                printf("DB>>num_solvent_atoms %d\n", num_solvent_atoms);
                /*count solvent molecules by counting O cores */
                if ((is_water_flag == IS_O) && (strstr(buffer, CORE) != NULL))
                   {
                    num_solvent_molecules++;
                   }
                }
              } /*end if CORE/SHEL*/
           else
              {
              /* finished*/
              strcpy(lines[line_count], buffer);
              line_count++;
              finish=1;
              }
        }/* end while */

     /*extra line which is read in while loop */
     strcpy(lines[line_count], buffer);
     line_count++;
        num_framework_atoms--;
        num_solvent_atoms--;
        num_atoms = num_framework_atoms+num_solvent_atoms;
	/** read lines and store until EOF    **/
        while ((fgets(buffer,BUFFER,input_fp) != NULL))
              {
               if ((strstr(buffer, DUMP) ==NULL) 
                && (strstr(buffer, OUTPUT) ==NULL))
                {
                strcpy(lines[line_count], buffer);
                line_count++;
                }
              }
 
printf("DB>>> molecues %d atoms %d\n", num_solvent_molecules, num_solvent_atoms);

/*****************************************************************/
/******* sort out lattice vectors if required ********************/
/*****************************************************************/

  if (pbc)
    {
       cart_latt_vecs( &abc[0], &latt_vec[0], &real_latt_sizes[0],
                        &recip_latt_vec[0],
                       &recip_latt_sizes[0], &cell_volume, num_atoms);




/******************************************************************/
/***** Assign vdw radii                                         ***/
/******************************************************************/

    set_elem(&framework[0],num_framework_atoms);
    steric_setup(&framework[0],num_framework_atoms);
    set_elem(&solvent[0],num_solvent_atoms);
    steric_setup(&solvent[0],num_solvent_atoms);
    
    generate_neighbours(&solvent[0], num_solvent_atoms, &mol_types[0],
                                                &num_mol_types, TRUE);

    }
    

printf("num_solvent_molecules %d\n",num_solvent_molecules);
if (num_solvent_molecules==0) 
   {
   printf("FINISHED : no more water\n");
   exit(1);
   }
/***ow write num_solvent_molecules Gulp files**/

/***work out which can be dehydrated***/

num_to_dehydrate = 0;
for (i=0;i<=num_solvent_atoms;i++)
    {
     for (j=0;j<num_dehydrate_labels;j++)
         {
          if ((strstr(solvent[i].label,dehydrate_this[j]) != NULL) 
             && (strstr(solvent[i].type, CORE) != NULL))
             {
              solvent[i].general_flag = 1;
              num_to_dehydrate++;
              printf("can dehyd %s cos %s\n",solvent[i].label,dehydrate_this[j]);
             }
         }
    }
printf("Will dehydrate %d from %d solvent molecules\n", num_to_dehydrate,num_solvent_molecules);

for (skip_this_solvent=0; skip_this_solvent<num_to_dehydrate; skip_this_solvent++)
  {
   /* open file*/
   sprintf(this_output_file, "%s_N%d.gin",output_file, skip_this_solvent);
   printf("DB>>this_output_file  %s\n",output_file);
   if ((output_fp = fopen(this_output_file, "w")) == NULL)
                        {
                        fprintf(stderr, "ABORT: Unable to open file %s for output.\n", 
                                this_output_file);
                        exit(1);
                        }
   /*print heady bit*/
   for (iloop = 1; iloop <line_before; iloop++)
       {
        fprintf(output_fp,"%s", lines[iloop]);
        /*printf("W: %d: %s\n", iloop, lines[iloop]);*/
       }
   
   for (iloop = 0; iloop <= num_framework_atoms; iloop++)
       {
        fprintf(output_fp,"%s %s %f %f %f\n", framework[iloop].label,
                                   framework[iloop].type,
                                   framework[iloop].x,
                                   framework[iloop].y,
                                   framework[iloop].z);
       }


     /**print solvent by molecule**/
     solvent_mol_no=-1;
     for (iloop =0; iloop< num_solvent_atoms; iloop++)
       {
   
         /*is it a Ow core */
         is_ow = FALSE;
         for (jloop = 0; jloop<num_ow_labels;jloop++)
            {
             if ((strstr(solvent[iloop].label, ow_label[jloop]) !=NULL)
                  && (strstr(solvent[iloop].type, CORE)!=NULL))
                {
                 is_ow = TRUE;
                 printf("DB>> %d %s %s is solvent %d %s\n", 
                      iloop,solvent[iloop].label,solvent[iloop].type,
                      jloop,ow_label[jloop]);
                }
            }
          if (is_ow == TRUE)
             {
              printf("#solvent molecule %d skip %d\n", 
                          solvent_mol_no,skip_this_solvent);
            
              skip_this_flag=FALSE;
              if (solvent[iloop].general_flag == 1)
                 {
                  solvent_mol_no++;
                  if (solvent_mol_no==skip_this_solvent) 
                     {
                      skip_this_flag=TRUE;
                      printf("will skip %d %d flag%d\n",
                      solvent_mol_no,skip_this_solvent,skip_this_flag);
                     }
                 }

              if (skip_this_flag)
                 {
                  fprintf(output_fp,"#");
                  printf("#");
                 }
              fprintf(output_fp,"%s %s %f %f %f\n", solvent[iloop].label, 
                                         solvent[iloop].type,
                                         solvent[iloop].x,
                                         solvent[iloop].y,
                                         solvent[iloop].z);
              printf("%s %s %f %f %f\n", solvent[iloop].label, 
                                         solvent[iloop].type,
                                         solvent[iloop].x,
                                         solvent[iloop].y,
                                         solvent[iloop].z);
             /***printf the neighbours as well**/
             for (jloop=0;jloop<solvent[iloop].num_neigh; jloop++)
                 {
                  if (skip_this_flag)
                     {
                      fprintf(output_fp,"#");
                  printf("#");
                     }
                  fprintf(output_fp,"%s %s %f %f %f\n", 
                        solvent[solvent[iloop].neighb[jloop]].label, 
                        solvent[solvent[iloop].neighb[jloop]].type, 
                        solvent[solvent[iloop].neighb[jloop]].x, 
                        solvent[solvent[iloop].neighb[jloop]].y, 
                        solvent[solvent[iloop].neighb[jloop]].z); 
                  printf("%s %s %f %f %f\n", 
                        solvent[solvent[iloop].neighb[jloop]].label, 
                        solvent[solvent[iloop].neighb[jloop]].type, 
                        solvent[solvent[iloop].neighb[jloop]].x, 
                        solvent[solvent[iloop].neighb[jloop]].y, 
                        solvent[solvent[iloop].neighb[jloop]].z); 
                 }
     
             }

       } /* end for (iloop =0*/



  for (iloop = line_before; iloop < line_count; iloop++)
    {
     fprintf(output_fp, "%s", lines[iloop]);
     /*printf("%s", lines[iloop]);*/
    }

  /**write explicit dump andn output lines**/
  fprintf(output_fp, "dump every   1 %s_N%d.dump\n",output_file,skip_this_solvent);
  fprintf(output_fp, "output xtl   %s_N%d.xtl\n",output_file,skip_this_solvent);

  fclose(output_fp);
  } /*end skip_this_solvent*/

/*** write a shell script to run all these files ***/
sprintf(script_file, "%s.csh",output_file, skip_this_solvent);
if ((script_fp = fopen(script_file, "w")) == NULL)
                        {
                        fprintf(stderr, "ABORT: Unable to open file %s for output.\n",
                                script_file);
                        exit(1);
                        }
/*print heady bit*/
        fprintf(script_fp, "#!/bin/csh -f\n");
        fprintf(script_fp, "#script for runing Gulp files from\n");
        fprintf(script_fp, "#dehydrate run %s with %d waters\n", input_file, num_solvent_molecules);

for (skip_this_solvent=1; skip_this_solvent<=num_to_dehydrate; skip_this_solvent++)
  {
   sprintf(this_output_file, "%s_N%d",output_file, skip_this_solvent);

        fprintf(script_fp,"%s < %s.gin > %s.gout\n",
                            RUN_GULP_COMMAND,this_output_file,this_output_file);
  }
fprintf(script_fp,"%s %s_N", FIND_MIN, output_file);
/***make script file executable***/
sprintf(append_pots_command, "chmod +x %s\n", script_file);
system (append_pots_command);
/**run it!**/
sprintf(append_pots_command, "csh %s\n", script_file);
system(append_pots_command);


}