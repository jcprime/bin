/****cation_pos calc position of cation from 2O coords and T coord****/
/****hacked from Rob version****/
/*****CARTESIAN****/
#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "header.h"
#define NA_BONDLENGTH 2.85


simple_atom X_pos(simple_atom *o1, simple_atom *o2, simple_atom *t)
{
   simple_atom cation,cation_alt;
   double o_o[4],o1_t[4],o2_t[4],norm[4],cation_vec[4],bit_vec[4];
   double dist_o1t,dist_o2t,mag_cationvec;
   double dx,dy,dz,dist1,dist2;
   int i;
/*
   strcpy (cation.atyp,"H   ");
   strcpy (cation.styp,"CORE");
   strcpy (cation_alt.atyp,"H   ");
   strcpy (cation_alt.styp,"CORE");
   cation_alt.us_ind = 0;
   cation.us_ind = 0;
*/


   
   for (i=1 ; i<=3 ; i++)
      o_o[i] = o1->coord[i]-o2->coord[i];

   for (i=1 ; i<=3 ; i++)
      o1_t[i] = o1->coord[i]-t->coord[i];
   dist_o1t = sqrt (o1_t[1]*o1_t[1] +
                     o1_t[2]*o1_t[2] +
                     o1_t[3]*o1_t[3]);

   for (i=1 ; i<=3 ; i++)
      o2_t[i] = o2->coord[i]-t->coord[i];
   dist_o2t = sqrt (o2_t[1]*o2_t[1] +
                     o2_t[2]*o2_t[2] +
                     o2_t[3]*o2_t[3]);

   norm[1] = o_o[2]*o1_t[3] - o_o[3]*o1_t[2];
   norm[2] = o_o[3]*o1_t[1] - o_o[1]*o1_t[3];
   norm[3] = o_o[1]*o1_t[2] - o_o[2]*o1_t[1];

   cation_vec[1] = norm[2]*o_o[3] - norm[3]*o_o[2];
   cation_vec[2] = norm[3]*o_o[1] - norm[1]*o_o[3];
   cation_vec[3] = norm[1]*o_o[2] - norm[2]*o_o[1];

   mag_cationvec = sqrt (cation_vec[1]*cation_vec[1] 
                         + cation_vec[2]*cation_vec[2] 
                         + cation_vec[3]*cation_vec[3]);
   for (i=1 ; i<=3 ; i++)
   {
      bit_vec[i] = cation_vec[i] * -(NA_BONDLENGTH/mag_cationvec) ;
      cation.coord[i] = t->coord[i] + bit_vec[i]; 
      cation_alt.coord[i] = t->coord[i] - bit_vec[i]; 
   }

   dx = cation.coord[1] - o1->coord[1];
   dy = cation.coord[2] - o1->coord[2];
   dz = cation.coord[3] - o1->coord[3];

   dist1 = (sqrt (dx*dx + dy*dy + dz*dz));

   dx = cation_alt.coord[1] - o1->coord[1];
   dy = cation_alt.coord[2] - o1->coord[2];
   dz = cation_alt.coord[3] - o1->coord[3];

   dist2 = (sqrt (dx*dx + dy*dy + dz*dz));

/*****DEBUG
   printf("Atom O1 %f %f %f\n", o1->coord[1], o1->coord[2], o1->coord[3]);
   printf("Atom O2 %f %f %f\n", o2->coord[1], o2->coord[2], o2->coord[3]);
   printf("Atom  T %f %f %f\n", t->coord[1], t->coord[2], t->coord[3]);
   printf("Atom  C %f %f %f\n", cation_alt.coord[1], cation_alt.coord[2], 
                                cation_alt.coord[3]);
*****DEBUG*/

   return cation_alt;
}
