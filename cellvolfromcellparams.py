#!/usr/bin/env python3
# Copied from cellvolumes.py (2018-11-11)
# Provide a list of cell lengths and cell angles, and get back the cell volume in cubic angstroms

import math
import sys

def print_usage():
    print("Usage: python cellvolfromcellparams.py a b c alpha beta gamma")

def cell_parameters(cellstuff):
    lengths = []
    for i in range(0, 3):
        lengths.append(float(cellstuff[i]))
    angles = []
    for i in range(3, 6):
        angles.append(math.radians(float(cellstuff[i])))
    return lengths, angles

def define_cell(lengths, angles):
    # See https://bytes.com/topic/python/answers/608886-lots-nested-if-statements
    comparisons = [lengths[0] == lengths[1] and lengths[1] == lengths[2] and lengths[2] == lengths[0], lengths[0] != lengths[1] and lengths[1] != lengths[2] and lengths[2] != lengths[0], angles[0] == (math.pi / 2), angles[1] == (math.pi / 2), angles[2] == (math.pi / 2), angles[0] == angles[1] and angles[1] == angles[2] and angles[2] == angles[0], angles[0] != angles[1] and angles[1] != angles[2] and angles[2] != angles[0], angles[0] == math.radians(120) or angles[1] == math.radians(120) or angles[2] == math.radians(120)]

    if comparisons[0] == True and comparisons[2:5] == [True, True, True]:
        celltype = 'cubic'
    if comparisons[0] == True and comparisons[2:5] == [False, False, False]:
        celltype = 'rhombohedral'
    if comparisons[0:2] == [False, False] and comparisons[2:5] == [True, True, True]:
        celltype = 'tetragonal'
    if comparisons[0:2] == [False, False] and comparisons[7] == True:
        celltype = 'hexagonal'
    if comparisons[0:2] == [False, False] and comparisons[5:7] == [False, False]:
        celltype = 'monoclinic'
    if comparisons[0] == False and comparisons[1] == True and comparisons[2:5] == [True, True, True]:
        celltype = 'orthorhombic'
    else:
        celltype = 'triclinic'
    return celltype
    # print(celltype)

def calculate_volume(lattice, lengths, angles):
    """Using the equations found on Wikipedia for different lattice types"""
    abc = lengths[0] * lengths[1] * lengths[2] #(lengths[2]/2) # Because multicell 1 1 2 doesn't get kept in restart files, for some reason!
    if lattice == "cubic" or lattice == "tetragonal" or lattice == "orthorhombic":
        volume = abc
    if lattice == "hexagonal":
        volume = (float(math.sqrt(3))/float(2)) * abc
    if lattice == "rhombohedral":
        volume = abc * math.sqrt(1 - (3 * ((math.cos(angles[0]))**2)) + (2 * ((math.cos(angles[0]))**3)))
    if lattice == "monoclinic":
        volume = abc * math.sin(angles[2])
    if lattice == "triclinic":
        volume = abc * math.sqrt(1 - ((math.cos(angles[0]))**2) - ((math.cos(angles[1]))**2) - ((math.cos(angles[2]))**2) + (2 * math.cos(angles[0]) * math.cos(angles[1]) * math.cos(angles[2])))
    # return volume
    print(volume)


cell_stuff = sys.argv[1:]
#print(cell_stuff)
lengths, angles = cell_parameters(cell_stuff)
calculate_volume(define_cell(lengths, angles), lengths, angles)
