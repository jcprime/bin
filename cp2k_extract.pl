#!/usr/bin/perl -w

# Script for getting useful stuff out of CP2K .log files
# (Hacked from general_info.pl)
# Command line usage: cp2k_extract.pl

@Files = glob "*.log";

#print "output filename?\n";
#chomp ($txtfile = <STDIN>);
#open TXTFILE, ">$txtfile" or die "Can't create $txtfile: $!\n";

printf STDOUT "%20s%4s%16s%8s%12s%12s%12s%12s%12s%12s%14s%16s\n",
  "Filename", "Opt", "Energy", "Gnorm", "A", "B", "C", "Alpha",
  "Beta", "Gamma", "Vol", "PV";

foreach $Files (@Files) {
	open OUTPUT, "<$Files" or die "Can't open $Files: $!\n";

	while (<OUTPUT>) {

        # Print filename
        #if (//) {
            printf STDOUT "\n%20s", $Files;
        #}

	    if (/SCF loop converged/) {
	        printf STDOUT "%4s", "Y";
	    }

        # Looking for ENERGY| Total FORCE_EVAL ( QS ) energy (a.u.)
	    if (/(ENERGY\| Total .*: \s+) (.\d+.\d+)/) {
	        $energy = $2;
	        $pcalc = 3;
		    $pv = 0.0;
	        printf STDOUT "%16.8f", $energy;
        # } elsif (/(ENTHALPY\| Total .*: \s+) (.\d+.\d+)/) {
        #     $energy = $2;
        #     $pcalc = 5;
        #     printf STDOUT "%16.8f", $energy;
	    } else {
		    $pcalc = 0
	    }

	    # if (/(Final Gnorm  =\s+) (\d+.\d+)/) {
	    #     $gnorm  = $2;
	    #     printf STDOUT "%12.5f", $gnorm;
	    # }

	    # if (/(Pressure\*volume            =   \s+) (.\d+.\d+)/) {
	    #     $pv = $2;
	    #     #printf STDOUT "%16.8f", $pv;
	    # }

	    if (/(       a         \s+) (\d+.\d+)/) {
	        $A = $2;
	        printf STDOUT "%12.6f", $A;
	    }

	    if (/(       b         \s+) (\d+.\d+)/) {
	        $B = $2;
	        printf STDOUT "%12.6f", $B;
	    }

	    if (/(       c         \s+) (\d+.\d+)/) {
	        $C = $2;
	        printf STDOUT "%12.6f", $C;
	    }

	    if (/(       alpha  \s+) (\d+.\d+)/) {
	        $alpha = $2;
	        printf STDOUT "%12.6f", $alpha;
	    }

	    if (/(       beta   \s+) (\d+.\d+)/) {
	        $beta = $2;
	        printf STDOUT "%12.6f", $beta;
	    }

	    if (/(       gamma  \s+) (\d+.\d+)/) {
	        $gamma = $2;
	        printf STDOUT "%12.6f", $gamma;
	    }

	    if (/(Non-primitive cell volume =\s+) (\d+.\d+)/) {
	        $vol = $2;
	        printf STDOUT "%14.6f", $vol;
	    }
	}
	close OUTPUT;
	printf STDOUT "%16.8f", $pv;
    print "Done $Files\n";
}

