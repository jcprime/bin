# def sum_array(arr):
#     # Ignore highest and lowest values (once in each case!) and print out sum excluding said values
#     highest = arr[0]
#     lowest = arr[0]
#     total = 0
#     for num in range(0,(len(arr)-1)):
#         if arr[num] > highest:
#             highest = arr[num]
#         elif arr[num] < lowest:
#             lowest = arr[num]
#     for num in range(0,(len(arr)-1)):
#         if arr[num] == highest:
#             continue
#         if arr[num] == lowest:
#             continue
#         else:
#             total += arr[num]
#     return total


def sum_array(arr):
    # Ignore highest and lowest values (once in each case!) and print out sum excluding said values
    if arr is None or len(arr) <= 1:
        total = 0
    else:
        sorted_arr = sorted(arr, key=int)
        # print sorted_arr
        # print len(sorted_arr)
        total = 0
        for num in range(1, (len(sorted_arr)-1)):
                total += sorted_arr[num]
    return total

# print sum_array([6, 2, 1, 8, 10])
