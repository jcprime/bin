#!/bin/csh -f
#pass working directory and set to $WORKDIR
#$ -cwd
setenv WORKDIR $cwd
mkdir -p /scr/$USER/$USER.$JOB_ID/
cd /scr/$USER/$USER.$JOB_ID/
cp $WORKDIR/test.* .

#run job
/usr/local/msi/MaterialsStudio/DMol3/bin/RunDMol3.sh  test

#copy answers
cp -f * $WORKDIR

#could remove directory!
