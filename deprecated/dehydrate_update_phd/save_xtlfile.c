/****writes an xtl file ****/
#include <stdio.h>
#include "structures.h"
#include "header.h"
void save_xtlfile(FILE *file_fp, atom *p_mol, 
                   int num_atoms, char *p_spacegroup, int lowenstein)
{
int i, iloop;
atom *p_atom;



             fprintf(file_fp,"TITLE mc generated structue\n");
             if (lowenstein) 
                  fprintf(file_fp,"TITLE Lowenstein rule obeyed\n");
             else
             fprintf(file_fp,"TITLE Lowenstein rule not obeyed\n");

             fprintf(file_fp,"SYMMETRY NUMBER %s\n", p_spacegroup);
             fprintf(file_fp,"CELL \n");
             for (iloop = 0; iloop < 6; iloop++)
              {
                fprintf(file_fp, "%10.6f   ",abc[iloop]);
              }
             fprintf(file_fp,"\nATOMS\nNAME X Y Z\n");
             for (i=0;i<=num_atoms;i++)
                     {
                     /**substituted u c**/
                     p_atom = p_mol+i;
                     fprintf(file_fp,"%s %lf %lf %lf\n", p_atom->label,
                            p_atom->x, p_atom->y, p_atom->z);
                     }
             /***now print out cations is done ****/
             fclose(file_fp);
             /***append pot file****/
printf("fin writing xtl fil..\n");
return;
}
