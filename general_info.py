#!/usr/bin/python
# gulp_coords.py
# Converts the fractional coordinates found in GULP input files and processes them into Cartesian coordinates for turning into CP2K coordinate files

import sys
import math

# Function to print usage
def printUsage():
    print "Usage: python general_info.py outputFile.ext"

def findUsefuls():
    stuff = []
    for blah in blahs:
        stuff.append()
    return stuff

# Make list of lengths
lengths = []
for i in range(1, 4):
    lengths.append(float(sys.argv[i]))

# Make list of angles, assuming given in degrees, and convert them to radians for use in fracToCart()
angles = []
for i in range(4, 7):
    angles.append(math.radians(float(sys.argv[i])))

# Open all *.gout files and read
reader = open(STUFF, 'r')
sourceFile = reader.readlines()
reader.close()

filename = BLAH.split('.')

# Ensure all fractional coordinates are actually floats
coords = []
for i in sourceFile:
    coord = i.split()
    coord[1] = float(coord[1])
    coord[2] = float(coord[2])
    coord[3] = float(coord[3])
    coords.append(coord)

# Convert the input coordinates into the other type
print "Converting to Cartesian coordinates..."
newCoords = []
for line in coords:
    newCoord = fracToCart(lengths, angles, line)
    newCoords.append(newCoord)

# Create the output strings and write them to the file
outputData = []
for line in newCoords:
    formattedLine = "{0[0]:<2}   {0[1]:> 10.6f}   {0[2]:> 10.6f}   {0[3]:> 10.6f}\n".format(line)
    outputData.append(formattedLine)
outputData.append('\n')

outputFilename = data.tsv
writer = open(outputFilename, 'w')
writer.write(''.join(outputData))
writer.close()

print "... aaaand we're done."
