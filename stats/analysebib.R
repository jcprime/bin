library(tidyverse)
library(bibtex)
theme_set(theme_minimal())

## You'll need to modify the next line to wherever your bib file is
entries <- read.bib("~/words/PhD.bib")
lentries <- length(entries)

years <- list(lentries)
type <- list(lentries)
journals <- list(lentries)
for (i in 1:length(entries)) {
  years[i] <- entries[[i]]$year
  type[i] <- attributes(unclass(entries)[[i]])$bibtype
  if (type[i] == "Article"){
    journals[i] <- entries[i]$journal
  } else journals[i] <- NA
}

entries <- data.frame(year = as.numeric(unlist(years)),
                      type = unlist(type),
                      journal = unlist(journals))

## Make a graph of year vs count, but exclude years before 1950
entries %>%
  filter(year > 1950) %>%
  ggplot(aes(year)) +
  geom_bar() +
  labs(x = "Year",
       y = "Count")

## Graph of type (article, book, etc)
entries %>%
  ggplot(aes(type)) +
  geom_bar()

## Graph of count per journal
entries %>%
  group_by(journal) %>%
  summarize(n = n()) %>%
  arrange(desc(n)) %>%
  mutate(journal = factor(journal, levels = .$journal[order(.$n)])) %>%
    filter(n > 4, !is.na(journal)) %>%
  ggplot(aes(journal, n)) +
  geom_bar(stat = "identity") +
  labs(x = "Journal",
       y = "Count") +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))
