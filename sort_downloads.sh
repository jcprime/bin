#!/bin/bash
# sort_downloads.sh
# An attempt to reproduce the Hazel rules I've currently got going for my Downloads directory,
# which would ideally be run as a cron job, and would likely be comparatively faster than
# Hazel is on my 2009 MBP!

for file in ~/Downloads/*; do
	# Grab extension (assuming no filenames contain full stops as part of the basename!)
	ext="${file##*.}"
	
	# If it's an archive, copy it to archives directory and un-archive it in main directory
	if [[ "$ext" == "tar.gz" ]] || [[ "$ext" == "tgz" ]]; then
		gunzip "$file" && tar xvf "${file%%.*}.tar"
		mv "$file" ~/Downloads/archives/
	elif [[ "$ext" == "tar.bz2" ]] || [[ "$ext" == "tar.bz" ]] || [[ "$ext" == "tbz2" ]] || [[ "$ext" == "tbz" ]]; then
		bunzip2 "$file" && tar xvf "${file%%.*}.tar"
		mv "$file" ~/Downloads/archives/
	elif [[ "$ext" == "zip" ]]; then
		unzip "$file"
		mv "$file" ~/Downloads/archives/
	elif [[ "$ext" == "tar" ]]; then
		tar xvf "$file"
		mv "$file" ~/Downloads/archives/
	fi

	# If it's a ttf or otf file, move it to the fonts directory
	if [[ "$ext" == "ttf" ]] || [[ "$ext" == "otf" ]]; then
		mv "$file" ~/Downloads/fonts/
	fi

	# If it's an image, move it to the Pictures diectory
	if [[ "$ext" == "jpg" ]] || [[ "$ext" == "png" ]] || [[ "$ext" == "bmp" ]] || [[ "$ext" == "gif" ]]; then
		mv "$file" ~/Pictures/
	fi

	# If it's a video, move it to the Movies directory
	if [[ "$ext" == "mp4" ]]; then
		mv "$file" ~/Movies/
	fi

	# If it's an audio file, move it to the Music directory
	if [[ "$ext" == "mp3" ]]; then
		mv "$file" ~/Music/
	fi

	# If it's a PDF that ends with "_MH.pdf", move it to the ~/Downloads/mh directory
	if [[ "${file##*_}" == "MH.pdf" ]]; then
		mv "$file" ~/Downloads/mh/
	fi
done
