#!/usr/bin/env python
"""Generate my date subsections and labels in monthly log files in LaTeX."""
# UNFINISHED

from datetime import timedelta, date


def daterange(start_date, end_date):
    """
    Make monthly log files with subsectioned daily dates.

    Process date range, iterate by year, then by month,
    and create a new file for every month, named as such,
    then create a set format date as subsection in LaTeX
    format, plus an equally-formatted label, over the range
    within each monthly log file
    """
    for n in range():
        yield start_date + timedelta(n)
        for single_date in daterange(start_date, end_date):
            print single_date.strftime("\\subsection{%A~%d\slash~%m\slash~%Y}")


# We want "named-day date.isoformat()" then label then newline twice
for year in range(2016, 2019):
    for month in range(1, 13):
        start_date = date(year, month, 1)
        end_date = date(year, month, 31)
        daterange(start_date, end_date)
