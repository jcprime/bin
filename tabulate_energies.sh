#!/bin/bash
# tabulate_energies.sh
# Pre-processing for R scatter plots

path="$HOME/bin/stats/phd/data/base"
cd "$path" || exit
declare -a zeo=("nat" "scol" "mes")
for z in "${zeo[@]}"; do
  #echo "Clearing ${z}_energies.csv"
	> "${z}_energies.csv"
	for num in {0..64}; do
    #echo "num = ${num}"
		padnum="$(printf '%02d' "$num")"
    #echo "padded num = ${padnum}"
		if [[ -f "${path}/${z}${padnum}.tsv" ]]; then
      #echo "${path}/${z}${padnum}.tsv exists!"
			file="${path}/${z}${padnum}.tsv"
			# Extract digit-y part of filename
			#step="${file%%.*}" && step="${step##*[a-zA-Z]}"
      #echo "Hopefully grabbing relevant fields now!"
			awk -v step="$num" '{print step, $(NF-1)}' "$file" >> "${z}_energies.csv"
		fi
	done
done
