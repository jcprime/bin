#!/bin/bash
# gulpconvert.sh
#  - Takes cell parameters and fractional coordinates from *SINGLE* GULP input file, formatted with a single space separating them in the string
#  - Reformats them as XYZ-type coordinates using Python script (gulp_coords.py)

# SEE GULPCONVERT_FUNCTIONS.SH FOR THE CURRENT STATE OF FUNCTIONS FOR LOOPING PURPOSES!

# If "-h" or "--help" is given when running the script as only argument, print usage information
if [[ ( "$#" -eq "1" ) && ( ( "$1" == "-h" ) || ( "$1" == "--help" ) ) ]]; then
    echo "Usage: gulpconvert.sh [gulp_filename] [coordinate_file]" && exit
fi

# Grab input filename via command-line arguments or user input
if [[ "$#" -ge "1" ]]; then
    gulpfile="$1"
else
    read -p "File to convert please: " gulpfile
fi

# Set working environment
work_dir="$(pwd)"
cd "${work_dir}" || exit

# Grab basename of the GULP input file (everything before the ".ext")
base="${gulpfile%%.*}"

# Set output-coordinate file to second argument, if given, or else taken as same basename as original .gin file
if [[ "$#" -ge "2" ]]; then
    coordinate_file="$2"
elif [[ "$#" -eq "0" ]]; then
    coordinate_file="${base}.cp2k"
fi

# Set cp2k-input file to first argument, if given, or else taken as same basename as original .gin file
if [[ "$#" -ge "1" ]]; then
    inpfile="$2"
elif [[ "$#" -eq "0" ]]; then
    inpfile="${base}.inp"
fi

# Grab cell parameters into a new variable (separated by SINGLE space)
cell=$(grep -A1 'cell' "${gulpfile}" | tail -n 1 | tr -s ' ')
echo "Cell parameters: ${cell}"

# Create temporary file
temp_file="${base}.tmp"
> "${temp_file}"

# THE SED COMMAND IS SPECIFIC TO UNIX/LINUX SYSTEMS, AND WON'T WORK ON MACOS:
# Output everything including "fractional" and "totalenergy" OR "species" (whichever comes first) to a temporary file for processing in the while loop to follow
#sed -n "/^fractional/,/^\(space\|totalenergy\|species\)/p" "${gulpfile}" >> "${temp_file}"
awk ' { if (NF == 8 && $0 ~ /^[A-Za-z0-9]+ +core|shel( +[0-9.]+){6}$/) { print } } ' "$gulpfile" >> "${temp_file}"

frac_file="${base}.frac"
if [ ! -e "${frac_file}" ]; then
    touch "${frac_file}"
elif [ -e "${frac_file}" ]; then
    > "${frac_file}"
fi

# # Remove first and last lines using sed
# sed '1d;$d' "${temp_file}" >> "${frac_file}"
# cat "${frac_file}"

# Grab coordinate lines for CORES ONLY and process them with awk, assuming the form below:
awk ' { if ($1 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } else if ($1 ~ /^[A-Za-z].*/ && $2 ~ "core") { print $1, $3, $4, $5; next } } ' "${temp_file}" >> "${frac_file}"

# Actually do the conversion, via gulp_coords.py
python3 ~/bin/gulp_coords.py "${cell}" "${frac_file}"
cart_file="${base}.cart"

# Copy lines with format * number number number (on the assumption that that's all there'll be) into "$coordinate_file"
awk ' { if ($2 ~ /^[0-9].*/ && $3 ~ /^[0-9].*/ && $4 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } } ' "${cart_file}" > "${coordinate_file}"

#mkdir "${base}"
# cp "${base}".* "${base}"/
#mv "${base}".* "${base}"/

if [ "${coordinate_file##*.}" == "xyz" ]; then
    noatoms="$(wc -l "${gulpfile%.*}.xyz" | cut -d' ' -f1)"
    energy="$(grep 'energy' "${gulpfile}" | tr -s ' ' | cut -d' ' -f2)"
    {   echo "${noatoms}";
        echo "${energy}";
        cat "${cart_file}";
    } > "${coordinate_file}"
fi
