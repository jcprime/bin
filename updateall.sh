#!/bin/bash
# updateall.sh
# Pulls latest versions of all relevant directories from Github

if [ "$#" -eq "1" ]; then
    dirlist_file="$1"
elif [ "$#" -eq "0" ]; then
	dirlist_file="$HOME/bin/updatelist.txt"
fi

#for directory in "bin" ".dotfiles" "words" "test" "writings" "dotfiles-universal" "ice" "webstuff" "mfarmer" "flp" "wallpapers" ".config/sublime-text-3/Packages/User" "Library/Application Support/Sublime Text 3/Packages/User"; do
while read -r directory; do
    if [ -d "$HOME/$directory" ]; then
        cd "$HOME/$directory" || continue
        echo
        git pull origin master
        #echo
        cd "$HOME"
    fi
done < "$dirlist_file"
