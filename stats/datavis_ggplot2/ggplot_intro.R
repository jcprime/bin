# General notes:
# If you want help, just type ?thingYouWantHelpOn into the console!

# Tell R that you're wanting to use the following packages please
library(ggplot2)
library(ggrepel)
library(ggthemes)
library(scales)

# Load dataset into variable called housing with "gets"
housing <- read.csv("https://altaf-ali.github.io/ggplot_tutorial/data/housing.csv")

# See what dataset looks like (first few observations)
head(housing)

# Convert data to appropriate data types (originally was "factor", then becomes "date")
housing$Date <- as.Date(housing$Date)

# NB: Factors are defined here: https://www.stat.berkeley.edu/classes/s133/factors.html
# As follows: Factors in R are stored as a vector of integer values with a corresponding set of character values to use when the factor is displayed

# Create subsets of data using subset(mainThing, variableOfInterest == "valueOfInterest")
newyork <- subset(housing, State == "NY")
northeast <- subset(housing, Region == "N. East")

# Plotting scatter plot using ggplot(data, mapping), where mapping gives details of x and y axes etc
# mapping is given using aes(x = variableX, y = variableY)
# geom_point() creates a scatterplot
# ggplot2 gives you the grey gridded background as the default
ggplot(newyork, aes(x = Date, y = Home.Value)) +
  geom_point()

# SECTION 6
# Exercise 1: Create histogram of Home.Value using housing data
ggplot(housing, aes(x = Home.Value)) +
  geom_histogram(bins = 70)

# Exercise 2: Create box plot of Home.Value using northeast dataset with State on the x-axis
ggplot(northeast, aes(x = State, y = Home.Value)) +
  geom_boxplot()

# Exercise 3: Create line plot using newyork dataset with Date on the x-axis and Home.Value on the y-axis
ggplot(newyork, aes(x = Date, y = Home.Value)) +
  geom_line()

# Exercise 4: Create a line plot using northeast dataset with Date on the x-axis and Home.Value on the y-axis and use a different colour for each state
ggplot(northeast, aes(x = Date, y = Home.Value, colour = State)) +
  geom_line()

# SECTION 7
# Shiny colourful plot based on region!
ggplot(housing, aes(x = Home.Value, fill = Region)) +
  geom_histogram(binwidth = 1000)

# The following is equivalent to the above:
ggplot(housing, aes(x = Home.Value)) +
  geom_histogram(aes(fill = Region), binwidth = 1000)
# except this will allow another plot to be plotted on the same axes but with different aesthetics than the original histogram

# Using stat_bin transformation (default geom is "area")
ggplot(housing, aes(x = Home.Value)) +
  stat_bin(binwidth = 1000)

# Changing default geom ("area") to "point"
ggplot(housing, aes(x = Home.Value)) +
  stat_bin(geom = "point", binwidth = 1000)

# Changing default geom ("area") to "line"
ggplot(housing, aes(x = Home.Value)) +
  stat_bin(geom = "line", binwidth = 1000)

# EXERCISE:   Create a subset of housing data from New York since 2000
# Exercise 1: Create a plot that includes multiple geometric objects, for example, lines and points
newyork2k <- subset(newyork, Year >= 2000)
ggplot(newyork2k, aes(x = Date, y = Home.Value)) +
  geom_line() +
  geom_point()

# Exercise 2: Change the shape to be hollow diamond (see "Shape Scales" in cheatsheet)
ggplot(newyork2k, aes(x = Date, y = Home.Value)) +
  geom_point(shape = 9)

# Exercise 3: Change the size of the point based on Land.Value and color based on Structure.Cost
ggplot(newyork2k, aes(x = Date, y = Home.Value)) +
  geom_point(aes(size = Land.Value, color = Structure.Cost))
#scale_fill_gradient(low = "black", high = "blue")

# SECTION 8
# Create a subset of housing for 2001 in the first quarter
housing2001q1 <- subset(housing, Year == 2001 & Quarter == 1)
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point()

# De-skew above plot by adding a log scale to the x-axis
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point() +
  scale_x_log10()

# Add dollar sign in front of axis labels
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point() +
  scale_x_log10(labels = dollar) +
  scale_y_continuous(labels = dollar)

# Change scale for x-axis in Date format and control the breaks for the continuous y-axis
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  scale_x_date(date_breaks ="3 year", date_minor_breaks ="1 year", date_labels = "'%y") +
  scale_y_continuous(breaks = seq(0, 500000, 50000), labels = dollar)

# SECTION 9
# Add a smoother for 2001 Q1 dataset
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point() +
  scale_x_log10() +
  stat_smooth()
# ^^ Gives output: `geom_smooth()` using method = 'loess'

# Fit linear model to the dataset
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point() +
  scale_x_log10() +
  stat_smooth(method = "lm")

# Specify the formula for the model
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point() +
  scale_x_log10() +
  stat_smooth(method = "lm", formula = y ~ log(x))

# Turn off the confidence interval
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point() +
  scale_x_log10() +
  stat_smooth(method = "lm", formula = y ~ log(x), se = FALSE)

# Using a General Additive Model (GAM)
ggplot(housing2001q1, aes(x = Land.Value, y = Structure.Cost)) +
  geom_point() +
  scale_x_log10() +
  stat_smooth(method = "gam", formula = y ~ s(x,k=10))

# SECTION 10
# Testing themes from "ggthemes" package: here's the Stata theme
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_stata()

# Here's the Economist theme
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_economist()

# Here's the Wall Street Journal theme
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_wsj()

# Here's the solarized theme
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_solarized()

# Here's the 538 theme
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_fivethirtyeight()

# Here's the minimal theme, suitable for further customisation
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_minimal()

# Removing minor gridlines from minimal theme
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_minimal() +
  theme(
    panel.grid.minor = element_blank()
  )

# Changing y-axis labels
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_minimal() +
  theme(
    panel.grid.minor = element_blank()
  ) +
  ylab("Home Value (US$)")

# Remove x-axis labels since year is self-explanatory
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_minimal() +
  theme(
    axis.title.x = element_blank(),
    panel.grid.minor = element_blank()
  ) +
  ylab("Home Value (US$)")

# Add a title to the plot
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line() +
  theme_minimal() +
  theme(
    axis.title.x = element_blank(),
    panel.grid.minor = element_blank()
  ) +
  ylab("Home Value (US$)") +
  ggtitle("Housing Market in New York (1975 - 2013)")

# SECTION 11
# Plotting northeast data again
ggplot(northeast, aes(x = Date, y = Home.Value, color = State)) +
  geom_line()

# Now plotting entire dataset
ggplot(housing, aes(x = Date, y = Home.Value, color = State)) +
  geom_line()

# Creating facets to split the plots by state for northeast dataset
ggplot(northeast, aes(x = Date, y = Home.Value)) +
  geom_line() +
  facet_wrap(~State, ncol = 3)

# Doing the same with the full housing dataset
ggplot(housing, aes(x = Date, y = Home.Value)) +
  geom_line() +
  facet_wrap(~State, ncol = 3)

# SECTION 12: CHALLENGE
# Recreating the Economist graph
# NEED THE DATA FROM SOMEWHERE PLEASE!
econ <- read.csv("https://altaf-ali.github.io/ggplot_tutorial/data/economist.csv")

# Exercise 1: Create a scatter plot of the economist data with CPI on the x-axis and HDI on the y-axis
ggplot(econ, aes(x = CPI, y = HDI)) +
  geom_point()

# Exercise 2: Colour the points based on Region using hollow points
ggplot(econ, aes(x = CPI, y = HDI, color = Region)) +
  geom_point(shape = 1)

# Exercise 3: Add a trend line
ggplot(econ, aes(x = CPI, y = HDI)) +
  geom_point(shape = 1, aes(color = Region)) +
  stat_smooth(method = "lm", formula = y ~ log(x), se = FALSE)
# Where the tilde means "follows", so y ~ x means "y follows x"
# And if your aes(color) is left in the main plot section, then you'll get a trend line for each grouping too

# Exercise 4: Reduce thickness of trend line
ggplot(econ, aes(x = CPI, y = HDI)) +
  geom_point(shape = 1, size = 4, stroke = 2, aes(color = Region)) +
  stat_smooth(method = "lm", formula = y ~ log(x), se = FALSE, size = 1)

# Exercise 5: Add text labels to the points
#       HINT: Create a subset of countries to label since we don't want to label every point
target_countries <- c("Russia", "Italy", "Singapore", "Germany", "Norway", "New Zealand", "Japan", "United States", "France", "Britain", "Barbados", "Singapore", "Greece", "Argentina", "Brazil", "Venezuela", "China", "South Africa", "Botswana", "Cape Verde", "Bhutan", "Rwanda", "Iraq", "India", "Myanmar", "Sudan", "Afghanistan", "Congo")
labeled_countries <- subset(econ, Country %in% target_countries)
ggplot(econ, aes(x = CPI, y = HDI)) +
  geom_point(shape = 1, size = 2.5, stroke = 1, aes(color = Region)) +
  geom_text_repel(aes(label = Country), data = labeled_countries, force = 10, col = "black") +
  stat_smooth(method = "lm", formula = y ~ log(x), se = FALSE, color = "red", size = 0.5)

# Find solutions here: https://github.com/altaf-ali/ggplot_tutorial/ (... challenge.Rmd)
