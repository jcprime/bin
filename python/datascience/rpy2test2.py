#!/usr/bin/env/python
# This script essentially does the same thing as rpy2test.py does, but all the
# Actual Work is done in R, and only gets passed back to Python when it's done

import rpy2.robjects as robjects
# pandas2ri() needed to transform certain datatypes from Python to R
from rpy2.robjects import pandas2ri
pandas2ri.activate()

rstring = """
    function(testdata){
        library(forecast)
        fitted_model<-auto.arima(testdata)
        forecasted_data<-forecast(fitted_model,h=16,level=c(95))
        outdf<-data.frame(forecasted_data$mean,forecasted_data$lower,forecasted_data$upper)
        colnames(outdf)<-c('forecast','lower_95_pi','upper_95_pi')
        outdf
    }
"""

rfunc = robjects.r(rstring)

# Assigns an R function to a Python variable using robjects.r() function
ts = robjects.r('ts')
traindf = pd.read_csv('UKgas_R.csv', index_col=0)
traindf.index = traindf.index.to_datetime()

rdata = ts(traindf.Price.values, frequency=4)
r_df = rfunc(rdata)

forecast_df = pandas2ri.ri2py(r_df)
forecast_df.index = pd.date_range(start=traindf.index.max(), periods=len(forecast_df)+1, freq='QS')[1:]

forecast_df

fig = plt.figure(figsize=(16, 7));
ax = plt.axes()
ax.plot(traindf.Price.index,traindf.Price.values,color='blue',alpha=0.5)
ax.plot(forecast_df.index,forecast_df.forecast.values,color='red')
ax.fill_between(forecast_df.index, forecast_df['lower_95_pi'], forecast_df['upper_95_pi'], alpha=0.2,color='red')
