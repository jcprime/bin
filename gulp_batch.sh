#!/bin/bash
# gulp_batch.sh
# Takes as arguments: highest last number of new .gin files for a given
# dehydration run, static first part of filename

argc="$#"
prefix="$1"
water_total="$2"

for i in {0.."$water_total"}; do
	/usr/local/gulp/gulp-4.2.mpi < "$prefix"_N"$i".gin > "$prefix"_N"$i".gout
done
