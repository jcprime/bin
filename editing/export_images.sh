#!/bin/bash
# export_images.sh (from lm_gatherings.sh)
# NEEDS EDITING TO TAKE OUT THE FAFF WITH BOND TOLERANCE SCRIPT BUSINESS, BECAUSE IT WON'T WORK UNLESS YOU GIVE IT SPECIFIC A AND B VALUES, FOR M[A] AND X[B] RESPECTIVELY

echo
echo "****************************************"
echo "*            Export_Images             *"
echo "*--------------------------------------*"
echo "* Takes Jmol-exported .jpg images      *"
echo "* for first [n] top structures of a    *"
echo "* given series and copies/renames them *"
echo "* into a \"structures\" directory.     *"
echo "*--------------------------------------*"
echo "*        Created by J.C. Prime         *"
echo "*          (18 February 2015)          *"
echo "****************************************"
echo

# Define your functions here, or forever hold your peace...
function get_image() {
    path="$1"
    zeo="$2"
    num="$3"
    if [[ "$#" -eq "4" ]]; then
        rank="$4"
    elif [[ "$#" -eq "3" ]]; then
        rank="1"
    fi

    # Check the file you need for awk-ing exists, otherwise complain at the user
    if [[ "$rank" -eq "1" ]]; then
        echo "Looking for $path/${zeo}_data.csv file, since rank is $rank"
        if [[ -f "$path/${zeo}_data.csv" ]]; then
            file="$path/${zeo}_data.csv"
        else
            echo "Erm, no luck finding it.  What's going on?!"
            exit
        fi
    elif [[ "$rank" -gt "1" ]]; then
        echo "Looking for $path/ene/${zeo}${num}_ene.tsv file, since rank is $rank"
        if [[ -f "$path/${zeo}${num}_ene.tsv" ]]; then
            file="$path/${zeo}${num}_ene.tsv"
        else
            echo "Erm, no luck finding it.  What's going on?!"
            exit
        fi
    fi

    echo "Finding optimised structure for $zeo$num with rank $rank."
    # Want to:
    #  - Find in $file:
    #     - $path/$zeo_data.csv for ground states, the line with $num in the filename
    #     - $path/ene/$zeo$num_ene.tsv for local minima, the $rank-th non-blank line
    #  - Take the string (filename) up to but not including the colon (delimiter) as "gout_to_find" variable
    gout_to_find=$(awk -v filename="$zeo$num\_" '$1 ~ filename { print $1 }' "$file")

    # Sets base_to_find to the result of gout_to_find with the longest match of ".*" removed from the end of it
    base_to_find=${gout_to_find%%.*}

    ########## NEED TO EDIT THE FIND-GREP PART PLEASE #################################
    # May need to change "$num" to gs depending on whether it's just GM or multiple ranks (where num would be the more flexible option!)
    dump_to_find=$(find ./"$num" ./* | grep "^$base_to_find.*dump")

    echo "Looking for $dump_to_find in $num directory"
    #cp "$path/$num/$dump_to_find" "$path/structures"
    cd "$path/structures" || exit

    # Exports JPEG of visualised structure (MUST NOT BE .DUMP FILE BECAUSE JMOL DOESN'T RECOGNISE IT; .GOUT INSTEAD PLEASE)
    echo "Exporting $gout_to_find structure from $num directory, via Jmol, as $zeo${num}_lm$rank.jpg image of optimised rank $rank structure, in current (structures) directory"
    # Calls Jmol to export the structure associated with the given rank to a .jpg file of the same name in the current directory, and then exit when finished
    jmol -i -g 700x657 "$path/$num/$gout_to_find" -s "$HOME"/bin/jmol_export.spt -w JPG:"$zeo$num}_lm$rank.jpg" -x
}

# Move into directory specified in $path (first argument or assumed current working directory)
# NOTE: $path should be parent directory (i.e. go into ~/data/gulp/sco and not ~/data/gulp/sco/gs, etc)
if [[ "$#" -ge "1" ]]; then
    path="$(dirname "$1")/$(basename "$1")"
elif [[ "$#" -eq "0" ]]; then
    path="$(pwd)"
fi
cd "$path" || exit

# Looks at listed files in (assumed present) 00 directory and takes the word-only common part of the filename(s) as a variable
zeo_step=$(find "$path/00" -type f -name "*.gout" | cut -d'.' -f1 | cut -d'_' -f1 | uniq) && zeo_step="$(basename $zeo_step)"
echo "Common part of filenames: $zeo_step"
# Gives you the zeolite name (everything before the first number) e.g. "scol"
zeo="${zeo_step%%[0-9]*}"
echo "zeo = $zeo"
# Gives you the dehydration step (the bit immediately following the zeolite name) e.g. "03"
num="${zeo_step##[A-Za-z]*[^0-9]}"
echo "num = $num"

# Define your variables here, or forever hold your peace...
max_water="64"
max_rank="1"

# Create structures directory if none exists; otherwise clears it out for a fresh canvas, as it were
if [[ ! -d "$path/structures" ]]; then
    echo "Creating directory to hold the JPEG files"
    mkdir "$path/structures"
else
    echo "\"Removing\" contents of existing directory, leaving an empty directory in its place"
    # Creates zzzz directory if none exists, for dumping failed previous attempts!
    if [[ ! -d "$path/zzzz" ]]; then
        mkdir "$path/zzzz"
    fi
    mv "$path/structures"/* "zzzz/"
fi

# In the case that no arguments (filenames) are given:
if [[ "$#" == 0 ]]; then
    # Set default values
    max_water="64"
    max_rank="1"
    num="00"
    #Loop over dehydration steps
    while [[ "$num" -le "$max_water" ]]; do
        #Move to directory containing the specific rank-ordered output text files
        cd "$path"/ene/ || exit
        #File with ranked structures and energies in order
        file=./"$zeo$num".txt
        rank="1"
        #Loop over rank
        while [[ "$rank" -le "$max_rank" ]]; do
            # Call get_image function to do the actual image processing
            get_image "$path" "$zeo" "$num" "$rank"
            #cp "$HOME/$path"/"$zeo$num_$suffix"_lm"$rank".jpg "$HOME/$path"/structures/"$zeo$num_$suffix"_lm"$rank".jpg
            rank=$((rank+1))
        done
        echo "Files copied across for $zeo$num\_$suffix, rank = $rank."
        num_inc=$((10#num+1))
        # Ensure num stays padded throughout!
        num=$(printf '%02d' "$num_inc")
    done
fi
echo "Finished."
