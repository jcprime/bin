#!/bin/bash
# usbsync.sh
# Uses unison to bidirectionally sync files when specific USB drive (with ID 0951:1665) is plugged in

for dir in "bin" ".dotfiles" "words" "test" ".config/sublime-text-3/Packages/User"; do
    #syncdir="$HOME/$dir" && echo "$syncdir"
    osync.sh "$dir".conf
done
