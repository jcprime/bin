#!/bin/bash
# doi_grabber.sh
# Grabs DOIs from non-BibTeX citations, to be passed to doi2bib and re-downloaded!

# work_dir="$HOME/Desktop/PhD/readings/citations"
work_dir="$(pwd)"
cd "$work_dir" || exit
nonbib_file="nonbib.txt"
bib_file="bib.txt"
total_file="dois.txt"

> "$nonbib_file"
# Iterate over files with non-bib or -bibtex extensions
for ext in ciw enw ris; do
    doi_ciw=$(grep "^DI\s*.*" *.ciw | cut -d" " -f2)
    doi_enw=$(grep "^DO\s*.*" *.enw | cut -d" " -f3)
    doi_ris=$(grep "^DO\s*.*" *.ris* | cut -d" " -f4)
    echo "$doi_${ext}" >> "$nonbib_file"

done

> "$bib_file"
# Iterate over files with .bib or .bibtex extensions
# for file in *.{bib,bibtex}; do
    doi_bib=$(grep "^doi\s*.*" * | cut -d" " -f3 | cut -d'"' -f2 | cut -d'{' -f2 | cut -d'}' -f1 | sed s/doi://g | sed "s/http:\/\/dx.doi.org\///g")
    echo "$doi_bib" >> "$bib_file"
# done

cat "$nonbib_file" > "$total_file"
cat "$bib_file" >> "$total_file.tmp"
cat "$total_file.tmp" | sort | uniq > "$total_file"
