#!/bin/bash
# tabulate.sh
# Grabs the important bits out of the ground state .gout files
# and squidges them all into a table-y format

for zeo in nat sco mes; do
	working_dir="/home/james/data/test/$zeo/gs"
	cd $working_dir
	# Squishes tab-separated titles into new file called tablings.txt
	echo "File\tEnergy\tGnorm\tA\tB\tC\tAlpha\tBeta\tGamma\tVolume\tPrimitive volume" > tablings.txt
	# Grabs energy values with grep, sort, and awk
	echo $(grep 'Final ene' *out | sort -n | awk SOMETHINGOROTHER >> tablings.txt)
	# Taken as optimised since in gs folder (has been checked before it gets copied there, y'see)
	# Grabs Gnorm values with grep, sort, and awk
	echo $(grep 'Final Gnorm' *out | sort -n | awk SOMETHINGOROTHER >> tablings.txt)
	# Grabs cell parameter values with grep and awk
	echo $(grep -A9 'Final cell parameters' | awk COMPLICATEDSOMETHINGOROTHER >> tablings.txt)
	# Grabs volume values with grep and awk
	echo $(grep 'Volume' *out | awk TAKE SECOND VALUE AFTER WORD 'VOLUME' IN LAST RECORD PER FILE FOR FINAL VOLUME PLEASE! >> tablings.txt)
	# Grabs primitive cell volume values with grep and awk
	echo $(grep 'Primitive cell volume' *out | awk SOMETHINGOROTHER >> tablings.txt)
# STILL NEED TO ADD TABS IN BETWEEN THE FIELDS!
# ALSO NEED TO GET AWK TO ADD EACH COLUMN AS A SET OF FIELDS UNDER THE ORIGINAL HEADINGS SOMEHOW!
done

# The AWK bit
# Print headers first
awk 'BEGIN{first_file = 'blah'; printf "File\tEnergy\tGnorm\tA\tB\tC\tAlpha\tBeta\tGamma\tVolume\tPrimitive volume"} {} END{}' file_to_be_acted_on.ext
# Outputs only the lines with whitespace preceding "Volume", just for later reference and all
grep 'volume' *out | awk '/\sVolume/'
