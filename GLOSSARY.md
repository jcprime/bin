# Glossary of Programming Terms

- **Bug:** Anything which stops code from working as expected
- **Debugging:** Fixing bugs in code
- **Refactoring:** Rewriting working code to be shorter without losing functionality