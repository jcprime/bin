#!/usr/bin/env python3
# gulp_coords.py
# Converts the fractional coordinates found in GULP input files and processes them into Cartesian coordinates for turning into CP2K coordinate files
# REQUIRES the cell parameters and each separate coordinate line to have SINGLE SPACES as separators!

import sys
import math

# Function to print usage
def printUsage():
    print("Usage: python gulp_coords.py a b c alpha beta gamma inputFile.ext")

# Function to find and store all lines after "cell"
# but before "end" or "species"

# Function to find and store cell parameters
def cellParameters(a, b, c, alpha, beta, gamma):
    lengths = []
    for i in range(1, 3):
        lengths.append(float(sys.argv[i]))
    angles = []
    for i in range(4, 6):
        angles.append(float(sys.argv[i]))
    return lengths, angles

# Function to convert from fractional to Cartesian coordinates
def fracToCart(lengths, angles, xyz):
    # lengths and angles are lists, each with three elements
    # xyz is a list of floats with the fractional coordinates for that given line
    atom = xyz[0]
    volume = math.sqrt(1 - (math.cos(angles[0]))**2 - (math.cos(angles[1]))**2 - (math.cos(angles[2]))**2 + (2 * math.cos(angles[0]) * math.cos(angles[1]) * math.cos(angles[2])))
    # Use matrix from Wikipedia to convert to Cartesian coordinates
    xCart = (lengths[0] * xyz[1]) + ((lengths[1] * math.cos(angles[2])) * xyz[2]) + ((lengths[2] * math.cos(angles[1])) * xyz[3])
    yCart = (0 * xyz[1]) + ((lengths[1] * math.sin(angles[2])) * xyz[2]) + ((lengths[2] * ((math.cos(angles[0]) - (math.cos(angles[1]) * math.cos(angles[2]))) / math.sin(angles[2]))) * xyz[3])
    zCart = (0 * xyz[1]) + (0 * xyz[2]) + ((lengths[2] * (volume / math.sin(angles[2]))) * xyz[3])
    return [atom, xCart, yCart, zCart]

# Split second Actual Argument (cell parameters as string) into lists
cellStuff = sys.argv[1].split(' ')

# Make list of lengths
lengths = []
for i in range(1, 4):
    #lengths.append(float(sys.argv[i]))
    lengths.append(float(cellStuff[i]))

# Make list of angles, assuming given in degrees, and convert them to radians for use in fracToCart()
angles = []
for i in range(4, 7):
    #angles.append(math.radians(float(sys.argv[i])))
    angles.append(math.radians(float(cellStuff[i])))

#reader = open(sys.argv[7], 'r')
reader = open(sys.argv[2], 'r')
sourceFile = reader.readlines()
reader.close()

#filename = sys.argv[7].split('.')
filename = sys.argv[2].split('.')

# Ensure all fractional coordinates are actually floats
coords = []
for i in sourceFile:
    coord = i.split(' ')
    for num in range(1, 4):
        if "/" in coord[num]:
            coord[num] = eval(coord[num])
        coord[num] = float(coord[num])
    coords.append(coord)

# Convert the input coordinates into the other type
print("Converting to Cartesian coordinates...")
newCoords = []
for line in coords:
    newCoord = fracToCart(lengths, angles, line)
    newCoords.append(newCoord)

# Create the output strings and write them to the file
outputData = []
for line in newCoords:
    formattedLine = "{0[0]:<2}   {0[1]:> 10.6f}   {0[2]:> 10.6f}   {0[3]:> 10.6f}\n".format(line)
    outputData.append(formattedLine)
outputData.append('\n')

if (filename[-1] == "frac"):
    outputFilename = filename[0] + ".cart"

writer = open(outputFilename, 'w')
writer.write(''.join(outputData))
writer.close()

print("... aaaand we're done.")
