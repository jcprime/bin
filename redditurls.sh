#!/bin/bash
# redditurls.sh
# Contains commands for grabbing submission URLs from the given timesearch.py-downloaded database

# Actual timesearch commands
function timesearch() {
    subname="$1"
    cd ~/Downloads/timesearch || exit
    python3 timesearch.py timesearch -r "$subname"
    python3 timesearch.py commentaugment -r "$subname"
}

# Command to grab URLs and reddit IDs from resulting database
function urlgrab() {
    dbfile="$1"
    urlfile="${dbfile%%.db}_urls.csv"
    # Actual SQL command: select idstr, url from submissions where url is not NULL;
    # Something to export them to a CSV file
    sqlite3 -header -csv "$dbfile" "select idstr,url from submissions where url is not NULL;" > "$urlfile"
    dos2unix "$urlfile" # Just in case, because it was misbehaving originally
}

# AWK-ing the SQL-output CSV data into a file of commands
function wgetsql () {
    urlfile="$1"
    wgetfile="$2"
    awk -F',' 'BEGIN{RS="\n";} !/^#/ { printf "wget %s -O %s.jpg \n", $2, $1; }' "$urlfile" > "$wgetfile"
    # Run file of wget commands
    bash "$wgetfile"
}

if [ "$#" -eq "1" ]; then
    subreddit="$1"
fi

timesearch "$subreddit"
urlgrab "$HOME/Downloads/timesearch/subreddits/${subreddit}/${subreddit}.db"
wgetsql "$HOME/Downloads/timesearch/subreddits/${subreddit}/${subreddit}_urls.csv" "$HOME/Downloads/timesearch/subreddits/${subreddit}/${subreddit}_wget.txt"
