# Modular `dehydrate`

For ease of reference:

File                        | Contents/purpose
----------------------------|----------------------------------------------------------------------
`classes.h`                 | Defining classes in C++ port
`dehydrate.c`               | Original main C file
`dehydrate.cpp`             | Main C++ port
`functions.h`               | Defining/prototyping functions in C++ port
`generate_neighbours.cpp`   | C++ port of original `generate_neighbours.c`
`header.h`                  | Defines external variables
`maxima.h`                  | Defines constants for maxima (file length, line length, etc)
`own_maths.h`               | Defines external maths-y things (fractional values, pi, etc)
`reader.c`                  | Original input-file reader in C
`reader.h`                  | Original input-file reader header
`structures.h`              | Defines `typedef struct`s for original C code