#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 12:47:09 2017

@author: james
"""

import re
import docx

document = docx.Document("/home/james/Dropbox/Lesley/LesleyPyEdit.docx")

# Effective "search" equivalent (from https://stackoverflow.com/questions/24805671/how-to-use-python-docx-to-replace-text-in-a-word-document-and-save)
def search(searchstring):
    for paragraph in document.paragraphs:
        if searchstring in paragraph.text:
            print(paragraph.text)

def regex(pattern, replace):
    for paragraph in document.paragraphs:
        re.findall(pattern, paragraph)
        re.sub(pattern, replace, paragraph)

document.save("/home/james/Dropbox/Lesley/LesleyPyEdited.docx")