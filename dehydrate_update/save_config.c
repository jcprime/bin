#include <stdio.h>
#include "structures.h"
#include "header.h"

void save_config(FILE *file_fp, int config, int num_replace_atoms,
                 double final_energy,
                 int *p_replaceable_index, int *p_replaceable_done)
{
    int i, iloop;
    int *index;
    int *done;

    fprintf(file_fp, "%s %10.6f %d ", insert_ele, final_energy,config);
    for (i = 0; i <= num_replace_atoms; i++)
    {
        done = p_replaceable_done + i;
        if (*done == 1)
        {
            index = p_replaceable_index + i;
            fprintf(file_fp, "%d ", *index);
        }
    }
    fprintf(file_fp, "\n");
    return;
}

