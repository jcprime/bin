#!/bin/bash
# structure_splodge.sh
# Copies files with given extensions in given file list to a different given directory

change="r1000"
cd /home/cirrusprime/james_p_rsync/klmc_"$change"/structures/

ext=("gin" "jpg" "xyz")

while [[ "$ext" -lt 3 ]]; do
 for i in *."$ext"; do
  cp "$i" /home/cirrusprime/Desktop/structures/"${i%."$ext"}_$change.$ext"
 done
done
