#!/bin/bash

path="james_p_rsync/dmol"

MX=("LiF" "NaCl" "KF" "RbBr")

argc=$#
if [[ "$argc" == 0 ]]; then
 a="0"
 while [[ "$a" -lt "4" ]]; do #Loops over MX
  for i in {1..24}; do
   cd ~/"$path"/"${MX[a]}"_"$i"/top_structures/ #Move to directory containing the statistics file, with structure ID in order of rank
   file=./statistics #File with ranked structures and energies in order
   for rank in {1..5}; do
    to_find=$(awk -v rankawk=$rank '$1 == rankawk {print $2}' $file)
    car_to_find=$(find . * | grep ^"$to_find"-0"$rank".*car)
    babel ~/"$path"/"${MX[a]}"_"$i"/top_structures/"$car_to_find" ~/"$path"/"${MX[a]}"_"$i"/top_structures/"${car_to_find%.car}.xyz"
   done
  done
  a=$((a+1))
 done
else
 echo "Something's gone wrong.  Sorry."
fi
echo "Finished."
