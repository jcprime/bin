#!/usr/bin/env python
# potentials.py
# Plots potentials for water on one set of axes

import numpy as np
import pandas as pd

epsilon0 = 8.854187817620 * (10**-12)
# Coulomb interaction: Ucoul = q_i * q_j / 4 * pi * epsilon0 * r_{ij}
coul_HO = (0.4 * -2.05)/(4 * np.pi * epsilon0 * r)

# Buckingham general equation: Ebuck = A*exp(-r/rho) - C/r**6
# Between 0.00 and 20.00 Angstroms
buck_HO = (396.27 * np.exp(-r/0.25)) - (10.00/r**6)

# LJ general equation: Elj = A/r**m - B/r**n
# Between 0.00 and 12.00 Angstroms:
lenn12_6_OO = (39344.98/r**12) - (42.15/r**6)
# Between 0.00 and 12.00 Angstroms:
lenn9_6_HO = (24.00/r**9) - (6.00/r**6)

# Morse general equation: Emorse = De*((1-exp(-a(r-r0)))**2 - 1.0) - coul*qi*qj/r
# Between 0.00 and 1.40 Angstroms:
morse_HO = 6.203713 * ((1-np.exp(-2.220030 * (r-1.18)))**2 - 1.0) - (50.0 * 0.4 * -2.05)/r
# Between 0.00 and 1.00 Angstroms:
morse_HH = 0.0 * ((1-np.exp(-2.840499 * (r-1.50)))**2 - 1.0) - (50.0 * 0.4 * 0.4)/r

# Three-body general equation: Ethree = (1/2) * k * (theta-theta0)**2
# Between 0.00 and 2.00 Angstroms for r and 0 and
three_HOH = (1/2) * 4.1998 * (theta-np.radians(108.69))**2

# Core-shell spring interaction: Ecs = (1/2)*k2*r**2 + (1/24)*k4*r**4
spring_O = (1/2) * 209.449602 * r**2

def plotpot(rmin=0.1, rmax=20.0):
    r = np.linspace((rmin*(10**(-10))), (rmax*(10**(-10))), 2000)
    #coul_OH = (0.4 * -2.05)/(4 * np.pi * epsilon0 * r)
    buck_HO = (396.27 * np.exp(-r/0.25)) - (10.00/r**6)
    lenn12_6_OO = (39344.98/r**12) - (42.15/r**6)
    lenn9_6_HO = (24.00/r**9) - (6.00/r**6)
    morse_HO = 6.203713 * ((1 - np.exp(-2.220030 * (r-1.18)))**2 - 1.0) - (50.0 * 0.4 * -2.05)/r
    morse_HH = 0.0 * ((1 - np.exp(-2.840499 * (r-1.50)))**2 - 1.0) - (50.0 * 0.4 * 0.4)/r
    #three_HOH = (1/2) * 4.1998 * (theta-np.radians(108.69))**2
    #spring_O = (1/2) * 209.449602 * r**2
    blm_total = (2 * coul_OH) + buck_HO + lenn12_6_OO + lenn9_6_HO + morse_HO + morse_HH
    plt.plot(r, blm_total, color='blue', linestyle='solid')
    plt.show()

test = plotpot()
#test.plt.savefig('test.pdf', format=pdf)
