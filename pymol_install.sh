#!/bin/bash -e

# THINGS TO INSTALL FIRST, IF NECESSARY
#sudo apt-get install python python-dev python-pmw python-numpy freeglut3-dev libglew-dev libpng12-dev libfreetype6-dev build-essential libxml++2.6-dev

# REQUIRES CMAKE TO BE INSTALLED: Get msgpack-c and install (because apparently it's not in Ubuntu's repositories, for some reason)
git clone https://github.com/msgpack/msgpack-c.git
cd msgpack-c
cmake -DMSGPACK_CXX11=ON .
sudo make install

# Download and extract latest version of PyMOL
cd ~/Downloads
wget http://sourceforge.net/projects/pymol/files/latest/download -O pymol_source.tar.bz2
tar -xvf pymol_source.tar.bz2
cd pymol

mkdir -p ~/pymol/modules
prefix=~/pymol
modules=$prefix/modules

# If you want to install as root, then split this line up in "build"
# and "install" and run the "install" with "sudo"
python setup.py build install --home=$prefix --install-lib=$modules --install-scripts=$prefix

# Make pymol callable on its own
sudo cp ~/pymol/pymol /usr/bin/pymol
