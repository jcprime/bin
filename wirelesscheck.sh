#!/bin/bash

#if iwconfig wlx74da3858711b | grep -o "Access Point: Not-Associated"; then
	#ifconfig wlx74da3858711b down
	#sleep 10
	#ifconfig wlx74da3858711b up
#fi

if nmcli d wifi list | grep "Ether"; then
    nmcli c up "Ether"
elif nmcli d wifi list | grep "eduroam"; then
    nmcli c up "eduroam"
fi
