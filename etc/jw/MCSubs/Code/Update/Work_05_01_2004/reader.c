/*****************************************************************************/ 
/******  reader.c                                                *************/ 
/******  Input file reader. started 23/11/94 DWL                 *************/ 
/******  from Tim Bush's reader.c                                *************/ 
/******                                                          *************/ 
/******  IMPORTANT CHANGE: dwl 10.4                              *************/
/******  interaction keyword now cumulative                      *************/
/******  i.e. if charges specificied then steric+nonbond automatic ***********/
/******  if nonbonded specified then steric automatic              ***********/
/******  Bomb out if steric = 0                                    ***********/
/******                                                            ***********/
/******  22/2/96 DWL new keywords                                  ***********/
/******  initial_minimize                                          ***********/
/******  flag whether to do template min and inpore min before we  ***********/
/******                                                            ***********/
/******  21/10/96 DWL                                              ***********/
/******  Logical flagging now included for most things:            ***********/
/******     keyword [on/off]                                       ***********/
/******        replacing earlier effort with yes/no which was cack ***********/
/******  Keywords implemented in this way are:                     ***********/
/******  steric, non_bonded, charges, initial_minimize             ***********/
/******  centre, check                                             ***********/
/******  NEW KEYWORD : random [on/off]                             ***********/
/******  NEW KEYWORD : determines if real_random is random or not! ***********/
/******  21/10/96 DWL                                              ***********/
/******  NEW KEYWORD : cost_function [steric/non_bond/energy]      ***********/
/******  NEW KEYWORD : determines which cost_function to use       ***********/
/******  7th March 1997 Dave Willock                               ***********/
/******  Decided that cost_function directive can set steric,      ***********/ 
/******  non_bond and charges logicals now so the explicit input   ***********/ 
/******  to set them has been removed!!                            ***********/
/******  Dewi and Dave June 97                                     ***********/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
/*#include <errno.h>*/
#include "maxima.h"
#include "global_values.h"
#include "structures.h"
#include "data.h"
#include "reader.h"

int  parse_line(void);

int locate_string( char *p_key, char *p_char, int num_of_chars );

int reader(char *file_to_read)
{
#include "header.h"
  char *fullstop;
  int abandon_ship, error;


  if (!(input_fp = fopen(file_to_read, "r")))
     {
       fprintf(output_fp,"ERROR: Problem opening file %s for input\n", file_to_read);
       exit(1);
     }

  for(;;)   /* get the info from the file */
    {
      abandon_ship = parse_line();

      fclose(input_fp);
      return(abandon_ship);
    }
}

/*****************************************************************************
  Parse_line, chops an input line from the input file and chops it into
  tokens, each of which is tested against the list of allowed directives
  in reader.h.  The correct function to read subsequent program parameters
  is then called..
*****************************************************************************/

int check_element(char *p_element);
int find_kind(char *token, int level);
char * tok_get(FILE *input_fp, int skip_lines);
void get_title(void);
void get_gulp_command(void);
void get_gulp_path(void);
void get_analyse_path(void);
void get_generate_path(void);
void get_regenerate_path(void);
int get_cell_volume_flags(void);
int get_coordinate_file(void);
int get_gulp_cycles(void);
void get_gulp_time(void);
void get_gulp_command(void);
void get_gulp_file(void);
int get_pots_file(void);
int get_max_cycles(void);
int get_max_subs(void);
void parse_error(char *last_tok);
int get_ratio(void);
int get_peek_freq(void);

void print_biosym_header(FILE *file_fp,  int pbc_flag);

int    parse_line(void)
{
#include "header.h"
  int   l;
  int   bail_out;
  int   token, second_token, third_token;
  char  *tok, *last_tok;
  char  *inform;
  

  l = 0;
  bail_out= FALSE;
  
  for (;;)
    {

      if (!(tok = tok_get(input_fp, TRUE)))
    	{
	    if (++l > 3) break; else continue;
	    }
      l = 0;
/*****************************************************************/
/**** If required print out the token you are about to process ***/
/*****************************************************************/

      token = find_kind(tok, PRIME_DIRECTIVE);

/****DEBUG ************************/
/*printf("Token = >>%s<<\n", tok);*/
/****DEBUG ************************/

/*****************************************************************/
/**** If the last command was not found let the user know ********/
/**** and then bail out after processing the entire file! ********/
/**** Added 4/3/97 Dave Willock **********************************/
/*****************************************************************/
      if (token == BLANK_DIRECT)
         {
            if (regenerate_run == FALSE)
               {
                /*chances are its just the configs which I can't be bothered
                  to error trap out  - so ignore*/
                fprintf(output_fp,
                       "ERROR: The command >>%s<< was not recognised\n",tok);
               }
            bail_out= TRUE;
         }
      last_tok = tok;
      switch (token)
	   {
	   case TITLE: get_title(); break;

	   case RATIO: bail_out= get_ratio() || bail_out; 
                             break;
	   case CONFIGS: bail_out=set_configurations_flag()||bail_out;
				break;
           case FIXED_CATION: cation_insert_run = FALSE; 
                              bail_out= get_fixed_cation_cutoff()||bail_out;
                              printf("FIXED_CATION\n");break;

	   case POTS_FILE: bail_out= get_pots_file() || bail_out; 
                           break;
	   case CELL_FILE: bail_out= get_coordinate_file() || bail_out; 
                           break;
           case CELL_VOLUME_FLAGS: bail_out=get_cell_volume_flags()||bail_out;
                           break;
           case GULP: tok = tok_get(input_fp,FALSE);
                      second_token = find_kind(tok, SECONDARY_DIRECTIVE);
                      last_tok =tok;
                      /*printf("DB>>getting GULP token\n");*/
                      switch (second_token)
                       {
                       case GULP_FILE: get_gulp_file() ; break;
                       case GULP_PATH: get_gulp_path() ; break;
                       case GULP_MAX_CYCLES: get_gulp_cycles(); break;
                       case GULP_TIME: get_gulp_time() ; break;
                       case GULP_COMMAND: get_gulp_command() ; break;
                       }
                      break;

          case SCRIPT: tok = tok_get(input_fp,FALSE);
                      second_token = find_kind(tok, SECONDARY_DIRECTIVE);
                      last_tok =tok;
                      switch (second_token)
                       {
                       case SCRIPT_RUN: script_type = SCRIPT_RUN_TYPE; break;
                       case SCRIPT_QSUB: script_type = SCRIPT_QSUB_TYPE; break;
                       }
                      break;

          case ANALYSE: tok = tok_get(input_fp,FALSE);
                      second_token = find_kind(tok, SECONDARY_DIRECTIVE);
                      last_tok =tok;
                      switch (second_token)
                       {
                       case ANALYSE_FULL: analyse_type = ANALYSE_TYPE_FULL; get_analyse_path(); break;
                       case ANALYSE_SIMPLE: analyse_type = ANALYSE_TYPE_SIMPLE;  get_analyse_path(); break;
                       }
                      break;

	   case CATION: bail_out= get_cation() || bail_out; 
                           break;
           case INSERT: bail_out= get_insert() ||bail_out;
                           break;
           case REPLACE: bail_out= get_replace() ||bail_out;
                           break;
           case GENERATE: get_generate_path();
                             break;
           case REGENERATE: get_regenerate_path();
                             break;


	   case MAX_SUBS: bail_out= get_max_subs() || bail_out; break;
	   case MAX_CYCLES: bail_out= get_max_cycles() || bail_out; break;
	   case PEEK_FREQ: bail_out= get_peek_freq() || bail_out; break;
	   case KEEP_LIST: bail_out= get_keep() || bail_out; break;

	   case LOWENSTEIN: tok = tok_get(input_fp,FALSE);
                          second_token = find_kind(tok, SECONDARY_DIRECTIVE);
                          last_tok =tok;
                          switch (second_token)
                {
                  case ON2  :  lowenstein = TRUE;break;
                  case OFF2 :  lowenstein = FALSE;break;
                }
             break;

       case RANDOM:    
             tok = tok_get(input_fp,FALSE);
 	     second_token = find_kind(tok, SECONDARY_DIRECTIVE);
             last_tok =tok;
             switch (second_token)
		{
                  case ON2  :  randomisation = TRUE;break;
                  case OFF2 :  randomisation = FALSE;break;
		}
             break;
       }
}

/*******************************************************************************/
/* errors will have been reported and the code should wind up if bail_out TRUE */  
/*******************************************************************************/

  return (bail_out); 
}

/******************************************************************************
  tok_get.c : parses command lines in a semi intelligent fashion
IMportant change 22/6/95 DWL Now ignores EVERYTHING after a #
******************************************************************************/


char * tok_get(FILE *input_fp, int skip_lines)
{
  int          i;
  char         *tok;
  static char  target[BUFFER];
  static char  buf[BUFFER];
  static char  *line, *last_tok;
  extern int   read_new_line, line_no;
  char         *comment;	

  i = 0;

/**************************************************************************/
/*** If the last token read was a NULL character last_tok will act ********/
/*** like a FALSE logical and force us to read the next line **************/
/**************************************************************************/

  if (!last_tok && skip_lines)
    {

/**************************************************************************/
/**** Read in the next line from input_fp, if it is NULL return NULL ******/
/**************************************************************************/

      if (!(line = fgets(target, BUFFER, input_fp))) return(line);

/**************************************************************************/
/**** Remove commented lines or parts of lines that are comments **********/
/**************************************************************************/

      if ((comment = strchr(line,'#')) != NULL) 
        {

/**************************************************************************/
/****** chop off the bits after the hash **********************************/
/**************************************************************************/

           *(comment) = '\0';

/**************************************************************************/
/****** if this is a simple comment line it will now have length 0 ********/
/****** so simply return a single # character                      ********/
/**************************************************************************/

           if (strlen(line) < 1) 
             {
               *line= '#';
               *(line+1)= '\0';
               return(line);
             }
         }

/**************************************************************************/
/***** read_new_line says we have just read a new line in !! **************/
/**************************************************************************/

       read_new_line = TRUE;
       line_no++;
    }
  else if (!last_tok && !skip_lines)
    {
       return(NULL);
    }

/**************************************************************************/
/****** If we have just read a new line copy it to the buffer *************/
/**************************************************************************/

  if (read_new_line) strncpy(buf, line, BUFFER);

/**************************************************************************/
/***** Now process the latest string of information ***********************/
/**************************************************************************/

  for(;;)
    {

/**************************************************************************/
/***** tok is read from the last line if some tokens are left *************/
/**************************************************************************/

      tok = (read_new_line) ? strtok(buf, " :;,\n\r\t") : strtok(NULL, " :;,\n\r\t");

      read_new_line = (!tok) ? TRUE : FALSE; 
      last_tok = tok;
      if (!tok || !isalpha(*tok)) break;

    /* DO NOT LOWER CASE CONVERT */
    /*  for (i = 0; tok[i] != '\0'; i++) tok[i] = tolower(tok[i]);*/
      if (find_kind(tok, REMAINING_DIRECTIVE) != UNIT) break;
      continue;
    }
  return(tok);
}
  
/******************************************************************************
  Find_kind takes a parsed token, and matches it against the directive list
  contained in reader.h, returns a BLANK_DIRECT value if token is unrecognised,
  and a UNIT value if the token is extraneous clutter..
******************************************************************************/

list null_namelist[]   = {NULL_DIRECTIVE_LIST};
list first_namelist[]  = {FIRST_DIRECTIVE_LIST};
list second_namelist[] = {SECOND_DIRECTIVE_LIST};
list third_namelist[] = {THIRD_DIRECTIVE_LIST}; 

/*****************************************************************************/
/*** find_kind altered to be context aware by knowing the level from *********/
/*** which the token was read. Dave Willock March 1997 ***********************/
/*****************************************************************************/

int find_kind(char *token, int level)
{
  int i, j,k,  l;
  
  i = 0;
  j = 0;
  k = 0;
  l = 0;

  if (!token) return(-1);

if (level == PRIME_DIRECTIVE || level == REMAINING_DIRECTIVE)
  {
    do
      {
        if (!strncmp(token, first_namelist[j].directive,4))
          {
	     return(first_namelist[j].token_index);
	  }
      }
  while(first_namelist[j++].token_index != BLANK_DIRECT);
  }
else if (level == SECONDARY_DIRECTIVE)
  {
    do
      {
        if (!strncmp(token,second_namelist[k].directive,4))
  	  {
	    return(second_namelist[k].token_index);
	  }
      }
    while(second_namelist[k++].token_index != BLANK_DIRECT); 
  }
else if (level == TERTIARY_DIRECTIVE)
  {
    do 
     { 
/**********************************************************/
/**** For 3rd directive compare only to 2nd character *****/
/**********************************************************/
       if (!strncmp(token,third_namelist[l].directive,2))
	 { 
	  return(third_namelist[l].token_index); 
	 } 
     } 
    while(third_namelist[l++].token_index != BLANK_DIRECT);
  }

    do
      {
        if (!strncmp(token, null_namelist[i].directive,2))
	  {
	    return(UNIT);
	  }
      }
    while(null_namelist[i++].token_index != BLANK_DIRECT);

  return(BLANK_DIRECT);
}
  
/******************************************************************************
  Get token routines for individual datatypes, token is taken from line and
  cast to apprpriate type...
******************************************************************************/
 
int get_integer(int *p_error, int skip_lines)
{
#include "header.h"
  int answer;
  char *tok;

  tok= tok_get(input_fp, skip_lines);

/*******************************************************************/
/**** Check all is well, tok will give FALSE if it is NULL *********/
/**** If there is a string there check the first character *********/
/**** to see if it is a valid decimal number               *********/
/**** Dave Willock March 1997 **************************************/
/*******************************************************************/

  if (tok)
    {
       if ((*tok >= '0' && *tok <= '9') || *tok == '-')
          {
             answer = atoi(tok);
             *p_error = FALSE;
          }
       else
          {
             answer= 0;
             *p_error = TRUE;
          }
    }
  else
    {
       answer= 0;
       *p_error = TRUE;
    }
  return (answer);

}

char * get_string(void)
{
#include "header.h"
  char *dummy;

  dummy = tok_get(input_fp,FALSE);
  return (dummy);
}

double get_double(int *p_error, int skip_lines)
{
#include "header.h"
double answer;
int itsanum;
char *tok;
  
  tok= tok_get(input_fp, skip_lines);

/*******************************************************************/
/**** Check all is well, tok will give FALSE if it is NULL *********/
/**** If there is a string there check the first character *********/
/**** to see if it is a valid decimal number               *********/
/**** Dave Willock March 1997 **************************************/
/*******************************************************************/

  if (tok)
    {
       if ((*tok >= '0' && *tok <= '9') || *tok == '-' || *tok == '.')
          {
             answer = atof(tok);
             *p_error = FALSE;
          }
       else
          {
             answer= 0;
             *p_error = TRUE;
          }
    }
  else
    {
       answer= 0;
       *p_error = TRUE;
    }
  return (answer);
}
  
/******************************************************************************
  Contains all the files used in parse_line to read specific data into the 
  correct variables.  Also contains error function returning error parsing
  file message, including error line and lasdt token read
******************************************************************************/

void parse_error(char *last_tok)
{
#include "header.h"
  if (!(output_fp =fopen(output_file, "w")))
    {
      fprintf(output_fp,"\nERROR: opening file \"%s\"\n", output_file);
      exit(1);
    }
  fprintf(output_fp,"\n\terror parsing input file: %s", output_file);
  fprintf(output_fp,"\n\t\tlast token read: %s", last_tok);
  fprintf(output_fp,"\t at or near line: %d\n\n", line_no);
  fclose(output_fp);
  exit(1);
} 

void get_title(void)
{ 
#include "header.h"
  fgets(title,BUFFER,input_fp);
  return;
}
void get_gulp_command(void)
{
#include "header.h"
  fgets(gulp_command_line,BUFFER,input_fp);
  return;
}
int get_cell_volume_flags(void)
{
#include "header.h"
  fgets(cell_volume_flags,BUFFER,input_fp);
  if (strstr(cell_volume_flags, CONP) != NULL)
     strcpy(cell_volume_flags, "1 1 1 1 1 1\n");
  else if (strstr(cell_volume_flags, CONV) != NULL)
     strcpy(cell_volume_flags, "0 0 0 0 0 0\n");
  return;
}


void get_analyse_path(void)
{
#include "header.h"
  int error;

  error=FALSE;

  strcpy(analyse_path, get_string());
  return;
}


void get_generate_path(void)
{ 
#include "header.h"
  int error;
  
  error=FALSE;

  strcpy(generate_path, get_string());
generate_only = TRUE;
  return;
}

int set_configurations_flag(void)
{
 #include "header.h"
 configurations_flag=TRUE;
 return;
}
void get_regenerate_path(void)
{ 
#include "header.h"
  int error;
  
  error=FALSE;

  strcpy(regenerate_path, get_string());
  regenerate_run = TRUE;
  return;
}
void get_gulp_path(void)
{
#include "header.h"
  int error;

  error=FALSE;

  strcpy(gulp_path, get_string());
  return;
}



int get_coordinate_file(void)
{
#include "header.h"
  int error;

  error=FALSE;

  strcpy(coordinate_file, get_string());
  return;
}

int get_pots_file(void)
{
#include "header.h"
  int error;

  error=FALSE;

  strcpy(pots_file, get_string());
  return;
}

int get_fixed_cation_cutoff(void)
{
#include "header.h"
  int error;

  error=FALSE;
  fixed_cation_cutoff=get_double(&error, FALSE);
  return;
}


int get_cation(void)
{
#include "header.h"
  int error;

  error=FALSE;

  strcpy(cation, get_string());
  if (strstr(cation, SMEAR) == NULL)
    {
      cation_charge = get_double(&error, FALSE); 
    }
  else 
    cation_charge = SMEAR_CHARGE;
  if (error) fprintf(output_fp,"\nERROR: Expecting cation charge\n");

  return;
}

int get_replace(void)
{
#include "header.h"
  int error;

  error=FALSE;

  strcpy(replace_ele, get_string());
  replace_charge = get_double(&error, FALSE);
  if (error) fprintf(output_fp,"\nERROR: Expecting replace charge\n");

  return;
}
int get_insert(void)
{
#include "header.h"
  int error;

  error=FALSE;

  strcpy(insert_ele, get_string());
  insert_charge = get_double(&error, FALSE);
  if (error) fprintf(output_fp,"\nERROR: Expecting insert charge\n");

  return;
}



int get_gulp_cycles(void)
{
#include "header.h"
int error;

  gulp_max_cycles = get_integer(&error, FALSE);
  if (error) fprintf(output_fp,"\nERROR: The maximum number of cyclestes directive (cycl) is given but it has no value.\n");

  if (gulp_max_cycles < 0)
     {
        fprintf(output_fp,"\nERROR: The maximum number of minimisation cycles cannot be negative.\n");
        error=TRUE;
     }
  return; 
}

int get_max_subs(void)
{
#include "header.h"
int error;

  max_subs = get_integer(&error, FALSE);
  if (error) fprintf(output_fp,"\nERROR: The maximum number of substituents directive (subs) is given but it has no value.\n");

  if (max_subs < 0)
     {
        fprintf(output_fp,"\nERROR: The maximum number of subs cannot be negative.\n");
        error=TRUE;
     }
  return (error);
}

int get_max_cycles(void)
{
#include "header.h"
int error;

  max_cycles = get_integer(&error, FALSE);
  if (error) fprintf(output_fp,"\nERROR: The maximum number of cyclestes directive (cycl) is given but it has no value.\n");
  
  if (max_cycles < 0)
     {
        fprintf(output_fp,"\nERROR: The maximum number of cycles cannot be negative.\n");
        error=TRUE;
     }
  return (error);
}


int get_ratio(void)
{
#include "header.h"
int error;
  ratio = get_double(&error, FALSE);
  if (error)
    {
      fprintf(output_fp,"\nERROR: Non-numeric supplied for ratio parameter (directive ratio).\n");
    }

  return(error);
}

void get_gulp_time(void)
{
#include "header.h"
int error;
  gulp_time_limit= get_double(&error, FALSE);

  if (error)
    {
      fprintf(output_fp,"\nERROR: Non-numeric supplied for gulp time limit\n");
    }

  return;
}

int get_peek_freq(void)
{
#include "header.h"
int error;

  peek_freq = get_integer(&error, FALSE);
  return(error);
}

int get_keep(void)
{
#include "header.h"
int error;

  keep = get_integer(&error, FALSE);
  return(error);
}

void get_gulp_file(void)
{
#include "header.h"

	strcpy(gulpfile, get_string());
	return;

}
