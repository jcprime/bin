#!/bin/bash
# energy_extract.sh
# To be called using, e.g. for num in {01..16}; do cd "$num"; for dir in nat*; do if [ -d "$dir" ]; then energy_extract.sh "$dir"/"$dir".log "${dir%%_*}".ssv; fi; done; cd ..; done

function print_usage() {
    echo "Usage: energy_extract.sh <log_file> [pdb_file] <output_file>"
}

# If "-h" or "--help" is given when running the script as only argument, print usage information
if [[ ( "$#" -le "1" ) || ( ( "$1" == "-h" ) || ( "$1" == "--help" ) ) ]]; then
    print_usage && exit
elif [[ "$#" -eq "3" ]]; then
    log_file="$1"
    pdb_file="$2"
    output_file="$3"
elif [[ "$#" -eq "2" ]]; then
    log_file="$1"
    output_file="$2"
else
    # Just in case!
    print_usage && exit
fi

inpath="$(realpath "$log_file")"
outpath="$(realpath "$output_file")"

# echo "Input path: $inpath"
# echo "Output path: $outpath"

# Append the filename and the last instance of the matched regex in specific format for ease of reading to the given output (.ssv) file
awk '/ENERGY\| Total/ {ene=$9} END{printf "%-30s%25.15f\n", FILENAME, ene}' "$log_file" >> "$output_file"

cat "$output_file" | sort -n -k2 | uniq > "${output_file%%.ssv}.tmp" && cp ${output_file%%.ssv}.tmp "$output_file" && rm "${output_file%%.ssv}.tmp"
