# For loops

# Using a list (with no commas or quotes) and "in"
for varname in list_item1 list_item2 list_item3 etc # OR predefined_list="list_item1 list_item2 list_item3 etc"; for varname in $predefined_list
do
 command1
 command2
 ..
done

#Or a range of numbers and "in"
for num in {1..10}
do
 echo "Number: $num"
done

# Ditto above, with the third part in the range specifying the increment (2 in this case)
for num in {1..10..2}
do
 echo "Number: $num"
done

# Using C-like syntax
for (( expr1; expr2; expr3 ))
do
 command1
 command2
 ..
done

# A more complicated example of the C-like for loop in bash
for ((i=1, j=10; i <= 5 ; i++, j=j+5))
do
 echo "Number $i: $j"
done

# This example uses backticks to take the output of the command enclosed in them as a list of values to be passed to the loop
i=1
for username in `awk -F: '{print $1}' /etc/passwd`
do
 echo "Username $((i++)) : $username"
done

# This example uses the files in the directory where the for loop is being invoked as the actual list items
i=1
cd ~
for item in *
do
 echo "Item $((i++)) : $item"
done

# This example uses regular expressions to print a more filtered list of files in the directory being looped in (all files beginning with a, b, c, or d and ending in .config) 
i=1
for file in /etc/[abcd]*.conf
do
 echo "File $((i++)) : $file"
done

# The "break;" command stops further iterations of the for loop
# The "continue;" command skips the current iteration but doesn't stop any further iterations

# Ctrl+C stops eg. an infinite loop
