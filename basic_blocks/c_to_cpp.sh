#!/bin/bash
# c_to_cpp.sh
# Takes C command as input and outputs equivalent C++ command

argc="$#"
if [[ "$argc" -eq "0" ]]; then
	echo "Usage: ./c_to_cpp.sh <C function>"
	echo "Output: <equivalent C++ function>"
elif [[ "$argc" -gt "1" ]]; then
	echo "Usage: ./c_to_cpp.sh <C function>"
	echo "Output: <equivalent C++ function>"
else
	c="$1"
	case $c in
		strstr)
			cpp="std::string::find"
			;;
		sscanf)
			cpp="std::istream::getline"
			;;
		scanf)
			cpp="std::getline"
			;;
		strcpy)
			cpp="std::string::copy"
			;;
		fopen)
			cpp=
			;;
		fgets)
			cpp=
			;;
		sprintf)
			cpp=
			;;
		fprintf)
			cpp=
			;;
		strcat)
			cpp=
			;;
	esac
fi