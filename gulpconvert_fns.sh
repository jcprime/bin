#!/bin/bash
# gulpconvert_functions.sh

# Function to check for and make directories as required
function check_directories() {
    # Assign path
    if [[ "$#" -gt "0" ]]; then
        path="$1"
    else
        path="$HOME/test/coords"
    fi
    # Make directory if not already there, and if there, remove all stuffs within... unless you're given / or /home/$USER as path!
    if [ ! -d "${path}" ]; then
        mkdir -p "${path}"
    elif [ "${path}" == "/" ] || [ "${path}" == "$HOME" ]; then
        echo "Nope, not removing root directory or user's home directory; How Very Dare You even suggest it!"
        exit 1
    elif [ -d "${path}" ] && [ "${path}" != "/" ] && [ "${path}" != "$HOME" ]; then
        rm -rf "${path:?}"/*
    fi
}

# Function to sort out Python-ing into Cartesian coordinates
function coordinate_faff() {
    file="$1"

    # Grab cell parameters into a new variable
    cell=$(grep -A1 'cell' "${file}" | tail -n 1 | tr -s ' ')
    echo "Cell parameters: ${cell}"

    #fractional_start=$() # Number of line beginning with "fractional"
    #fractional_end=$() # Number of line ending "fractional" section

    # Create temporary file
    temp_file="${base}.tmp"
    touch "${temp_file}"

    #sed "/REGEXP/,/REGEXP2/" selects lines matching REGEXP and all following lines up to and including the line matching REGEXP2
    # THIS IS SPECIFIC TO UNIX/LINUX SYSTEMS, AND WON'T WORK ON MAC OS X:
    # Output everything including "fractional" and "totalenergy" OR "species" (whichever comes first) to a temporary file for processing in the while loop to follow
    sed -n "/^fractional/,/^\(totalenergy\|species\|pressure\)/p" "${file}" >> "${temp_file}"

    # Remove first and last lines using sed and redirect into fractional file (without -n option here, obviously!)
    #sed '1d;$d' "${temp_file}"

    # Redirect stdin to the temporary file created above
    #exec <"${temp_file}"

    # Create fractional file for use with coordinateTransform.py (from https://github.com/ghevcoul/coordinateTransform/blob/master/coordinateTransform.py)
    frac_file="${base}.frac"
    if [ ! -e "${frac_file}" ]; then
        touch "${frac_file}"
    elif [ -e "${frac_file}" ]; then
        > "${frac_file}"
    fi

    # Grab coordinate lines for CORES ONLY and process them with awk, assuming the form below:
    # atom_symbol <species_type> x y z <charge> <occupancy> <radius> <3 x flags>
    # OR: at no. x y z <charge> <occupancy> <radius> <3 x optimisation flags>
    awk ' { if ($1 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } else if ($1 ~ /^[A-Za-z].*/ && $2 ~ "core") { print $1, $3, $4, $5; next } } ' "${temp_file}" >> "${frac_file}"
    #num_atoms=$(wc -l "${temp_file}")

    # { echo "Number of atoms: ${num_atoms}"; echo; echo "Lattice vectors:"; echo; printf "Fractional coordinates:\n"; cat "${temp_file}"; } >> "${frac_file}"
}

# Function to replace the coordinate file placeholder in the CP2K input file with the actual filename of the newly-created coordinate file
function edit_input() {
    input_file="$1"
    coordinate_file="$2"
    # Edit using sed in-place
    sed -i "s/coord\_placeholder/$coordinate_file/" "${input_file}"
}

#------------------------------------------------------------------------------
# USAGE
#------------------------------------------------------------------------------

check_directories "$path"

# Sorts out all the multi-temp files and sed-ing and awk-ing and all that crap
coordinate_faff "${file}"
