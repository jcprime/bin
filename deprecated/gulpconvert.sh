#!/bin/bash
# gulpconvert.sh
#  - Takes cell parameters and fractional coordinates from *SERIES* of GULP input files
#  - Reformats them as XYZ-type coordinates
#  - Pastes the cell parameters in the appropriate section of the current input file

# OLD VERSION; NOT CURRENTLY WORKING PROPERLY (SEE TODO)

# Function to check for and make directories as required
function check_directories() {
    # Assign path
    if [[ "$#" -gt "0" ]]; then
        path="$1"
    else
        path="$HOME/test/coords"
    fi
    # Make directory if not already there, and if there, remove all stuffs within... unless you're given / or /home/$USER as path!
    if [ ! -d "${path}" ]; then
        mkdir -p "${path}"
    elif [ "${path}" == "/" ] || [ "${path}" == "$HOME" ]; then
        echo "Nope, not removing root directory or user's home directory; How Very Dare You even suggest it!"
        exit 1
    elif [ -d "${path}" ] && [ "${path}" != "/" ] && [ "${path}" != "$HOME" ]; then
        rm -rf "${path:?}"/*
    fi
}

# Function to sort out Python-ing into Cartesian coordinates
function coordinate_faff() {
    file="$1"

    # Grab cell parameters into a new variable
    cell=$(grep -A1 'cell' "${file}" | tail -n 1 | tr -s ' ')
    echo "Cell parameters: ${cell}"

    #fractional_start=$() # Number of line beginning with "fractional"
    #fractional_end=$() # Number of line ending "fractional" section

    # Create temporary file
    temp_file="${base}.tmp"
    touch "${temp_file}"

    #sed "/REGEXP/,/REGEXP2/" selects lines matching REGEXP and all following lines up to and including the line matching REGEXP2
    # THIS IS SPECIFIC TO UNIX/LINUX SYSTEMS, AND WON'T WORK ON MAC OS X:
    # Output everything including "fractional" and "totalenergy" OR "species" (whichever comes first) to a temporary file for processing in the while loop to follow
    sed -n "/^fractional/,/^\(totalenergy\|species\|pressure\)/p" "${file}" >> "${temp_file}"

    # Remove first and last lines using sed and redirect into fractional file (without -n option here, obviously!)
    #sed '1d;$d' "${temp_file}"

    # Redirect stdin to the temporary file created above
    #exec <"${temp_file}"

    # Create fractional file for use with coordinateTransform.py (from https://github.com/ghevcoul/coordinateTransform/blob/master/coordinateTransform.py)
    frac_file="${base}.frac"
    if [ ! -e "${frac_file}" ]; then
        touch "${frac_file}"
    elif [ -e "${frac_file}" ]; then
        > "${frac_file}"
    fi

    # Grab coordinate lines for CORES ONLY and process them with awk, assuming the form below:
    # atom_symbol <species_type> x y z <charge> <occupancy> <radius> <3 x flags>
    # OR: at no. x y z <charge> <occupancy> <radius> <3 x optimisation flags>
    awk ' { if ($1 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } else if ($1 ~ /^[A-Za-z].*/ && $2 ~ "core") { print $1, $3, $4, $5; next } } ' "${temp_file}" >> "${frac_file}"
    #num_atoms=$(wc -l "${temp_file}")

    # { echo "Number of atoms: ${num_atoms}"; echo; echo "Lattice vectors:"; echo; printf "Fractional coordinates:\n"; cat "${temp_file}"; } >> "${frac_file}"
}

# Function to replace the coordinate file placeholder in the CP2K input file with the actual filename of the newly-created coordinate file
function edit_input() {
    input_file="$1"
    coordinate_file="$2"
    # Edit using sed in-place
    sed -i "s/coord\_placeholder/$coordinate_file/" "${input_file}"
}

#--------------------------------------------------------------------------------------------------
# ACTUAL START OF SCRIPT
#--------------------------------------------------------------------------------------------------

# If "-h" or "--help" is given when running the script as only argument, print usage information
if [[ ( "$#" -eq "1" ) && ( ( "$1" == "-h" ) || ( "$1" == "--help" ) ) ]]; then
    echo "Usage: gulp_convert.sh [coordinate_file] [input_file]"
fi

# If any arguments are given, take them as the list of zeolites for looping over later
if [[ "$#" -gt "0" ]]; then
    declare -a zeolites=("$@")
else
    declare -a zeolites=("nat" "sco" "mes" "lau")
fi

# Keep count of the number of coordinate files you're writing out!
coord_number="0"
# Loop over zeolites
for zeo in "${zeolites[@]}"; do
    path="$HOME/test/coords/"
    gin_dir="$HOME/test/${zeo}"
    #template_dir="$HOME/data/template"
    check_directories "$path"
    # Set working environment
    work_dir="${path}"
    cd "${work_dir}" || continue
    # Set coord_total to the total number of .gin files in all directories for the given zeolite (i.e. how many CP2K coordinate files you want to have overall by the end)
    coord_total=$(find -E ${gin_dir}/[0-9]* -name *.gin | wc -l)
    # Loop over existing GULP coordinates (in .gin files)
    for file in ${gin_dir}/[0-9]*/*.gin; do
        base="$(basename "${file}" .gin)"
        mkdir -p "${work_dir}/${base}"
        cd "${work_dir}/${base}" || continue
        cp "${file}" "./${base}.gin"
# TODO: ARGUMENTS AS ABOVE ARE SUPPOSED TO BE LIST OF ZEOLITES, BUT NOW WE'RE TALKING ABOUT FIRST AND SECOND ARGUMENTS AS SEPARATE THINGS (see xenon:~/bin/testagain.sh for working version that doesn't loop)
        # Set output-coordinate file to first argument, if given, or else taken as same basename as original .gin file
        if [[ "$#" -ge "1" ]]; then
            coordinate_file="$1"
        elif [[ "$#" -eq "0" ]]; then
            coordinate_file="${base}.cp2k"
        fi
        echo > "${coordinate_file}"
        # Set cp2k-input file to second argument, if given, or else taken as same basename as original .gin file
        if [[ "$#" -ge "1" ]]; then
            input_file="$2"
        elif [[ "$#" -eq "0" ]]; then
            input_file="${base}.inp"
        fi

        # Sorts out all the multi-temp files and sed-ing and awk-ing and all that crap
        coordinate_faff "${file}"

        # Convert from .frac coordinate file to .cart coordinate file
        python2.7 ~/bin/gulp_coords.py "${cell}" "${frac_file}"
        cart_file="${base}.cart"

        # Copy lines with format * number number number (on the assumption that that's all there'll be) into "$coordinate_file"
        awk ' { if ($2 ~ /^[0-9].*/ && $3 ~ /^[0-9].*/ && $4 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } } ' "${cart_file}" > "${coordinate_file}"
        cp "${cart_file}" "${path}"/

        # Copy input file from template directory to the current working directory (${work_dir}/${base})
        #cp "${template_dir}/cp2k/test_input.inp" "${work_dir}/${base}/${input_file}"

        #edit_input "$input_file" "$coordinate_file"
        #------------------------------------------
        # Increment number of coordinate file
        coord_number=$((coord_number+1))
    done
    # Check that coord_number == coord_total
    if [[ "${coord_number}" -eq "${coord_total}" ]]; then
        echo "All done!"
    elif [[ "${coord_number}" -gt "${coord_total}" ]]; then
        echo "Somehow you've ended up with more files than you should have.  Try again."
        exit
    else
        echo "There are still some files missing.  Re-run, please."
        exit
    fi
done
