/****writes a gulp file ****/
#include <stdio.h>
#include "structures.h"
#include "header.h"
void save_gulpfile(FILE *file_fp, char *p_dump_name, atom *p_mol, 
                   int num_atoms, char *p_spacegroup,
                   char *p_xtl_name, char *p_pots_file)
{
char append_pots_command[256];
int i, iloop;
atom *p_atom;



             fprintf(file_fp,"%s\n",gulp_command_line);
             fprintf(file_fp,"#user supplied max cycles\n");
             fprintf(file_fp,"maxcyc %d\n", gulp_max_cycles);
             fprintf(file_fp,"#user supplied time limit\n");
             fprintf(file_fp,"time %f\n", gulp_time_limit);
             if (strstr(p_xtl_name, "NONE") ==0)
               {
                fprintf(file_fp,"output xtl %s\n", p_xtl_name);
               }

             if (strstr(p_dump_name, "NONE") ==0)
               {
                fprintf(file_fp,"dump every 1 %s\n", p_dump_name);
               }

             fprintf(file_fp,"#unit cell from %s file\n", coordinate_file);
             fprintf(file_fp,"#with substitutions made\n");
             fprintf(file_fp, "cell\n");
             for (iloop = 0; iloop < 6; iloop++)
              {
                fprintf(file_fp, "%8.5f ",abc[iloop]);
              }
             /*u.c. fixed unless flagged with conp*/
             fprintf(file_fp, "%s",cell_volume_flags);
             fprintf(file_fp, "space \n%s\n", p_spacegroup);
             fprintf(file_fp, "fractional\n");
             for (i=0;i<=num_atoms;i++)
                     {
                     /**substituted u c**/
                     p_atom = p_mol+i;
                     fprintf(file_fp,"%s %s %lf %lf %lf %lf %d %d %d\n",
                            p_atom->label,
                            p_atom->type,
                            p_atom->x, p_atom->y, p_atom->z,
                            p_atom->charge,
                            p_atom->flag_x, p_atom->flag_y,
                            p_atom->flag_z);
                     }
             /***now print out cations is done ****/
             fprintf(file_fp,"#potentials and options from %s file\n", 
                       p_pots_file);
             fprintf(file_fp,"#append potentials\n");
             fclose(file_fp);
             /***append pot file****/
return;
}
