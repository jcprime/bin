#!/bin/bash
# setup.sh
# Sets up directories for GULP-conversions

path="/home/james/quickstep/sco"
gulp_path="/home/james/data/gulp/sco"
template_file="/home/james/quickstep/template/template.inp"

# Make directories (per number of waters lost) and subdirectories (per file to be tested)
for num in {00..24}; do
    # If corresponding numbered directory does not exist in gulp_path, skip to the next iteration
    if [[ ! -d "${gulp_path}/$num" ]]; then
        #rmdir "$path/$num"
        continue
    fi
    # Make directory for number of waters lost
    if [[ ! -d "$path/$(printf '%02d' "$((10#$num))")" ]]; then
        mkdir -p "$path/$(printf '%02d' "$((10#$num))")"
    fi
    for file in "${gulp_path}/$num/"*.gin; do
        # Make directory for given .gin file (in gulp_path/num/ directory) in path/num/ directory
        gulp_file="${file##*/}" && gulp_file="${gulp_file%%.*}"
        #echo "${gulp_file}"
        if [[ ! -d "$path/"$num/"${gulp_file}" ]]; then
            mkdir -p "$path/$num/${gulp_file}"
        fi
        # Copy original .gin file from original location into newly-created directory
        cp "$file" "$path/$num/${gulp_file}/${gulp_file}.gin"
        # From cutoff_tests/cutoff_inputs.sh
#        sed -e "s/LT_rel_cutoff/${rel_cutoff}/g" \
#            -e "s/LT_cutoff/${ii}/g" \
#            "${template_file}" > "${path}/${num}/${gulp_file}/${gulp_file}.inp"
    done
done
