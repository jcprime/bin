#!/bin/bash
# highenergy.sh

# The script to run after you've run dehydrate and your files have been run through GULP
#   * Takes existing files, grabs important information (zeolite name, dehydration step, etc)
#   * Calculates the next (padded) dehydration step (i.e. output filenames to expect)
#   * Checks existing output files for failures, errors, and unconverged structures
#   * Checks count of actual input and dump files against expected file count to make sure
#     none are missing (i.e. avoids silent failures!)
#   * Prints details of fail/error-checking to console
#   * Shuffles files as required and grabs important information:
#       * Lists output files, sorted by energy, to console, and output to a file in ene/ directory
#       * Grabs lowest-energy/enthalpy output file and associated generic bit pre-extension
#       * Copies lowest-energy/enthalpy dumpfile to next-step-numbered input file
#       * Copies lowest-energy/enthalpy stuffs (all files with the pre-extension bit in common)
#         into gs/ directory
#       * Moves everything from that step into the relevant (step-numbered) directory
#   * Calls ~/bin/dehydrate on next-step-numbered input file
#   * Submits the newly-dehydrated next-step-numbered input files for running via ~/bin/qgulp

# Move into directory specified in $path (first argument or assumed current working directory)
if [[ "$#" -eq "1" ]]; then
    path="$(dirname "$1")/$(basename "$1")"
elif [[ "$#" -eq "0" ]]; then
    path="$(pwd)"
fi
cd "$path" || exit

# Looks at listed files in current directory and takes the word-only common part of the filename(s) as a variable
zeo_step=$(find "$path" *.gout -maxdepth 0 -type f | cut -d'_' -f1 | uniq)
echo "Common part of filenames: $zeo_step"
# Gives you the zeolite name (everything before the first number) e.g. "scol"
zeolite="${zeo_step%%[0-9]*}"
echo "Zeolite abbreviation: $zeolite"
#step_full=$(echo "$zeo_step" | awk '{print $2}') # gives you e.g. "scol03" and NOW SUPERCEDED BY ZEO_STEP
# Gives you the dehydration step (the bit immediately following the zeolite name) e.g. "03"
step_number="${zeo_step##[A-Za-z]*[^0-9]}"
echo "Dehydration step: $step_number"

# DEPRECATED: ASSUMED THE ABOVE WAS IN FUNCTION, AND NOW IT ISN'T
# Resets step_number to the re-padded version of the first argument, if given
# if [[ "$#" -eq "1" ]]; then
#     step_number="$(printf '%02d' "$1")"
# fi

# Obtain step_new_padded (next stage of dehydration - ie. number of water molecules missing in the next step)
# Increments the step number, producing the un-padded new step number for which files now need to be created (the 10# specifies decimal, or else the leading zero in 08 and 09 will be taken as octal)
step_new=$((10#$step_number+1))
echo "The next step for simulating: $step_new"
# step_new_padded = step_new with padding, so you get the same number of digits for ease of sorting!
step_new_padded=$(printf '%02d' "$step_new")
echo "And the padded equivalent: ${step_new_padded}"

fail_check=$(grep -n 'fail' ./*out)
error_check=$(grep -n 'ERROR' ./*out)
min_check=$(grep 'Conditions for a minimum have not been satisfied' ./*out)

# Counts number of .gin files there are in the current directory (using word count for characters, and printing a single character per instance of a new file found)
ginfile_list=$(find . -maxdepth 1 -type f -name "$zeolite$step_number\_N*.gin")
ginfile_count=$(find . -maxdepth 1 -type f -name "$zeolite$step_number\_N*.gin" -exec printf x \; | wc -c)
dumpfile_list=$(find . -maxdepth 1 -type f -name "$zeolite$step_number\_N*.dump")
dumpfile_count=$(find . -maxdepth 1 -type f -name "$zeolite$step_number\_N*.dump" -exec printf x \; | wc -c)

# Check for lines in output files indicating failures, errors, or unconverged structures
if [[ "$fail_check" == "" ]]; then
    echo "No obvious failures so far!"
else
    echo
    echo "We've got a few failures to sort out, sorry!  Here they come:"
    echo "$fail_check"
    echo "Erm, we'll just be over here in the corner while you sort 'em out, okay?"
    echo
    exit 1
fi

if [[ "$error_check" == "" ]]; then
    echo "No obvious errors so far!"
else
    echo
    echo "We've got a few errors to sort out, sorry!  Here they come:"
    echo "$error_check"
    echo "Erm, we'll just be over here in the corner while you sort 'em out, okay?"
    echo
    exit 2
fi

if [[ "$min_check" == "" ]]; then
    echo "No obvious minimisation troubles so far!"
else
    echo
    echo "We've got a few minimisation troubles to sort out, sorry!  Here they come:"
    echo "$min_check"
    echo "Erm, we'll just be over here in the corner while you sort 'em out, okay?"
    echo
    exit 3
fi

if [[ "$dumpfile_count" == "$ginfile_count" ]]; then
    echo "We've got all the dumpfiles we're expecting!"
else
    echo
    echo "We've not got the expected number of dumpfiles, sorry!"
    echo "Input files: $ginfile_count"
    echo "=> $ginfile_list"
    echo "Dumpfiles: $dumpfile_count"
    echo "=> $dumpfile_list"
    echo "Erm, we'll just be over here in the corner while you sort 'em out, okay?"
    echo
    exit 4
fi

if [[ "$fail_check" == "" && "$error_check" == "" && "$min_check" == "" && "$dumpfile_count" == "$ginfile_count" ]]; then
    echo
    echo "Looks like no obvious errors, and all your dumpfiles have appeared as expected, so huzzah!"
    echo
    # Print the output energies/enthalpies to terminal and output them to an appropriately-named file in the ene/ directory
    grep 'Final en' *out | sort -nr -k5
    grep 'Final en' *out | sort -nr -k5 > ene/"$zeo_step"_ene.tsv
    # Find lowest energy structure for the last dehydration step (where -k5 means sort by the value in the 5th column, with whitespace as the default delimiter, and where n is numerical sort)
    # Gives e.g. scol03_N18.gout
    top_gout=$(head -n1 ene/"$zeo_step"_ene.tsv | cut -d':' -f1)
    # Gives e.g. scol03_N18
    top_generic="${top_gout%.*}"
    echo "The lowest-energy configuration for dehydration step $step_number is $top_generic." # Without extension, for submitting to dehydrate!
    echo
    echo "Copying $top_generic.dump to $zeolite${step_new_padded}.gin"
    cp "$top_generic.dump" "$zeolite${step_new_padded}.gin"
    echo
    echo "Checking ground-state directory exists"
    if [[ ! -d "gs" ]]; then
      echo "... and no such luck, so creating one now!"
      mkdir gs
    elif [[ -d "gs" ]]; then
      echo "... and yep, there it is!"
    fi
    echo
    echo "Copying all $top_generic files into ground-state directory"
    cp "$top_generic".* gs/
    echo
    echo "Checking $step_number directory exists"
    if [[ ! -d "$step_number" ]]; then
      echo "... and no such luck, so creating one now!"
      mkdir "$step_number"
    elif [[ -d "$step_number" ]]; then
      echo "... and yep, there it is!"
    fi
    echo
    echo "Moving all $zeo_step files into $step_number directory"
    mv "$zeo_step"* "$step_number"

    # Check coordinate lines for fractional values, and replace them with decimals
    #awk ' { if (($2 ~ /core/ || $2 ~ /shel/) && $NF >= 5 && ) { print $1, $2, $3, $4; next } else { print $0; next } } ' "$zeolite${step_new_padded}".gin >> "$zeolite${step_new_padded}".tmp && cp "$zeolite${step_new_padded}".tmp "$zeolite${step_new_padded}".gin

    # printf "%.7f", "value"
    # 1/12  = 0.0833333
    # 1/9   = 0.1111111
    # 1/6   = 0.1666667
    # 2/9   = 0.2222222
    # 1/3   = 0.3333333
    # 5/12  = 0.4166667
    # 4/9   = 0.4444444
    # 5/9   = 0.5555556
    # 7/12  = 0.5833333
    # 2/3   = 0.6666667
    # 7/9   = 0.7777778
    # 5/6   = 0.8333333
    # 8/9   = 0.8888889
    # 11/12 = 0.9166667

    # Generate the next set of test configurations by calling dehydrate
    echo
    echo "Calling dehydrate for $zeolite${step_new_padded}"
    dehydrate "$zeolite${step_new_padded}"

    # TESTING TESTING TESTING STILL
    # Check how many files there are starting with "$zeolite${step_new_padded}\_N"
    # file_number=$(find . -maxdepth 1 -type f -name "$zeolite${step_new_padded}\_N*.gin" -exec printf x \; | wc -c)
    echo
    echo "Submitting the newly-dehydrated .gin files to The Queue"
    for i in $zeolite${step_new_padded}_N*.gin; do
        qgulp "$i"
    done
    # i="0"
    # while [[ "$i" -le "$file_number" ]]; do
    #     qgulp "$zeolite${step_new_padded}\_N$i".gin
    #     i=$((i+1))
    # done

    # Strip out ${step_new_padded} from start of $top and replace it with ${step_new_padded+1}
    # next="${top_generic/${step_number#0}/${step_new_padded}}"
    # Copy dumpfile for the lowest-energy structure to the equivalent next-step incremented input file
    # cp "$top_generic".dump "$next".gin
fi

# RECURSION EXTENSION
# Run dehydration_check once, with intention to make recursive and include waiting for checks that the runs have finished at each stage!
# while [[ "${step_number#0}" -ge "0" ]]; do
#     dehydration_check "$step_number"
# done

# Check all .gout files have actually achieved optimisation where they're supposed to at the end!
# for file in *.gout; do
#     opti_check=$(grep 'achi' "$file")
#     if [[ "$opti_check" != "" ]]; then
#         pass
#     fi
# done
