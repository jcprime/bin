/****************************************************************/
/* mc_subs:  utility to generate subsituted silica polymorphs   */
/*           for running in  gulp                               */
/* dwl 05/02/98                                                 */
/****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "maxima.h"
#include "global_values.h"
#include "structures.h"

#define MAIN 0
#include "header.h"
#include "data.h"
#include "ewald.h"
#include "own_maths.h"
#undef MAIN

#define TRUE        1
#define FALSE       0

#define YES "Y"
#define NO "N"
#define GULP_ACHIEVED "optimisation achieved"
#define FINAL_STRUCTURE_START "Final asymmetric unit coordinates"
#define NO_TITLE "NO TITLE"
#define UNITS_EV "eV"
#define UNITS_KJ "kJ/(mole unit cells)"
#define KJ_2_EV 96.483

int reader(char *file_to_read);
double atom_separation_squared(atom *p_a, atom *p_b, int pbc);
simple_atom h_pos(simple_atom *si1, simple_atom *si2, simple_atom *o);
simple_atom X_pos(simple_atom *si1, simple_atom *si2, simple_atom *o);
void save_config(FILE *file_fp, int config, int num_replace_atoms, 
                 double final_energy, double final_gnorm,
                 char *p_gulp_achieved_flag,
                 int *p_replaceable_index,int *p_replaceable_done, atom *whereisthecation); 

void generate_neighbours( atom *p_molecule, int num_atoms,
                          atom_number *p_types, int *p_num_types,
                          int use_pbc);
void print_neighbours( atom *p_molecule, int num_atoms, FILE *fp);

void print_molecule(atom *p_molecule, int num_atoms, FILE *output_fp);

void set_elem(atom *p_molecule, int num_atoms);
void steric_setup(atom *p_molecule, int num_atoms);
double real_random(int done);
void index_sort(int n, double *p_arrin  ,int *p_indx);
void save_xtlfile(FILE *file_fp, atom *p_mol, 
                   int num_atoms, char *p_spacegroup);
                   
void save_gulpfile(FILE *file_fp, char *p_dump_name, atom *p_mol, 
                   int num_atoms, char *p_spacegroup,
                   char *p_xtlname, char *p_pots_file);

void main(argc, argv)
	int   argc;
	char  **argv;
{
        int clash;
        int found_shell, done_this,got_one;
        int first_shell;
        double core_shell_sep;
        double rdummy;
        double actual_dist;
	int i, j;
	int iloop;
	int temp_counter;
        int ivec;
        int max_trials, this_trial;
        int  subs_done;
	int num_atoms;
        int lowenstein_break;
	int new_num_atoms;
        int num_core_atoms;
        int this_neigh, that_neigh;
	int ineigh;
	int jneigh;
	int num_mol_types;
	char dummy[BUFFER+1];
	char dummy2[BUFFER+1];
	char buffer[BUFFER+1];
	char marker[BUFFER+1];
	char sdum[81];
	double fdum;
	int rank;
	int bingo;
	atom mol[MAXATOMS], whereisthecation[MAXATOMS];
	atom orig_mol[MAXATOMS];
        simple_atom cation_pos, O1, O2, T1, T2, bridgeO;
	int replaceable_index[MAXATOMS];
	int replaceable_done[MAXATOMS];
	atom_number mol_types[10];
        int random_seed;
	FILE *infp;
	FILE *reopen_fp;
	FILE *analyse_fp;
	FILE *this_analyse_fp;
	FILE *gulpfp;
	FILE *dumpfp;
	FILE *script_fp;
	FILE *gulpoutputfp;
	FILE *coordinate_fp;
	FILE *config_fp;
	char *fullstop;
	char gulpfile_root[81];
	char analyse_filename[256];
	char this_analyse_filename[256];
	char script_name[81];
	char dumpfile[81];
	char gulpdumpfile[81];
	char gulpxtlfile[81];
	char config_file[81];
	char pot_file[81];
        char spacegroup[20];
        char append_pots_command[256];
        char general_command[256];
        int final_energy_flag;
        int single_flag;
        int initial_energy_flag;
        int replace_this;
        int swap;
        int got_it;
        int found_replacable;
        int num_replace_atoms;
        int in_list;
        int hot_list_index[100];
        int hot_list_sites[100][30];
        double hot_list_energy[100];
        double final_energy;
        double final_gnorm;
	char this_label[4];
	int num_si, num_h, num_oh, num_o, num_other;
        double a,b,c, alpha, beta, gamma;
        atom temp_atom;
        int num_cation_site;
        atom cation_site[MAXATOMS];
        int cation_replaced[MAXATOMS];
        char gulp_achieved_flag[3];
        char gulp_max_iter_reached_flag[3];
        int cycles_done;
	int eof_config;

        time_t start_time, time_now;
	
    if (argc != 2) 
		{
		fprintf(stderr, "Usage: %s  file\n", argv[0]);
          	exit(1);
    	        }
	else
		{
		if ((infp = fopen(argv[1], "r")) == NULL) 
			{
			fprintf(stderr, "Unable to open file %s for input.\n", argv[1]);
			exit(1);
			}
		}
	

       strcpy(output_file, argv[1]);
       strcpy(config_file, argv[1]);
       strcat(output_file, OUTPUT_EXTENSION);
       strcat(config_file, CONFIG_EXTENSION);
        if ((output_fp = fopen(output_file, "w")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for output.\n", 
                            output_file);
            exit(1);
            }


        /*defaults and error catchers*/
        max_subs = -1;
        ratio = -1;
        /****by default do an insert cation job**/
        cation_insert_run = TRUE;
        fixed_cation_cutoff = FIXED_CATION_CUTOFF_DEFAULT;
        strcpy(title,NO_TITLE);
        strcpy(cell_volume_flags, CELL_VOLUME_FLAGS_DEFAULT);
        gulp_max_cycles = 100;
        gulp_time_limit = 6000;
        generate_only = FALSE;
        regenerate_run = FALSE;
        configurations_flag = FALSE;
        script_type = SCRIPT_NONE_TYPE;
        analyse_type  = ANALYSE_TYPE_NONE;

        strcpy(gulp_path, RUN_GULP_COMMAND);

        start_time= time(&start_time);
        /* read in command from file */
        reader(argv[1]);
        /* read in commands from file argv[1] */
        /* print out input paramaters */
        fprintf(output_fp,"************************************************\n");
        fprintf(output_fp,"* MC_subs                                      *\n");
        fprintf(output_fp,"*        DWL 2000-2004                         *\n");
        fprintf(output_fp,"************************************************\n");
        fprintf(output_fp,"Job started : %i\n\n",start_time);
        fprintf(output_fp,"\nInput file %s read....\n\n",argv[1]);
        fprintf(output_fp,"Run type: ");
        if (generate_only == TRUE) 
          fprintf(output_fp,"Generate - output to directory \"%s\"\n",
                             generate_path);
        else if (regenerate_run == TRUE) 
         {
          fprintf(output_fp,"ReGenerate - Output to directory :\"%s\"\n",
                             regenerate_path);
          fprintf(output_fp,"             Configuration file  : %s\n",
                             config_file);
         }
        else if ((cation_insert_run == TRUE) && (analyse_type == ANALYSE_TYPE_NONE))
          fprintf(output_fp,"Cation Insertion Run\n");
        else if (analyse_type != ANALYSE_TYPE_NONE)
	  {
	   if (analyse_type == ANALYSE_TYPE_FULL)  fprintf(output_fp,"Full Analysis - ");
           else if (analyse_type == ANALYSE_TYPE_SIMPLE)  fprintf(output_fp,"Simple Analysis - ");
           else  fprintf(output_fp,"Analysis - Unknown Type!\n");
            fprintf(output_fp," of directory \"%s\"\n",analyse_path);
	  }
        else /*fixed actions, just change Al???*/
          fprintf(output_fp,"Fixed cations - Substitute Framework only\n");

        if (strstr(title,NO_TITLE)==NULL)
            fprintf(output_fp,"Title:   \n%s\n", title);
        if (script_type != SCRIPT_NONE_TYPE)
           {
            fprintf(output_fp,"Output Script Type: ");
            if (script_type == SCRIPT_RUN_TYPE)
               fprintf(output_fp,"Run\n");
            else if (script_type == SCRIPT_QSUB_TYPE)
               fprintf(output_fp,"Qsub\n");
            else 
               fprintf(output_fp,"Unknown!\n");
           }

        fprintf(output_fp,"\nInput Parameters:\n");
        fprintf(output_fp,"Unit Cell     : %s\n", coordinate_file);
        fprintf(output_fp,"Potentials    : %s\n", pots_file);
        fprintf(output_fp,"\n");
        fprintf(output_fp,"Cycles        : %i\n", max_cycles);
        fprintf(output_fp,"\n");
        fprintf(output_fp,"Replace       : %s with charge %f\n",
                           replace_ele, replace_charge);
        fprintf(output_fp,"(with) Insert : %s with charge %f\n", 
                           insert_ele, insert_charge);
        fprintf(output_fp,"Cation        : %s with charge %f\n", 
                           cation, cation_charge);
        if (max_subs > 0) 
          fprintf(output_fp,"Substitute    : %d\n %s\n", max_subs,replace_ele);
        else 
          fprintf(output_fp,"%s/%s ratio   : %f\n", 
                           replace_ele,insert_ele,ratio);
        fprintf(output_fp,"\n");
        fprintf(output_fp,"Peek every    : %i\n", peek_freq);
        fprintf(output_fp,"Keep          : %i configurations\n", keep);
        fprintf(output_fp,"\n");
        fprintf(output_fp,"GULP params   : \n");
        fprintf(output_fp,"Executable    : %s\n", gulp_path);
        fprintf(output_fp,"Filename      : %s\n", gulpfile);
        fprintf(output_fp,"Cell flags    : %s\n", cell_volume_flags);
        fprintf(output_fp,"Max Cycles    : %i\n", gulp_max_cycles);
        fprintf(output_fp,"Max Time      : %f\n", gulp_time_limit);
        fprintf(output_fp,"Command line  : %s\n", gulp_command_line);
        /* fprintf(output_fp,"RANDOM: %d\n", randomisation);*/

	if (regenerate_run && !configurations_flag)
		{
		fprintf(output_fp,
		"ERROR! Regenerate Run requested but no Condifurations in file\n");
		exit(1);
		}

	if (regenerate_run == FALSE)
	  {
          if ((config_fp = fopen(config_file, "w")) == NULL)
              {
              fprintf(stderr, "Unable to open file %s for configurations\n",
                              config_file);
              exit(1);
              }
	  }

	/****** read in unit cell file ******/
        if ((coordinate_fp = fopen(coordinate_file, "r")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for input.\n", 
                             coordinate_file);
            exit(1);
            }



	/** expecting cell line, followed by space line followed by frac **/

	fgets(dummy,BUFFER,coordinate_fp);
	if (strstr(dummy, CELL) == NULL)
		{
		fprintf(output_fp, "Expecting cell paramters. Exiting...\n");
		exit(0);
		}
    	fgets(dummy,BUFFER,coordinate_fp);
	sscanf(dummy,"%lf%lf%lf%lf%lf%lf", 
			&abc[0],
                     &abc[1], &abc[2], &abc[3], &abc[4],
                     &abc[5]);

        fprintf(output_fp,"\nUnit Cell read....\n");
        fprintf(output_fp,"Cell parameters :\n");
        fprintf(output_fp,"     a = %10.6f\n",abc[0]);
        fprintf(output_fp,"     b = %10.6f\n",abc[1]);
        fprintf(output_fp,"     c = %10.6f\n",abc[2]);
        fprintf(output_fp," alpha = %10.6f\n",abc[3]);
        fprintf(output_fp,"  beta = %10.6f\n",abc[4]);
        fprintf(output_fp," gamma = %10.6f\n",abc[5]);
        fprintf(output_fp,"\n");

        pbc = 1; /*****FUDGE!***/	
        fgets(dummy,BUFFER,coordinate_fp);
        if (strstr(dummy, SPACE) == NULL)
                {
                fprintf(output_fp,"Expecting spacegroup info. Exiting...\n");
                exit(0);
                }
        sscanf(dummy,"%*s%s", spacegroup);

        fgets(dummy,BUFFER,coordinate_fp);
        if (strstr(dummy, FRAC) == NULL)
                {
                fprintf(output_fp,"Expecting fractional coord. Exiting...\n");
                exit(0);
                }

	 i = 0;
         num_cation_site=0;
         while ((fgets(buffer,BUFFER,coordinate_fp) != NULL))
		{
/* woohoo - aileen's bit - now it just looks at the non-cations here, and ignores the cations */
                if(strstr(buffer, cation)==NULL)
                  {
                  sscanf(buffer,"%s%s%lf%lf%lf%lf", &orig_mol[i].label,
                                &orig_mol[i].type,
                                &orig_mol[i].x, &orig_mol[i].y, &orig_mol[i].z,
                                &orig_mol[i].charge);

                  /***init opti flag info ****/
                  orig_mol[i].flag_x = 0; 
                  orig_mol[i].flag_y = 0; 
                  orig_mol[i].flag_z = 0;
		  /***init neighbour info ***/
		  orig_mol[i].num_neigh=0;
		  for (ineigh=0;ineigh<4;ineigh++) orig_mol[i].neighb[ineigh]=-1;


		  i++;
                  }
/*woohoo - aileen's bit - until it stores them under a different section */
                 else
                  {
                  sscanf(buffer,"%s%s%lf%lf%lf", 
                         &cation_site[num_cation_site].label, 
                         &cation_site[num_cation_site].type, 
                         &cation_site[num_cation_site].x, 
                         &cation_site[num_cation_site].y, 
                         &cation_site[num_cation_site].z);
                  num_cation_site++;
                  }
	        }
	num_atoms = i-1;		
        num_cation_site = num_cation_site-1;
	fclose(coordinate_fp);
        for(i=0;i<=num_atoms;i++)
            {
            whereisthecation[i].x=0;
            whereisthecation[i].y=0;
            whereisthecation[i].z=0;
            }
/*****************************************************************/
/******* sort out lattice vectors if required ********************/
/*****************************************************************/

  if (pbc)
    {
       cart_latt_vecs( &abc[0], &latt_vec[0], &real_latt_sizes[0],
                        &recip_latt_vec[0],
                       &recip_latt_sizes[0], &cell_volume, num_atoms);
    }

/******************************************************************/
/***** Assign vdw radii                                         ***/
/******************************************************************/

  set_elem(&orig_mol[0],num_atoms);

  steric_setup(&orig_mol[0],num_atoms);
fprintf(output_fp, "Contains %d atoms\n",num_atoms);
/****************************DEBUG
for (i=0;i<=num_atoms;i++)
    {
fprintf(output_fp, 
        "Atom %d: %s %s %s %lf %lf %lf %lf %lf\n", i, orig_mol[i].label,
               orig_mol[i].elem,
                                orig_mol[i].type,
                                orig_mol[i].x, orig_mol[i].y, orig_mol[i].z,
                                orig_mol[i].charge,orig_mol[i].vdw);

    }
****************************DEBUG*/
	
/************ ONLY DO NEIGHBOURS ON CORES *******/
/**assume input is T core, O core, O shel**/
/*calc number of cores*/
        num_core_atoms=0;
        for (i=0;i<=num_atoms;i++)
           {
           if (strstr(orig_mol[i].type, CORE) != NULL)
            num_core_atoms++;
           }
        num_core_atoms--;
/*find first shells*/
first_shell = num_core_atoms+1;
	/****** generate neigbours *****/
	generate_neighbours(&orig_mol[0], num_core_atoms, &mol_types[0],
						&num_mol_types, TRUE);
	/*print_neighbours(&orig_mol[0], num_core_atoms,output_fp);*/

/**** initialise gulp input file ****/
        if ((fullstop = strchr(gulpfile,'.')) !=NULL) *fullstop = '\0';




/***calcualte max-sub = no.Si/ratio ****/
/***determine how many "replace" atoms there are***/
/* woohoo aileen added in cations to replaceable list if its a fixed cation run  
if(cation_insert_run)
{ */
num_replace_atoms=0;
for (i=0;i<=num_core_atoms;i++)
    {
     if (strcmp(replace_ele, orig_mol[i].elem) == 0)
          {
           replaceable_index[num_replace_atoms] = i;
           replaceable_done[num_replace_atoms] = FALSE;
           num_replace_atoms++;
          }
    }
num_replace_atoms--; 

/*****determin number to be substituted********/
        if (ratio > 0)
           {
           /***ratio given in file****/
           max_subs = num_replace_atoms/ratio;
            fprintf(output_fp,
                 "Target ratio given = %f. Equivalent to inserting %d atoms\n",
                      ratio, max_subs);
           }
         else
           {
            fprintf(output_fp,"Target substitution given = %d\n", max_subs);
           }


/***************************WRITE HEADER FOR config.s file*********/
if (regenerate_run != TRUE) 
  {
  fprintf(config_fp, "#CONFIGURATION file\n");
  if (generate_only == TRUE)
     {
      fprintf(config_fp, "generate %s\n", generate_path); 
     }
  fprintf(config_fp, "gulp command\n%s\n",gulp_command_line);
  fprintf(config_fp, "gulp time %f\n",gulp_time_limit);
  fprintf(config_fp, "gulp file %s\n",gulpfile);
  fprintf(config_fp, "gulp maxcycles %d\n",gulp_max_cycles);
  fprintf(config_fp, "gulp path %s\n", gulp_path);
		  
  fprintf(config_fp, "cell %s\n", coordinate_file);
  fprintf(config_fp, "replace %s  %6.3f\n", replace_ele, replace_charge);
  fprintf(config_fp, "insert %s  %6.3f\n", 
                      insert_ele, insert_charge);
  fprintf(config_fp, "subs %d\n", max_subs);
  fprintf(config_fp, "lowenstein ");
  if (lowenstein) fprintf(config_fp, " on\n");
  else fprintf(config_fp, " off\n");
  fprintf(config_fp, "cation %s ", cation);
  if (strstr(cation,SMEAR) ==0) fprintf(config_fp, "%f", cation_charge);
  fprintf(config_fp, "\n");
  fprintf(config_fp, "pots %s\n", pots_file);
  fprintf(config_fp, "keep %d\n", keep);
  fprintf(config_fp, "#configuration format\n");
  fprintf(config_fp, "#subs energy gnorm id N{atom_subbed cation_pos}\n");
  fprintf(config_fp, "%s\n", CONFIGURATIONS);

  }
else /*its a regenerate run*/
  {
	  /*initialise script*/
	  sprintf(script_name, "%s/%s_gulp_%d.csh", 
                        regenerate_path,gulpfile, script_type);
	  if ((script_fp = fopen(script_name, "w")) == NULL)
	       {
	       fprintf(stderr, "Unable to open file %s for writing.\n", script_name);
	        exit(1);
	        }
	  else
	       {
	         fprintf(script_fp, "#!/bin/csh -f\n");
                 if (script_type == SCRIPT_RUN_TYPE)
                  {
	          fprintf(script_fp, "#script for RUNNING Gulp files from\n");
                  }
                 else 
                  {
	          fprintf(script_fp, "#script for QUEUING Gulp files from\n");
                  }
	         fprintf(script_fp, "#mc_regen run %s\n", argv[1]);
	         }

  }


if (generate_only== TRUE)
   {
    /*see if directory exists and create if necessary*/

    /* initialise script*/
    sprintf(script_name, "%s/%s_gulp_%d.csh",  
            generate_path,gulpfile,script_type);
    if ((script_fp = fopen(script_name, "w")) == NULL)
       {
        fprintf(stderr, "Unable to open file %s for writing.\n", script_name);
        exit(1);
       }
    else
       {
          fprintf(script_fp, "#!/bin/csh -f\n");
          if (script_type == SCRIPT_RUN_TYPE)
           {
            fprintf(script_fp, "#script for RUNNING Gulp files from\n");
           }
          else
           {
            fprintf(script_fp, "#script for QUEUING Gulp files from\n");
           }
          fprintf(script_fp, "#mc_regen run %s\n", argv[1]);
      }

    
   }
/*seed random number generator*/
rdummy = real_random(0);


if ((regenerate_run != TRUE) && (analyse_type == ANALYSE_TYPE_NONE))
{ 
/***************************LOOP: no configs to try ***********************/
/***************************LOOP: no configs to try ***********************/
/***************************LOOP: no configs to try ***********************/
time_now= time(&time_now);
time_now= time_now-start_time;

fprintf(output_fp,"\n\nBeginning  %d cycles....\n\n", max_cycles);
fprintf(output_fp,"Cycle     Input          Output and Status                    Time\n");
fprintf(output_fp,"-------------------------------------------------------------------\n");
fflush(output_fp);

for (this_trial = 0; this_trial < max_cycles; this_trial++)
    {
        fprintf(output_fp,"%6d :",this_trial);
        /****re-initialise atom list****/
        for (i=0;i<=num_atoms;i++)
         {
          strcpy(mol[i].label, orig_mol[i].label);
          strcpy(mol[i].type, orig_mol[i].type);
          mol[i].x =  orig_mol[i].x;
          mol[i].y = orig_mol[i].y;
          mol[i].z = orig_mol[i].z;
          strcpy(mol[i].elem ,orig_mol[i].elem);
          mol[i].charge = orig_mol[i].charge;
          mol[i].num_neigh= orig_mol[i].num_neigh;
          mol[i].flag_x =orig_mol[i].flag_x;
          mol[i].flag_y =orig_mol[i].flag_y;
          mol[i].flag_z =orig_mol[i].flag_z;
          for (j=0;j< 4;j++) mol[i].neighb[j] = orig_mol[i].neighb[j];
          

         }
        /****re-initialise replaceable_done****/
        for (i=0;i<=num_replace_atoms;i++)
         {
         replaceable_done[i] = 0;
         }

/*****DEBUG
        for (i=0;i<=num_replace_atoms;i++)
            {
             printf("Index %d %d Atom %s status = %d\n", 
                      i, replaceable_index[i], 
                      mol[replaceable_index[i]].label,
                              replaceable_done[i]);
            }
******DEBUG*/

   	 /******LOOP: for each substituent required *****/
        subs_done = 0;
        new_num_atoms = num_atoms;
        while (subs_done < max_subs)
            {
             /***generate random substituent****/
             replace_this = real_random(1)* num_replace_atoms;
             /*find 'replace_this'th replaceable atom*/
             /*is it already replaced*/
             /* no - then ...*/
             if (replaceable_done[replace_this] == FALSE)
               {
                /***is Lowensteinian behaviour okay - optioN? ****/
                if (lowenstein == TRUE)
                   {
                    /**loop over all neighbours of replace_this and see if**/
                    /**any of their neighbours are "done"                 **/
                    lowenstein_break = FALSE;
                    for (ineigh = 0; 
                         ineigh<mol[replaceable_index[replace_this]].num_neigh; 
                         ineigh++)
                        {
                         this_neigh 
                           =mol[replaceable_index[replace_this]].neighb[ineigh];
                         for (jneigh = 0; jneigh<mol[this_neigh].num_neigh; jneigh++)
                           {
                            that_neigh = mol[this_neigh].neighb[jneigh];
                            if (strstr(mol[that_neigh].label, insert_ele) !=0)
                               {
                                lowenstein_break = TRUE;
                                break;
                               }
                           }
                        }
                   }

                if ((lowenstein == FALSE) || (lowenstein_break == FALSE))
                   {
                    replaceable_done[replace_this] =TRUE;
                    swap = replaceable_index[replace_this];
                    strcpy(mol[swap].label, insert_ele);
                    strcpy(mol[swap].elem, insert_ele);
                    mol[swap].charge = insert_charge;
                 
                    subs_done++;
                   }
                 else
                   {
                   }
                
               }
             else 
               {
               }
            }
    	/***endLOOP****/

        /****CHARGE COMPENSATE*****/
        if (strstr(cation, SMEAR) != NULL)
           { 
           new_num_atoms = num_atoms;
         
           /***spread charge on other T sites ***/
           /*fprintf(output_fp,"Smearing charge\n");*/
           cation_charge= -subs_done*(replace_charge-insert_charge);
           cation_charge= cation_charge/(num_replace_atoms-subs_done+1);
           
           for (i=0;i<=num_replace_atoms;i++)
             {
              if (replaceable_done[i] != TRUE)
                 {
                 swap = replaceable_index[i];
                 mol[swap].charge = mol[swap].charge - cation_charge;
                 }
             }
           }
        else if (strstr(cation, OH) != NULL)
           {
            /*fprintf(output_fp,"Doing OH insertion\n");*/
            /**do OH Subs***/
            /***LOOP ALL REPLACED ATOMS****/
            for (i=0;i<=num_replace_atoms;i++)
             {
              if (replaceable_done[i] == TRUE)
               {
                /***select random O****/
                got_one = FALSE;
                while (got_one != TRUE)
                 {
                 replace_this=
                     real_random(1)*mol[replaceable_index[i]].num_neigh;
                 this_neigh = mol[replaceable_index[i]].neighb[replace_this];
                 /***check no OH ****/
                 if (strstr(mol[this_neigh].label, OH_SPECIES) == 0) 
                    got_one= TRUE; 
                 }
                 /*find it's other T site neighbour*/
                 got_one = FALSE;
                 while (got_one != TRUE)
                  {
                   for (j=0;j<=mol[this_neigh].num_neigh;j++)
                    {
                     that_neigh = mol[this_neigh].neighb[j];

                     if (that_neigh != replaceable_index[i])
                      {
                       got_one = TRUE;
                       break;
                      }
                    }
                  if (got_one != TRUE)
                    {
                    fprintf(output_fp,"HELP NO NEIGHB\n");exit(1);
                    } 
                  }
            
                 
                /*****calculate H position *****/
                /*copy triad atoms to simple_atom structure*/
/*****
                T1.coord[1] = mol[replaceable_index[i]].x;
                T1.coord[2] = mol[replaceable_index[i]].y;
                T1.coord[3] = mol[replaceable_index[i]].z;
                T2.coord[1] = mol[that_neigh].x;
                T2.coord[2] = mol[that_neigh].y;
                T2.coord[3] = mol[that_neigh].z;
                bridgeO.coord[1] = mol[this_neigh].x;
                bridgeO.coord[2] = mol[this_neigh].y;
                bridgeO.coord[3] = mol[this_neigh].z;
*****/
                fract_to_cart(&T1.coord[1], &T1.coord[2],&T1.coord[3],
                 mol[replaceable_index[i]].x, 
                 mol[replaceable_index[i]].y,mol[replaceable_index[i]].z,
                 &latt_vec[0]);
                fract_to_cart(&T2.coord[1], &T2.coord[2],&T2.coord[3],
                 mol[that_neigh].x, 
                 mol[that_neigh].y,mol[that_neigh].z,
                 &latt_vec[0]);
                fract_to_cart(&bridgeO.coord[1], 
                 &bridgeO.coord[2],&bridgeO.coord[3],
                 mol[this_neigh].x, 
                 mol[this_neigh].y,mol[this_neigh].z,
                 &latt_vec[0]);

                cation_pos = h_pos(&T1, &T2, &bridgeO);
                /*****change O ****/
                strcpy(mol[this_neigh].label,OH_SPECIES);
                mol[this_neigh].charge = -1.426;
                mol[this_neigh].flag_x = 1;
                mol[this_neigh].flag_y = 1;
                mol[this_neigh].flag_z = 1;
                /*****add to list instead of SHEL****/
                /*****assume cores and shells in same order ****/
                j=this_neigh+(num_core_atoms - num_replace_atoms);
                /*****DEBUG
                printf("core %s %f %f %f shel %s %f %f %f\n",
                        mol[this_neigh].label,
                        mol[this_neigh].x,mol[this_neigh].y,mol[this_neigh].z,
                        mol[j].label,
                        mol[j].x, mol[j].y,mol[j].z);
                *****DEBUG*/
                strcpy(mol[j].label, H);
                strcpy(mol[j].type, "core");
                /*convert to frac again*/
                cart_to_fract(cation_pos.coord[1],cation_pos.coord[2],
                              cation_pos.coord[3],
                              &mol[j].x, &mol[j].y,&mol[j].z,
                              &recip_latt_vec[0]);
                
                mol[j].num_neigh = 1;
                mol[j].neighb[0] = this_neigh;
                mol[j].charge = 0.426;
                mol[j].flag_x = 1;
                mol[j].flag_y = 1;
                mol[j].flag_z = 1;
                   
                
               }/*end if (replaceable_done[i] != TRUE) */
             }/*end for i*/
 
           }/*end if (strstr(cation, OH) != NULL)*/
        else
           {
           /*woohoo - aileen added an if loop so there could be a else at the end for fixed cation option... */
           if (cation_insert_run)
             {
              /**straight cation insertion**/
              /*fprintf(output_fp, "Doing %s cation insertion\n", cation);*/
              /***LOOP ALL REPLACED ATOMS****/
              i=0;
              while (i<=num_replace_atoms)
               {
               if (replaceable_done[i] == TRUE)
                {
                /**find two oxygen neighbours to atom**/
                done_this = FALSE;
                while (done_this == FALSE)
                 {
                  replace_this =  
                       real_random(1)*mol[replaceable_index[i]].num_neigh;
                  this_neigh = mol[replaceable_index[i]].neighb[replace_this];
                  got_one = FALSE;
                  while (got_one != TRUE)
                   {
                     replace_this =  
                       real_random(1)*mol[replaceable_index[i]].num_neigh;
                     that_neigh = mol[replaceable_index[i]].neighb[replace_this];
                      /******DEBUG
                      printf("this_neigh %s %d that_neigh %s %d\n",
                       mol[this_neigh].label, this_neigh,
                       mol[that_neigh].label, that_neigh);
                      ******DEBUG*/
                     if (that_neigh != this_neigh) got_one = TRUE;
                   }
                  /**calc cation position**/
               
                  fract_to_cart(&O1.coord[1],
                                &O1.coord[2],&O1.coord[3],
                                mol[this_neigh].x,
                                mol[this_neigh].y,mol[this_neigh].z,
                                &latt_vec[0]);
                  fract_to_cart(&T1.coord[1], &T1.coord[2],&T1.coord[3],
                                mol[replaceable_index[i]].x,
                                mol[replaceable_index[i]].y,
                                mol[replaceable_index[i]].z, &latt_vec[0]);
                  fract_to_cart(&O2.coord[1], &O2.coord[2],&O2.coord[3],
                                mol[that_neigh].x,
                                mol[that_neigh].y,mol[that_neigh].z,
                                &latt_vec[0]);
  
                  cation_pos = X_pos(&O1, &O2, &T1);
                
                  cart_to_fract(cation_pos.coord[1],cation_pos.coord[2],
                                cation_pos.coord[3],
                                &temp_atom.x, &temp_atom.y,&temp_atom.z,
                                &recip_latt_vec[0]);
                  /**add to list**/
                  /***check if not near another cation ***/
                  clash = FALSE;
                  for (j=num_atoms+1;j<=new_num_atoms;j++)
                   {
                    actual_dist = atom_separation_squared(&temp_atom,
                                                          &mol[j],TRUE);
                   actual_dist = sqrt(actual_dist);
  
                   if (actual_dist < CATION_CATION_CUTOFF)
                     {
                      clash = TRUE;
                     }
                   }
                  if (clash == FALSE)
                   {
                    /*printf("adding this cation\n");*/
                    new_num_atoms++;
                    mol[new_num_atoms].x = temp_atom.x;
                    mol[new_num_atoms].y = temp_atom.y;
                    mol[new_num_atoms].z = temp_atom.z;
                    mol[new_num_atoms].num_neigh = 0;
                    mol[new_num_atoms].charge = cation_charge;
                    strcpy(mol[new_num_atoms].label ,cation);
                    strcpy(mol[new_num_atoms].type ,"core");
                    mol[new_num_atoms].flag_x = 1;
                    mol[new_num_atoms].flag_y = 1;
                    mol[new_num_atoms].flag_z = 1;
                    done_this = TRUE;
/* woohoo - aileen's added a bit to record these Na positions */
                    whereisthecation[i].x=mol[new_num_atoms].x;
                    whereisthecation[i].y=mol[new_num_atoms].y;
                    whereisthecation[i].z=mol[new_num_atoms].z; 
                   }
                  else
                   {
                   /*printf("cation site already occupied\n");*/
                   }
                } /*end while (done_this == FALSE) */
              }   /* end if (replaceable_done[i] == TRUE) */
              i++;
             } /* end (while (i<=num_replace_atoms)*/
            }
/*woohoo aileen - this whole else loop is added by me for fixed cation option */
/*woohoo aileen - for the next bit this is practically the same as the Al insertion */
          else 
            {
            new_num_atoms = num_atoms;
            i=0;
            for (i=0;i<=num_cation_site; i++)
              {
              cation_replaced[i] = FALSE; 
              }
            for(i=0; i<=num_replace_atoms;i++)
/*          for(i=0; i<max_subs; i++) */
             {
             if (replaceable_done[i] == TRUE)
               {
               done_this = FALSE;
               while (done_this ==FALSE)
                 {
                 replace_this = real_random(1)* num_cation_site;
                 clash = FALSE;
                 for (j=num_atoms+1;j<new_num_atoms;j++)
                   {
                   temp_atom=cation_site[replace_this];
                   actual_dist = atom_separation_squared(&temp_atom, &mol[j],TRUE);
                   actual_dist = sqrt(actual_dist);
                   if (actual_dist < CATION_CATION_CUTOFF)
                      {
                      clash = TRUE;
                      }
                   }
                 if (((cation_replaced[replace_this]) == FALSE)&&(clash == FALSE)) 
                   { 
                   cation_replaced[replace_this] = TRUE;
                   new_num_atoms++;
                   mol[new_num_atoms].x = cation_site[replace_this].x;
                   mol[new_num_atoms].y = cation_site[replace_this].y; 
                   mol[new_num_atoms].z = cation_site[replace_this].z;
                   mol[new_num_atoms].num_neigh = 0;
                   mol[new_num_atoms].charge = cation_charge;
                   strcpy(mol[new_num_atoms].label ,cation);
                   strcpy(mol[new_num_atoms].type ,"core");
                   mol[new_num_atoms].flag_x = 1;
                   mol[new_num_atoms].flag_y = 1;
                   mol[new_num_atoms].flag_z = 1;
                   done_this = TRUE;
                   whereisthecation[i].x=mol[new_num_atoms].x;
                   whereisthecation[i].y=mol[new_num_atoms].y;
                   whereisthecation[i].z=mol[new_num_atoms].z;
                   }   
                 } 
               } 
             }

            }
          } /*end else */

       /*make substituted atom moveable and its neightbours too*/
       /*catons alreasdy done above*/
       for (i=0;i<=num_replace_atoms;i++)
             {
              if (replaceable_done[i] == TRUE)
                 {
                 swap = replaceable_index[i];
                 mol[swap].flag_x = 1;
                 mol[swap].flag_y = 1;
                 mol[swap].flag_z = 1;
                 for (ineigh=0;ineigh<mol[swap].num_neigh;ineigh++)
                     {
                      mol[mol[swap].neighb[ineigh]].flag_x = 1;
                      mol[mol[swap].neighb[ineigh]].flag_y = 1;
                      mol[mol[swap].neighb[ineigh]].flag_z = 1;
                      /***NB! doesn't do shells at the mo***/
                     }
         
                 }
             }


    

/**** set fielnames if a generate run */
        if (generate_only == TRUE )
          {
           sprintf(gulpfile_in,"%s/%s_%d.gin",generate_path,gulpfile,this_trial);
           sprintf(gulpfile_out,"%s/%s_%d.gout",generate_path,gulpfile,this_trial);
           sprintf(gulpdumpfile,"%s_%d.dump",gulpfile,this_trial);
           sprintf(gulpxtlfile,"%s_%d.xtl",gulpfile,this_trial);
          }
        else
          {
           sprintf(gulpfile_in, "%s_temp.gin",gulpfile);
           sprintf(gulpfile_out, "%s_temp.gout",gulpfile);
           sprintf(gulpdumpfile,"NONE");
           sprintf(gulpxtlfile,"NONE");
          }
        fprintf(output_fp,"%s ", gulpfile_in);
        /*fprintf(output_fp,"Will read output  : %s\n", gulpfile_out);*/

   	/****** write out gulp file ****/
	if ((gulpfp = fopen(gulpfile_in, "w")) == NULL)
            {
            fprintf(stderr, "Unable to open file %s for output.\n", gulpfile);
            exit(1);
            }
        else
            {
             save_gulpfile(gulpfp, gulpdumpfile,  &mol[0],
                       new_num_atoms, &spacegroup[0], gulpxtlfile, &pots_file[0]);
            }
        /*append potentials*/
        sprintf(append_pots_command, "cat %s >> %s\n",
                                           pots_file, gulpfile_in);
        system (append_pots_command);




    	/*** run GULP ***/
        sprintf(run_gulp_command,"%s < %s > %s", 
                      gulp_path, gulpfile_in, gulpfile_out); 
 if (generate_only == FALSE)
     {
    	system(run_gulp_command);


    	/*** read in final energy from GULP ***/
    	final_energy_flag = 0;
    	final_gnorm = -9999.0;
    	final_energy =  9999.0;
        strcpy(gulp_achieved_flag,NO);
        strcpy(gulp_max_iter_reached_flag,NO);
        if (strstr(gulp_command_line, SINGLE) != NULL)
         {
    	   /*initial_energy_flag = 1;*/
           final_gnorm = -999.0;
         }
        else
         {
    	   initial_energy_flag = 0;
         }
    	if ((gulpoutputfp = fopen(gulpfile_out, "r")) == NULL)
            	{
            	fprintf(stderr, "Unable to open file %s for output.\n", 
                             	gulpfile_out);
            	exit(1);
            	}
    	else
            	{
			while ((fgets(buffer,BUFFER,gulpoutputfp) != NULL) 
                        	&& (!final_energy_flag))
                   		{
                     		if (strstr(buffer, FINAL_ENERGY) != NULL)
                        		{
                         		     /*final_energy_flag = 1; NOT! cos need gnorm as well*/
                                             /*so if a single it'll just run out of file*/
                         		     sscanf(buffer, "%*s%*s%*s%*s%lf", &final_energy);
                                             if (strstr(buffer,UNITS_KJ)) final_energy=final_energy/KJ_2_EV;
                  		         }
                                 /*read Gnorm as well now DWL*/
                                 if (strstr(buffer,FINAL_GNORM) != NULL)
                                         {
                                          sscanf(buffer,
                                                    "%*s%*s%*s%lf",
                                                        &final_gnorm);
                         		  final_energy_flag = 1;
                                         }
                                 if (strstr(buffer,GULP_ACHIEVED) != NULL)
					 {
                                          strcpy(gulp_achieved_flag,YES);
                                         }
                                 if (strstr(buffer,GULP_MAX_ITER_REACHED) != NULL)
					 {
                                          strcpy(gulp_max_iter_reached_flag,YES);
                                         }
                   
                   		}
                }
       
        fclose(gulpoutputfp); 
        fprintf(output_fp," > %s ",gulpfile_out);
        if (strstr(gulp_command_line,SINGLE) != NULL) final_energy_flag =1; 
        if (final_energy_flag)
          {
           if (final_gnorm == -999.0)
              {
               fprintf(output_fp,"(read - E=%8.2f Single",final_energy);
              }
           else
              {
               fprintf(output_fp,"(read - E=%8.2f G=%6.4f",final_energy,final_gnorm);
              }
           if (cycles_done>0) fprintf(output_fp," CYC=%d)",cycles_done);

          }
        else
          {
           fprintf(output_fp,"(no_read)");
          }

       /*** insert into list ****/
       /*** did we get an energy **/
           /****WRITE OUT SUBSTITUTION SITE *******/
/** woohoo aileen changed this too, so that the cation x, y, and z positions are recorded to the config file... */
           save_config(config_fp, this_trial, num_replace_atoms, 
                       final_energy, final_gnorm, gulp_achieved_flag,
                        &replaceable_index[0],
                        &replaceable_done[0], &whereisthecation[0]);
           fflush(config_fp);

       
 }/*end if generrate_only*/
else
 {
  /*its a generate run*/
  /*so just save config to file with zero as the energy and -999 as gnorm */
  /* N as the achieved convergence flag*/
    strcpy(gulp_achieved_flag,NO);
    save_config(config_fp, this_trial, num_replace_atoms,
			0.0, -999.0,  
                       gulp_achieved_flag,
                       &replaceable_index[0],
		       &replaceable_done[0], &whereisthecation[0]);
    fflush(config_fp);
    fprintf(output_fp," Generated ");
    
   /*write to script as well*/
    if (script_type == SCRIPT_RUN_TYPE)
             {
              fprintf(script_fp,"%s < %s_%d.gin > %s_%d.gout\n",
                              gulp_path,
                              gulpfile,  this_trial,
                              gulpfile,  this_trial);
             }
    else 
             {
              fprintf(script_fp,"qgulp %s_%d\n",
                              gulpfile,  this_trial);
             }


} /* end if cation_insert_run??*/
time_now= time(&time_now);
time_now= time_now-start_time;
fprintf(output_fp,":%17d s\n",time_now);
fflush(output_fp);
} /*loop???*/
}/*XXXX*/

else if (analyse_type != ANALYSE_TYPE_NONE)
 {
  /*analysis run*/
  fprintf(output_fp,"\nAnalysis Run\n");
  fprintf(output_fp,"\n------------\n");
  fprintf(output_fp,"Type: ");
  if (analyse_type == ANALYSE_TYPE_FULL) fprintf(output_fp,"Full\n");
  else if (analyse_type == ANALYSE_TYPE_SIMPLE) fprintf(output_fp,"Simple\n");
  else 
       {
        fprintf(output_fp,"Unknown!\n");
        exit(-1);
       }
  fprintf(output_fp,"Analysing directory \"%s\"\n",analyse_path);
  fflush(output_fp);
  /****create list of "gout" files*****/
  time_now= time(&time_now);
  sprintf(analyse_filename,"mcsubs_analysis_%d.tmp",time_now);
  sprintf(general_command, "\\ls -l %s/*.gout> %s",analyse_path,analyse_filename); 
  printf("DB>>ANAL>>%s %s \n",analyse_filename,general_command);
  system(general_command);
  
  /*loop while line in list file*/

  if ((analyse_fp = fopen(analyse_filename, "r")) == NULL)
      {
        fprintf(stderr, "Unable to open file %s for reading.\n",
                        analyse_filename);
        exit(1);
      }
  else
      {
       iloop =0;
       while (fgets(buffer,BUFFER,analyse_fp) != NULL)
            {
             iloop++; /*just counting in the list*/
             sscanf(buffer, "%*s%*d%*s%*s%*d%*s%*d%*s%s", this_analyse_filename);
             printf(">>ANAL>%s\n",this_analyse_filename);
             /*reset flags*/
             initial_energy_flag=0;
             single_flag=FALSE;
             final_energy_flag = 0;
             strcpy(gulp_achieved_flag,NO);
             cycles_done = 0;
    	     final_gnorm = -999.0;
    	     final_energy =  9999.0;
             if ((this_analyse_fp = fopen(this_analyse_filename, "r")) == NULL)
                 {
                   fprintf(output_fp, "Unable to open file %s for analysis. Skipping\n",
                                   this_analyse_filename);
                 }
             else
                 {
                   fprintf(output_fp, "ANAL>>%d>>%s >",iloop,this_analyse_filename);
                   while (fgets(buffer,BUFFER,this_analyse_fp) != NULL)
                         {
                          if (strstr(buffer, SINGLE) !=NULL) 
                                  {
                                   single_flag = TRUE;
                                   final_energy_flag = 1;
                                  }
                          if (strstr(buffer, CYCLE) != NULL)
                                  {
                                   cycles_done++;
                                  }
                          if (strstr(buffer, FINAL_ENERGY) != NULL)
                                  {
                                  if (initial_energy_flag == 0)
                                     {
                                       initial_energy_flag =1;
                                     }
                                  else
                                     {
                                       /*final_energy_flag = 1; NOT! cos need gnorm as well*/
                                       sscanf(buffer, "%*s%*s%*s%*s%lf", &final_energy);
                                       if (strstr(buffer,UNITS_KJ)) final_energy=final_energy/KJ_2_EV;
                                     }
                                  }
                          /*read Gnorm as well now DWL*/
                         if (strstr(buffer,FINAL_GNORM) != NULL)
                              {
                                  sscanf(buffer, "%*s%*s%*s%lf", &final_gnorm);
                                  final_energy_flag = 1;
                              }
                         if (strstr(buffer,GULP_ACHIEVED) != NULL)
                              {
                                strcpy(gulp_achieved_flag,YES);
                              }
                         if (strstr(buffer,FINAL_STRUCTURE_START) != NULL)
                              {
                                /*now read final structure in and we can do some stuff*/
                                /*in Gulp1.3 coords start after and line of ------ */
                                temp_counter=0;
/****WORKING HERE*****/
                                /*simple_analysis = get Al sites and final cation posn*/
                              }

                         }
                   if (final_energy_flag==TRUE)
                      {
                       fprintf(output_fp, "E=%f, G=%f,   CONV=%s, CYC=%d", final_energy,final_gnorm,gulp_achieved_flag, cycles_done);
                      }
                   else
                      {
                       fprintf(output_fp, "No Energy found!");
                      }
                   fprintf(output_fp, " >>EOF\n");
                 }
             
             /*read filename from list*/
             /*analyse*/
             /*if  (analyse_type == ANALYSE_TYPE_FULL) get cation pos etc etc*/
             /*if  (analyse_type == ANALYSE_TYPE_SIMPLE) get energy, gnorm, cell param*/
            }
             printf(">>EOF\n");
             fclose(this_analyse_fp);
      }
  /*DB>> exit(-99);*/
  /*delete the tmp file*/
  sprintf(general_command, "\\rm -f  %s",analyse_filename); 
  printf("DB>>ANAL>> %s \n",general_command);
  system(general_command);
 }
else /*(regenerate_run) */
 {
   fprintf(output_fp,"Regenerate Run\n");
   this_trial = 0;
   eof_config = FALSE;
   subs_done = max_subs;
   /*open input file again and wind to config*/
   /****** HERERERERERERERERERERER*/
   if ((reopen_fp = fopen(argv[1], "r")) == NULL)
       {
        fprintf(stderr, "Unable to open file %s for input.\n", argv[1]);
        exit(1);
       }
   /*wind to start of configs*/
   regenerate_run = FALSE;
   while (!regenerate_run)
     {
       fgets(buffer,BUFFER,reopen_fp);
       if (strstr(buffer, CONFIGURATIONS) != NULL)
             regenerate_run = TRUE;
     }
   fprintf(output_fp,"\nRegenerate: Configurations found in file\n");
   fprintf(output_fp,"Config      Output and Status               Time\n");
   fprintf(output_fp,"-------------------------------------------------\n");
   fflush(output_fp);

   while ((this_trial < keep) && (fgets(dummy, BUFFER, reopen_fp) != NULL))
        {
	   fprintf(output_fp,"%6d", this_trial);

	/****re-initialise atom list****/
        for (i=0;i<=num_atoms;i++)
         {
           strcpy(mol[i].label, orig_mol[i].label);
           strcpy(mol[i].type, orig_mol[i].type);
           mol[i].x =  orig_mol[i].x;
           mol[i].y = orig_mol[i].y;
           mol[i].z = orig_mol[i].z;
           strcpy(mol[i].elem ,orig_mol[i].elem);
           mol[i].charge = orig_mol[i].charge;
           mol[i].num_neigh= orig_mol[i].num_neigh;
           mol[i].flag_x =orig_mol[i].flag_x;
           mol[i].flag_y =orig_mol[i].flag_y;
           mol[i].flag_z =orig_mol[i].flag_z;
           for (j=0;j< 4;j++) mol[i].neighb[j] = orig_mol[i].neighb[j];
										          }

     /***reset index****/
     for (i=0;i<=num_replace_atoms;i++)
        {
         replaceable_done[i] = FALSE;
        }
     /**** read in atoms to replace ******/
     /*(fgets(dummy, BUFFER, reopen_fp);*/
     /****major kludge; each one different line****/
     /** woohoo - aileen's addition - for each subs it reads in al position (bingo)+cation x, y, and z **/
         for (i=0;i<max_subs;i++)
            {
             strcpy(marker,"%*s%*f%*f%*s%i");
             for (j=0;j<i;j++)
                {
                 strcat(marker,"%*i%*f%*f%*f");
                }
             strcat(marker,"%i%lf%lf%lf");
             sscanf(dummy, &marker[0], &rank,&bingo, &fdum, &fdum, &fdum);
             for (j=0;j<=num_replace_atoms;j++)
                 {
                  if (bingo == replaceable_index[j]) 
                       {
                         replaceable_done[j] = TRUE;
                         sscanf(dummy, &marker[0], &rank,&sdum[0], 
                                &whereisthecation[bingo].x, &whereisthecation[bingo].y, 
                                &whereisthecation[bingo].z);
                       }
                 }
            }
  
         for (j=0;j<=num_replace_atoms;j++)
            {
             if (replaceable_done[j] == TRUE)
                {
                 swap = replaceable_index[j];
                 strcpy(mol[swap].label, insert_ele);
                 strcpy(mol[swap].elem, insert_ele);
                 mol[swap].charge = insert_charge;
                }
            }
  
         
  
          /****CHARGE COMPENSATE*****/
          if (strstr(cation, SMEAR) != NULL)
             { 
             /***spread charge on other T sites ***/
             new_num_atoms = num_atoms;
             cation_charge= -subs_done*(replace_charge-insert_charge);
             cation_charge= cation_charge/(num_replace_atoms-subs_done+1);
             for (i=0;i<=num_replace_atoms;i++)
               {
                if (replaceable_done[i] != TRUE)
                   {
                   swap = replaceable_index[i];
                   mol[swap].charge = mol[swap].charge - cation_charge;
                   }
               }
             }
          else if (strstr(cation, OH) != NULL)
             {
              /**do OH Subs***/
              new_num_atoms = num_atoms;
  /******ADDED*****/
              /**do OH Subs***/
              /***LOOP ALL REPLACED ATOMS****/
              for (i=0;i<=num_replace_atoms;i++)
               {
                if (replaceable_done[i] == TRUE)
                 {
                  /***select random O****/
                  got_one = FALSE;
                  while (got_one != TRUE)
                   {
                   replace_this=
                       real_random(1)*mol[replaceable_index[i]].num_neigh;
                   this_neigh = mol[replaceable_index[i]].neighb[replace_this];
                   /***check no OH ****/
                   if (strstr(mol[this_neigh].label, OH_SPECIES) == 0)
                      got_one= TRUE;
                   }
                   /*find it's other T site neighbour*/
                   got_one = FALSE;
                   while (got_one != TRUE)
                    {
                     for (j=0;j<=mol[this_neigh].num_neigh;j++)
                      {
                       that_neigh = mol[this_neigh].neighb[j];
                                             
                       if (that_neigh != replaceable_index[i])
                        {
                         got_one = TRUE;
                         
                         break;
                        }
                      }
                    if (got_one != TRUE)
                      {
                      printf("HELP NO NEIGHB\n");exit(1);
                      }
                    }
  
                   
  
                  /*****calculate H position *****/
                  fract_to_cart(&T1.coord[1], &T1.coord[2],&T1.coord[3],
                   mol[replaceable_index[i]].x,
                   mol[replaceable_index[i]].y,mol[replaceable_index[i]].z,
                   &latt_vec[0]);
                  fract_to_cart(&T2.coord[1], &T2.coord[2],&T2.coord[3],
                   mol[that_neigh].x,
                   mol[that_neigh].y,mol[that_neigh].z,
                   &latt_vec[0]);
                  fract_to_cart(&bridgeO.coord[1],
                   &bridgeO.coord[2],&bridgeO.coord[3],
                   mol[this_neigh].x,
                   mol[this_neigh].y,mol[this_neigh].z,
                   &latt_vec[0]);
  
                  cation_pos = h_pos(&T1, &T2, &bridgeO);
                  /*****change O ****/
                  strcpy(mol[this_neigh].label,OH_SPECIES);
                  mol[this_neigh].charge = -1.426;
                  mol[this_neigh].flag_x = 1;
                  mol[this_neigh].flag_y = 1;
                  mol[this_neigh].flag_z = 1;
  
                  /*****add to list instead of SHEL****/
                  /*****assume cores and shells in same order ****/
                  j=this_neigh+(num_core_atoms - num_replace_atoms);
  
                  strcpy(mol[j].label, H);
                  strcpy(mol[j].type, "core");
                  /*convert to frac again*/
                  
                  cart_to_fract(cation_pos.coord[1],cation_pos.coord[2],
                                cation_pos.coord[3],
                                &mol[j].x, &mol[j].y,&mol[j].z,
                                &recip_latt_vec[0]);
  
                  mol[j].num_neigh = 1;
                  mol[j].neighb[0] = this_neigh;
                  mol[j].charge = 0.426;
                  mol[j].flag_x = 1;
                  mol[j].flag_y = 1;
                  mol[j].flag_z = 1;
  
  
  
                 }/*end if (replaceable_done[i] != TRUE) */
               }/*end for i*/
  
  /******ADDED*****/
   
             }
          else
             {
              /**straight cation insertion**/
  /******ADDED*****/
              /***LOOP ALL REPLACED ATOMS****/
               new_num_atoms = num_atoms;
              i=0;
              while (i<=num_replace_atoms)
               {
               if (replaceable_done[i] == TRUE)
                {
                /**find two oxygen neighbours to atom**/
               done_this = FALSE;
                  while (done_this == FALSE)
                 {
                    new_num_atoms++;
                    mol[new_num_atoms].x = whereisthecation[i].x;
                    mol[new_num_atoms].y = whereisthecation[i].y;
                    mol[new_num_atoms].z = whereisthecation[i].z;
                    mol[new_num_atoms].num_neigh = 0;
                    mol[new_num_atoms].charge = cation_charge;
                    strcpy(mol[new_num_atoms].label ,cation);
                    strcpy(mol[new_num_atoms].type ,"core");
                    mol[new_num_atoms].flag_x = 1;
                    mol[new_num_atoms].flag_y = 1;
                    mol[new_num_atoms].flag_z = 1;
                    done_this = TRUE;
                } /*end while (done_this == FALSE) */
              }   /* end if (replaceable_done[i] == TRUE) */
              i++;
             } /* end (while (i<=num_replace_atoms)*/
  

  /******ADDED*****/
             }
  
  
         /*make substituted atom moveable and its neightbours too*/
         /*catons alreasdy done above*/
         for (i=0;i<=num_replace_atoms;i++)
               {
                if (replaceable_done[i] == TRUE)
                   {
                   swap = replaceable_index[i];
                   mol[swap].flag_x = 1;
                   mol[swap].flag_y = 1;
                   mol[swap].flag_z = 1;
                   for (ineigh=0;ineigh<mol[swap].num_neigh;ineigh++)
                       {
                        mol[mol[swap].neighb[ineigh]].flag_x = 1;
                        mol[mol[swap].neighb[ineigh]].flag_y = 1;
                        mol[mol[swap].neighb[ineigh]].flag_z = 1;
                        /***NB! doesn't do shells at the mo***/
                       }
  
                   }
               } /*end for i*/
  
      
  
    	  /****** write out gulp file ****/
          sprintf(gulpfile_in, "%s/%s_%d_%d.gin", regenerate_path,
			  gulpfile, rank, this_trial); 
	  if ((gulpfp = fopen(gulpfile_in, "w")) == NULL)
              {
              fprintf(stderr, "Unable to open file %s for output.\n", 
			       gulpfile_in);
              exit(1);
              }
          else
              {
               sprintf(dummy, "%s_%d_%d.xtl", gulpfile, rank, this_trial);
               sprintf(dummy2, "%s_%d_%d.dump", gulpfile, rank, this_trial);
               save_gulpfile(gulpfp, &dummy2[0], &mol[0],
                             new_num_atoms, "1", &dummy[0], &pots_file[0]);
              } /*end if/else gulpfp*/
	  fprintf(output_fp,"   > %s_%d_%d.gin",gulpfile, rank, this_trial);
          sprintf(append_pots_command, "cat %s >> %s\n",
                                             pots_file, gulpfile_in);
          system (append_pots_command);
  
          /*fclose(gulpfp); */
          
          /****** write out script line *****/
          if (script_type == SCRIPT_RUN_TYPE)
             {
              fprintf(script_fp,"%s < %s_%d_%d.gin > %s_%d_%d.gout\n", 
                              gulp_path,
                              gulpfile, rank, this_trial,
			      gulpfile, rank, this_trial);
             }
          else /* (script_type == SCRIPT_QSUB_TYPE)*/
             {
              fprintf(script_fp,"qgulp %s_%d_%d\n", 
                              gulpfile, rank, this_trial);
             }
          time_now= time(&time_now);
          time_now= time_now-start_time;
          fprintf(output_fp," :%17d s\n",time_now);
          fflush(output_fp);

          this_trial++;
    }
  /***endLOOP****/
  
  fclose(script_fp);
  /***make script file executable***/
  sprintf(append_pots_command, "chmod +x %s\n", script_name);
  system (append_pots_command);
  fprintf(output_fp,"-------------------------------------------------\n");
  fprintf(output_fp,"\n\nScript for running jobs: %s\n\n",script_name);
  }
  


 /* Epilogue*/
 time_now= time(&time_now);
 fprintf(output_fp,"\n\n");
 fprintf(output_fp,"-------------------------------------------------------\n");
 fprintf(output_fp,"Job Finished at: %d\n", time_now);
 fprintf(output_fp,"Elasped Time   : %d s\n",time_now-start_time);
 fprintf(output_fp,"-------------------------------------------------------\n");

 }

/*}*/
