# Setup
library(zoo)
library(ggplot2)
library(reshape2)
library(stringr)
senitstats <- read.csv("SENITStats.csv", sep = ",", quote = "\"", stringsAsFactors = FALSE)

senitstats$MonthYear <- as.yearmon(senitstats$MonthYear, "%b-%y")
senitstats$StudentGeneralContact <- as.integer(senitstats$StudentGeneralContact)
senitstats$StudentInduction <- as.integer(senitstats$StudentInduction)
senitstats$StudentTrainingDemo <- as.integer(senitstats$StudentTrainingDemo)
senitstats$StaffAndOHContact <- as.integer(senitstats$StaffAndOHContact)
senitstats$StaffTraining <- as.integer(senitstats$StaffTraining)
senitstats$LaptopWorkOrLab <- as.integer(senitstats$LaptopWorkOrLab)
senitstats$Testing <- as.integer(senitstats$Testing)
senitstats$DisabilityService <- as.integer(senitstats$DisabilityService)
senitstats$BookScanner <- as.integer(senitstats$BookScanner)

senittotals <- data.frame(senitstats[0,1:10], row.names = NULL)
colnames(senittotals) <- c("Year", "StudentGeneralContact", "StudentInduction", "StudentTrainingDemo", "StaffAndOHContact", "StaffTraining", "LaptopWorkOrLab", "Testing", "DisabilityService", "BookScanner")
senittotals$Year <- as.numeric(senittotals$Year)

for (year in 2008:2016) {
    # Aggregate from September of the current year to August of the next year
    yeartotals <- sapply(senitstats[senitstats$MonthYear>=sprintf("Sep %i", year) & senitstats$MonthYear<=sprintf("Aug %i", (year+1)), 2:10], sum, na.rm = TRUE)
    senittotals[nrow(senittotals)+1,] <- rbind(c(year, yeartotals))
}

rm(year, yeartotals)

totals <- melt(senittotals, id.vars = "Year", na.rm = TRUE, variable.name = "Category", value.name = "Total")

#
# Feedback forms
#
feedback.staff <- read.csv("/home/james/Dropbox/SENIT/FeedbackForms/StaffFeedbackForms.csv", sep = ",", quote = "\"", stringsAsFactors = FALSE)
feedback.student <- read.csv("/home/james/Dropbox/SENIT/FeedbackForms/StudentFeedbackForms.csv", sep = ",", quote = "\"", stringsAsFactors = FALSE)

feedback.staff$Question <- str_wrap(feedback.staff$Question, width = 35)
feedback.student$Statement <- str_wrap(feedback.student$Statement, width = 50)

feedback.staff.long <- melt(feedback.staff, id.vars = "Question", variable.name = "Response", value.name = "Total")
feedback.student.long <- melt(feedback.student, id.vars = "Statement", variable.name = "Response", value.name = "Total")
