#!/bin/bash
# datagrab.sh
# Specifically for GULP files
# Just calls to gout_extract.pl

cd ~/data/gulp/nat/ene && for dir in {00..15}; do cd ../"$dir"; gout_extract.pl > "nat$dir".tsv;  mv "nat$dir".tsv ../ene/; done
cd ~/data/gulp/sco/ene && for dir in {00..24}; do cd ../"$dir"; gout_extract.pl > "scol$dir".tsv;  mv "scol$dir".tsv ../ene/; done
cd ~/data/gulp/mes/ene && for dir in {00..64}; do cd ../"$dir"; gout_extract.pl > "mes$dir".tsv;  mv "mes$dir".tsv ../ene/; done
cd ~/data/gulp/lau_pressure && for pressure in [np][0-9]*; do cd "$pressure/ene"; for dir in {00..64}; do cd ../"$dir"; gout_extract.pl > "mes$dir".tsv;  mv "mes$dir".tsv ../ene/; done; cd ../..; done
