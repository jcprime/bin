#!/bin/bash
# template_replacements.sh
# Taken from cutoff_tests/cutoff_inputs.sh for the variable-replacement inline-editing code
 
template_file=scol_template.inp

project_name=""
coord_file=""
abc=""
angles=""
restart_file=""
scf_guess=""
converge_to=""
inner_convergence=""
outer_convergence=""
minimiser="CG"
preconditioner="FULL_SINGLE_INVERSE"
xc="PBE"
max_inner_cycles="1000"
max_outer_cycles="5000"
cutoff="750"
rel_cutoff="60"
extrapolation="ASPC" # Or something
run_type="GEOMETRY_OPTIMIZATION"

for ii in $cutoffs ; do
    work_dir=cutoff_${ii}Ry
    if [ ! -d $work_dir ] ; then
        mkdir $work_dir
    else
        rm -r $work_dir/*
    fi
    sed -e "s/PH_project_name/${project_name}/g" \
        -e "s/PH_coord_file/${coord_file}/g" \
        -e "s/PH_abc/${abc}/g" \
        -e "s/PH_angles/${angles}/g" \
        -e "s/PH_restart_file/${restart_file}/g" \ # AKA ${project_name}-1.restart
        -e "s/PH_scf_guess/${scf_guess}/g" \
        -e "s/PH_converge_to/${converge_to}/g" \
        -e "s/PH_inner_convergence/${inner_convergence}/g" \
        -e "s/PH_outer_convergence/${outer_convergence}/g" \
        -e "s/PH_minimiser/${minimiser}/g" \
        -e "s/PH_preconditioner/${preconditioner}/g" \
        -e "s/PH_xc/${xc}/g" \
        -e "s/PH_max_inner_cycles/${max_inner_cycles}/g" \
        -e "s/PH_max_outer_cycles/${max_outer_cycles}/g" \
        -e "s/PH_cutoff/${cutoff}/g" \
        -e "s/PH_rel_cutoff/${rel_cutoff}/g" \
        -e "s/PH_extrapolation/${extrapolation}/g" \
        -e "s/PH_run_type/${run_type}/g" \
        $template_file > $work_dir/$template_file
    cp scol00_N0.cp2k $work_dir/scol00.cp2k
    #rm $work_dir/scol00.inp
done
