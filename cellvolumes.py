#!/usr/bin/python3
# cellvolumes.py
# Given an input file (in PDB format) and an optional list of cell parameters
# REQUIRES the cell parameters and each separate coordinate line to have SINGLE SPACES as separators!

import glob
import math
import os
import re
#import sys

# Function to print usage
def print_usage():
    print("Usage: python cellvolumes.py [a b c alpha beta gamma] inputFile.ext")

def grab_params(file):
    pattern = re.compile('CRYST1\s+(?P<a>[0-9\.]*)\s+(?P<b>[0-9\.]*)\s+(?P<c>[0-9\.]*)\s+(?P<alpha>[0-9\.]*)\s+(?P<beta>[0-9\.]*)\s+(?P<gamma>[0-9\.]*)')
    cell = []
    # lines = [re.split("\n+", line) for line in pdb]
    # [re.split('\s+', line) for line in lines]
    with open(file) as pdb:
        for line in pdb:
            for match in re.finditer(pattern, line):
                cell = [match.group('a'), match.group('b'), match.group('c'), match.group('alpha'), match.group('beta'), match.group('gamma')]
    return cell

# Function to find and store cell parameters
def cell_parameters(cellstuff):
    lengths = []
    for i in range(0, 3):
        lengths.append(float(cellstuff[i]))
    angles = []
    for i in range(3, 6):
        angles.append(math.radians(float(cellstuff[i])))
    return lengths, angles

def define_cell(lengths, angles):
    # See https://bytes.com/topic/python/answers/608886-lots-nested-if-statements
    comparisons = [lengths[0] == lengths[1] and lengths[1] == lengths[2] and lengths[2] == lengths[0], lengths[0] != lengths[1] and lengths[1] != lengths[2] and lengths[2] != lengths[0], angles[0] == (math.pi / 2), angles[1] == (math.pi / 2), angles[2] == (math.pi / 2), angles[0] == angles[1] and angles[1] == angles[2] and angles[2] == angles[0], angles[0] != angles[1] and angles[1] != angles[2] and angles[2] != angles[0], angles[0] == math.radians(120) or angles[1] == math.radians(120) or angles[2] == math.radians(120)]

    if comparisons[0] == True and comparisons[2:5] == [True, True, True]:
        celltype = 'cubic'
    if comparisons[0] == True and comparisons[2:5] == [False, False, False]:
        celltype = 'rhombohedral'
    if comparisons[0:2] == [False, False] and comparisons[2:5] == [True, True, True]:
        celltype = 'tetragonal'
    if comparisons[0:2] == [False, False] and comparisons[7] == True:
        celltype = 'hexagonal'
    if comparisons[0:2] == [False, False] and comparisons[5:7] == [False, False]:
        celltype = 'monoclinic'
    if comparisons[0] == False and comparisons[1] == True and comparisons[2:5] == [True, True, True]:
        celltype = 'orthorhombic'
    else:
        celltype = 'triclinic'
    return celltype
#    print(celltype)

def calculate_volume(lattice, lengths, angles):
    """Using the equations found on Wikipedia for different lattice types"""
    abc = lengths[0] * lengths[1] * (lengths[2]/2) # Because multicell 1 1 2 doesn't get kept in restart files, for some reason!
    if lattice == "cubic" or lattice == "tetragonal" or lattice == "orthorhombic":
        volume = abc
    if lattice == "hexagonal":
        volume = (float(math.sqrt(3))/float(2)) * abc
    if lattice == "rhombohedral":
        volume = abc * math.sqrt(1 - (3 * ((math.cos(angles[0]))**2)) + (2 * ((math.cos(angles[0]))**3)))
    if lattice == "monoclinic":
        volume = abc * math.sin(angles[2])
    if lattice == "triclinic":
        volume = abc * math.sqrt(1 - ((math.cos(angles[0]))**2) - ((math.cos(angles[1]))**2) - ((math.cos(angles[2]))**2) + (2 * math.cos(angles[0]) * math.cos(angles[1]) * math.cos(angles[2])))
    return volume

# Create the output strings and write them to the file
def write_output(infile, cellstuff, volume, outfile):
    formatted_line = "{0:<30} {1[0]:> 10.3f} {1[1]:> 10.3f} {1[2]:> 10.3f} {1[3]:> 10.2f} {1[4]:> 10.2f} {1[5]:> 10.2f} {2:> 14.6f}".format(infile, cellstuff, volume)

    with open(outfile, 'a+') as writer:
        writer.write(formatted_line)
        writer.write('\n')

# START OF ACTUAL SCRIPT
#if len(sys.argv) > 3:
#    *rest, infile = sys.argv[2:]
#else:
#    infile = sys.argv[2]

infiles = glob.glob('*-final.pdb')

for infile in infiles:
    # Grab one big list of the cell parameters from the given file
    cell_stuff = list(map(float, grab_params(infile)))

    # Make lists of lengths and angles, assigned at once, using cell_parameters
    lengths, angles = cell_parameters(cell_stuff)

    # Test printout
    #print("Cell is %s!" % define_cell(lengths, angles))

    # Grab volume from multi-function calls
    vol = calculate_volume(define_cell(lengths, angles), lengths, angles)

    # Split infile name for ease of tweaking for outfile use
    temp = os.path.splitext(infile)
    outfile = temp[0][0:5] + '_cellvol.ssv'
    #outfile = 'nat_gs_cellvol.ssv'
    write_output(os.path.basename(infile), cell_stuff, vol, outfile)
