#!/bin/bash
# link_words.sh
# Ensuring most recent versions of words are updated in both Google Drive and ~/words/ from either source

for file in "01_Introduction" "02_LitReview" "03_Methodology" "04_ResultsDiscussion" "05_PlanTimetable"; do
	if [ "$HOME"/Google\ Drive/"$file.tex" -ot "$HOME/words/$file.tex" ]; then
		cp "$HOME/words/$file.tex" "$HOME"/Google\ Drive/"$file.tex"
	fi
	ln -f "$HOME"/Google\ Drive/"$file.tex" "$HOME/words/$file.tex"
	chmod go-w "$HOME/words/$file.tex"
done
