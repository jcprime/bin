#!/bin/bash
# unlink_fyreport.sh

for file in gulp dehydrate cp2k gulp_parameters introduction literature_review methodology original_contribution plan_timetable; do
	cp ~/Google\ Drive/"$file".tex ~/Desktop/PhD/writings_mac/fyreport/mmd/temp"$file".tex
	rm ~/Desktop/PhD/writings_mac/fyreport/mmd/"$file".tex
	mv ~/Desktop/PhD/writings_mac/fyreport/mmd/temp"$file".tex ~/Desktop/PhD/writings_mac/fyreport/mmd/"$file".tex
	chmod go-w ~/Desktop/PhD/writings_mac/fyreport/mmd/"$file".tex
done

# From unlink_overleaf.sh

cp ~/Google\ Drive/introduction.tex          ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp01_Introduction.tex
cp ~/Google\ Drive/literature_review.tex     ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp02_Chapter2.tex
cp ~/Google\ Drive/methodology.tex           ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp03_Chapter3.tex
cp ~/Google\ Drive/original_contribution.tex ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp04_Chapter4.tex
cp ~/Google\ Drive/plan_timetable.tex        ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp05_Chapter5.tex

rm ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/01_Introduction.tex
rm ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/02_Chapter2.tex
rm ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/03_Chapter3.tex
rm ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/04_Chapter4.tex
rm ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/05_Chapter5.tex

mv ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp01_Introduction.tex ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/01_Introduction.tex
mv ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp02_Chapter2.tex     ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/02_Chapter2.tex 
mv ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp03_Chapter3.tex     ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/03_Chapter3.tex
mv ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp04_Chapter4.tex     ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/04_Chapter4.tex
mv  ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/temp05_Chapter5.tex    ~/Desktop/PhD/writings_mac/fyreport/tex_overleaf/05_Chapter5.tex
