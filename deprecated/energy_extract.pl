#!/usr/bin/perl -w

# DEPRECATED: USE ENERGY_EXTRACT.SH INSTEAD!

# script for getting useful stuff out of CP2K .log files
# includes Pressure*volume info for pressure calcs
# but not average psi calculation
# command line is: cp2k_extract.pl

if (glob("*.gin")) {
    print STDOUT "Looks like we're looking at GULP files here...";
    @Files = glob "*.gout";
}
if (glob("*.inp")) {
    print STDOUT "Looks like we're looking at CP2K files here...";
    @Files = glob "*.pdb";
}

#print "output filename?\n";
#chomp ($txtfile = <STDIN>);
#open TXTFILE, ">$txtfile" or die "Can't create $txtfile: $!\n";

printf STDOUT "%20s%16s\n",
  "Filename", "Energy (au)";

foreach $Files (@Files) {
    open OUTPUT, "<$Files" or die "Can't open $Files: $!\n";

    while (<OUTPUT>) {

        # Print filename
        #if (//) {
            printf STDOUT "\n%20s", $Files;
        #}

        # if (/SCF loop converged/) {
        #     printf STDOUT "%4s", "Y";
        # }

        # Looking for REMARK    Step [0-9]*, E = -4898.9478741620
        # Using scalar reverse() as suggested here: https://www.perl.com/pub/2001/05/01/expressions.html
        # $_ = scalar reverse($Files);
        if (/(REMARK)\s+(Step [0-9]*,)\s+(E = )(-?[\d.]*)(?!REMARK)/s) {
        # if (/([\d.]*-?)( = E)\s+(,[0-9]* petS)\s+(KRAMER)/) {
            $energy = $4;
            # $pcalc = 3;
            # $pv = 0.0;
            printf STDOUT "%16.8f", $energy;
        # } elsif (/(ENTHALPY\| Total .*: \s+) (.\d+.\d+)/) {
        #     $energy = $2;
        #     $pcalc = 5;
        #     printf STDOUT "%16.8f", $energy;
        # } else {
        #     $pcalc = 0
        }
    }
    close OUTPUT;
    # printf STDOUT "%16.8f", $pv;
    # print "Done $Files\n";
}
