/* structures.h */
/* Mine first then those from orig (tim) after for reference */

typedef struct
{
    int steric;
    double non_bonded;                      /* non-bonding energy */
    double vdw_rep;                         /* van der Waals repulsive */
    double vdw_disp;                        /* van der Waals dispersive */
    double charges;                         /* coulombic energy */
    int acceptance;
    double minimizer_init_total;            /* minimizer non-bonding energy */
    double minimizer_end_total;             /* minimizer total energy */
    double minimizer_init_nonbond;          /* minimizer non-bonding energy */
    double minimizer_end_nonbond;           /* minimizer total energy */
} energy;

typedef struct
{
    double stretch;
} internal_energy;

/* rudimentary statistics structure */
typedef struct
{
    int tries;
    int tries_after_build;
    int accepted;
    int accepted_after_build;
} stats;

/* structure for the atoms */
typedef struct
{
    char label[7];
    char type[6];
    double x;
    double y;
    double z;
    int flag_x;                             /* GULP optimisation flags */
    int flag_y;
    int flag_z;
    char pot[4];
    char group[5];
    char group_no[9];
    char elem[3];
    double charge;
    double part_chge;
    int nb_list;                            /* index of non-bonding parameters */
    double vdw;                             /* van der Waals radius of the atom */
    int num_neigh;                          /* number of neighbours */
    int neighb[5];                          /* initialise -1 = not bonded */
    int neighb_stretch_list[5];             /* index for intra stretch potential */
    double vdw_energy;                      /* this atom's vdW energy contribution to the total */
    int general_flag;                       /* multipurpose flag - used here for "dehydratable" */
} atom;

/* structure for simple atom */
typedef struct
{
    char label[7];
    char type[6];
    double coord[4];
} simple_atom;

typedef struct
{
    char atom1[3];
    char atom2[3];
} bond;

typedef struct
{
    char atom_type[3];
    int num;
} atom_number;

typedef struct
{
    int start;
    int end;
} links;

typedef struct
{
    double matrix[9];
    double translation[3];
} symm_ops;

typedef struct
{
    int start;
    int end;
    int num;
} list_partition;

typedef struct
{
    char name[3];
} types;

typedef struct
{
    double energy;
    int sites[100];
} hot_list;
