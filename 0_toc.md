Table of Contents
---------------------
0_toc.txt				Erm, this file right here... designed to stay at the top of the file list
awking.awk				?
bashsyntax.sh			?
bin.sublime-project		?
bin.sublime-workspace	?
car_to_xyz				?
colortest				?
coordinateTransform.py	?
cp2k					?
current_work.sublime-workspace
datagrab.sh				?
date_generator.py		?
date_generator.sh		?
dehydrated.sh			SYMLINK to editing/dehydrated.sh
dehydrate.sh			Sends a given set of calculations off to be GULPed - expects 2 arguments: <path> <waters> - now included in erm.sh
dehydrate_update.diff	?
doi_grabber.sh			?
emin.py					?
exercism				?
faff.sh					?
ganal					NOT WRITTEN BY ME!
gdis					Links to MBP gdis binary
general_info.pl			NOT WRITTEN BY ME!
general_info.py			Currently a copy of gulp_coords.py
gout_extract.pl			My edited version of general_info.pl for my preferred formatting (for ease of use in R)
gulp					Just in case... just a link to /usr/local/gulp/
gulp_batch.sh			Takes arguments <file prefix eg. "nat01"> <total H2O (or highest number after "_N"!)>
						Runs them through GULP front-end (ie. skipping the queue)
gulpconvert.sh			?
gulpconvert_fns.sh		?
gulp_coords.py			?
gulp_coords_xenon2017-06-17.py
highenergy.sh			?
history_backup.sh		?
imgur.py				?
jmol_cp2k.spt			?
jmol_export.spt			?
jmol_setup.sh			?
jmol_test.spt			?
jumping_into_c++.pdf	?
klmc					?
link_bib.sh				?
linkfiles_debugging		?
linkfiles.pc			?
linkfiles.sh			?
link_fyreport.sh		?
link_googledrive.sh		?
link_syreport.sh		?
linkwords.sh			?
md_setup.sh				?
musings.sh				?
precond_tests.md		?
pymol_install.sh		?
qgulp					Queues the given files (at the moment needs to be done via a loop, but still)
reader.sty				?
README.md				Explains what sorts of files can be expected to end up here!
rgulp					Called by qgulp in the queueing process...
rsync_exclusions		?
scholar.py				?
sort_downloads.sh		?
space_stealers.md		?
sub2srt					?
tabulate_energies.sh	?
tabulate.sh				?
tabulate_v2.sh			?
template.vim			?
tumblr_backup.py		?
unlink_fyreport.sh		?
updateall.sh			?
usefuls.sh				?
wirelesscheck.sh		?
words.sh				?
xterm					Links to MBP /Applications/Utilities/XQuartz.app/Contents/MacOS/X11
xyz_dehydrate.sh		?

backups/
	.qgulp.bak					Backup of qgulp pre-editing for my specific user details
	qgulp_working.bak			Backup of qgulp post-editing for my specific user details
basic_blocks/
	0_toc.txt					Sub-ToC for the given subdirectory, obviously
	argument_testing.txt		?
	arrays.txt					?
	brace_expansions.txt		?
	bracketing.txt				?
	case_statements.txt			?
	C_Functions.md				?
	comparison_operators.txt	?
	conditional_expressions.txt	?
	copy_from_clipboard.txt		?
	cron.txt					?
	c_to_cpp.sh					?
	exit_status.txt				?
	for_loops.txt				?
	functions.txt				?
	if_elif_fi.txt				?
	looping_by_line.txt			?
	select_statements.txt		?
	special_characters.txt		?
	strings.txt					?
	tilde_expansion.txt			?
	until_loops.txt				?
	useful_commands.txt			?
	variables.txt				?
	while_loops.txt				?
DeHydrate/
	dehydrate.c		Copy of original ~/DeHydrate/Code/dehydrate.c file for edit-testing attempts
dehydrate_update/
deprecated/
	erm_copy_v2.py	?
	erm_copy_v2.sh	?
	erm.sh			Does all the faffy bits for a given hydration step:
					Checks no failures have happened in previous step
					Outputs "grep 'Final ene' *out | sort -n -k5" to new file in gs/ directory
					Takes lowest-E dumpfile of previous step
					Copies to new input file for next step
	from_export_images.sh
	gulp_convert.py	?
	template.sh		The original line taken from ~/Tanya/Mesolite/16/meso_16.csh, which was altered and expanded on in dehydrate.sh
	test.sh			?
	testagain.sh	?
	xenontest.sh	?
	dehydrate_cpp/
	dehydrate_update_phd/
editing/
	blues.vim					?
	datamine_runjob_edit.sh		?
	dehydrated.sh				?
	erm_cp2k.sh					?
	export_images.sh			?
	qstep						?
	rstep						?
	set_up_cp2k_files.sh		?
	setup.sh					?
	structure_splodge.sh		?
	tabulate_energies.sh		?
	template_replacements.sh	?
	tex_edit.sh					?
	tex_to_txt.sh				?
etc/
	Add_all_in_one			?
	append_data.pl			?
	charge_replace			?
	cleaner					?
	copy_to_platinum		?
	c.sh					?
	dewi.input				?
	dewi.run				?
	dmol.run				?
	dos2unix				?
	energy.csh				?
	energy_extractor		?
	fillspec.csh			?
	Gaussian_output_to_PDB	?
	gaussian_xenon_run		?
	gmin.csh				?
	gulp_analysis.pl		?
	gulp_intro.txt			?
	help.txt				?
	HOW_TO_COMPRESS			?
	HOW_TO_Q				?
	md_anal.pl				?
	multi_xtlanal			?
	newzmat					?
	perlscript				?
	pots.gin				?
	qabinit					?
	q_dmol.sh				?
	qgauss					?
	qruncry06				?
	qsiesta					?
	queue					?
	qzebedde				?
	rgauss					?
	rsiesta					?
	rzebedde				?
	sge.txt					?
	tarch					?
	tdevice					?
	tsites.txt				?
	xenon_DFT_run			?
	xenon_opt_-1_run		?
	jw/
jmol-full/
	Contains full copy of the most up-to-date Jmol stuff, because it's not on Xenon and just in case again...
manpages/
	bash.txt	?
	cron.txt	?
	rsync.txt	?
pdfs/
pwtools/
python/
	codewars/
	datascience/
	ggplot_demos/
	paintracker/
	researchpython/
	rodeo/
	stats/
scripts/
	backups/
	deprecated/
	general_info.pl		?
	scott/
stats/
	1_RBasics.R			?
	datavis_ggplot2/
	isaacfloof/
	phd/
	repres2016/
	repres2017/
	RStudioIntroCourse/
Thesis_LevBishop/
tips/
	install_certificates_ubuntu.txt		?
	install_packages_from_dep.txt		?
