/************************************************************************/
/* move_file : Utility to rename .dump file to new input file           */
/* DWL 19/12/00                                                         */
/* takes filename.anything then does "mv filename.dump filename_MIN.gin */
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define DEHYDRATE "/home/dwl22/DeHydrate/Code/dehydrate"
#define GIN "gin"
#define DUMP "dump"
#define BUFFER 256

void main(argc, argv)
    int   argc;
    char  **argv;
{
    char *fullstop;
    char filename_root[BUFFER];
    char output_file[BUFFER];
    char dump_file[BUFFER];
    char command[BUFFER];

    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s  file\n", argv[0]);
        exit(1);
    }
    else
    {
        strcpy(filename_root, argv[1]);
    }

    if ((fullstop = strchr(filename_root, '.')) != NULL)
        *(++fullstop) = '\0' ;
    /* now take off the stop */
    *(--fullstop) = '\0' ;

    printf("root: %s\n", filename_root);
    sprintf(dump_file, "%s.%s", filename_root, DUMP);
    sprintf(output_file, "%s_min.%s", filename_root, GIN);

    sprintf(command, "cp -f %s %s", dump_file, output_file);
    printf("%s\n", command);
    system (command);

    /***now run dehydrate on it ***/
    sprintf(command, "%s %s_min", DEHYDRATE, filename_root);
    printf("%s\n", command);
    system (command);
}

