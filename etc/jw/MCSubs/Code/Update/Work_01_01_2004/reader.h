enum { PRIME_DIRECTIVE= 1, SECONDARY_DIRECTIVE, TERTIARY_DIRECTIVE, REMAINING_DIRECTIVE };

enum {TITLE = 1, 
	CELL_FILE, POTS_FILE, MAX_CYCLES,
        REPLACE, SUBSTITUENT, CONFIGS,
	GULP, RATIO, CATION,
	LOWENSTEIN, 
        MAX_SUBS,
        PEEK_FREQ,  INSERT,
        FIXED_CATION,
        CELL_VOLUME_FLAGS,
        KEEP_LIST, 
        GENERATE, 
        REGENERATE, 
        MAKE_CFG,
        SCRIPT, 
        RANDOM};

enum {ATTEMPTS = 101, 
        GULP_MAX_CYCLES,
        GULP_FILE,
        GULP_PATH,
        GULP_TIME,
        GULP_COMMAND,
        SCRIPT_RUN,
        SCRIPT_QSUB,
	ON2, OFF2};


enum {MAYBE = 201,
        ON3, OFF3};


enum {DEFAULT = 666};    
enum {UNIT = 998};    
enum {PARSE = 1001};

enum {BLANK_DIRECT = 999};

/*** note two defintions of non_bonded as nonb and non_ 21.10.96 DWL ***/
#define FIRST_DIRECTIVE_LIST \
	"titl", TITLE,           "cell", CELL_FILE,      \
	"pots", POTS_FILE,  "cycl", MAX_CYCLES, \
	"lowe", LOWENSTEIN,\
        "subs", MAX_SUBS,\
    "rati", RATIO, \
    "keep", KEEP_LIST, \
    "gene", GENERATE, \
    "conf", CONFIGS, \
    "rege", REGENERATE, \
    "make", MAKE_CFG, \
    "peek", PEEK_FREQ, \
    "rand", RANDOM, \
    "cati", CATION, \
    "repl", REPLACE, \
    "inse", INSERT, \
    "gulp", GULP, \
    "scri", SCRIPT, \
    "fixe", FIXED_CATION,\
    "volu", CELL_VOLUME_FLAGS,\
    "",BLANK_DIRECT
                        

#define SECOND_DIRECTIVE_LIST \
        "time", GULP_TIME,\
        "file", GULP_FILE,\
        "path", GULP_PATH,\
        "comm", GULP_COMMAND,\
        "run", SCRIPT_RUN,\
        "qsub", SCRIPT_QSUB,\
        "maxc", GULP_MAX_CYCLES,\
        "on", ON2, \
        "of", OFF2,  \
	"",BLANK_DIRECT

/*** NOTE replacing of yes/no wiht on/off which solves probs  21.10.96 DWL ***/
#define THIRD_DIRECTIVE_LIST \
       "ma", MAYBE,\
           "on", ON3, \
           "of", OFF3, \
	   "", BLANK_DIRECT

#define NULL_DIRECTIVE_LIST \
       "an", UNIT,             "(a", UNIT,           \
       "(c", UNIT,             "k ", UNIT,           "(k",  UNIT, \
       "#", PARSE, "", BLANK_DIRECT

typedef struct 
{
  char *directive;
  int token_index;
}list;

