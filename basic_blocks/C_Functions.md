# `strcpy`
## Copy string
### Syntax: `char * strcpy ( char * destination, const char * source )`;
* Copies the C string pointed by `source` into the array pointed by `destination`, including the terminating null character (and stopping at that point)
* To avoid overflows, the size of the array pointed by `destination` shall be long enough to contain the same C string as `source` (including the terminating null character), and should not overlap in memory with `source`
* Returns `destination`

# `fopen`
## Open file
### Syntax: `FILE * fopen ( const char * filename, const char * mode )`;
* Opens the file whose name is specified in the parameter `filename` and associates it with a stream that can be identified in future operations by the `FILE` pointer returned
* The operations that are allowed on the stream and how these are performed are defined by the `mode` parameter
* The returned stream is fully buffered by default if it is known to not refer to an interactive device (see `setbuf`)
* The returned pointer can be disassociated from the file by calling `fclose` or `freopen`. All opened files are automatically closed on normal program termination.
* The running environment supports at least `FOPEN_MAX` files open simultaneously.
Where `mode` can be:
"r"     read: Open file for input operations. The file must exist.
"w"     write: Create an empty file for output operations. If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
"a"     append: Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist.
"r+"    read/update: Open a file for update (both for input and output). The file must exist.
"w+"    write/update: Create an empty file and open it for update (both for input and output). If a file with the same name already exists its contents are discarded and the file is treated as a new empty file.
"a+"    append/update: Open a file for update (both for input and output) with all output operations writing data at the end of the file. Repositioning operations (fseek, fsetpos, rewind) affects the next input operations, but output operations move the position back to the end of file. The file is created if it does not exist.
* With the mode specifiers above, the file is open as a text file
* In order to open a file as a binary file, a "b" character has to be included in the mode string
* This additional "b" character can either be appended at the end of the string (thus making the following compound modes: "rb", "wb", "ab", "r+b", "w+b", "a+b") or be inserted between the letter and the "+" sign for the mixed modes ("rb+", "wb+", "ab+")

# `fgets`
## Get string from stream
### Syntax: `char * fgets ( char * str, int num, FILE * stream )`;
* Reads characters from `stream` and stores them as a C string into `str` until (`num-1`) characters have been read or either a newline or the end-of-file is reached, whichever happens first.
* A newline character makes fgets stop reading, but it is considered a valid character by the function and included in the string copied to `str`.
* A terminating `null` character is automatically appended after the characters copied to `str`.
* Notice that `fgets` is quite different from `gets`: not only `fgets` accepts a stream argument, but also allows to specify the maximum size of `str` and includes in the string any ending newline character.

### Parameters
`str`: Pointer to an array of chars where the string read is copied.
`num`: Maximum number of characters to be copied into `str` (including the terminating null-character).
`stream`: Pointer to a `FILE` object that identifies an input stream. (`stdin` can be used as argument to read from the standard input)

# `strstr` --> SUPERCEDED BY `STD::STRING::FIND`
## Locate substring
### Syntax:  `const char * strstr ( const char * str1, const char * str2 )`;
###                `char * strstr (       char * str1, const char * str2 )`;
* Returns a pointer to the first occurrence of `str2` in `str1`, or a null pointer if `str2` is not part of `str1`.
* The matching process does not include the terminating null-characters, but it stops there.

### Parameters
`str1`: C string to be scanned.
`str2`: C string containing the sequence of characters to match.

###Return value
A pointer to the first occurrence in str1 of the entire sequence of characters specified in str2, or a null pointer if the sequence is not present in str1.

# `sscanf` --> `std::istream::getline`
## Read formatted data from string
### Syntax: `int sscanf ( const char * s, const char * format, ...)`;
* Reads data from `s` and stores them according to parameter `format` into the locations given by the additional arguments, as if `scanf` was used, but reading from `s` instead of the standard input (`stdin`).
* The additional arguments should point to already allocated objects of the type specified by their corresponding format specifier within the format string.

### Parameters
`s`: C string that the function processes as its source to retrieve the data.
`format`: C string that contains a format string that follows the same specifications as `format` in scanf (see scanf for details).
... (additional arguments): Depending on the format string, the function may expect a sequence of additional arguments, each containing a pointer to allocated storage where the interpretation of the extracted characters is stored with the appropriate type. There should be at least as many of these arguments as the number of values stored by the `format` specifiers. Additional arguments are ignored by the function.

### Return value
* On success, the function returns the number of items in the argument list successfully filled. This count can match the expected number of items or be less (even zero) in the case of a matching failure.
* In the case of an input failure before any data could be successfully interpreted, `EOF` is returned.

# `scanf` takes "%lf" for double --> `std::getline`
## Read formatted data from stdin
### Syntax: `int scanf ( const char * format, ... )`;
* Reads data from `stdin` and stores them according to the parameter `format` into the locations pointed by the additional arguments.
* The additional arguments should point to already allocated objects of the type specified by their corresponding `format` specifier within the format string.

### Parameters
`format`: C string that contains a sequence of characters that control how characters extracted from the stream are treated:

* Whitespace character: the function will read and ignore any whitespace characters encountered before the next non-whitespace character (whitespace characters include spaces, newline and tab characters -- see isspace). A single whitespace in the format string validates any quantity of whitespace characters extracted from the stream (including none).
* Non-whitespace character, except format specifier (%): Any character that is not either a whitespace character (blank, newline or tab) or part of a format specifier (which begin with a % character) causes the function to read the next character from the stream, compare it to this non-whitespace character and if it matches, it is discarded and the function continues with the next character of format. If the character does not match, the function fails, returning and leaving subsequent characters of the stream unread.
* Format specifiers: A sequence formed by an initial percentage sign (%) indicates a format specifier, which is used to specify the type and format of the data to be retrieved from the stream and stored into the locations pointed by the additional arguments.

**A format specifier for scanf follows this prototype**:
`%[*][width][length]specifier`
Where the specifier character at the end is the most significant component, since it defines which characters are extracted, their interpretation and the type of its corresponding argument.
See [here](http://www.cplusplus.com/reference/cstdio/scanf/) for more details.

### Return value
* On success, the function returns the number of items of the argument list successfully filled. This count can match the expected number of items or be less (even zero) due to a matching failure, a reading error, or the reach of the end-of-file.
* If a reading error happens or the end-of-file is reached while reading, the proper indicator is set (`feof` or `ferror`). And, if either happens before any data could be successfully read, `EOF` is returned.
* If an encoding error happens interpreting wide characters, the function sets `errno` to `EILSEQ`.

# `sprintf`
## Write formatted data to string
### Syntax: `int sprintf ( char * str, const char * format, ... )`;
* Composes a string with the same text that would be printed if `format` was used on `printf`, but instead of being printed, the content is stored as a C string in the buffer pointed by `str`.
* The size of the buffer should be large enough to contain the entire resulting string (see `snprintf` for a safer version).
* A terminating null character is automatically appended after the content.
* After the `format` parameter, the function expects at least as many additional arguments as needed for `format`.

### Parameters
`str`: Pointer to a buffer where the resulting C-string is stored.  The buffer should be large enough to contain the resulting string.
`format`: C string that contains a format string that follows the same specifications as `format` in `printf` (see `printf` for details).
... (additional arguments): Depending on the format string, the function may expect a sequence of additional arguments, each containing a value to be used to replace a format specifier in the format string (or a pointer to a storage location, for `n`). There should be at least as many of these arguments as the number of values specified in the format specifiers. Additional arguments are ignored by the function.

### Return value
* On success, the total number of characters written is returned. This count does not include the additional null-character automatically appended at the end of the string.
* On failure, a negative number is returned.

# `fprintf`
## Write formatted data to stream
### Syntax: `int fprintf ( FILE * stream, const char * format, ... )`;
* Writes the C string pointed by `format` to the `stream`. If `format` includes format specifiers (subsequences beginning with %), the additional arguments following `format` are formatted and inserted in the resulting string replacing their respective specifiers.
* After the `format` parameter, the function expects at least as many additional arguments as specified by `format`.

### Parameters
`stream`: Pointer to a FILE object that identifies an output stream.
`format`: C string that contains the text to be written to the stream.  It can optionally contain embedded format specifiers that are replaced by the values specified in subsequent additional arguments and formatted as requested.

**A format specifier follows this prototype**:
`%[flags][width][.precision][length]specifier`
Where the specifier character at the end is the most significant component, since it defines the type and the interpretation of its corresponding argument:
specifier   Output                                              Example
d or i      Signed decimal integer                              392
u           Unsigned decimal integer                            7235
o           Unsigned octal                                      610
x           Unsigned hexadecimal integer                        7fa
X           Unsigned hexadecimal integer (uppercase)            7FA
f           Decimal floating point, lowercase                   392.65
F           Decimal floating point, uppercase                   392.65
e           Scientific notation (mantissa/exponent), lowercase  3.9265e+2
E           Scientific notation (mantissa/exponent), uppercase  3.9265E+2
g           Use the shortest representation: %e or %f           392.65
G           Use the shortest representation: %E or %F           392.65
a           Hexadecimal floating point, lowercase               -0xc.90fep-2
A           Hexadecimal floating point, uppercase               -0XC.90FEP-2
c           Character                                           a
s           String of characters                                sample
p           Pointer address                                     b8000000
n           Nothing printed.
            The corresponding argument must be a pointer to a
            signed int. The number of characters written so
            far is stored in the pointed location.
%           A % followed by another % character will write a    %
            single % to the stream.
See [here](http://www.cplusplus.com/reference/cstdio/fprintf/) for more information

### Return value
* On success, the total number of characters written is returned.
* If a writing error occurs, the error indicator (`ferror`) is set and a negative number is returned.
* If a multibyte character encoding error occurs while writing wide characters, `errno` is set to `EILSEQ` and a negative number is returned.

# `strcat`
## Concatenate strings
### Syntax: `char * strcat ( char * destination, const char * source )`;
* Appends a copy of the `source` string to the `destination` string. The terminating null character in `destination` is overwritten by the first character of `source`, and a null-character is included at the end of the new string formed by the concatenation of both in destination.
* `destination` and `source` shall not overlap.

### Parameters
`destination`: Pointer to the destination array, which should contain a C string, and be large enough to contain the concatenated resulting string.
`source`: C string to be appended. This should not overlap `destination`.

### Return value
`destination`