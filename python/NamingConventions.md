# [Python naming conventions](https://www.python.org/dev/peps/pep-0008/)

Thing             | Case
------------------|----------------------------------------------------------
Class names       | `StudlyCaps` (aka `CapWords`)
Exception names   | `StudlyCaps` (because exceptions actually *are* classes)
Constants         | `ALL_CAPS_WITH_UNDERSCORES`
Variable names    | `snake_case`
Method names      | `snake_case`
Function names    | `snake_case`
Module names      | `snake_case` or `lowercasewithoutunderscores`
Package names     | `lowercasewithoutunderscores`
Public attributes | No leading underscore(s)


* Use `_single_leading_underscore` for weak "internal use" indicators; `from blah import *` will not import anything with a leading underscore
* Use `single_trailing_underscore_` to distinguish from Python keywords and thus avoid conflicts, e.g. `class_`
* Use `__double_leading_underscore` for naming class attributes; invokes *name mangling*
* Use `__double_leading_and_trailing_underscore__` for "magic" objects or attributes that live in user-controlled namespaces, such as `__init__`, `__import__`, or `__file__`
