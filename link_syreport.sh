#!/bin/bash
# link_syreport.sh

for file in "01_Introduction" "02_LitReview" "03_Methodology" "04_ResultsDiscussion" "05_PlanTimetable"; do
	ln -f ~/Google\ Drive/"$file".tex ~/Desktop/PhD/writings_mac/syreport/tex_plain/"$file".tex
	chmod go-w ~/Desktop/PhD/writings_mac/syreport/tex_plain/"$file".tex
	ln -f ~/Google\ Drive/"$file".tex ~/Desktop/PhD/writings_mac/syreport/tex_thesis/"$file".tex
        chmod go-w ~/Desktop/PhD/writings_mac/syreport/tex_thesis/"$file".tex
done
