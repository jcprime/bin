
/*************************************************************************/
/* potent.c                                                               */
/* Sets up the non-bonding parameters and calculates intermediate values */
/* Mode of calculation determined by input file                          */
/*                                                                       */
/*                                                                       */
/* Started DWL 27/11/94                                                  */
/*************************************************************************/


/***** I rewrote this on 13/4/95 to take pointers ******/
/***** steric_setup assigns vdw radii*****/
/***** setup_nonbond calcs the intermediate bits for NB calcs ****/
/***** assign_nonbond assigns the pot type to a structure of atoms *****/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "maxima.h"
#include "global_values.h"
#include "structures.h"
#include "data.h"

void steric_setup(atom *p_molecule, int num_atoms)
{
#include "header.h"
atom *p_atom;

int i,j;
BOOLEAN found_atom_flag;

/****** Loop through the atom structure                **********/
/******     calculate the non-bonding parameters       **********/
/******     and assign van der Waals radii             **********/

/****** Loop through ***********************************/

for (i=0; i<num_atoms;i++)
	{

	found_atom_flag = FALSE;
	p_atom = p_molecule + i;

	for (j=0;j<NUM_ELEMENTS;j++)
		{
		if ((strcmp(p_atom->elem, period_table[j].elem)) == 0)
			{
			p_atom->vdw = period_table[j].vdw;
			found_atom_flag = TRUE;	
			break;
			}
		}
	/*** if we haven't found the right element then Serious Error ***/
	if (found_atom_flag == FALSE)
		{
		fprintf(output_fp,"ILLEGAL Element\n");
		fprintf(output_fp,"Atom number %i, element given as %s\n",
							 i,p_atom->elem);
		fprintf(output_fp,"Aborting......\n");
                fflush(output_fp);
                fflush(stdout);
		exit(1);
		}
	}

return;
}

