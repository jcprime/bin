#!/bin/bash
# checkallgitrepos.sh
# Credit to stackoverflow.com for this (originally as a one-liner) in a comment somewhere!
# If I can find the link, I'll update this bit!

for d in `find ~ -name ".git"`; do
	cd "$d"/..
	echo `pwd`:
	git status
	echo
done
