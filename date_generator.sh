#!/bin/bash
# date_generator.sh
# Automates creation of my monthly log LaTeX files

# Print usage if first argument is "-h"
if [[ "$1" == "-h" ]]; then
    echo "Usage: date_generator.sh [start_month-start_year] [end_month-end_year]" && exit
fi

# Assign array values, depending on arguments or lack thereof
if [[ "$#" -gt "0" ]]; then
    start="$1"
    end="$2"
    from_month="${start%-*}"
    from_year="${start#*-}"
    to_month="${end%-*}"
    to_year="${end#*-}"
    declare -a from=("$from_year" "$from_month")
    declare -a to=("$to_year" "$to_month")
else
    declare -a from=("2017" "05")
    declare -a to=("2018" "09")
fi

# Set path for output files to go
path="$HOME/log"
#echo "Output files to go into $path" # TEST

# Return number of days for the month in question
function how_many_days() {
    month_num="$1"
    if [[ "$#" -eq "2" ]]; then
        year_num="$2"
    fi
    # Set number of days in each month
    if [[ "$month_num" -eq "1" ]] || [[ "$month_num" -eq "3" ]] || [[ "$month_num" -eq "5" ]] || [[ "$month_num" -eq "7" ]] || [[ "$month_num" -eq "8" ]] || [[ "$month_num" -eq "10" ]] || [[ "$month_num" -eq "12" ]]; then
        total_days="31"
    elif [[ "$month_num" -eq "4" ]] || [[ "$month_num" -eq "6" ]] || [[ "$month_num" -eq "9" ]] || [[ "$month_num" -eq "11" ]]; then
        total_days="30"
    elif [[ "$month_num" -eq "2" ]]; then
        if [[ "$((10#$year_num%4))" -eq "0" ]]; then
            total_days="29"
        else
            total_days="28"
        fi
    fi
    #echo "Total days in $month_num = $total_days" # TEST
    #return "$total_days" # REMOVED because variable got empty string from return on its own
    echo "$total_days" # ... but this works just fine!
}

# Loop over years, months, and days, formatting dates into my monthly log files
year=${from[0]}
while [[ "${year}" -le "${to[0]}" ]]; do
    if [[ "${year}" -eq "${from[0]}" ]]; then
        month="$((10#${from[1]}))"
    else
        month="01"
    fi
    if [[ "${year}" -eq "${to[0]}" ]]; then
        endmonth="$((10#${to[1]}))"
    else
        endmonth="12"
    fi
    while [[ "${month}" -le "${endmonth}" ]]; do
        month_padded="$(printf '%02d' "${month}")"
        if [[ -f "${path}/${year}-${month_padded}.tex" ]]; then
            echo "${path}/${year}-${month_padded}.tex already exists!"
            echo "Exiting..."
            continue
        else
            touch "${path}/${year}-${month_padded}.tex"
        fi
        day="1"
        endday="$(how_many_days "${month}" "${year}")"
        #echo "Last day = ${endday}" # TEST
        while [[ "${day}" -le "${endday}" ]]; do
            # NB: Details of the "-v" bits must be given in the year-month-day order, because otherwise it won't allow 31d if following on from a non-31-day month (as it adjusts one variable at a time, every time!)
            { echo "$(date "-v${year:(-2)}y" "-v${month}m" "-v${day}d" "+\\subsection*{%A~%d\\stroke{}%m\\stroke{}%Y}%n\\label{%Y-%m-%d}%n%n%n")" ; echo ; echo ; echo ; } >> "${path}/${year}-${month_padded}.tex"
            #>> "${path}/${year}-${month_padded}.tex" # Doesn't output to stdout as well
			# Removed: | tee -a "${path}/${year}-${month_padded}.tex" # Does output to stdout as well (safest for testing!)
            day=$((10#${day}+1))
        done
        month=$((10#${month}+1))
    done
    year=$((year+1))
done
