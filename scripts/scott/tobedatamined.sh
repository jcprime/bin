#!/bin/bash
# tobedatamined.sh

path="faraday_ignore/dmol/datamine" # Path to datamined structures
path2="faraday_ignore/dmol" # Path to directories for editing

M=("Li" "Na" "K" "Rb")
X=("F" "Cl" "Br" "I")

# Defined existing structures to be used for datamining, to allow the later if-elif conditions to edit the respective run.job files correctly
MX=("LiF" "NaCl" "KF" "RbBr")

argc=$#
if [[ "$argc" == 0 ]]; then
 a="0"
# Loops over M
 while [[ "$a" -lt "4" ]]; do
  b="0"
# Loops over X
  while [[ "$b" -lt "4" ]]; do
# Loops over cluster size
   for i in {1..24}; do
# Makes directories for the remaining clusters and copies in the general information for them from the template directory
    mkdir ~/"$path2"/"${M[a]}{X[b]}"_"$i"
    cp -rf ~/faraday_ignore/template/* ~/"$path2"/"${M[a]}{X[b]}"_"$i"
    c="0"
# Loops over rank
    for rank in {1..10}; do
# Copies all the files of the given rank and size (for LiF, NaCl, KF, and RbBr) into their respective restart directories by CALLING DATAMINE_RUNJOB_EDIT.SH
     while [[ "$c" -lt "4" ]]; do
      ~/james_p_rsync/bin/datamine_runjob_edit.sh "$a" "$b" "$c" "$i" "$rank"
     done
    done
   done
   a=$((a+1))
  done
  b=$((b+1))
 done
else
 echo "Something's gone wrong.  Sorry."
fi
echo "Finished."
