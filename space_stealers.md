total 176
 0 drwxr-xr-x  14 jcprime  staff    476  4 Oct 16:35 .git
 8 -rw-r--r--   1 jcprime  staff     14  4 Oct 16:30 .gitignore
 8 -rw-r--r--   1 jcprime  staff      6  4 Oct 16:18 README.md
 0 drwxr-xr-x@ 32 jcprime  staff   1088  4 Oct 16:27 bin
 8 -rwxr-xr-x   1 jcprime  staff    502 14 May 23:19 boot_disk_creation.md
16 -rwxr-xr-x   1 jcprime  staff   4971  4 Oct 16:23 imgur.py
 8 -rwxr-xr-x   1 jcprime  staff    547 25 Jun 20:48 link_bib.sh
 8 -rwxr-xr-x   1 jcprime  staff    887 25 Jun 20:50 link_fyreport.sh
 8 -rwxr-xr-x   1 jcprime  staff    480 25 Jun 21:10 link_syreport.sh
 8 -rwxr-xr-x   1 jcprime  staff    135 14 May 19:48 rsync_exclusions
96 -rwxr-xr-x   1 jcprime  staff  48658 10 May 12:46 scholar.py
 0 -rw-r--r--   1 jcprime  staff      0  4 Oct 16:36 space_stealers.md
 8 -rwxr-xr-x   1 jcprime  staff   2149 25 Jun 20:15 unlink_fyreport.sh

./.git:
total 448
  8 -rw-r--r--    1 jcprime  staff      57  4 Oct 16:29 COMMIT_EDITMSG
  8 -rw-r--r--    1 jcprime  staff      95  4 Oct 16:18 FETCH_HEAD
  8 -rw-r--r--    1 jcprime  staff      23  4 Oct 16:16 HEAD
  0 drwxr-xr-x    2 jcprime  staff      68  4 Oct 16:16 branches
  8 -rw-r--r--    1 jcprime  staff     245  4 Oct 16:17 config
  8 -rw-r--r--    1 jcprime  staff      73  4 Oct 16:16 description
  0 drwxr-xr-x   11 jcprime  staff     374  4 Oct 16:16 hooks
408 -rw-r--r--    1 jcprime  staff  206867  4 Oct 16:35 index
  0 drwxr-xr-x    3 jcprime  staff     102  4 Oct 16:16 info
  0 drwxr-xr-x    4 jcprime  staff     136  4 Oct 16:18 logs
  0 drwxr-xr-x  260 jcprime  staff    8840  4 Oct 16:28 objects
  0 drwxr-xr-x    5 jcprime  staff     170  4 Oct 16:18 refs

./.git/branches:

./.git/hooks:
total 80
 8 -rwxr-xr-x  1 jcprime  staff   478  4 Oct 16:16 applypatch-msg.sample
 8 -rwxr-xr-x  1 jcprime  staff   896  4 Oct 16:16 commit-msg.sample
 8 -rwxr-xr-x  1 jcprime  staff   189  4 Oct 16:16 post-update.sample
 8 -rwxr-xr-x  1 jcprime  staff   424  4 Oct 16:16 pre-applypatch.sample
 8 -rwxr-xr-x  1 jcprime  staff  1642  4 Oct 16:16 pre-commit.sample
 8 -rwxr-xr-x  1 jcprime  staff  1348  4 Oct 16:16 pre-push.sample
16 -rwxr-xr-x  1 jcprime  staff  4951  4 Oct 16:16 pre-rebase.sample
 8 -rwxr-xr-x  1 jcprime  staff  1239  4 Oct 16:16 prepare-commit-msg.sample
 8 -rwxr-xr-x  1 jcprime  staff  3610  4 Oct 16:16 update.sample

./.git/info:
total 8
8 -rw-r--r--  1 jcprime  staff  240  4 Oct 16:16 exclude

./.git/logs:
total 8
8 -rw-r--r--  1 jcprime  staff  704  4 Oct 16:29 HEAD
0 drwxr-xr-x  4 jcprime  staff  136  4 Oct 16:18 refs

./.git/logs/refs:
total 0
0 drwxr-xr-x  3 jcprime  staff  102  4 Oct 16:18 heads
0 drwxr-xr-x  3 jcprime  staff  102  4 Oct 16:18 remotes

./.git/logs/refs/heads:
total 8
8 -rw-r--r--  1 jcprime  staff  704  4 Oct 16:29 master

./.git/logs/refs/remotes:
total 0
0 drwxr-xr-x  3 jcprime  staff  102  4 Oct 16:18 origin

./.git/logs/refs/remotes/origin:
total 8
8 -rw-r--r--  1 jcprime  staff  320  4 Oct 16:24 master

./.git/objects:
total 0
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 00
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 01
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 02
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 03
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 04
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 05
0 drwxr-xr-x  16 jcprime  staff  544  4 Oct 16:28 06
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 07
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 08
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 09
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 0a
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 0b
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 0c
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 0d
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 0e
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 0f
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 10
0 drwxr-xr-x  14 jcprime  staff  476  4 Oct 16:29 11
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:29 12
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 13
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 14
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 15
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 16
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 17
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 18
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 19
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:27 1a
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 1b
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:28 1c
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 1d
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 1e
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 1f
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 20
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 21
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 22
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 23
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:29 24
0 drwxr-xr-x  14 jcprime  staff  476  4 Oct 16:29 25
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 26
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 27
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 28
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 29
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 2a
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 2b
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 2c
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 2d
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:28 2e
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 2f
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 30
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 31
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:29 32
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 33
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 34
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:28 35
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 36
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 37
0 drwxr-xr-x  13 jcprime  staff  442  4 Oct 16:29 38
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 39
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 3a
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 3b
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 3c
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 3d
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 3e
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 3f
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 40
0 drwxr-xr-x  13 jcprime  staff  442  4 Oct 16:29 41
0 drwxr-xr-x   3 jcprime  staff  102  4 Oct 16:28 42
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 43
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 44
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 45
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 46
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 47
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 48
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 49
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 4a
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 4b
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 4c
0 drwxr-xr-x  18 jcprime  staff  612  4 Oct 16:28 4d
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 4e
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 4f
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 50
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:28 51
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 52
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 53
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 54
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 55
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 56
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 57
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 58
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 59
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 5a
0 drwxr-xr-x  13 jcprime  staff  442  4 Oct 16:29 5b
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 5c
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:35 5d
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 5e
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 5f
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 60
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 61
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 62
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 63
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 64
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 65
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 66
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 67
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 68
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 69
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 6a
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 6b
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 6c
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 6d
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 6e
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 6f
0 drwxr-xr-x  13 jcprime  staff  442  4 Oct 16:28 70
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:28 71
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 72
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 73
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 74
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 75
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 76
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 77
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:29 78
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 79
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 7a
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 7b
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 7c
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 7d
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 7e
0 drwxr-xr-x  13 jcprime  staff  442  4 Oct 16:29 7f
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:28 80
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:28 81
0 drwxr-xr-x   3 jcprime  staff  102  4 Oct 16:27 82
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 83
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 84
0 drwxr-xr-x   3 jcprime  staff  102  4 Oct 16:28 85
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 86
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 87
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 88
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 89
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 8a
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 8b
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 8c
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 8d
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 8e
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 8f
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 90
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 91
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 92
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 93
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 94
0 drwxr-xr-x   3 jcprime  staff  102  4 Oct 16:28 95
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 96
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 97
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 98
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:28 99
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 9a
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 9b
0 drwxr-xr-x  17 jcprime  staff  578  4 Oct 16:28 9c
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 9d
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 9e
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 9f
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 a0
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 a1
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 a2
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 a3
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:29 a4
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 a5
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 a6
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 a7
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 a8
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 a9
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 aa
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 ab
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 ac
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 ad
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 ae
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 af
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:29 b0
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 b1
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 b2
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 b3
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 b4
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 b5
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 b6
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 b7
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 b8
0 drwxr-xr-x  13 jcprime  staff  442  4 Oct 16:28 b9
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 ba
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 bb
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 bc
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 bd
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 be
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 bf
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 c0
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 c1
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 c2
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 c3
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 c4
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 c5
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 c6
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 c7
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 c8
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 c9
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 ca
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 cb
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:28 cc
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:29 cd
0 drwxr-xr-x   4 jcprime  staff  136  4 Oct 16:28 ce
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 cf
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:29 d0
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:29 d1
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 d2
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 d3
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 d4
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 d5
0 drwxr-xr-x  14 jcprime  staff  476  4 Oct 16:29 d6
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 d7
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 d8
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 d9
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:29 da
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 db
0 drwxr-xr-x  12 jcprime  staff  408  4 Oct 16:28 dc
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 dd
0 drwxr-xr-x  10 jcprime  staff  340  4 Oct 16:28 de
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 df
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 e0
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 e1
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 e2
0 drwxr-xr-x  15 jcprime  staff  510  4 Oct 16:28 e3
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:28 e4
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 e5
0 drwxr-xr-x  14 jcprime  staff  476  4 Oct 16:29 e6
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 e7
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 e8
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 e9
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:28 ea
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 eb
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 ec
0 drwxr-xr-x  11 jcprime  staff  374  4 Oct 16:29 ed
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:29 ee
0 drwxr-xr-x  15 jcprime  staff  510  4 Oct 16:28 ef
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 f0
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 f1
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 f2
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:29 f3
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 f4
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 f5
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 f6
0 drwxr-xr-x   9 jcprime  staff  306  4 Oct 16:28 f7
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 f8
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:28 f9
0 drwxr-xr-x  14 jcprime  staff  476  4 Oct 16:28 fa
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 fb
0 drwxr-xr-x   6 jcprime  staff  204  4 Oct 16:29 fc
0 drwxr-xr-x   8 jcprime  staff  272  4 Oct 16:28 fd
0 drwxr-xr-x   7 jcprime  staff  238  4 Oct 16:29 fe
0 drwxr-xr-x   5 jcprime  staff  170  4 Oct 16:28 ff
0 drwxr-xr-x   2 jcprime  staff   68  4 Oct 16:16 info
0 drwxr-xr-x   2 jcprime  staff   68  4 Oct 16:16 pack

./.git/objects/00:
total 320
248 -r--r--r--  1 jcprime  staff  123650  4 Oct 16:28 11714d322628bc6221c6e183e88ec189a461ec
  8 -r--r--r--  1 jcprime  staff     947  4 Oct 16:28 1dc4ee9642d5f284818abc05ed06838fb81a5b
 40 -r--r--r--  1 jcprime  staff   16518  4 Oct 16:28 39a568bf3be21af22492345df1f839fa43396f
  8 -r--r--r--  1 jcprime  staff    1192  4 Oct 16:28 a4e8537b959fc65b0840dab962a19b4833d03b
  8 -r--r--r--  1 jcprime  staff     829  4 Oct 16:28 c5299839822e013f74689435bf643874c66d52
  8 -r--r--r--  1 jcprime  staff    2233  4 Oct 16:27 d6ef2705b986f51f920a68d12c86f494ce2c42

./.git/objects/01:
total 88
 8 -r--r--r--  1 jcprime  staff    342  4 Oct 16:27 401c41328ff87f008e8aa90673772105627871
 8 -r--r--r--  1 jcprime  staff    226  4 Oct 16:28 51697b981ac5bf025664d3624753e89b7cfadb
32 -r--r--r--  1 jcprime  staff  14864  4 Oct 16:28 68ccd48a089b950f5acdde8d33ff8b2e9eab58
16 -r--r--r--  1 jcprime  staff   7853  4 Oct 16:28 8b7678d4c03e11ec73a274a444dbe23b4877fa
 8 -r--r--r--  1 jcprime  staff    834  4 Oct 16:28 9d39c444541b1c5a3a7879234456761019592e
 8 -r--r--r--  1 jcprime  staff   1553  4 Oct 16:28 aeca82aa30d3b466f7c3c7bc2175fc084060e6
 8 -r--r--r--  1 jcprime  staff   2124  4 Oct 16:28 d1c47418427735633c7eddd11c0e82c1e922d5

./.git/objects/02:
total 248
  8 -r--r--r--  1 jcprime  staff   2980  4 Oct 16:28 1d2e84f2afc59afec7fa7093adc747007dfb58
168 -r--r--r--  1 jcprime  staff  82471  4 Oct 16:28 2fd6e6d6304364725069cd931de7972440ac08
  8 -r--r--r--  1 jcprime  staff     23  4 Oct 16:28 4136747329a9637021f16124b3ae56996d56af
  8 -r--r--r--  1 jcprime  staff   1229  4 Oct 16:28 4e870bd03d814232bdc46c6384743832a03f15
  8 -r--r--r--  1 jcprime  staff    192  4 Oct 16:29 5848cc462b4fc2baee94dc1cf35ff7c7560d08
  8 -r--r--r--  1 jcprime  staff   2907  4 Oct 16:27 6f328950e533e224c1b4937470224087857313
  8 -r--r--r--  1 jcprime  staff    423  4 Oct 16:28 d3841a5e7e86fa0fc6d4a65c88621aac38a438
 32 -r--r--r--  1 jcprime  staff  12359  4 Oct 16:28 fdacd32f1554c93373b4545ea6f43ee25d3a49

./.git/objects/03:
total 56
8 -r--r--r--  1 jcprime  staff  1512  4 Oct 16:28 3ddedb3e9d58658df4d1bad12148d1ce8aacd2
8 -r--r--r--  1 jcprime  staff  2143  4 Oct 16:27 547d32a16545e66fd3980333bf8775417ba894
8 -r--r--r--  1 jcprime  staff  1947  4 Oct 16:24 78263beb067eec60328d764b86e2c61027262b
8 -r--r--r--  1 jcprime  staff   748  4 Oct 16:28 7926f24ff6641b1751710c647d680a8e996cb1
8 -r--r--r--  1 jcprime  staff  1517  4 Oct 16:28 80fcf080def2be61351ec76878bed5ee24779d
8 -r--r--r--  1 jcprime  staff  1444  4 Oct 16:28 9321182394b26edfdc1460ac6b580b59d52a53
8 -r--r--r--  1 jcprime  staff   510  4 Oct 16:28 9fb795500a64bdf3ecf9cc1d13212e20d753f7

./.git/objects/04:
total 56
16 -r--r--r--  1 jcprime  staff  5889  4 Oct 16:28 17a07976bf32b571e91c251f778ae1d70ba420
 8 -r--r--r--  1 jcprime  staff   171  4 Oct 16:28 2f072af87bbcc4beab32acbf00ffe383308b8c
16 -r--r--r--  1 jcprime  staff  4150  4 Oct 16:27 47826fc5e39d928fdf749b7368f9aebc855f5e
 8 -r--r--r--  1 jcprime  staff   885  4 Oct 16:28 5063469544baa3d51659d30d8cec47bd71e8f0
 8 -r--r--r--  1 jcprime  staff  1377  4 Oct 16:28 7b803d15368aed9337d71641729f3fbe403010

./.git/objects/05:
total 136
 8 -r--r--r--  1 jcprime  staff   1935  4 Oct 16:27 1fe2746a43ed23299985401a1a72bcf6265af5
48 -r--r--r--  1 jcprime  staff  20946  4 Oct 16:28 4664090c39ae4356f9f0f6ced64ff0218f5497
16 -r--r--r--  1 jcprime  staff   5415  4 Oct 16:28 a8db89b9278dfa62ba3496c8718ac37f4e783d
 8 -r--r--r--  1 jcprime  staff   1453  4 Oct 16:28 b8ca286cd6cce7ba93c6da8c0c9d4c3d1cfc35
16 -r--r--r--  1 jcprime  staff   6453  4 Oct 16:28 d7736eb473c783e6e7b84b77c4ff837aa5e2bb
32 -r--r--r--  1 jcprime  staff  14337  4 Oct 16:28 daf9831a82625acda170288f301b516cdbcb0d
 8 -r--r--r--  1 jcprime  staff   1500  4 Oct 16:28 df094aec0ab85c64a366d23198f7f2aa6fbd50

./.git/objects/06:
total 200
24 -r--r--r--  1 jcprime  staff   8377  4 Oct 16:28 08471e375a03f69412fc6bf1ea54a1f65c34cd
 8 -r--r--r--  1 jcprime  staff   1668  4 Oct 16:28 162dc178948f05e4912bce1c32f57f9c7ff87d
 8 -r--r--r--  1 jcprime  staff   3271  4 Oct 16:28 27851d481aa281da4bdf6c8e8075d31c641d3a
64 -r--r--r--  1 jcprime  staff  28791  4 Oct 16:28 42d3c74385e91226303e7692dce2acfd0b9ec9
 8 -r--r--r--  1 jcprime  staff    972  4 Oct 16:28 4d0fe3c1cce91108fd5de6ba7f801dceef6d39
 8 -r--r--r--  1 jcprime  staff    297  4 Oct 16:28 51c05baa524a115b1adc8bda44b2fbc11505ec
 8 -r--r--r--  1 jcprime  staff    252  4 Oct 16:28 7173ee53c013182f241426107c3c5f9332e8c6
 8 -r--r--r--  1 jcprime  staff   2045  4 Oct 16:28 970067a08af006fc9a5acdb8a5e414fa63188e
16 -r--r--r--  1 jcprime  staff   4904  4 Oct 16:28 a61b7f3b15bfe293088e29071471c92375e279
16 -r--r--r--  1 jcprime  staff   5037  4 Oct 16:27 a9b82ef5d455a98e718206de6aa2717e8b37c2
 8 -r--r--r--  1 jcprime  staff    152  4 Oct 16:28 c25a8b481adaef4f0b34b11b81148c19fb854e
 8 -r--r--r--  1 jcprime  staff   1841  4 Oct 16:28 dfacedb3bc630da5d72a02e32d22d5abc934ee
 8 -r--r--r--  1 jcprime  staff   1452  4 Oct 16:28 e5fc1530f616752862788cbf20baf143e7836c
 8 -r--r--r--  1 jcprime  staff   2227  4 Oct 16:27 f18893465b9d27b8666c414f82e49bd37b5a27

./.git/objects/07:
total 64
 8 -r--r--r--  1 jcprime  staff     99  4 Oct 16:28 0a9387dc832e71e7c9599d78275551b908f80d
 8 -r--r--r--  1 jcprime  staff   1097  4 Oct 16:28 24cdc27fe97152fa45e8685a19841180c78c88
 8 -r--r--r--  1 jcprime  staff   1456  4 Oct 16:28 3f9878f36d8018cfcab6721016016c4110cc1a
24 -r--r--r--  1 jcprime  staff  11042  4 Oct 16:28 95dbb357b790f28b6e13761a09734133e6e724
 8 -r--r--r--  1 jcprime  staff    194  4 Oct 16:28 d675f4bbdf8141f970a6d91d11fefd7411d9ee
 8 -r--r--r--  1 jcprime  staff    400  4 Oct 16:27 d7eeb5c365930833a2c6673c9e56f29531346c

./.git/objects/08:
total 48
8 -r--r--r--  1 jcprime  staff   750  4 Oct 16:28 07e95080068e80791548109b41b0aaaaff5f22
8 -r--r--r--  1 jcprime  staff   467  4 Oct 16:28 37377c298848cd8456386d6671cdb85086cd2b
8 -r--r--r--  1 jcprime  staff   748  4 Oct 16:28 4cc7bf51939c5e004f7a430e95606d60ea8baa
8 -r--r--r--  1 jcprime  staff   850  4 Oct 16:28 aaf7010d4a81f6978892d3101979b2ff30b40a
8 -r--r--r--  1 jcprime  staff  1293  4 Oct 16:28 c67b439f6607081af8ddd0fcf0d2e9fbf58070
8 -r--r--r--  1 jcprime  staff    83  4 Oct 16:29 d6c6716b78abee9a6f8ec8ad15b8a38c747330

./.git/objects/09:
total 112
 8 -r--r--r--  1 jcprime  staff   1250  4 Oct 16:28 0267bbb136abe4352d60bc72810a537a244207
 8 -r--r--r--  1 jcprime  staff    738  4 Oct 16:28 3966ef0ffcd4e51558f103ef78338f36d8e30e
64 -r--r--r--  1 jcprime  staff  28794  4 Oct 16:28 7e4f9d8b4d4a063a94c279b83d95b00e32d7bf
 8 -r--r--r--  1 jcprime  staff    120  4 Oct 16:28 9d53e6f7df03a9b7b6bf1e113be56cbffa400c
 8 -r--r--r--  1 jcprime  staff   2223  4 Oct 16:27 e6e30d14a5c540a434368f245424ee7b7473fd
16 -r--r--r--  1 jcprime  staff   5858  4 Oct 16:28 fb81cbfbb37c76b269285a2c5a46bed1349585

./.git/objects/0a:
total 24
8 -r--r--r--  1 jcprime  staff  1452  4 Oct 16:28 2166c943ee9a066081b58400207e0e9a29c694
8 -r--r--r--  1 jcprime  staff   635  4 Oct 16:28 785233b4e7423499369800dc5f4e04d3d47e7f
8 -r--r--r--  1 jcprime  staff  4054  4 Oct 16:28 e5e820f772efb69a995c83ecbbad62f68c4223

./.git/objects/0b:
total 944
 40 -r--r--r--  1 jcprime  staff   17054  4 Oct 16:28 02e7f8c6f5d30349afe86ea1e12187b20c7b07
  8 -r--r--r--  1 jcprime  staff    1299  4 Oct 16:27 3b6886fb9d16eec85aa83cf0f5f00c56d69dcb
  8 -r--r--r--  1 jcprime  staff    4011  4 Oct 16:28 3ec9c2431b0f5a63eb5eb2941bd8ce426212c4
  8 -r--r--r--  1 jcprime  staff     316  4 Oct 16:28 67ccfaa5852818037988062e7cc9cc082f8647
  8 -r--r--r--  1 jcprime  staff    2665  4 Oct 16:27 73aff2e5a524314bd60eafc3aeb669d29bb33d
 16 -r--r--r--  1 jcprime  staff    6359  4 Oct 16:28 9a4a1677f93e63211d0b5af30e566dfaab4688
 16 -r--r--r--  1 jcprime  staff    6533  4 Oct 16:28 9bd79095b2b004ee15fc0bf72ea5df43693b17
840 -r--r--r--  1 jcprime  staff  429722  4 Oct 16:28 bf9d9219e32338c9c8388ea9ff0fa8ec42a143

./.git/objects/0c:
total 48
 8 -r--r--r--  1 jcprime  staff  1943  4 Oct 16:27 7de4a8522845c4607cec1b0eff3d8b91f40ff8
 8 -r--r--r--  1 jcprime  staff  3003  4 Oct 16:28 91c5c09ca004beb28ac374bff9950659d640a1
 8 -r--r--r--  1 jcprime  staff  1634  4 Oct 16:27 9bed2d2ec251768a7c526fa829b6912771be28
 8 -r--r--r--  1 jcprime  staff   922  4 Oct 16:28 c227ec1551ad6992dbadd30536d21dccf9b765
16 -r--r--r--  1 jcprime  staff  6368  4 Oct 16:28 cff41cb3bb8a67ab60d8b1568e658e0e331a42

./.git/objects/0d:
total 40
8 -r--r--r--  1 jcprime  staff  2417  4 Oct 16:28 40ffff8304a1b1af41e0a6f08f7362ed5f8d47
8 -r--r--r--  1 jcprime  staff  2940  4 Oct 16:28 4f204a055b7ad1347cfe95852e31003f298575
8 -r--r--r--  1 jcprime  staff  3634  4 Oct 16:28 511c3e2c37ed519da32fdf7f8e9ccaf81e0062
8 -r--r--r--  1 jcprime  staff  1285  4 Oct 16:27 5c7f53c42463b956159bb91bd0e7163d55b397
8 -r--r--r--  1 jcprime  staff  1148  4 Oct 16:29 c24763f77db8d0cb73272159ba2212968b6d3b

./.git/objects/0e:
total 88
 8 -r--r--r--  1 jcprime  staff   805  4 Oct 16:28 060844b79e8db482eac24d38ea7c39d4cc75bd
 8 -r--r--r--  1 jcprime  staff  1238  4 Oct 16:27 13c77ff174afb9beb91203b0b4f0b4c8c451f3
16 -r--r--r--  1 jcprime  staff  4897  4 Oct 16:28 2963f77b0c0be30d9fa41d20025f647dad783a
 8 -r--r--r--  1 jcprime  staff  2197  4 Oct 16:27 2dc2526b9a92b0bf224a9b0b3f1fbee7305596
 8 -r--r--r--  1 jcprime  staff  1430  4 Oct 16:28 3d1e004fee6ccc857f807518dd9061f34fd63a
24 -r--r--r--  1 jcprime  staff  9353  4 Oct 16:28 53a0a1946681949056beeb18d75abd2164a96a
 8 -r--r--r--  1 jcprime  staff  2309  4 Oct 16:28 6f89e30cc9c568d9cb1a62f749ee7640e4aa80
 8 -r--r--r--  1 jcprime  staff  2286  4 Oct 16:28 9020ca1cb56c4e65dd27da29c5c613f474d9d3

./.git/objects/0f:
total 328
  8 -r--r--r--  1 jcprime  staff    1752  4 Oct 16:28 14a8bb98be94cb3e3cfc9b0036ae3e0a9c3d9b
 16 -r--r--r--  1 jcprime  staff    7883  4 Oct 16:28 20f65d3b21b2c3002e7edc5d1caf9a9160a762
  8 -r--r--r--  1 jcprime  staff    2278  4 Oct 16:28 50733dabd73f0217481b036289a05f58e89bcc
  8 -r--r--r--  1 jcprime  staff    1549  4 Oct 16:27 50d326c2bdc30b7002e1c914eecab6966d7300
 16 -r--r--r--  1 jcprime  staff    4740  4 Oct 16:28 76bce41b074fae3cc493f51819f15f98b38cca
232 -r--r--r--  1 jcprime  staff  115854  4 Oct 16:28 7b7aabf846ce0f332e11921b2a8b3bb4a4b233
 40 -r--r--r--  1 jcprime  staff   19936  4 Oct 16:28 a61e68f527954eddef0749df2afea8a73b7f11

./.git/objects/10:
total 6024
   8 -r--r--r--  1 jcprime  staff      209  4 Oct 16:28 39a23843f30c99c96d125117a7a0f3b00413a5
 288 -r--r--r--  1 jcprime  staff   147318  4 Oct 16:28 9b55515b0d8bade258ed7f7d3fd017546fdca0
   8 -r--r--r--  1 jcprime  staff     1144  4 Oct 16:28 a44923c6454dfa0c618171fa96968f615f970d
5688 -r--r--r--  1 jcprime  staff  2911900  4 Oct 16:28 a831eae3d649ef2427f21bd5e53367462fc0ee
   8 -r--r--r--  1 jcprime  staff     1388  4 Oct 16:27 bd011853f34a4d45c86840d8ae25abc4013f03
   8 -r--r--r--  1 jcprime  staff      210  4 Oct 16:29 c8473ee937d5e911e5d0001e6ff2641b2d275e
   8 -r--r--r--  1 jcprime  staff     3496  4 Oct 16:28 c9e3d514525c9059fb5d562f39c8b5a09321ff
   8 -r--r--r--  1 jcprime  staff     1117  4 Oct 16:28 eeb00bd4b2b87dd035b39f1175f22d7bc3a45d

./.git/objects/11:
total 232
16 -r--r--r--  1 jcprime  staff   7620  4 Oct 16:28 1694f1cca42730a52a34e9e99db1c44d5057f8
 8 -r--r--r--  1 jcprime  staff    177  4 Oct 16:28 4530fb837732d9902fdb14313574f48cad2279
 8 -r--r--r--  1 jcprime  staff   1123  4 Oct 16:28 55a8bfe0cf06a60600fba155143bfbeb3bbf12
 8 -r--r--r--  1 jcprime  staff     93  4 Oct 16:29 566a6d5566c11cb57020a3f2dace6750450e93
16 -r--r--r--  1 jcprime  staff   4507  4 Oct 16:28 c615d327f04dd4b5b45cc3831e9b79f6b25941
24 -r--r--r--  1 jcprime  staff  11753  4 Oct 16:28 cb95f7de9bbd5ca85bfbc366a53ad95fc7e113
 8 -r--r--r--  1 jcprime  staff    276  4 Oct 16:29 cdc488fae0fac72674f0d7a3b2fe59f73c1b42
 8 -r--r--r--  1 jcprime  staff   1082  4 Oct 16:28 d1632ca1d5420ced2534d62d75f019a9e2cfe9
 8 -r--r--r--  1 jcprime  staff   2608  4 Oct 16:28 d66bbc7293ae98c6707d0129a3a86932e1b814
96 -r--r--r--  1 jcprime  staff  46290  4 Oct 16:28 e1904a4995a9cbea375e65234a709cdd3ef83d
24 -r--r--r--  1 jcprime  staff  10591  4 Oct 16:28 f676b6a743feecff47ad88ea500165e41a2da0
 8 -r--r--r--  1 jcprime  staff   1288  4 Oct 16:27 fc6bfb3f02989063e6fb76922ab21dc3cdf11a

./.git/objects/12:
total 192
 8 -r--r--r--  1 jcprime  staff   3562  4 Oct 16:28 23c74ac627bf2ca032ac67b0a5a3bd4d2615a8
 8 -r--r--r--  1 jcprime  staff   1745  4 Oct 16:27 6adf1cd8e745dcd0c620611f6965e774fe8c87
16 -r--r--r--  1 jcprime  staff   4595  4 Oct 16:28 6e4c95d5b3a293c790f632510f13797ceda7d2
 8 -r--r--r--  1 jcprime  staff   2688  4 Oct 16:28 781f849f5485e3cba6a40160d027279c060947
 8 -r--r--r--  1 jcprime  staff    841  4 Oct 16:28 870a8e4ed1ca8e29695ebdc8e66831a78275e9
24 -r--r--r--  1 jcprime  staff   8473  4 Oct 16:29 9598cddfa3a629835938e552a1e72e21d74198
 8 -r--r--r--  1 jcprime  staff   2192  4 Oct 16:28 9830ed5761efaaef40e8273d7d8992285a9efb
24 -r--r--r--  1 jcprime  staff  12188  4 Oct 16:28 9fa985453d9f2790ac59b8c57a0719f91861a5
 8 -r--r--r--  1 jcprime  staff    158  4 Oct 16:28 b0dbb306480780fbb23fabb60cfe18008ed1f2
80 -r--r--r--  1 jcprime  staff  39520  4 Oct 16:27 dbf31dcc953814d7a989be0c63931fb87ec0c6

./.git/objects/13:
total 24
8 -r--r--r--  1 jcprime  staff  3776  4 Oct 16:28 3b138d510091451258215d25b962cc47e5f3c6
8 -r--r--r--  1 jcprime  staff   588  4 Oct 16:28 9501ca374df08b713c3d810ba1ddd6480f1377
8 -r--r--r--  1 jcprime  staff   917  4 Oct 16:28 bac9a187c22fa4e4e5401a5ca0522648992855

./.git/objects/14:
total 4680
4608 -r--r--r--  1 jcprime  staff  2358221  4 Oct 16:28 2b3662f372ea393e7925c0c2d8a7b7c4faf27d
  16 -r--r--r--  1 jcprime  staff     4118  4 Oct 16:29 36e95dbadf80877f550453f184ba8eff4ea0d7
   8 -r--r--r--  1 jcprime  staff      461  4 Oct 16:28 490d1bdfa880992b169f67f0d2746dbd7272ff
   8 -r--r--r--  1 jcprime  staff      166  4 Oct 16:29 5cfdef4f5e9cd469e541439293f39a7bf7f030
   8 -r--r--r--  1 jcprime  staff     1818  4 Oct 16:28 8769b3509f8fad74ccc4363561c200516b010a
   8 -r--r--r--  1 jcprime  staff     2940  4 Oct 16:28 f6438d8526db7767cd1de1ee41ec8a916c5c9d
  24 -r--r--r--  1 jcprime  staff     9367  4 Oct 16:28 f703ba68c32a314cdc271a62c32929c10b2bd5

./.git/objects/15:
total 128
 8 -r--r--r--  1 jcprime  staff    660  4 Oct 16:28 33dedcc1be3f531c3ee4545d38e2aff718e591
72 -r--r--r--  1 jcprime  staff  33128  4 Oct 16:28 606a456d8afe5623d93e80507688673107f157
16 -r--r--r--  1 jcprime  staff   5830  4 Oct 16:28 91056869582d8f71eb33474792806d27aacd31
 8 -r--r--r--  1 jcprime  staff   1692  4 Oct 16:27 bcc296119fc5e03d81e4ccee97c6d23f0c6026
 8 -r--r--r--  1 jcprime  staff    296  4 Oct 16:29 bdb1de8e6c1706e23e1b641668a76156d3f2cd
 8 -r--r--r--  1 jcprime  staff   1490  4 Oct 16:28 ce2e268dd81569627a1d8a6fca41f39dfecb0c
 8 -r--r--r--  1 jcprime  staff   2250  4 Oct 16:27 f7be60a344d50d1da7bd4eaa3e81756f83f482

./.git/objects/16:
total 56
8 -r--r--r--  1 jcprime  staff   690  4 Oct 16:28 0398fd7e3e8178ff2d51b9251468c71072d26a
8 -r--r--r--  1 jcprime  staff  2175  4 Oct 16:28 40d537abd201acaa14f157d572f1c59595e569
8 -r--r--r--  1 jcprime  staff   140  4 Oct 16:28 4411f0570a133d36cd1f9cf5ea19e65cc6c751
8 -r--r--r--  1 jcprime  staff   277  4 Oct 16:28 4c319978430ce15d5f97c2a22508fbc66fa0f7
8 -r--r--r--  1 jcprime  staff  2781  4 Oct 16:28 5b505c74fed4c9bcb75f11fdc56fedfe6fdbfc
8 -r--r--r--  1 jcprime  staff  1444  4 Oct 16:28 b7e7c6675bdde401707d4a774761d695efca2e
8 -r--r--r--  1 jcprime  staff  1496  4 Oct 16:28 ef9bdf6e60822b6bb7344975edf7080a6a1e12

./.git/objects/17:
total 96
40 -r--r--r--  1 jcprime  staff  17186  4 Oct 16:28 15d4d69800d34b8d6c640cee0c6d43bf1b9819
 8 -r--r--r--  1 jcprime  staff    512  4 Oct 16:28 60e695d7cc24d9315ff8c30e7970f9cc630095
 8 -r--r--r--  1 jcprime  staff   2175  4 Oct 16:27 6eca1ff3db492eca7f41ae1dd1f0c8749ee961
 8 -r--r--r--  1 jcprime  staff   2303  4 Oct 16:28 a3b9f334ef5a584ac975c1f35f98d3c16b5995
 8 -r--r--r--  1 jcprime  staff   2293  4 Oct 16:28 d20f73de2fda4643b216713493b73c864793f0
 8 -r--r--r--  1 jcprime  staff   1521  4 Oct 16:28 d302dc8103b07b827326c3c038188b41aedd71
 8 -r--r--r--  1 jcprime  staff   1455  4 Oct 16:28 d817e6904789f3ab9b874181e7fa450a6599ea
 8 -r--r--r--  1 jcprime  staff   1616  4 Oct 16:28 e1c1b5eebf484de9d2d6730039d1148d0db630

./.git/objects/18:
total 64
16 -r--r--r--  1 jcprime  staff   5861  4 Oct 16:28 52b3d089eefea4efd06e56188bad186a01f77d
 8 -r--r--r--  1 jcprime  staff    854  4 Oct 16:27 6251633f871f606926749d55c492a082b97b9d
 8 -r--r--r--  1 jcprime  staff   1719  4 Oct 16:27 76cd1a8f40e6a9f74e1f89eabd818438746a7d
 8 -r--r--r--  1 jcprime  staff    151  4 Oct 16:29 aae6735d55d0ad015975a55be9eaa09541540d
24 -r--r--r--  1 jcprime  staff  10322  4 Oct 16:28 e13796d67202102be17c7d68227d7f045da799

./.git/objects/19:
total 48
 8 -r--r--r--  1 jcprime  staff  1863  4 Oct 16:27 11b379881d233512e8e0f0641df9073b4b1cdb
16 -r--r--r--  1 jcprime  staff  4255  4 Oct 16:27 65ff609562bdac22357ee90395ba43cccd4ba5
 8 -r--r--r--  1 jcprime  staff  2350  4 Oct 16:28 81e8c556a05ba50d2c210259500bd27b7fcec0
 8 -r--r--r--  1 jcprime  staff  2315  4 Oct 16:28 db4ede3045870a3266b038053c35ff35ccf774
 8 -r--r--r--  1 jcprime  staff   912  4 Oct 16:28 febd4d3371cfaceb9fa61576ac158ee9d8ade2

./.git/objects/1a:
total 16
8 -r--r--r--  1 jcprime  staff  1682  4 Oct 16:27 47fcb91d9c2954fc909c4fadafdd213922e972
8 -r--r--r--  1 jcprime  staff  3959  4 Oct 16:28 feffc3774656124a81e15b8226dae1d4fc60a4

./.git/objects/1b:
total 32
8 -r--r--r--  1 jcprime  staff  2300  4 Oct 16:28 548d03dd74e500901e798f32c4fe0d331ce25b
8 -r--r--r--  1 jcprime  staff  2303  4 Oct 16:28 7ff8afde183e607cf4550ef265aea9703a7987
8 -r--r--r--  1 jcprime  staff  2339  4 Oct 16:28 bde62a37a8ace73afb37f7e5a4f3564f8143f2
8 -r--r--r--  1 jcprime  staff   433  4 Oct 16:28 eb0aeadf05533a944f38860bb78ad07e23aa92

./.git/objects/1c:
total 16
8 -r--r--r--  1 jcprime  staff  1107  4 Oct 16:28 5d43f03bf623f70a5f12abca6f257d7a353070
8 -r--r--r--  1 jcprime  staff  1133  4 Oct 16:27 cd34d8fe70c64c81d477925bc041513a4b2458

./.git/objects/1d:
total 24
8 -r--r--r--  1 jcprime  staff   771  4 Oct 16:28 4b5ebe10be04e408e08cdb1a663b6ac8969617
8 -r--r--r--  1 jcprime  staff  1838  4 Oct 16:28 732409311e961b2634a253fe29078175c75520
8 -r--r--r--  1 jcprime  staff   109  4 Oct 16:28 ce4b2f0eb7e6293dc9490d894783c001967b1e

./.git/objects/1e:
total 48
8 -r--r--r--  1 jcprime  staff  1371  4 Oct 16:28 1043b1349f9b72c6a74fd4122ea234e017bf8c
8 -r--r--r--  1 jcprime  staff  1001  4 Oct 16:28 350780344346a9534975a6469751e708fc1795
8 -r--r--r--  1 jcprime  staff  1784  4 Oct 16:17 36c33013e3c976605d3aa8555a823f16ae08ca
8 -r--r--r--  1 jcprime  staff  1649  4 Oct 16:27 48c780ff24897e7c8f64bc86dc52c282d2bfc3
8 -r--r--r--  1 jcprime  staff  1463  4 Oct 16:28 48e8eb85fe0c283c232e9e9de9ca77f95c2d63
8 -r--r--r--  1 jcprime  staff   168  4 Oct 16:28 f5dc430a0f0c93b3fdc0c96d0369c7de321873

./.git/objects/1f:
total 72
 8 -r--r--r--  1 jcprime  staff  1170  4 Oct 16:27 1865226954a850ed33e0a91ce8fa030631cd5f
 8 -r--r--r--  1 jcprime  staff   109  4 Oct 16:28 440d4466dc8c73c03f85dbfdf313496e2a659e
 8 -r--r--r--  1 jcprime  staff  1402  4 Oct 16:28 5735f1a76aec5eaa22879b84654f74f7c63df7
16 -r--r--r--  1 jcprime  staff  6537  4 Oct 16:28 844cf973a6c0e08ab2f73969efec0b6bdd00ec
 8 -r--r--r--  1 jcprime  staff   739  4 Oct 16:28 acf080a167d5935ee8df82a558b492e7ead552
 8 -r--r--r--  1 jcprime  staff  4045  4 Oct 16:28 ad0c6046969fbaa7d0ed077cb7ba5c7dc464f3
16 -r--r--r--  1 jcprime  staff  5567  4 Oct 16:27 e55ecf8c300cb1fa4300450b5d7ca9b7a175a9

./.git/objects/20:
total 72
 8 -r--r--r--  1 jcprime  staff  2303  4 Oct 16:28 2733eec045e1a655bcda69a6bad152759fe78c
 8 -r--r--r--  1 jcprime  staff   238  4 Oct 16:28 41467c71392953c890cd90f14031cf41114d8e
 8 -r--r--r--  1 jcprime  staff  3522  4 Oct 16:27 616acb97e10c7ebb300c2e976434cf051e090f
 8 -r--r--r--  1 jcprime  staff    50  4 Oct 16:29 88798d489412042db2e5d7c32399296db6bf70
24 -r--r--r--  1 jcprime  staff  9411  4 Oct 16:28 be3c58c4fdaf9d7b6ef5a309cd6e516e98b10a
 8 -r--r--r--  1 jcprime  staff  1488  4 Oct 16:28 cbecbb076908c252d3fbb5f9c84725a2b5b4dd
 8 -r--r--r--  1 jcprime  staff  1889  4 Oct 16:27 d059ca003584d553d20bc9103f3ace1c0dd0e3

./.git/objects/21:
total 88
16 -r--r--r--  1 jcprime  staff   6733  4 Oct 16:28 13eee6cfbc2cd72200abf6876bc00ffe9a58df
 8 -r--r--r--  1 jcprime  staff   2334  4 Oct 16:28 3a8a421e04168b003b9efd3717a478edf40c9f
 8 -r--r--r--  1 jcprime  staff   1737  4 Oct 16:28 578ab1f4f52ff72ca39d529d897dadfa58e63d
 8 -r--r--r--  1 jcprime  staff    476  4 Oct 16:28 688e5a8e2fe962193653cc7424aea6e153ea4f
 8 -r--r--r--  1 jcprime  staff   3369  4 Oct 16:28 6cd73f427f3db9850a84572c234005e7677049
32 -r--r--r--  1 jcprime  staff  16117  4 Oct 16:17 f26aa3b31eb7f81d62368b6b3bee2525f653f6
 8 -r--r--r--  1 jcprime  staff    246  4 Oct 16:28 f3849a2d6c6128a2447d5846f0e36f6101173c

./.git/objects/22:
total 32
8 -r--r--r--  1 jcprime  staff   943  4 Oct 16:27 02445084ed231b654fbe6dabf8f2d95434a1cc
8 -r--r--r--  1 jcprime  staff  1562  4 Oct 16:28 a0490c8d6663c63bcc75d78907ddfd6500a7a2
8 -r--r--r--  1 jcprime  staff   637  4 Oct 16:28 c3b74504aa9292476f1d03e9f25a6d0831d924
8 -r--r--r--  1 jcprime  staff  1780  4 Oct 16:27 cbcf49cacf7c5b7bf2ab67b565581787ebeb1a

./.git/objects/23:
total 56
 8 -r--r--r--  1 jcprime  staff   854  4 Oct 16:28 6fd69174fef067ab91a6530dc90421f978c6ea
16 -r--r--r--  1 jcprime  staff  4900  4 Oct 16:28 ad98a887ea2fe9f3023790d411fe8c9fc14d61
 8 -r--r--r--  1 jcprime  staff  2042  4 Oct 16:28 bc54bf2656e66f496becea223d3f4e15db5281
24 -r--r--r--  1 jcprime  staff  8769  4 Oct 16:28 da35471efd30ab18432f014c9f781dee682369

./.git/objects/24:
total 16
8 -r--r--r--  1 jcprime  staff  1908  4 Oct 16:27 23a55d869f9e5f285982305ab0e1e171d21e7b
8 -r--r--r--  1 jcprime  staff   121  4 Oct 16:29 c597c6c42652aa89d189871d9e7fffb8609e4e

./.git/objects/25:
total 96
8 -r--r--r--  1 jcprime  staff  1831  4 Oct 16:27 14d5240b7ea424ad58f6c83c16a9bfe2ee7b8a
8 -r--r--r--  1 jcprime  staff  3081  4 Oct 16:28 2025aa7d033d6f889b7f843642ce0a2bfce6c3
8 -r--r--r--  1 jcprime  staff  3316  4 Oct 16:28 3d56afb66d8ce8b458c51f93f78ef239bd2d05
8 -r--r--r--  1 jcprime  staff   729  4 Oct 16:28 8fdfaf22b7a4cd4d4859048d1f1ae463cfe688
8 -r--r--r--  1 jcprime  staff   711  4 Oct 16:29 953d3b2a171c8f29037c57ab37a08cc1a15f71
8 -r--r--r--  1 jcprime  staff  1718  4 Oct 16:28 acb45020d7a274bd366ade998b97fb3393c0f3
8 -r--r--r--  1 jcprime  staff   492  4 Oct 16:28 b6fb8a4dfaa848adfa5622dddba54f3f65ef06
8 -r--r--r--  1 jcprime  staff  1041  4 Oct 16:27 c8ddaa56d1218e5ea9dc2770e200957935a91d
8 -r--r--r--  1 jcprime  staff  3089  4 Oct 16:27 cf59369ba1f2ea04e3b379b397b442a62567a6
8 -r--r--r--  1 jcprime  staff   106  4 Oct 16:28 d71abf8f6c0e58d00ee2a2cd4a70ea300dfdae
8 -r--r--r--  1 jcprime  staff  1458  4 Oct 16:28 fe4e11f386aed27cefca3f0ff75d888ca276b7
8 -r--r--r--  1 jcprime  staff  1344  4 Oct 16:28 ff4e9abbdfb7987177da2b75522742d06445ef

./.git/objects/26:
total 256
  8 -r--r--r--  1 jcprime  staff    1557  4 Oct 16:28 1c6a830fbc8d9f448254df526c88975ae1d759
  8 -r--r--r--  1 jcprime  staff     295  4 Oct 16:17 23dca447f9a71f81cc147a1f54c8a19de833e8
 16 -r--r--r--  1 jcprime  staff    5406  4 Oct 16:28 2824366d27d998ae9489a118fabba69f2b19c1
  8 -r--r--r--  1 jcprime  staff    1288  4 Oct 16:28 881a84352f2b16c8363aa40b910cda76c18aa0
  8 -r--r--r--  1 jcprime  staff     216  4 Oct 16:29 aa9e396c56fe641dc1d95fcc55cdffd01a70d5
208 -r--r--r--  1 jcprime  staff  103159  4 Oct 16:28 dffd80568ad71ef541a417cab75eb43b1fd32e

./.git/objects/27:
total 72
8 -r--r--r--  1 jcprime  staff  2184  4 Oct 16:27 09429a518e8b51097ee72bedbcf85b18cf7d53
8 -r--r--r--  1 jcprime  staff   141  4 Oct 16:28 1f0438d2efa0d60e7493d6dbf7e35bd02e8726
8 -r--r--r--  1 jcprime  staff  1695  4 Oct 16:27 219c85520def23a0c7b67a7232b68c3afcd9ee
8 -r--r--r--  1 jcprime  staff   270  4 Oct 16:28 2d3a75538140243d2cd49fc4f6e0ee5d88402f
8 -r--r--r--  1 jcprime  staff  1068  4 Oct 16:28 2f5cd6764cad9dfdd3ec38f1b1c0f4acfc17f9
8 -r--r--r--  1 jcprime  staff   688  4 Oct 16:28 4bce0509f84f58b4848dda3aac05092243a342
8 -r--r--r--  1 jcprime  staff  1992  4 Oct 16:28 9535572e888c56d78610175e826f49788c6277
8 -r--r--r--  1 jcprime  staff  1256  4 Oct 16:27 9d61044a7f134e67528254ea3f03c0eec4dad2
8 -r--r--r--  1 jcprime  staff  1821  4 Oct 16:27 b8316da0d87f15a0efa9f56c57ccc88400a686

./.git/objects/28:
total 120
16 -r--r--r--  1 jcprime  staff   4410  4 Oct 16:28 0e832517d7ac4c2977f28313fe1a4f0d6d65f8
 8 -r--r--r--  1 jcprime  staff    321  4 Oct 16:27 2eb77bbec0cb731edc6c1435029736c6cb7cc6
 8 -r--r--r--  1 jcprime  staff   1253  4 Oct 16:28 3858c94efed46a116ab97e6c334fd57fb3a633
16 -r--r--r--  1 jcprime  staff   7021  4 Oct 16:28 3e79a64bd01fbbc7ab289b82ee469e677eacf6
 8 -r--r--r--  1 jcprime  staff    114  4 Oct 16:28 62d03f434490ca86180f934f3d38762f67ccf0
 8 -r--r--r--  1 jcprime  staff   2202  4 Oct 16:28 877109b512d1187297226c743d3f5ff8356a86
48 -r--r--r--  1 jcprime  staff  21174  4 Oct 16:28 c59153e4e78fa4c8bec51f081cc1f24d1bfb1d
 8 -r--r--r--  1 jcprime  staff   2510  4 Oct 16:27 ce2be17249cc58e3bb8bab2b91088084c8bdb3

./.git/objects/29:
total 72
 8 -r--r--r--  1 jcprime  staff   2302  4 Oct 16:27 1fa5084b3a82834ff8d6863b6468fad88d2b1a
 8 -r--r--r--  1 jcprime  staff   2289  4 Oct 16:27 43d73a10439dee236c852801e46532f835004b
24 -r--r--r--  1 jcprime  staff  10945  4 Oct 16:28 6b25688b49791e6fcf20e4538c841867895c9c
 8 -r--r--r--  1 jcprime  staff    977  4 Oct 16:28 87593ce2dc158751af9bd3625cfc010e6ae7bb
16 -r--r--r--  1 jcprime  staff   4997  4 Oct 16:28 c3c0fe215bbbdf4ea1621bd917910523ec6f20
 8 -r--r--r--  1 jcprime  staff   1017  4 Oct 16:28 c607847af4c05c448163cc81d8419269f8eaf5

./.git/objects/2a:
total 136
 8 -r--r--r--  1 jcprime  staff    486  4 Oct 16:28 1cdee350947e468da3375508464719bae2b606
 8 -r--r--r--  1 jcprime  staff   1880  4 Oct 16:28 3e5367befe42b96be7e520ff73c684c32e47eb
 8 -r--r--r--  1 jcprime  staff   1320  4 Oct 16:28 5b0fdc3872c51911b669daf2541f422b626329
 8 -r--r--r--  1 jcprime  staff    210  4 Oct 16:29 77546e4700ab5511c8f122007ca002473ed016
24 -r--r--r--  1 jcprime  staff   9878  4 Oct 16:28 8a19c36788d266e5fdb09699a07b36d320fb70
16 -r--r--r--  1 jcprime  staff   5867  4 Oct 16:28 8c46c5627a8fa60a1491b6d6c6247c6073daaa
56 -r--r--r--  1 jcprime  staff  26826  4 Oct 16:28 b8e664e8d182d7007966ab3dfe2d2b723ffcf9
 8 -r--r--r--  1 jcprime  staff   2577  4 Oct 16:28 ba5630a32f344e4555095ceb78013754e4e230

./.git/objects/2b:
total 64
 8 -r--r--r--  1 jcprime  staff   1496  4 Oct 16:28 244a2038249076d0b842fd3fa3bedf75247792
 8 -r--r--r--  1 jcprime  staff   2140  4 Oct 16:27 4ca84a997a2fe76b38a7f36824c49f9776be15
 8 -r--r--r--  1 jcprime  staff   1069  4 Oct 16:28 50c38dbba16110fdc3dcba776f625241c005d3
40 -r--r--r--  1 jcprime  staff  16865  4 Oct 16:28 eec0c9c510cd38293f4574ac3a4e80570437b1

./.git/objects/2c:
total 64
8 -r--r--r--  1 jcprime  staff  2301  4 Oct 16:28 0b4b1811979e3f9ea349b0904750575d877d36
8 -r--r--r--  1 jcprime  staff  1342  4 Oct 16:28 2154ff8279abe91afa96defb9d698b7d7be48b
8 -r--r--r--  1 jcprime  staff   370  4 Oct 16:28 5a97f9076319d4e0eda8e81aa4d94478ea5053
8 -r--r--r--  1 jcprime  staff  1869  4 Oct 16:27 77dc8658de04354d222637f2918516428ee45d
8 -r--r--r--  1 jcprime  staff  1432  4 Oct 16:28 8bb37d5d7e0d1a673b80b2b673c6364920b137
8 -r--r--r--  1 jcprime  staff  2272  4 Oct 16:28 a108c671ed3895756b02076eb63f8dd40a41d9
8 -r--r--r--  1 jcprime  staff  2495  4 Oct 16:28 e20c37615ce119396fee19cf122af6d74e8435
8 -r--r--r--  1 jcprime  staff  1517  4 Oct 16:27 f0aeee2f6bb32caa7303459bb9773421b5ca5d

./.git/objects/2d:
total 48
 8 -r--r--r--  1 jcprime  staff  1808  4 Oct 16:28 1e61a2b0e60be024f484a7866861236dafbd69
 8 -r--r--r--  1 jcprime  staff   260  4 Oct 16:28 5170bb8811dc4babc596bc24d8c18047b8f827
16 -r--r--r--  1 jcprime  staff  7419  4 Oct 16:28 aa68acb1a055c527603e872a0a50b08a24eb4b
16 -r--r--r--  1 jcprime  staff  6486  4 Oct 16:28 e2323911eb7d1751b411045ff9c57520648a3f

./.git/objects/2e:
total 40
 8 -r--r--r--  1 jcprime  staff   1832  4 Oct 16:27 34deffc2b871e8bea7a682937a687a5dcef4b0
32 -r--r--r--  1 jcprime  staff  12637  4 Oct 16:28 9f0e9e56522b0f930c7b5ab37db8599f1f5816

./.git/objects/2f:
total 64
 8 -r--r--r--  1 jcprime  staff   803  4 Oct 16:28 0644da0addec802223e2e784346f403586af15
16 -r--r--r--  1 jcprime  staff  5860  4 Oct 16:28 533d14a137c2aebbe0d4d46529d0a8378df624
 8 -r--r--r--  1 jcprime  staff   679  4 Oct 16:28 86315a4bd023fe9b2fb26939519c96d0a87778
 8 -r--r--r--  1 jcprime  staff  1259  4 Oct 16:27 8b980ede363dcd165d8e8ec613aa15ae9203a3
 8 -r--r--r--  1 jcprime  staff  1814  4 Oct 16:27 b0503571fdd42dd0fdd715a72b8a58a753ab68
16 -r--r--r--  1 jcprime  staff  4512  4 Oct 16:28 c8e5cea70c103fff982fcff2a48c8f93d6e685

./.git/objects/30:
total 3280
   8 -r--r--r--  1 jcprime  staff     2343  4 Oct 16:28 09e3b81840829425472cb16240cb8582552e82
  16 -r--r--r--  1 jcprime  staff     4541  4 Oct 16:28 0ccc6e5aef0cbf8bf4e6a21548b0dff828537a
3176 -r--r--r--  1 jcprime  staff  1622863  4 Oct 16:28 17c3edd538ed582304fae0ccb81f5f8290be11
   8 -r--r--r--  1 jcprime  staff     1191  4 Oct 16:28 3aa78d3930988abc832b06a3f51967a88b714d
   8 -r--r--r--  1 jcprime  staff     1299  4 Oct 16:27 3e856d0e3f52dbaf5be7d87d7d5e7d6a73169f
   8 -r--r--r--  1 jcprime  staff     2089  4 Oct 16:28 990e3b3c1b5b59ebd94b29c53f5b7599b76f31
   8 -r--r--r--  1 jcprime  staff      963  4 Oct 16:28 bfeba741d732c040bb6c8ae368628f13ab8eb5
  48 -r--r--r--  1 jcprime  staff    21407  4 Oct 16:28 d15850a72193fdf775a9b60f7939c54c231f15

./.git/objects/31:
total 104
 8 -r--r--r--  1 jcprime  staff   1372  4 Oct 16:28 0ed64c03076c5fd577fe2b43943a7adbeb6166
16 -r--r--r--  1 jcprime  staff   4905  4 Oct 16:27 111b845b61a61427d06d63482d4c60dd495634
 8 -r--r--r--  1 jcprime  staff   1561  4 Oct 16:27 327762b29daf40a67bd32f7885b5ee8937bd50
 8 -r--r--r--  1 jcprime  staff    832  4 Oct 16:28 37c51cdd56dbec70c22e7f48a303943c7ba196
 8 -r--r--r--  1 jcprime  staff   1628  4 Oct 16:27 441036c020c8b804eed32640744eb692b0c62b
40 -r--r--r--  1 jcprime  staff  19922  4 Oct 16:28 a2f39ee71f45426fb3050d211f5c759634bf09
 8 -r--r--r--  1 jcprime  staff   1080  4 Oct 16:27 afbc0bd2d15f7d7cd44a2eb37a2e1558a9c93c
 8 -r--r--r--  1 jcprime  staff   1394  4 Oct 16:27 e56f62b8411479756a2cec8beaa30bea97fe71

./.git/objects/32:
total 56
 8 -r--r--r--  1 jcprime  staff   3202  4 Oct 16:28 13ba1d5ce590ee0acea60712c293d95d6e2ecd
 8 -r--r--r--  1 jcprime  staff     51  4 Oct 16:29 7b4c9e9d3ee89a80a9b2eda62c585f4c3289d3
40 -r--r--r--  1 jcprime  staff  16721  4 Oct 16:28 80bcc3c257b9806dd94bc44d8380cb1b0f6421

./.git/objects/33:
total 40
8 -r--r--r--  1 jcprime  staff  1403  4 Oct 16:28 4d70baef56fb3c383bf00b173ae524aca31b54
8 -r--r--r--  1 jcprime  staff   555  4 Oct 16:28 71f57674c28bd25978ffca1273a8d16d0282f8
8 -r--r--r--  1 jcprime  staff  1714  4 Oct 16:28 76794cc0c1e215fdb41915c71e0a9f20db15c2
8 -r--r--r--  1 jcprime  staff   414  4 Oct 16:28 e29c18a8899fbf6dfadede0544db4369870623
8 -r--r--r--  1 jcprime  staff  3844  4 Oct 16:28 e3f6a7dd30644c39b570f1e6f68c0b60118721

./.git/objects/34:
total 48
 8 -r--r--r--  1 jcprime  staff   108  4 Oct 16:29 3d4e9ef2398e8174f97e1fa100cf842e403a51
16 -r--r--r--  1 jcprime  staff  8023  4 Oct 16:28 6d81571917eb60623912c850dea92e041c916f
 8 -r--r--r--  1 jcprime  staff   148  4 Oct 16:28 99c89460ec827ac4c1ed4151720cf8e5c791e5
 8 -r--r--r--  1 jcprime  staff  1680  4 Oct 16:28 a8ae3a7919a30d117d76433a9fdcea9016825a
 8 -r--r--r--  1 jcprime  staff  1613  4 Oct 16:28 d549ab986dd283911b9cb3afe08eb0e763db54

./.git/objects/35:
total 16
8 -r--r--r--  1 jcprime  staff   485  4 Oct 16:28 2f092ed5a4100226576e6614204d5ec3756dbf
8 -r--r--r--  1 jcprime  staff  3248  4 Oct 16:28 744b05e16998bc58eefb39aa7e6edec96a17d5

./.git/objects/36:
total 128
 8 -r--r--r--  1 jcprime  staff   1333  4 Oct 16:28 02ebf4fd9fb97956ba367ff703a9d6aab7ab50
 8 -r--r--r--  1 jcprime  staff   3720  4 Oct 16:28 114f8a91b82d0d1083a2f5aff4a4ab44e64f6d
 8 -r--r--r--  1 jcprime  staff   1842  4 Oct 16:27 1849bf7e5e7912ebd409e79916ca288cc82d02
 8 -r--r--r--  1 jcprime  staff   2577  4 Oct 16:28 630b91d7c58b5ddfeaf1f68fc570dc4bef8c0c
16 -r--r--r--  1 jcprime  staff   4151  4 Oct 16:27 8034ce4f0d833500cad0c4392f69160840712a
24 -r--r--r--  1 jcprime  staff  11958  4 Oct 16:28 b438da3698a7e4884fe12919e28308ad79f9fe
48 -r--r--r--  1 jcprime  staff  24160  4 Oct 16:28 be4921a7762d6549f59fd0bc199d822fbfc08f
 8 -r--r--r--  1 jcprime  staff   2570  4 Oct 16:28 edd6b4306022bf4ccab0d64fbcad7dae5187fd

./.git/objects/37:
total 80
 8 -r--r--r--  1 jcprime  staff   2220  4 Oct 16:27 34b4b2d805370bb82cb8fc813795cb53098400
 8 -r--r--r--  1 jcprime  staff   2047  4 Oct 16:28 64a1c81362ecf20763da064a5d699029d6a388
 8 -r--r--r--  1 jcprime  staff   2304  4 Oct 16:28 65649c838b6e19477d0e8bc66abc9d5e66e300
 8 -r--r--r--  1 jcprime  staff     43  4 Oct 16:27 96e73a12c4238eab8a8fa5c7efec5d9f7062bf
 8 -r--r--r--  1 jcprime  staff    492  4 Oct 16:28 a8f73ff23e2adc88b35b4cee51ba8500eb820e
 8 -r--r--r--  1 jcprime  staff    450  4 Oct 16:28 e0dc5d23fc33385086ada7014f1a1c7eac07fb
32 -r--r--r--  1 jcprime  staff  13082  4 Oct 16:28 e82e06202120eabd6b0d6884a77ddcd30c0774

./.git/objects/38:
total 336
200 -r--r--r--  1 jcprime  staff  101943  4 Oct 16:28 1713c81d6c951962fed5cf666a13faf6c0305a
  8 -r--r--r--  1 jcprime  staff    1259  4 Oct 16:28 1a4582a0a0b377d954573f7ab172c4a9af878d
  8 -r--r--r--  1 jcprime  staff    3573  4 Oct 16:28 32ea3a46874977c44f1735555bf37c0da88349
  8 -r--r--r--  1 jcprime  staff    2324  4 Oct 16:28 5864c84c0d9a209552b6342b564d68b66aae74
  8 -r--r--r--  1 jcprime  staff    2318  4 Oct 16:27 669aaebb2185f5739190d8e41fae1fb33ad38e
 16 -r--r--r--  1 jcprime  staff    7463  4 Oct 16:28 acf3d35ecc464748816802393d263229018aa0
  8 -r--r--r--  1 jcprime  staff    3101  4 Oct 16:28 bd68256f3199a3fa8eff11a636e0904bf893bd
  8 -r--r--r--  1 jcprime  staff     184  4 Oct 16:29 c12bad59e68eb3deb0e559727a8a2472d31a9a
 24 -r--r--r--  1 jcprime  staff    8259  4 Oct 16:28 c1e60f3a78b3fa3328a11703706419f6de22d2
 40 -r--r--r--  1 jcprime  staff   16787  4 Oct 16:28 c52d838898e2cba06b81e0154f648fc41659e5
  8 -r--r--r--  1 jcprime  staff      46  4 Oct 16:29 df41be5d20795e979bc8eb2a7e44814840a8d4

./.git/objects/39:
total 120
 8 -r--r--r--  1 jcprime  staff   1254  4 Oct 16:28 12b7ca38c9ec1e3700bd22310b9ef410087903
 8 -r--r--r--  1 jcprime  staff    654  4 Oct 16:28 1752576f41fe07f45e53940e29878d9cb9599c
96 -r--r--r--  1 jcprime  staff  47047  4 Oct 16:28 3e714d50effcfd0407dd21a5f95653d7616653
 8 -r--r--r--  1 jcprime  staff   4009  4 Oct 16:28 bf9844d936f28fbbae028ca4db8c5c29699f7b

./.git/objects/3a:
total 88
16 -r--r--r--  1 jcprime  staff   4828  4 Oct 16:28 6cdecad248432fb6119849e976127a0bcdc339
32 -r--r--r--  1 jcprime  staff  14844  4 Oct 16:28 7c3fd3784e5cf17fd9bbb5835efcf0f2238f43
 8 -r--r--r--  1 jcprime  staff    366  4 Oct 16:28 9b50ee9b62f40087bec9482a1f17f9e551006c
 8 -r--r--r--  1 jcprime  staff   1739  4 Oct 16:27 a6a92b3861f2c49bba4b3b8b29c047a95efc00
 8 -r--r--r--  1 jcprime  staff   1257  4 Oct 16:28 b5da5aef8e4f78da6f812bb6aa9dbb957ba81d
 8 -r--r--r--  1 jcprime  staff   1486  4 Oct 16:28 c831823b347d50f3d86778e403aa26fd4ff911
 8 -r--r--r--  1 jcprime  staff   2335  4 Oct 16:28 d7b9a80d59f245314a167c38eae93bd9edfd88

./.git/objects/3b:
total 48
8 -r--r--r--  1 jcprime  staff  1688  4 Oct 16:28 49c92c49fb004a09741495f16033afb83c0356
8 -r--r--r--  1 jcprime  staff   653  4 Oct 16:29 64dfd80bf57b46131192c964b11b34b11256af
8 -r--r--r--  1 jcprime  staff   617  4 Oct 16:28 6655b1e24c8c79dc278c01b705d27269769fd6
8 -r--r--r--  1 jcprime  staff  1249  4 Oct 16:28 a947da46d9d118c3cda3259ba8d3a2c9f5bf0e
8 -r--r--r--  1 jcprime  staff  2331  4 Oct 16:28 c9eb2fd99b295828626e0525335ffba6d9da6a
8 -r--r--r--  1 jcprime  staff  1545  4 Oct 16:27 ebc220b510c79c5a97a8c343896768ebab38a3

./.git/objects/3c:
total 64
24 -r--r--r--  1 jcprime  staff  8506  4 Oct 16:28 2aea1e57a953a8e4b08fa510ac9c30d38631e3
16 -r--r--r--  1 jcprime  staff  4936  4 Oct 16:28 7ace913ee0b08d7c34a838f5a362cd0feb1cb8
 8 -r--r--r--  1 jcprime  staff   743  4 Oct 16:28 a16b1124a41005953568a028b7d302d98fcb2e
 8 -r--r--r--  1 jcprime  staff    54  4 Oct 16:18 ce93882e2c0e9d741459c1583b483b1e0aa1f3
 8 -r--r--r--  1 jcprime  staff  1478  4 Oct 16:28 de645cf68c0b36ddfa70643431f4603acb7a91

./.git/objects/3d:
total 80
16 -r--r--r--  1 jcprime  staff  4206  4 Oct 16:28 11a191f60ed4ac24cc56287a1d6081dc501e6e
 8 -r--r--r--  1 jcprime  staff  1564  4 Oct 16:28 15ff99202c01ee3c4f613df337e8f5b6e2abf0
 8 -r--r--r--  1 jcprime  staff   541  4 Oct 16:28 4c71798041c98e07d8411e5fca7f674d0682a6
 8 -r--r--r--  1 jcprime  staff   765  4 Oct 16:28 56fe7d2c61e3de776c3911b49aca54cf3b5ad3
 8 -r--r--r--  1 jcprime  staff  2529  4 Oct 16:27 6387fefe4cbf8fc35c004a8e15d5fcebbc75fd
 8 -r--r--r--  1 jcprime  staff   711  4 Oct 16:28 86a1fd9117de998b6aa92e3e559f1c081009c1
 8 -r--r--r--  1 jcprime  staff  2440  4 Oct 16:28 ac99585083020831b845dc9cae8545a137c78e
 8 -r--r--r--  1 jcprime  staff   692  4 Oct 16:28 d6756db041c7391aab0d413bd65b5cb4a3a372
 8 -r--r--r--  1 jcprime  staff   250  4 Oct 16:28 da34e63db5b77fc54ebe6930ee48bd888da637

./.git/objects/3e:
total 64
16 -r--r--r--  1 jcprime  staff  5718  4 Oct 16:28 21321d56f918fd1ee90cb2d84ca0efae2e221f
 8 -r--r--r--  1 jcprime  staff   108  4 Oct 16:28 45a04d76b94abef7671550f03fdcced5d5ad72
 8 -r--r--r--  1 jcprime  staff  1131  4 Oct 16:28 8492bbba4201fd63bc1235a0e7737eef927ee3
 8 -r--r--r--  1 jcprime  staff  1142  4 Oct 16:28 bc5b8d465d295dca71474e70063128b30e4fc3
16 -r--r--r--  1 jcprime  staff  6141  4 Oct 16:28 c8162119c5cfb6b6c11f5facf98a0ce580a638
 8 -r--r--r--  1 jcprime  staff  2575  4 Oct 16:27 d8c1eacc6c8147bd48713367cac8f111cb28f4

./.git/objects/3f:
total 24
8 -r--r--r--  1 jcprime  staff   846  4 Oct 16:28 262ff55433b733ce472d150409e85543a004fe
8 -r--r--r--  1 jcprime  staff  1052  4 Oct 16:27 3192568ef9e87c70bccbbce61bc7484d98e52b
8 -r--r--r--  1 jcprime  staff  2165  4 Oct 16:28 7f79e8a92b61686c7b05fa52a7e7a39e26f304

./.git/objects/40:
total 96
 8 -r--r--r--  1 jcprime  staff   1499  4 Oct 16:28 2a7e6ac269cdd5cb05794890c366bea36e1cd5
 8 -r--r--r--  1 jcprime  staff   2253  4 Oct 16:27 5e28cef91032d649ee73da17fa152df5ac9db8
56 -r--r--r--  1 jcprime  staff  28434  4 Oct 16:28 75dc42122b7bed1f871a059f9f41ca7ac4c844
16 -r--r--r--  1 jcprime  staff   5734  4 Oct 16:28 bf1664677356da67b83834c27c312d37c9ccb0
 8 -r--r--r--  1 jcprime  staff   2289  4 Oct 16:28 c7375cea2977668b156be7a8735bb62c37f2f6

./.git/objects/41:
total 152
 8 -r--r--r--  1 jcprime  staff   2306  4 Oct 16:28 24b3bad55df87f64a4d6b6765d3f001ae05369
 8 -r--r--r--  1 jcprime  staff   1486  4 Oct 16:28 3fa66fa794a056b993b3db2ddf2d44ff4f2014
 8 -r--r--r--  1 jcprime  staff   1532  4 Oct 16:27 4a7e2201e19cd1d8bac88d071bf43ff570d270
 8 -r--r--r--  1 jcprime  staff   1760  4 Oct 16:27 7f140a8823964af3c1e3f008bf4d1e4b417ff3
 8 -r--r--r--  1 jcprime  staff    733  4 Oct 16:28 8a3e08e4eb7e1ee45abfaf06df723b36e624c3
16 -r--r--r--  1 jcprime  staff   5314  4 Oct 16:27 8a8f04ab61b3ab5702bb5650002d259e2ac847
 8 -r--r--r--  1 jcprime  staff    454  4 Oct 16:28 b7fc2f7fb1e349efb35b15a4422f2ef9534f39
 8 -r--r--r--  1 jcprime  staff    179  4 Oct 16:29 b97e6970248fbb1a51cab2d685ce84735ef815
24 -r--r--r--  1 jcprime  staff   8391  4 Oct 16:28 b9e46072441c1ab9ded14013b7b303f7af05a8
40 -r--r--r--  1 jcprime  staff  19264  4 Oct 16:27 c2ef3bf41366ace8f90392ade329ec1baf61f1
16 -r--r--r--  1 jcprime  staff   5202  4 Oct 16:28 cc12d000687d6958523576828c421cd2611ad4

./.git/objects/42:
total 8
8 -r--r--r--  1 jcprime  staff  2006  4 Oct 16:28 62a9d32ced6cc02fdf0f8a923eaa2082364e0a

./.git/objects/43:
total 32
8 -r--r--r--  1 jcprime  staff  1463  4 Oct 16:28 29ccb8f770fbaed985d9a67e5fc51e2e3053b0
8 -r--r--r--  1 jcprime  staff  2261  4 Oct 16:27 2f0d84f5988ee75a1741e407962dbabaf3da48
8 -r--r--r--  1 jcprime  staff  1534  4 Oct 16:27 727cf4ebbf812d8cd1280a254c6c8787d588ba
8 -r--r--r--  1 jcprime  staff   579  4 Oct 16:28 fbd0e5239337d93f0a1d37fdb59028429da313

./.git/objects/44:
total 408
  8 -r--r--r--  1 jcprime  staff    1524  4 Oct 16:28 30d445ccb2fe3699ed221e8bc97acb5d5ecced
 48 -r--r--r--  1 jcprime  staff   21589  4 Oct 16:28 72d157b5f2cd3f4692a5240908f6d6adaedb4f
336 -r--r--r--  1 jcprime  staff  168110  4 Oct 16:27 822001c82eec61a5cd9af8126a4be8793e52bc
  8 -r--r--r--  1 jcprime  staff    3556  4 Oct 16:28 89c47a38bd0f0bb2bfa5100509ec399f7cb35e
  8 -r--r--r--  1 jcprime  staff     978  4 Oct 16:28 a9a2bc9eb227dd4d46e4b18044a5922be7404b

./.git/objects/45:
total 40
 8 -r--r--r--  1 jcprime  staff  1025  4 Oct 16:28 37354f19ccf42f0eeb250fbb938078bdaececd
16 -r--r--r--  1 jcprime  staff  5467  4 Oct 16:28 3ca4880e0ca40d23c8a56225248d038c0c6668
 8 -r--r--r--  1 jcprime  staff   679  4 Oct 16:28 561d9a48e93bc8e686816844cb13bb65cd8af9
 8 -r--r--r--  1 jcprime  staff  1053  4 Oct 16:27 f6c4f5c4d4a29b2a07ffa4c430757b548503ef

./.git/objects/46:
total 256
  8 -r--r--r--  1 jcprime  staff    1661  4 Oct 16:28 24eaf9517fd98c8f5b970fa60b61c567f76d5a
  8 -r--r--r--  1 jcprime  staff    1808  4 Oct 16:28 35a6b01a667adbe38f3d1c43fe2d14fe64ec23
  8 -r--r--r--  1 jcprime  staff    1890  4 Oct 16:27 6f9f5d41f35b6e296ef8ba9cac12cecef1a747
  8 -r--r--r--  1 jcprime  staff     189  4 Oct 16:28 ae054dae7cd7d0d300d739961e471eaf84ad33
  8 -r--r--r--  1 jcprime  staff     553  4 Oct 16:28 dad192530596f75c0bfc376c1b82b60352e3a0
200 -r--r--r--  1 jcprime  staff  100034  4 Oct 16:28 f00249e287c380818705828fbbb9b77a00245a
 16 -r--r--r--  1 jcprime  staff    6824  4 Oct 16:28 f93f117a132984560797e5112e256e4b7a57fc

./.git/objects/47:
total 136
 8 -r--r--r--  1 jcprime  staff   3169  4 Oct 16:28 1b998beb47827e3a44a1bc26576c05ab2df19d
 8 -r--r--r--  1 jcprime  staff    467  4 Oct 16:28 25bf0571806420e53e54b4f93074e407e46a98
24 -r--r--r--  1 jcprime  staff   9478  4 Oct 16:28 349c815c4b5772255e754a05fa352013b398b7
32 -r--r--r--  1 jcprime  staff  13433  4 Oct 16:28 7e9099ea12a019231ca056ab86eef785f85793
 8 -r--r--r--  1 jcprime  staff   1978  4 Oct 16:28 bc5c7145dbebdf03199fa942bb031468aea1a3
 8 -r--r--r--  1 jcprime  staff    253  4 Oct 16:28 c61eb567951b63ae768612f091082b97138eab
 8 -r--r--r--  1 jcprime  staff    262  4 Oct 16:28 e76b6da829a09f03e5503add0dabc08dbb17ed
40 -r--r--r--  1 jcprime  staff  19058  4 Oct 16:28 f2b07f33c6742e4ca7ca5e5a3dc09a527423b6

./.git/objects/48:
total 32
 8 -r--r--r--  1 jcprime  staff  3390  4 Oct 16:28 3254d1f9161c79af67cfa54edf5eb382cd6a48
 8 -r--r--r--  1 jcprime  staff  1220  4 Oct 16:28 bf0ff4fc0dbb3f18086a6ae3a5e6bae99e4e10
16 -r--r--r--  1 jcprime  staff  5832  4 Oct 16:28 f4d473e8fecf52fe56e2c8e21787fb6dda1d48

./.git/objects/49:
total 64
 8 -r--r--r--  1 jcprime  staff  1093  4 Oct 16:28 32105bd413949972c043e7f95720f928dff85a
 8 -r--r--r--  1 jcprime  staff   546  4 Oct 16:28 448de5225be6424eeb940d30b6429c089554aa
24 -r--r--r--  1 jcprime  staff  9620  4 Oct 16:28 60c5c24617a3b7a59f84561fd0c3b6a7724c4e
 8 -r--r--r--  1 jcprime  staff  2985  4 Oct 16:28 82120dca0e43274ac29dd7dca1e62c0aa18505
 8 -r--r--r--  1 jcprime  staff  1343  4 Oct 16:28 c9f38d4fff7091384fbdb664d1b56b5224e5b1
 8 -r--r--r--  1 jcprime  staff  1475  4 Oct 16:28 cedca91eee7733f52e6b26224bd04f5fa0db63

./.git/objects/4a:
total 32
 8 -r--r--r--  1 jcprime  staff   464  4 Oct 16:28 51faaaac5439b7c6ff9b406224a130c05edf8f
16 -r--r--r--  1 jcprime  staff  6807  4 Oct 16:28 9e729f301afddaaffa6bffc20fc2c28994446a
 8 -r--r--r--  1 jcprime  staff  2206  4 Oct 16:28 fe2aedcd12fe04e15ade6287440536e25952fa

./.git/objects/4b:
total 80
 8 -r--r--r--  1 jcprime  staff   2296  4 Oct 16:27 06087fc7862e0bf473cdcd58536150c4e28f19
24 -r--r--r--  1 jcprime  staff  10662  4 Oct 16:28 517384dfd56144131543004a718d7cbf043e51
 8 -r--r--r--  1 jcprime  staff   1581  4 Oct 16:27 52d26f7a30be187a6649af690119e3fa2f42a3
32 -r--r--r--  1 jcprime  staff  12320  4 Oct 16:28 5518fedf9704eab1786d0a9f1a938909a5654f
 8 -r--r--r--  1 jcprime  staff    697  4 Oct 16:28 e6985761cdd7ecac9fe90494313d58ce717b6d

./.git/objects/4c:
total 48
 8 -r--r--r--  1 jcprime  staff   728  4 Oct 16:28 01eab90e6a5ad1acf863a26a7e31fe8b00d549
 8 -r--r--r--  1 jcprime  staff  1391  4 Oct 16:28 06cf51821ebdf38240f4b357ffa5b742891cd4
24 -r--r--r--  1 jcprime  staff  8981  4 Oct 16:28 ce7cd0510636a5a8cdf39195cd0dacfbaad324
 8 -r--r--r--  1 jcprime  staff    50  4 Oct 16:28 d555c0c6afe6c21d5ccff9bc797db1732b892a

./.git/objects/4d:
total 11792
    8 -r--r--r--  1 jcprime  staff      742  4 Oct 16:28 019d5dca526a90333d1e18302b07552ddefab3
    8 -r--r--r--  1 jcprime  staff      503  4 Oct 16:28 0be622d2ea74b6fe8d6f20ee78f9fcfa82d9e8
   16 -r--r--r--  1 jcprime  staff     5407  4 Oct 16:27 0dc79782108997f1a5793441c71e8852af1801
 1064 -r--r--r--  1 jcprime  staff   543269  4 Oct 16:28 1710bc4f266118917ed25017ce3d47f71f923b
    8 -r--r--r--  1 jcprime  staff     2711  4 Oct 16:28 2ef7c267badf7eb6daf236ba751c1c62e0ab19
10528 -r--r--r--  1 jcprime  staff  5387605  4 Oct 16:28 3361fdf0f345bc76f9f4043ca8efea5083b3d7
    8 -r--r--r--  1 jcprime  staff     3901  4 Oct 16:28 3926d4aa3b673878a33886b0e7d340f08916c5
    8 -r--r--r--  1 jcprime  staff     1782  4 Oct 16:27 4ff7bc5a58dbeb90318d733f3fda7b27584155
   16 -r--r--r--  1 jcprime  staff     6278  4 Oct 16:28 54b0d7d3a97407ca9242666e4a0550205f4fa4
    8 -r--r--r--  1 jcprime  staff     2743  4 Oct 16:28 7153e238214fdc5959dc5acd96915252c06b40
    8 -r--r--r--  1 jcprime  staff      596  4 Oct 16:28 8a159216c70630f9b21404c8c3315e86ff9aed
    8 -r--r--r--  1 jcprime  staff     1782  4 Oct 16:27 b83db6ebd9ca93d121ecd0c94c3c27111fbd6d
   16 -r--r--r--  1 jcprime  staff     7026  4 Oct 16:28 d0d215037176f5a929f7e21680bfb7e21e5b14
    8 -r--r--r--  1 jcprime  staff     1420  4 Oct 16:28 ee3d5fc4937db389417b4b3bc9ac1809e9f34d
    8 -r--r--r--  1 jcprime  staff     2720  4 Oct 16:27 fbb1a7ed08c273d52bdc67eaf69d3a6ddfed7a
   72 -r--r--r--  1 jcprime  staff    35561  4 Oct 16:28 fefc2eea199caf942592f03475f3f724233c43

./.git/objects/4e:
total 56
8 -r--r--r--  1 jcprime  staff   205  4 Oct 16:29 190d8e91eb26bbfff1c9d0455d42c9c13e3174
8 -r--r--r--  1 jcprime  staff  1504  4 Oct 16:28 41584acb42f6d4e51c87ec6a0725ec090996f0
8 -r--r--r--  1 jcprime  staff  3714  4 Oct 16:27 500285a60dd5a5cabd1b1a55d811c4bccf07f5
8 -r--r--r--  1 jcprime  staff   148  4 Oct 16:29 b3411410a129bf5f4d15e7df09d0809ffe7796
8 -r--r--r--  1 jcprime  staff  1462  4 Oct 16:28 dc609708944b0c90451ad8835e9eaa668fa54f
8 -r--r--r--  1 jcprime  staff  1298  4 Oct 16:28 fcbdc742e3c0796c7e39ac3160b391354b997e
8 -r--r--r--  1 jcprime  staff  1715  4 Oct 16:27 fcd792782d3eb8f3b20ce1e57e320b980ded45

./.git/objects/4f:
total 344
224 -r--r--r--  1 jcprime  staff  112690  4 Oct 16:28 17be7837d2192d99bfd280cd01256079fb0e77
104 -r--r--r--  1 jcprime  staff   52976  4 Oct 16:28 309be7f6ad138ec936f463bfad8c1fe4fc65d0
  8 -r--r--r--  1 jcprime  staff    1318  4 Oct 16:27 9882d1bb0bab366f4211e5066d745d94100ce5
  8 -r--r--r--  1 jcprime  staff    2311  4 Oct 16:28 fd47324291337b83a468f4c8d0869998691070

./.git/objects/50:
total 64
 8 -r--r--r--  1 jcprime  staff    58  4 Oct 16:29 1460a0983960f5bd97748fdbda71e10635c928
 8 -r--r--r--  1 jcprime  staff  1146  4 Oct 16:28 586b8502d75cefd4b38a8e59157e94786104a5
 8 -r--r--r--  1 jcprime  staff  2533  4 Oct 16:28 b5577b464114ea8582c42b5e4a17574e6d405c
 8 -r--r--r--  1 jcprime  staff  1494  4 Oct 16:28 bce38fc658811a3c08d42a1b8d24bbbd2d3083
24 -r--r--r--  1 jcprime  staff  8520  4 Oct 16:28 d08800a505df5a28717c232349219ab87ceed2
 8 -r--r--r--  1 jcprime  staff  1513  4 Oct 16:28 f68df16d63432e2d70ca859e9b058cc58853a0

./.git/objects/51:
total 32
24 -r--r--r--  1 jcprime  staff  10110  4 Oct 16:28 c68aebae8943c4a1f824a52cde90db0f7de0f6
 8 -r--r--r--  1 jcprime  staff   3322  4 Oct 16:28 f18219f334c3188a6f002461a12896182e83d8

./.git/objects/52:
total 72
 8 -r--r--r--  1 jcprime  staff   3107  4 Oct 16:28 6ddedb7cf32df3c8c71bb070ca43d37e920604
56 -r--r--r--  1 jcprime  staff  25203  4 Oct 16:28 961f92ab779750ee304dd88c83a45036efce66
 8 -r--r--r--  1 jcprime  staff   1167  4 Oct 16:27 dc9966a1da809d1353525facda0f7de6858ef3

./.git/objects/53:
total 96
 8 -r--r--r--  1 jcprime  staff     61  4 Oct 16:29 29343bda90c51134905260a7f500563e698069
 8 -r--r--r--  1 jcprime  staff    270  4 Oct 16:29 38f4c67ba5f15804fcbedee0083c18b281d119
 8 -r--r--r--  1 jcprime  staff    790  4 Oct 16:28 575556b1f46b70168dd1489552a1cd4c524dba
 8 -r--r--r--  1 jcprime  staff   2203  4 Oct 16:27 81db120055044413217109c3f89667f9371fd5
40 -r--r--r--  1 jcprime  staff  16774  4 Oct 16:28 8f789c4a4b6232bf38aa0e947153730c3772cc
 8 -r--r--r--  1 jcprime  staff   3685  4 Oct 16:28 c8fb47536acb393cc347911c33c7330c1366ed
 8 -r--r--r--  1 jcprime  staff   1596  4 Oct 16:28 ce1fb684c9d6005f8c2eb37d18de777190c983
 8 -r--r--r--  1 jcprime  staff   1267  4 Oct 16:28 f18ea947b3a09ea749f2717bb93d55777825fe

./.git/objects/54:
total 25792
    8 -r--r--r--  1 jcprime  staff      1387  4 Oct 16:28 0cb23f09625e9028ef7ad421cb1855efe602ce
    8 -r--r--r--  1 jcprime  staff       885  4 Oct 16:28 10978e013f6dba8eda47e7bd738fbb32d8005f
    8 -r--r--r--  1 jcprime  staff      1880  4 Oct 16:27 15fda40c969dcf7749544a8e167ae866ac62e2
   24 -r--r--r--  1 jcprime  staff     10987  4 Oct 16:28 905a60505cd2036d4daecce31d375d8154b82c
25744 -r--r--r--  1 jcprime  staff  13180660  4 Oct 16:28 e151b8b75dde9ef2660697930c4d5c14eda9a3

./.git/objects/55:
total 40
8 -r--r--r--  1 jcprime  staff  1466  4 Oct 16:28 2df5ae04c420c094e2074422ccc1b0f8802992
8 -r--r--r--  1 jcprime  staff  1436  4 Oct 16:28 b95cdf1df1d207b8a182e6f520a812a0a4887a
8 -r--r--r--  1 jcprime  staff  1839  4 Oct 16:28 c0576aadafa37d56e018cd4765649424090db9
8 -r--r--r--  1 jcprime  staff   138  4 Oct 16:28 c1b87df3b54bb41ba513bac5bfa7cbf9e1381f
8 -r--r--r--  1 jcprime  staff  2340  4 Oct 16:27 d7f5891cb7c6e68ef646853feee06ecaf7a8bf

./.git/objects/56:
total 88
 8 -r--r--r--  1 jcprime  staff  1451  4 Oct 16:28 5ee48f91501ace3354b50de0e67d09816303e8
 8 -r--r--r--  1 jcprime  staff    72  4 Oct 16:28 7074ed41e0e7e9619e83f491841e50da5703bd
 8 -r--r--r--  1 jcprime  staff   488  4 Oct 16:28 9a71f5d76813256d194c724574676542827952
16 -r--r--r--  1 jcprime  staff  5372  4 Oct 16:28 ad0342a7b2df64eabcb04cb547a4c85ac96840
 8 -r--r--r--  1 jcprime  staff  1558  4 Oct 16:27 ae7b2fd5a089b40402bf5abd7d1e7667e58790
24 -r--r--r--  1 jcprime  staff  9295  4 Oct 16:28 b74b2d845b9022363a94491a7ce358c4ce1867
 8 -r--r--r--  1 jcprime  staff  1397  4 Oct 16:28 bddac0233b81f4a27ad4050a43e9323761f2c2
 8 -r--r--r--  1 jcprime  staff   612  4 Oct 16:28 f9a60480833155321fbc086cee201467e4f946

./.git/objects/57:
total 48
16 -r--r--r--  1 jcprime  staff  7476  4 Oct 16:28 5a5a3e0633de6abb14af0e3cc239ccb0b36d98
 8 -r--r--r--  1 jcprime  staff  4081  4 Oct 16:28 7088fa724fe49d9a380cfd1960ee72c52f98fe
 8 -r--r--r--  1 jcprime  staff  1696  4 Oct 16:28 9d3c5c0b94d3b070a526f68b097b2a34ad7b18
 8 -r--r--r--  1 jcprime  staff    85  4 Oct 16:28 b0cf4836194d759d71ae8f6ff7490099e05320
 8 -r--r--r--  1 jcprime  staff  2242  4 Oct 16:28 f399104f8f4d91384d7aabdf3f2606b0c5aa36

./.git/objects/58:
total 80
24 -r--r--r--  1 jcprime  staff  9209  4 Oct 16:28 0e622d7619eba7b16928041e53cbaab67a2ed0
 8 -r--r--r--  1 jcprime  staff  2569  4 Oct 16:27 1245247faefff854e6c796e28865ff9dc34316
 8 -r--r--r--  1 jcprime  staff   432  4 Oct 16:28 1772987380100b2dc055490a313d1be135bf4d
 8 -r--r--r--  1 jcprime  staff  2603  4 Oct 16:28 2e7a022b5a4ad5a4b5414f68a45738ddef3b7e
 8 -r--r--r--  1 jcprime  staff  1864  4 Oct 16:28 737705e05955f585da597f7ecbbe3d0287674a
 8 -r--r--r--  1 jcprime  staff  1555  4 Oct 16:28 92fc5fe5d60d9b342d4c860b4c13540dbb7fed
 8 -r--r--r--  1 jcprime  staff    58  4 Oct 16:29 9e26d05ab45dc6be6e83cdcfc4b151d935d775
 8 -r--r--r--  1 jcprime  staff  1312  4 Oct 16:27 edf33e064b71f84526443efba2431215bcf806

./.git/objects/59:
total 88
16 -r--r--r--  1 jcprime  staff  6106  4 Oct 16:28 239d947c6d5cc3fe5edb10153e1aecb74e067f
 8 -r--r--r--  1 jcprime  staff  2326  4 Oct 16:27 2b3ab4ee4cc9289dbc5708f666b6593119f466
 8 -r--r--r--  1 jcprime  staff   524  4 Oct 16:28 3d6a71e69259a40c0361815349d22c1c07b257
24 -r--r--r--  1 jcprime  staff  9931  4 Oct 16:28 5106e354d98856ef75f82ecb0e9c97e0178e19
 8 -r--r--r--  1 jcprime  staff  1702  4 Oct 16:27 68264d5f9f746ac3822a0a8becb179c53631c4
 8 -r--r--r--  1 jcprime  staff  3623  4 Oct 16:28 7c9473f29b1268099478ada71852ced12d6e56
 8 -r--r--r--  1 jcprime  staff  1004  4 Oct 16:28 80ecc350300aa37b758706ef70372bfdb4abb9
 8 -r--r--r--  1 jcprime  staff  1261  4 Oct 16:28 85be6d1085c7f28caa5c60b3af4f3029de4ee3

./.git/objects/5a:
total 32
8 -r--r--r--  1 jcprime  staff  3067  4 Oct 16:28 3f7c1e0ff70fc007e76236317e8b5b02758f8f
8 -r--r--r--  1 jcprime  staff  1262  4 Oct 16:28 5e28e09355ae07875078b986a768e4758d9c21
8 -r--r--r--  1 jcprime  staff  3313  4 Oct 16:28 8ee357d830dc9e3f7dc2be36aa189bc1ec3d09
8 -r--r--r--  1 jcprime  staff  2328  4 Oct 16:28 fcc681fa795fc02049e5d8615ebf0308398bca

./.git/objects/5b:
total 96
 8 -r--r--r--  1 jcprime  staff  2201  4 Oct 16:28 263a2868758b4d26b787ff620535ff65df887a
 8 -r--r--r--  1 jcprime  staff   692  4 Oct 16:28 3636e9ff5d1024a7446efed33098104bcbb823
 8 -r--r--r--  1 jcprime  staff   926  4 Oct 16:28 3f3a50efae3ebb806e1b3e0e7a5fdc73e07289
 8 -r--r--r--  1 jcprime  staff  2548  4 Oct 16:28 ab553e5ce6a889ffa68746e71e9f3330cf64f2
 8 -r--r--r--  1 jcprime  staff   376  4 Oct 16:28 b750f649d1225be8555bdd7f3101988a3983a4
 8 -r--r--r--  1 jcprime  staff  2307  4 Oct 16:27 cdb34b5508868dbef0905b413de5d052b88f5f
 8 -r--r--r--  1 jcprime  staff   802  4 Oct 16:28 e3aec83c0e8366d2eae532386ab16fa89ebd12
16 -r--r--r--  1 jcprime  staff  4519  4 Oct 16:28 e7cbcf7cc839740fe8813fe453966ed3c8d7fe
 8 -r--r--r--  1 jcprime  staff  2283  4 Oct 16:27 f85f28fa7300e673714ac1c4b142fad7b6aea0
 8 -r--r--r--  1 jcprime  staff   300  4 Oct 16:29 fc0c059fc041fe17ad496a52b77a193b144696
 8 -r--r--r--  1 jcprime  staff    69  4 Oct 16:28 fcb4779ef24acff84ebb2902f1ed33f60803d7

./.git/objects/5c:
total 6328
   8 -r--r--r--  1 jcprime  staff     2315  4 Oct 16:27 47f1e1e11b4520670d8180dfd73b135a240dd9
6264 -r--r--r--  1 jcprime  staff  3204316  4 Oct 16:28 56d92d52d695fd145ba6a106431864f8fe2b5d
   8 -r--r--r--  1 jcprime  staff     2787  4 Oct 16:28 84419d2c5b116659bfe8e6edbdbf1e788bb321
   8 -r--r--r--  1 jcprime  staff      369  4 Oct 16:28 ad70aa9a8e4d14028ecc8ccfa7f54abdc4e8a0
   8 -r--r--r--  1 jcprime  staff     4057  4 Oct 16:28 bfe3069b4e992e9e655711ab34969523aa26e4
   8 -r--r--r--  1 jcprime  staff     2410  4 Oct 16:27 c31b6ca3163054a0fe06777a4069e3fd86f733
   8 -r--r--r--  1 jcprime  staff     1444  4 Oct 16:28 dad9bdbc4854617e25882d80a08226b4340288
   8 -r--r--r--  1 jcprime  staff      244  4 Oct 16:28 eedd2b4881b9e2787d08c002816b9b631e9d7a
   8 -r--r--r--  1 jcprime  staff     1515  4 Oct 16:28 fba4b669281be10799ae0d052ec8d7d25c8bef

./.git/objects/5d:
total 40
16 -r--r--r--  1 jcprime  staff  7277  4 Oct 16:28 4f776ffa7fc719683d3817b5e07d45a6ef2c2d
 8 -r--r--r--  1 jcprime  staff  2299  4 Oct 16:28 9b1641bdb58f280f51216e8615768a7bc34de8
 8 -r--r--r--  1 jcprime  staff  3949  4 Oct 16:28 b1d90e2a827c4640bedf95ccbd34a7b0cbffac
 8 -r--r--r--  1 jcprime  staff    30  4 Oct 16:35 f139ac121716148ab8ea1d3102186075480fe1

./.git/objects/5e:
total 24
8 -r--r--r--  1 jcprime  staff  2775  4 Oct 16:28 3e0c0dd73da15009cf3331e1239ecbc19de830
8 -r--r--r--  1 jcprime  staff   311  4 Oct 16:17 82864b8e2e65ab5a690ea8ab76ac50212b7fe9
8 -r--r--r--  1 jcprime  staff  3521  4 Oct 16:28 f524f15eb4284d2181dddd90226fc36bd0f6c5

./.git/objects/5f:
total 48
 8 -r--r--r--  1 jcprime  staff  1018  4 Oct 16:28 2ca4ab28056b85ae9c2f902e954493cf366f02
16 -r--r--r--  1 jcprime  staff  7936  4 Oct 16:28 458a911ef82cc0cd1749980e875c69e27b917d
 8 -r--r--r--  1 jcprime  staff  1552  4 Oct 16:28 46657c8376c1224cbfbf13d78fd6ae160c581d
 8 -r--r--r--  1 jcprime  staff   958  4 Oct 16:28 4f699cec857374f8451fc1eea11c4ab6e47ce2
 8 -r--r--r--  1 jcprime  staff  2567  4 Oct 16:28 721524178ea9e167ebe33b304a62bfb83fe6e0

./.git/objects/60:
total 64
 8 -r--r--r--  1 jcprime  staff   890  4 Oct 16:28 088c4587a1af63e92195f941dd0b23bbbec7a9
 8 -r--r--r--  1 jcprime  staff  1291  4 Oct 16:28 3b7c48ad8b1c8d826a2e6afa66db9232e63259
 8 -r--r--r--  1 jcprime  staff  1729  4 Oct 16:27 54c1839a94dbc296839eb5e7cabd34b3c054d1
16 -r--r--r--  1 jcprime  staff  8167  4 Oct 16:28 7edf068aca7a6d958ed69147d6c19f53d2604f
 8 -r--r--r--  1 jcprime  staff  1579  4 Oct 16:28 a4e867e57b45fb70c2b0b9ffd1d8c3bfc62a2a
 8 -r--r--r--  1 jcprime  staff   659  4 Oct 16:28 c352091d0d4884cf4e0fceb9970ee0547bd815
 8 -r--r--r--  1 jcprime  staff  3920  4 Oct 16:28 dc53d03118813b2092b0036180fc3c17b6867c

./.git/objects/61:
total 56
8 -r--r--r--  1 jcprime  staff  1288  4 Oct 16:27 379c8f30743453ae3d4de635cb805941b7dc4a
8 -r--r--r--  1 jcprime  staff  2969  4 Oct 16:28 3b3873fc63842aa170c7d4ffbd85825308107b
8 -r--r--r--  1 jcprime  staff  2318  4 Oct 16:28 3ee9519c8416c6aac328849953ab7e634ea7be
8 -r--r--r--  1 jcprime  staff   529  4 Oct 16:28 621f991943b189ae61fc03c6960e966b83f61c
8 -r--r--r--  1 jcprime  staff   535  4 Oct 16:28 b879a5d0b36d2dae37a482edb94bef88a38862
8 -r--r--r--  1 jcprime  staff   302  4 Oct 16:28 df590f1d16cd7bf89189b694a11fab599bc547
8 -r--r--r--  1 jcprime  staff  1457  4 Oct 16:27 e2020ea9babaabe694ac5be424c2324d9035d3

./.git/objects/62:
total 56
 8 -r--r--r--  1 jcprime  staff   144  4 Oct 16:28 1b1e0fb3a7402bac76c12b585238559c65f81f
 8 -r--r--r--  1 jcprime  staff  2219  4 Oct 16:27 1ee58267dbf663ef85ca26e0d054316269dc77
 8 -r--r--r--  1 jcprime  staff  1305  4 Oct 16:28 277b10e5f885a033c7a22249c6d31799d0b170
 8 -r--r--r--  1 jcprime  staff  1426  4 Oct 16:28 a53361abc223e54ffaee64a192976da13898e2
 8 -r--r--r--  1 jcprime  staff  1141  4 Oct 16:28 b0878d01229762de463cf77cb762c1f65a1f06
16 -r--r--r--  1 jcprime  staff  7486  4 Oct 16:28 d0ac4792d77d7c07f299cc85f6d27efc7c1b58

./.git/objects/63:
total 24
8 -r--r--r--  1 jcprime  staff  1805  4 Oct 16:28 b59a0e172c1b15bdb4ac7b84202edf448a604a
8 -r--r--r--  1 jcprime  staff   748  4 Oct 16:28 da5085398bec7ae3363bbd1ee8260a22ba7175
8 -r--r--r--  1 jcprime  staff   179  4 Oct 16:24 fd06fe3c1e9cb59ba81a36b940326dfe85c210

./.git/objects/64:
total 64
40 -r--r--r--  1 jcprime  staff  18570  4 Oct 16:28 2055de65a28654447bbca2199b0f0daee1a124
 8 -r--r--r--  1 jcprime  staff   3155  4 Oct 16:28 7014a054b423b5412e5765e193450a9d674c27
 8 -r--r--r--  1 jcprime  staff   2005  4 Oct 16:27 8c1e9a8f1a7e0c51484e5a79cb8f350ed4e5d1
 8 -r--r--r--  1 jcprime  staff    792  4 Oct 16:28 d0f73ac9a1a13ea2662a43306d4246df0434f9

./.git/objects/65:
total 104
 8 -r--r--r--  1 jcprime  staff   3025  4 Oct 16:28 0e13d2320ee758367e859cdf36d53c786d1632
32 -r--r--r--  1 jcprime  staff  14958  4 Oct 16:28 1c9a627fee85a55b7f6ba6d6096de18e945e74
 8 -r--r--r--  1 jcprime  staff   1514  4 Oct 16:27 25d907a5e5cc21d579ca2e72bab1b5cd143530
16 -r--r--r--  1 jcprime  staff   5024  4 Oct 16:28 7a33201717f9ed36523f4aae5ca847287c8d1b
 8 -r--r--r--  1 jcprime  staff   1728  4 Oct 16:28 bf70a2781271d82a5c86805215325220e48811
 8 -r--r--r--  1 jcprime  staff   2395  4 Oct 16:28 d4b23137bf79b46a6f81847b5fb77c04b0e310
24 -r--r--r--  1 jcprime  staff  11284  4 Oct 16:28 f43ea792f028646018f5e57fd52c6945857199

./.git/objects/66:
total 32
8 -r--r--r--  1 jcprime  staff  1827  4 Oct 16:29 2270c58d0311b443219b9c0d6750a722c6fd7a
8 -r--r--r--  1 jcprime  staff  1049  4 Oct 16:28 34d87410163d5a5c49938973a4da49ab58152c
8 -r--r--r--  1 jcprime  staff   846  4 Oct 16:28 4dd01dc09609691d8f6e4189de4a6a0703fb78
8 -r--r--r--  1 jcprime  staff  2270  4 Oct 16:27 e2504c4476e1aae3d7023b82e2f6a82870d72b

./.git/objects/67:
total 80
24 -r--r--r--  1 jcprime  staff  9441  4 Oct 16:28 05c0401b2896c78c1f6c6e8861b38e0aff3dc7
 8 -r--r--r--  1 jcprime  staff  1127  4 Oct 16:28 18c0543a77e7c8036afed72dfce39f85597522
 8 -r--r--r--  1 jcprime  staff  2299  4 Oct 16:28 1e8060be0e1efdd1707635c674f5bfa0dcf197
24 -r--r--r--  1 jcprime  staff  8228  4 Oct 16:28 82d1083fbc7f2e152f0c0b6c84111deb181543
 8 -r--r--r--  1 jcprime  staff  3328  4 Oct 16:28 b0304bfa0de83bb5cfd206f8c844eb1c3dc5c6
 8 -r--r--r--  1 jcprime  staff  1551  4 Oct 16:27 b37ebb14859d650f00d932381170c2bca5f532

./.git/objects/68:
total 40
16 -r--r--r--  1 jcprime  staff  5846  4 Oct 16:28 3a3995f2e6e9926b374be50f4a994348d46142
 8 -r--r--r--  1 jcprime  staff   870  4 Oct 16:28 59688c047a277db6277520116d6ad10a9a514d
 8 -r--r--r--  1 jcprime  staff  1557  4 Oct 16:27 7e94291cf901bf8129e8d4745ea03b2a31d980
 8 -r--r--r--  1 jcprime  staff   113  4 Oct 16:27 8d5e15dbf31df5ce599453f16b69ec9f4530ac

./.git/objects/69:
total 136
 8 -r--r--r--  1 jcprime  staff   1484  4 Oct 16:29 1b92fce757f4a2716e0d0c04ab2714a222b390
 8 -r--r--r--  1 jcprime  staff   1754  4 Oct 16:27 1f14ce7d7e81a52de57f0bdfe9167c9826bc26
 8 -r--r--r--  1 jcprime  staff   3735  4 Oct 16:28 76eed02eb8cf136fecffd0533e04d945098478
16 -r--r--r--  1 jcprime  staff   4330  4 Oct 16:28 80235ee44e58237a8d2df06bdd066e940a6b6c
 8 -r--r--r--  1 jcprime  staff     72  4 Oct 16:28 a08d9bd05fb662f4d6a01ac020b9541c94598b
48 -r--r--r--  1 jcprime  staff  21522  4 Oct 16:28 b26cf7590500f4801ac6e93ccc631d98619d95
 8 -r--r--r--  1 jcprime  staff    768  4 Oct 16:28 e9c8bb42af78836a60d1f4461df83b4b04bfaa
32 -r--r--r--  1 jcprime  staff  12817  4 Oct 16:28 fa5a5999e661e50af1cde83fe3b160ec7d8679

./.git/objects/6a:
total 56
 8 -r--r--r--  1 jcprime  staff   321  4 Oct 16:28 40c97501842e40b2b0e62f8bc6f14cc4446fdd
 8 -r--r--r--  1 jcprime  staff    79  4 Oct 16:29 4db91692a17c72740f3f469f329f22746b1d23
 8 -r--r--r--  1 jcprime  staff   947  4 Oct 16:28 7cd5bd96e12a06bb16a73ba8fde89fc763276e
 8 -r--r--r--  1 jcprime  staff  1148  4 Oct 16:27 946dac1e6c56facc344bd5f65eb9343949bb5f
16 -r--r--r--  1 jcprime  staff  5897  4 Oct 16:28 a6bfbc1bfd9ce5c12b3a5faad4027986655834
 8 -r--r--r--  1 jcprime  staff   508  4 Oct 16:28 d342ef00bb6319c2911868ce71c9c4cea941ce

./.git/objects/6b:
total 320
 16 -r--r--r--  1 jcprime  staff   5938  4 Oct 16:28 02203b632514ee073481f24c588b27c402ae96
  8 -r--r--r--  1 jcprime  staff    614  4 Oct 16:28 0345581073ff39357a99acc55bc5d9203b1dbb
  8 -r--r--r--  1 jcprime  staff    444  4 Oct 16:28 30de44630d66c29d6df971415c4d1c8707b396
200 -r--r--r--  1 jcprime  staff  99915  4 Oct 16:28 615f641cb35f1a0c60de9299f57c567288c10a
  8 -r--r--r--  1 jcprime  staff   3089  4 Oct 16:28 730aea405b357ab61a197d49d0708fcaa6e895
  8 -r--r--r--  1 jcprime  staff    513  4 Oct 16:28 a61c1c62c5dd7ac8b8d224fa2e7482993dfe9b
 56 -r--r--r--  1 jcprime  staff  28070  4 Oct 16:28 ad00d86f857407b58df9aba1811097c31c0bcc
  8 -r--r--r--  1 jcprime  staff   2055  4 Oct 16:28 be5e2f6b320af670b7f62e22900b6e575f295a
  8 -r--r--r--  1 jcprime  staff   2374  4 Oct 16:28 cc5639f7d231499bb2e1adbdf551448a9bf729

./.git/objects/6c:
total 120
32 -r--r--r--  1 jcprime  staff  16004  4 Oct 16:28 0f4cc0083398265ad7b0eadb2bdd0bb8881663
 8 -r--r--r--  1 jcprime  staff    457  4 Oct 16:29 10faf908908f7eff472b08b03ac78bbaccf576
 8 -r--r--r--  1 jcprime  staff   3535  4 Oct 16:28 270bcda5982ea4727d28280fc76c2dd9c0c4a6
 8 -r--r--r--  1 jcprime  staff   2287  4 Oct 16:27 664cf782986bdca5ec732995053fb9a36f2149
16 -r--r--r--  1 jcprime  staff   4256  4 Oct 16:28 7624e1d1175e409c526a9bf856651b2c22b0af
 8 -r--r--r--  1 jcprime  staff   1677  4 Oct 16:28 b9e13feae96388c22b3188c0d50cacf5d06579
32 -r--r--r--  1 jcprime  staff  13121  4 Oct 16:28 ca59697fb309bcd41685fa0038191058f920a2
 8 -r--r--r--  1 jcprime  staff    900  4 Oct 16:28 ceb194e80a27673e29f23ad16611c68ed0850d

./.git/objects/6d:
total 216
  8 -r--r--r--  1 jcprime  staff   2150  4 Oct 16:28 14d2c0fc601683c2d2f8d40e0094ef9a566a49
  8 -r--r--r--  1 jcprime  staff   3311  4 Oct 16:29 41dbdfdef6d482f48ee21dbbf5eaeeb8ecfe07
  8 -r--r--r--  1 jcprime  staff   3815  4 Oct 16:28 655ef3fdb4db8c6d8e0c000b4aa8ee96af1b9a
  8 -r--r--r--  1 jcprime  staff    810  4 Oct 16:28 7504ef9c3a8e6ea7a59fc5bc0c1aee56717099
  8 -r--r--r--  1 jcprime  staff   2634  4 Oct 16:29 8600bf3610742e801856b1f45c935dde7691cf
144 -r--r--r--  1 jcprime  staff  72388  4 Oct 16:28 93baa9ceb4f950fd29d9ca3a1a1af75192c4c8
  8 -r--r--r--  1 jcprime  staff   1252  4 Oct 16:27 bc2bc48bbce0131f86de97c57222c043bd8040
  8 -r--r--r--  1 jcprime  staff    748  4 Oct 16:28 c02df511bdd57be5dca8dccaa39b088007db89
 16 -r--r--r--  1 jcprime  staff   8147  4 Oct 16:28 e0b08a51d8156c5e0f2f49e6293eebeeffe67e

./.git/objects/6e:
total 48
 8 -r--r--r--  1 jcprime  staff   761  4 Oct 16:28 1c2e6a4e1b4e49df0610aa1f5d005800f98b90
 8 -r--r--r--  1 jcprime  staff  3817  4 Oct 16:27 725c6673002f997433fde381ecc33ef1a1ceae
16 -r--r--r--  1 jcprime  staff  6571  4 Oct 16:28 7508902baff00ccb37582b9fa7f27390dc006f
 8 -r--r--r--  1 jcprime  staff  2208  4 Oct 16:27 7fbec1b3293d8fc7d84f4b440d30f2453881fc
 8 -r--r--r--  1 jcprime  staff   740  4 Oct 16:28 f559113cbc11e9db147eb02c2845e6a1200bd9

./.git/objects/6f:
total 56
 8 -r--r--r--  1 jcprime  staff    105  4 Oct 16:28 0f39278b9b280914199048a24a6d68930ad308
 8 -r--r--r--  1 jcprime  staff    741  4 Oct 16:28 4d22e93521deaedf52e5aed572554205c1a1dc
 8 -r--r--r--  1 jcprime  staff    516  4 Oct 16:28 78216f98d11b22ebf8392d00d87fedaf4bc746
 8 -r--r--r--  1 jcprime  staff   1920  4 Oct 16:27 8c562544376d42881eda4d2236dd6e0b549492
24 -r--r--r--  1 jcprime  staff  12012  4 Oct 16:28 c9bd826919a0f072ec717f69f0335a4d41484d

./.git/objects/70:
total 744
  8 -r--r--r--  1 jcprime  staff    2253  4 Oct 16:27 13ffe781b34bdff379d5955fdc7bca94d7ed70
  8 -r--r--r--  1 jcprime  staff    2289  4 Oct 16:27 1959ff5eee0228ed02745edd873ba9b51a4c6d
592 -r--r--r--  1 jcprime  staff  302870  4 Oct 16:28 3e1e6697a5c139c44ed11b3212999792cbee66
 16 -r--r--r--  1 jcprime  staff    4808  4 Oct 16:28 45dd08e47003772066dcbbb239ab9cc5404267
  8 -r--r--r--  1 jcprime  staff     865  4 Oct 16:28 52771fa17ba0cf5164d1d9dd69c9d87e342b8d
  8 -r--r--r--  1 jcprime  staff    1921  4 Oct 16:27 5e118a395dce5c282b8d15525fe684c52166a5
  8 -r--r--r--  1 jcprime  staff    1491  4 Oct 16:28 81096add80238e5f9143ab92587572f8fa294f
 64 -r--r--r--  1 jcprime  staff   31116  4 Oct 16:28 82344d8ef6bb73f31511373aa4bbe194c93c70
  8 -r--r--r--  1 jcprime  staff     542  4 Oct 16:28 987d897e977d60353000c9694fbe88e21d20c1
  8 -r--r--r--  1 jcprime  staff     152  4 Oct 16:28 c0ee796d8c5d57d6e2937dda48a0ab370f1fde
 16 -r--r--r--  1 jcprime  staff    4911  4 Oct 16:28 ca123d66553028060a8edcd65d12fce6666b32

./.git/objects/71:
total 96
 8 -r--r--r--  1 jcprime  staff  1211  4 Oct 16:28 3eed1f2c992efbfd5ecf6284f6435d4d451d11
 8 -r--r--r--  1 jcprime  staff  2306  4 Oct 16:28 5907da9b6a2e2d87eb32185bfd60a8fe5aed19
16 -r--r--r--  1 jcprime  staff  6446  4 Oct 16:28 62520c14670ca99836e13b64df8b9b05ac4ec8
 8 -r--r--r--  1 jcprime  staff  3452  4 Oct 16:28 6628de29450ed7ddb2718918c72f0912851d1c
 8 -r--r--r--  1 jcprime  staff  1582  4 Oct 16:28 669718c1f847dfc9aa30f892e3f38f10aedbe4
 8 -r--r--r--  1 jcprime  staff  3577  4 Oct 16:28 6d7cb238450cc0c02e168de839ecf8652f4b37
 8 -r--r--r--  1 jcprime  staff  1810  4 Oct 16:27 837ec1e57092d8545cb52121f18f6fd1253372
 8 -r--r--r--  1 jcprime  staff  1980  4 Oct 16:28 a5b8a6c3581359c1f4d3deb4fbe6f18ef9fcd9
 8 -r--r--r--  1 jcprime  staff  1534  4 Oct 16:27 b30b4e736379015d25f54d216c45a604611a6e
16 -r--r--r--  1 jcprime  staff  7761  4 Oct 16:28 b3a618e62dc04321580b24ef3418ce2b0dac8e

./.git/objects/72:
total 128
64 -r--r--r--  1 jcprime  staff  31271  4 Oct 16:28 4ad6ce3e781f3675e68ab35a2b7f8fe49d0c5c
 8 -r--r--r--  1 jcprime  staff    488  4 Oct 16:29 4bfb3ec6829c8855b50264a5e0540ddc7fcc97
 8 -r--r--r--  1 jcprime  staff    844  4 Oct 16:28 889d2b3e0f6550305b4cfe194883af32c1d0a2
 8 -r--r--r--  1 jcprime  staff    890  4 Oct 16:27 916367b97b59c6983006edb50fd2055407f818
32 -r--r--r--  1 jcprime  staff  12606  4 Oct 16:28 e303f3c22f1d3e6c4a7024b7dcd90c5db82dbc
 8 -r--r--r--  1 jcprime  staff   3939  4 Oct 16:28 efb837a845edb26b0360af925a1902360ef8cd

./.git/objects/73:
total 96
 8 -r--r--r--  1 jcprime  staff   2288  4 Oct 16:28 009aab9b4b91eaccf3dfb86acbf55e019ac85b
32 -r--r--r--  1 jcprime  staff  12714  4 Oct 16:28 2e449f9716a641e37f0114e8b94962a28bd953
 8 -r--r--r--  1 jcprime  staff   1044  4 Oct 16:28 7329ae795b2bdc5b3b75d0bd04af3171d770d3
24 -r--r--r--  1 jcprime  staff  11927  4 Oct 16:28 8cf2237cf32e84c5c127af3b755e289a699f87
 8 -r--r--r--  1 jcprime  staff   3950  4 Oct 16:28 ae6d5d96363bc640981aa14b2031538d0ea28b
 8 -r--r--r--  1 jcprime  staff   1260  4 Oct 16:28 bd7052902d35c75aa6fd5670b006d9d2da97cb
 8 -r--r--r--  1 jcprime  staff   1151  4 Oct 16:28 f7d72c8054591e6e10b9d6b0567bf9606f4434

./.git/objects/74:
total 1352
   8 -r--r--r--  1 jcprime  staff    2306  4 Oct 16:27 012f84aa698643272a65a241a48aaf426a9890
   8 -r--r--r--  1 jcprime  staff      64  4 Oct 16:29 32ab1f384cce46978eec0787e978c2fb0bcb94
   8 -r--r--r--  1 jcprime  staff     534  4 Oct 16:28 4a1ea36fc25e261828c10e8963cd3f901c1011
1288 -r--r--r--  1 jcprime  staff  657041  4 Oct 16:28 7073f1b192ee74ba8612cb7ea58d908e6e7734
   8 -r--r--r--  1 jcprime  staff     447  4 Oct 16:28 8d55bd38d12b433e53c2604170c76212cff309
   8 -r--r--r--  1 jcprime  staff    1789  4 Oct 16:28 aa85455cb14530077c4fb52fcd205ddda26945
   8 -r--r--r--  1 jcprime  staff    2227  4 Oct 16:27 af6a4f099aa0c3fddacdc04c0405e901597faa
   8 -r--r--r--  1 jcprime  staff    1312  4 Oct 16:27 bde7b85b1e50a4747fe9ea07c6b2f7f212c618
   8 -r--r--r--  1 jcprime  staff     338  4 Oct 16:28 d2cf4792df08920026c24558faff7daf6a3d56

./.git/objects/75:
total 104
64 -r--r--r--  1 jcprime  staff  31610  4 Oct 16:28 54eeb60bfa3ffab3862bbdf1d9a31d7fdb3dde
24 -r--r--r--  1 jcprime  staff  10219  4 Oct 16:28 74d2c9e266cb523cc2fcd65db1d7ccaf54b368
 8 -r--r--r--  1 jcprime  staff    435  4 Oct 16:29 94d4504b702afeae03e733a3f3201204317848
 8 -r--r--r--  1 jcprime  staff   2195  4 Oct 16:28 c042a4cb836dadaedca195cb4d8aa67618419b

./.git/objects/76:
total 56
 8 -r--r--r--  1 jcprime  staff  1290  4 Oct 16:28 1743aacc4eef1c8f51ec668b287d2ea6b056b3
 8 -r--r--r--  1 jcprime  staff   805  4 Oct 16:28 1da0d0cb60ca284ff9d93d2388cc565df40836
 8 -r--r--r--  1 jcprime  staff   993  4 Oct 16:28 273230750652ebd7e8dbf20293166c9e8701e1
16 -r--r--r--  1 jcprime  staff  5193  4 Oct 16:28 3ff190b4f2c46e2b7924e4452549adb8d6d128
 8 -r--r--r--  1 jcprime  staff  1163  4 Oct 16:28 c1955e1d8e28c18176e27544136f28b611cc81
 8 -r--r--r--  1 jcprime  staff   184  4 Oct 16:29 c72469402e8b1212ed62b293087070353d4e1e

./.git/objects/77:
total 8088
   8 -r--r--r--  1 jcprime  staff      862  4 Oct 16:28 46e190da3dba6c5ac6242f2a013ec9e9c419c1
8064 -r--r--r--  1 jcprime  staff  4125137  4 Oct 16:28 807ecb85fdbf48c9746f0cdece87a73d95fcd6
   8 -r--r--r--  1 jcprime  staff      600  4 Oct 16:28 940de6d085d4070f66a5ce5a3f3fa8452cce99
   8 -r--r--r--  1 jcprime  staff      269  4 Oct 16:28 a831bdd6d1d59fe9f1a96e20fec5bd70101068

./.git/objects/78:
total 96
 8 -r--r--r--  1 jcprime  staff  2572  4 Oct 16:27 276efe972526ab383f3f930f6a81510fcad2d2
 8 -r--r--r--  1 jcprime  staff   869  4 Oct 16:28 2e8bae6019c4c8c2249572c99979f852005b62
 8 -r--r--r--  1 jcprime  staff   387  4 Oct 16:28 715dff19c4f6ae10bd5d2b5ce8e79a4be5a5ba
16 -r--r--r--  1 jcprime  staff  6473  4 Oct 16:28 8490229c2dcf285b7c5f6a09727b797002ff3a
 8 -r--r--r--  1 jcprime  staff   167  4 Oct 16:28 873c1a44a7cb7da65e74d69f73af5f13af0c5e
 8 -r--r--r--  1 jcprime  staff   468  4 Oct 16:28 ac8fc6eb24e64dcba4455afdb67d911c10ff10
16 -r--r--r--  1 jcprime  staff  4261  4 Oct 16:27 c7edf43c40170f47353c9e8a91e9293915156f
 8 -r--r--r--  1 jcprime  staff   860  4 Oct 16:28 e2f58faa20c892f4e081eca9f2463705d852ef
 8 -r--r--r--  1 jcprime  staff   142  4 Oct 16:29 ec9f93cc94b7a9ce4c45f21b6ca920179d29dd
 8 -r--r--r--  1 jcprime  staff  1809  4 Oct 16:28 f7cd812839a957587ea45561315a0db08c5b16

./.git/objects/79:
total 104
72 -r--r--r--  1 jcprime  staff  33195  4 Oct 16:28 12482a5d81d824e57a0fb3778c05f284f2d6bc
 8 -r--r--r--  1 jcprime  staff   1729  4 Oct 16:28 3da72aeae5aad144ab072b2f9011eaf3519663
16 -r--r--r--  1 jcprime  staff   6302  4 Oct 16:28 e22cc85e4946456d51f3ede4fe8e2f69fc8aee
 8 -r--r--r--  1 jcprime  staff   2292  4 Oct 16:28 e47a96ed749331a86ac78e85463e86c2f9fbc4

./.git/objects/7a:
total 72
 8 -r--r--r--  1 jcprime  staff  2852  4 Oct 16:27 71d500e359e57199a644d1d90deb37f0cff794
 8 -r--r--r--  1 jcprime  staff   322  4 Oct 16:28 8d91b38f3c3b001935ba6a0673f44b64952f86
24 -r--r--r--  1 jcprime  staff  8433  4 Oct 16:28 a19aa8f35945884b8a5fc4431b8e7e9cfbd7d9
16 -r--r--r--  1 jcprime  staff  5134  4 Oct 16:28 a81a32f333fe6710db4b23fe82005a111e7a27
 8 -r--r--r--  1 jcprime  staff  2778  4 Oct 16:28 b25c821ccab5d792a3432007bf928cf4ac4e38
 8 -r--r--r--  1 jcprime  staff   219  4 Oct 16:28 b6a1fadb201c0ca4abf266dbb2574715af1709

./.git/objects/7b:
total 56
 8 -r--r--r--  1 jcprime  staff    654  4 Oct 16:28 0379f35c3cf6585863d39728166d25762daa03
 8 -r--r--r--  1 jcprime  staff    107  4 Oct 16:28 40dfa9379940dc6c1a45b133caade3a147481b
24 -r--r--r--  1 jcprime  staff  10029  4 Oct 16:28 931b9027a82822bf38fbeedcc173f2bbd159b2
 8 -r--r--r--  1 jcprime  staff   1785  4 Oct 16:28 bcef0d71388e553a2856396a83879e371ed3e9
 8 -r--r--r--  1 jcprime  staff    856  4 Oct 16:28 e1929cec41d09f744d5d89a304acf246131043

./.git/objects/7c:
total 96
 8 -r--r--r--  1 jcprime  staff   1819  4 Oct 16:27 2105b95ebbc073fa946e37b646f86b37776e4d
 8 -r--r--r--  1 jcprime  staff     54  4 Oct 16:28 3deacbcbf3deaf40c661b9d89ccd98603ebe46
 8 -r--r--r--  1 jcprime  staff   1519  4 Oct 16:28 489d42884e9604bd140ae3b003eb86c64bc793
 8 -r--r--r--  1 jcprime  staff   2481  4 Oct 16:28 534f07f52cfae983322dabda255b25d53e4e3f
 8 -r--r--r--  1 jcprime  staff    711  4 Oct 16:28 acc52ae4316bd55477edb46bba59ced6b4c019
 8 -r--r--r--  1 jcprime  staff   3513  4 Oct 16:28 cce3b4a4c833c1d8338e37ef0bda7cd5e93024
16 -r--r--r--  1 jcprime  staff   4251  4 Oct 16:28 e476919eddcda70e3d22e8354ae0f6347e5e56
 8 -r--r--r--  1 jcprime  staff   2308  4 Oct 16:27 eb652bb6b45efb7b52d640f36e37d3997ce494
24 -r--r--r--  1 jcprime  staff  10723  4 Oct 16:28 eccc5bf067b34ca4d2b1ee96c98dc442d79ed0

./.git/objects/7d:
total 64
 8 -r--r--r--  1 jcprime  staff  1019  4 Oct 16:27 1a2426ba85116acbd06784c3ef278272259c43
16 -r--r--r--  1 jcprime  staff  5808  4 Oct 16:28 323d8693e2e80705aad695b9e0e4369fb17615
16 -r--r--r--  1 jcprime  staff  4368  4 Oct 16:28 769c45e9f3f6c5507f6d1e47572c80d129af59
 8 -r--r--r--  1 jcprime  staff  3959  4 Oct 16:28 96aaa6490043018645453d2c18857a61e85600
 8 -r--r--r--  1 jcprime  staff  1310  4 Oct 16:28 b4c12f4fdfcb33d70b12534f6f564c23d27952
 8 -r--r--r--  1 jcprime  staff  1666  4 Oct 16:27 c00244eded74b9a3d0bde8abcb83ea69010725

./.git/objects/7e:
total 1608
  16 -r--r--r--  1 jcprime  staff    6024  4 Oct 16:28 191c3d4880ec13aa0d2a14e29aaab84d62b566
   8 -r--r--r--  1 jcprime  staff    2870  4 Oct 16:28 25856c626edba77696f651aace5940faaaec5b
   8 -r--r--r--  1 jcprime  staff     902  4 Oct 16:27 5e5cd23389fc2612ea5b044052e8c89ed1a285
   8 -r--r--r--  1 jcprime  staff     375  4 Oct 16:28 7572da6cce679abcb66d465ff0159a2aad9182
  40 -r--r--r--  1 jcprime  staff   17794  4 Oct 16:28 9f6344a37d69ec11306289382c073741d1da51
   8 -r--r--r--  1 jcprime  staff    2241  4 Oct 16:28 a05aa97bded1e9f8c0fb67d9a288fa807f1e38
   8 -r--r--r--  1 jcprime  staff     517  4 Oct 16:28 d09aa0e74118ea620e57e2f6b16a27b576de5b
1512 -r--r--r--  1 jcprime  staff  771322  4 Oct 16:28 e10fa76b61c8833346e9180fb73b8ea13218d0

./.git/objects/7f:
total 232
16 -r--r--r--  1 jcprime  staff   5492  4 Oct 16:28 50c82aba358c269187f8e99d15c5cc79791c50
24 -r--r--r--  1 jcprime  staff  11578  4 Oct 16:28 595af5128950c7798e86bce3ea0ce99ab3a748
 8 -r--r--r--  1 jcprime  staff    405  4 Oct 16:29 610f22aa420bf104c778ae0bf4987f394814c5
 8 -r--r--r--  1 jcprime  staff    666  4 Oct 16:28 689ea06455441c6fb6948dbd8ff4db319eb693
 8 -r--r--r--  1 jcprime  staff    554  4 Oct 16:28 77fb388f9f4a3594f59f18666404ae1782b4c4
 8 -r--r--r--  1 jcprime  staff   1410  4 Oct 16:28 79543dc50f2015b169e16b75abb3e3ebf05f60
 8 -r--r--r--  1 jcprime  staff   1541  4 Oct 16:27 8f28008e4824add54f34799aac24f0be94082e
80 -r--r--r--  1 jcprime  staff  37726  4 Oct 16:28 96a62e69acf57e3830b21292581d9a4421de3f
48 -r--r--r--  1 jcprime  staff  24155  4 Oct 16:28 ad296964b69dfb9ff77a6af05d88d12b539e5e
16 -r--r--r--  1 jcprime  staff   8093  4 Oct 16:28 e41654d8bd14cf51fb35dd406f0567718d902c
 8 -r--r--r--  1 jcprime  staff   3527  4 Oct 16:28 fa22b44e02772319eeb2501fbeb40d47324b5f

./.git/objects/80:
total 56
 8 -r--r--r--  1 jcprime  staff    312  4 Oct 16:28 a2cd043910cff10da4efaf922a05a01d0a2e2d
48 -r--r--r--  1 jcprime  staff  23838  4 Oct 16:28 a391ca549651f222df5f2b0c3453365c1f8d51

./.git/objects/81:
total 4136
   8 -r--r--r--  1 jcprime  staff     3123  4 Oct 16:27 02f683ce5b7450446ca62b2067d5ed262e6783
   8 -r--r--r--  1 jcprime  staff     1813  4 Oct 16:28 49d99be33351448764cc6a48a08932987176cd
   8 -r--r--r--  1 jcprime  staff       60  4 Oct 16:28 5dd78aa3c084c38599f879be76467ee45b2a1e
   8 -r--r--r--  1 jcprime  staff     2205  4 Oct 16:28 87183fbb3589e87d8799551ad990fcac647d9b
   8 -r--r--r--  1 jcprime  staff     1089  4 Oct 16:28 8a202f3ec6466b2a23da4776a0cb426a02bb7e
   8 -r--r--r--  1 jcprime  staff     2308  4 Oct 16:27 92b28a48ce024bad1705964d5ff215fde799b7
   8 -r--r--r--  1 jcprime  staff     4021  4 Oct 16:28 ad96451ec5cc6b10e0448b548a08ad56331202
4040 -r--r--r--  1 jcprime  staff  2064911  4 Oct 16:28 bb54243df668667fc72d3888ccdf57f05c360f
   8 -r--r--r--  1 jcprime  staff      860  4 Oct 16:28 de651343e80238685b7575b16836dd6bb56f45
  32 -r--r--r--  1 jcprime  staff    14927  4 Oct 16:28 fbf994cb589d036356b85975b9efb5f30337c2

./.git/objects/82:
total 8
8 -r--r--r--  1 jcprime  staff  1525  4 Oct 16:27 1db8f7b7434d1a095083fb3df54ee7f6b3aa52

./.git/objects/83:
total 1496
   8 -r--r--r--  1 jcprime  staff    1457  4 Oct 16:28 133a8d2ef2e0aea0d8f33400b1b732557f5a37
   8 -r--r--r--  1 jcprime  staff     329  4 Oct 16:24 3264c0b07552e40db032278715fce08e9e0644
   8 -r--r--r--  1 jcprime  staff    1579  4 Oct 16:28 538b06abca1a34065b09703936985fd01d194f
 248 -r--r--r--  1 jcprime  staff  126352  4 Oct 16:28 7a043bc2fab55b9e4e0e3fceac4dccfa55a8db
   8 -r--r--r--  1 jcprime  staff    1399  4 Oct 16:28 7c5f4a55413851a452e897a4e92266dbba0060
   8 -r--r--r--  1 jcprime  staff     220  4 Oct 16:29 85348976416dc70d8c1ec892045fd9198282a7
   8 -r--r--r--  1 jcprime  staff    1064  4 Oct 16:29 a409b640741196c06787ffe304c36efedc2079
1192 -r--r--r--  1 jcprime  staff  606838  4 Oct 16:28 d6d08a0c8cf2bc53427346d5dcb3041de44d86
   8 -r--r--r--  1 jcprime  staff     741  4 Oct 16:28 fef16b2cd9e1a9ffe63d9acdf35c89cb5ba701

./.git/objects/84:
total 40
24 -r--r--r--  1 jcprime  staff  9642  4 Oct 16:28 247667db9111dfcdd80958d5116ca0444a6703
 8 -r--r--r--  1 jcprime  staff  3067  4 Oct 16:28 2545ee19e4f3c8de15b4afccc89626c89a579f
 8 -r--r--r--  1 jcprime  staff  3935  4 Oct 16:28 bccf811eb50a16111dca5a44280955aa7579be

./.git/objects/85:
total 16
16 -r--r--r--  1 jcprime  staff  5050  4 Oct 16:28 f655d6c54a5d7bb49a539378e0af12ba679a0f

./.git/objects/86:
total 2856
   8 -r--r--r--  1 jcprime  staff     2671  4 Oct 16:27 2080a2263c09ddb028c1efbfc5bf4adf7b690a
   8 -r--r--r--  1 jcprime  staff     1679  4 Oct 16:28 2863be4aea192c289bd039d6f340634464ad6e
   8 -r--r--r--  1 jcprime  staff      875  4 Oct 16:28 673330592f25e5ce4a8d0bf2321833a2c128d7
   8 -r--r--r--  1 jcprime  staff     3754  4 Oct 16:28 812cf2841f927fa649c7fce94921f2da0510c1
 120 -r--r--r--  1 jcprime  staff    57403  4 Oct 16:28 95ce2bdd60afe664d5fc7876ce7cd176399ca2
   8 -r--r--r--  1 jcprime  staff     1360  4 Oct 16:28 aa6b68ace4ebf83ef66a60d8f470db829d4832
2696 -r--r--r--  1 jcprime  staff  1376515  4 Oct 16:28 de4d44cb0d32f02b8a9a5bbc08d5504de4e863

./.git/objects/87:
total 720
704 -r--r--r--  1 jcprime  staff  358804  4 Oct 16:28 62563d6fdd402899573358ceee05116b932420
  8 -r--r--r--  1 jcprime  staff    2221  4 Oct 16:27 e62fb94ced16c495dbae4a45d0718312698fc5
  8 -r--r--r--  1 jcprime  staff    2038  4 Oct 16:28 e9d13b421bac1590588e79fc261813299a4304

./.git/objects/88:
total 32
8 -r--r--r--  1 jcprime  staff   398  4 Oct 16:28 3b4983cba3d96dcc7262042441e83239de602e
8 -r--r--r--  1 jcprime  staff  3923  4 Oct 16:28 572349383ee7a4af8f3f1e74bd597b5135cd8e
8 -r--r--r--  1 jcprime  staff   735  4 Oct 16:28 63b256e709537bbe1c5f97819c3cf0ac71942b
8 -r--r--r--  1 jcprime  staff   378  4 Oct 16:28 94289f04a89d0c65b39b57ac53ae35ba133749

./.git/objects/89:
total 56
16 -r--r--r--  1 jcprime  staff  5715  4 Oct 16:28 1eab827a9c2c1a84470e6c56f63f42df2e06f1
24 -r--r--r--  1 jcprime  staff  8401  4 Oct 16:28 491230009df0d3499c1b09d3834e765292d37c
 8 -r--r--r--  1 jcprime  staff  1902  4 Oct 16:27 a3f62a3a6b31d099342f530f22d5640fa4fe4a
 8 -r--r--r--  1 jcprime  staff   740  4 Oct 16:28 fb8b080460dc4ec680da5c3539566dc1d29a6e

./.git/objects/8a:
total 72
16 -r--r--r--  1 jcprime  staff   7352  4 Oct 16:28 30979db60aa3a84063d5a5828b5b0487a5f8e4
 8 -r--r--r--  1 jcprime  staff    444  4 Oct 16:28 789ae993cd1c097d14787e645236432869d0d6
48 -r--r--r--  1 jcprime  staff  21770  4 Oct 16:28 8ccb4157b70a30da5b16547c1303a699d99c5c

./.git/objects/8b:
total 432
  8 -r--r--r--  1 jcprime  staff    2156  4 Oct 16:27 1204bca6a078e4782bb22b7f198151b4c7d321
  8 -r--r--r--  1 jcprime  staff     977  4 Oct 16:29 42787206db354a3fff9ac16e651d9a204abe23
  8 -r--r--r--  1 jcprime  staff    1776  4 Oct 16:28 824fab162dce3982009f5f5534f39f2612e20a
  8 -r--r--r--  1 jcprime  staff    2469  4 Oct 16:28 8c6f1eab260c03752be0b8902125ed24d4a70b
  8 -r--r--r--  1 jcprime  staff    4082  4 Oct 16:28 a81cabd64f8248daec1b8159b9329bd03325ba
352 -r--r--r--  1 jcprime  staff  177377  4 Oct 16:28 b4d582f2d7478f9cb4315e6643d00db571a025
 24 -r--r--r--  1 jcprime  staff   10755  4 Oct 16:28 c3df3f56ee82b5dd8c8ac38589b5f10ed12ae8
  8 -r--r--r--  1 jcprime  staff     739  4 Oct 16:28 d2482ceb56555196c7f253391b736d670e2917
  8 -r--r--r--  1 jcprime  staff     911  4 Oct 16:28 f69faa3a62c9bcd16b5e1dc42b646723776129

./.git/objects/8c:
total 40
8 -r--r--r--  1 jcprime  staff  1552  4 Oct 16:28 2c3c72caffb63920ecba8e2d44f71263c698d4
8 -r--r--r--  1 jcprime  staff    58  4 Oct 16:29 60e14bf9380dac1deb0bbb6e6ce189b03adf07
8 -r--r--r--  1 jcprime  staff  3260  4 Oct 16:28 b1697e6ebd3868692270e75c245d69693c50a7
8 -r--r--r--  1 jcprime  staff  1432  4 Oct 16:28 befe142bf3ad2f9e9a12bef9f796c57dc80b50
8 -r--r--r--  1 jcprime  staff  1580  4 Oct 16:27 da0c564bbd4eeb1c0c8485ad20e42ae65d532d

./.git/objects/8d:
total 1256
   8 -r--r--r--  1 jcprime  staff    2130  4 Oct 16:28 2ae1203ec724795cfb09e3c469117013c1c0f9
1152 -r--r--r--  1 jcprime  staff  585765  4 Oct 16:28 31f86685a7b44b9c98b9521dd8bdbb13808e0f
   8 -r--r--r--  1 jcprime  staff    1970  4 Oct 16:28 374a006a9fd629fb037b67f71d0a875917b76c
  16 -r--r--r--  1 jcprime  staff    6788  4 Oct 16:28 4fec088aa211f4106af1937a83eecb3bfb2a08
   8 -r--r--r--  1 jcprime  staff    1468  4 Oct 16:28 81359d06d1091cd2a929d49f560ecee601562a
   8 -r--r--r--  1 jcprime  staff    1389  4 Oct 16:27 a4dd8b33bc6d430ae5bcf1c4e2b40a8b2569af
  32 -r--r--r--  1 jcprime  staff   15504  4 Oct 16:28 abcd28da018ec5d8b036a73b43708bcb13e233
   8 -r--r--r--  1 jcprime  staff    2811  4 Oct 16:27 b0c0b3385f9524da06c3b20380f4357c77b7dc
  16 -r--r--r--  1 jcprime  staff    7753  4 Oct 16:28 d951258d0eff5dc410fb6b0d948c45d828be75

./.git/objects/8e:
total 48
 8 -r--r--r--  1 jcprime  staff  1604  4 Oct 16:28 226efd6f45dda26557c66d700ee14f3e23d730
 8 -r--r--r--  1 jcprime  staff  2176  4 Oct 16:28 26d638f8fd381652fc071104ab5dd682b4584e
 8 -r--r--r--  1 jcprime  staff   381  4 Oct 16:29 37f519e01bf572b1c11cc57b2fedd55be58fc6
 8 -r--r--r--  1 jcprime  staff   119  4 Oct 16:28 7a95a5e330cc913b06552f02b5a9f40a5e6f22
16 -r--r--r--  1 jcprime  staff  5762  4 Oct 16:28 ec1cc8439bfbd69d4b5160e9f100de00a284fd

./.git/objects/8f:
total 120
 8 -r--r--r--  1 jcprime  staff    650  4 Oct 16:28 42156be4a6efc5b80e82d6b08d9bcb5ee5bed4
 8 -r--r--r--  1 jcprime  staff    515  4 Oct 16:28 5380274c07943af4bd5dbf5d48f13642d78c99
72 -r--r--r--  1 jcprime  staff  34503  4 Oct 16:28 74207ed52658d694ccc934d3d7d90ef5d88061
 8 -r--r--r--  1 jcprime  staff    358  4 Oct 16:29 b6500025de699a5e97ca21b63dcf33dfd498ac
 8 -r--r--r--  1 jcprime  staff   1920  4 Oct 16:28 b700e6662538b2fcbdbeea0cbe81077943b3fb
 8 -r--r--r--  1 jcprime  staff    373  4 Oct 16:28 c01d44f3ad63ef0403ad3b023dfaeba514e14f
 8 -r--r--r--  1 jcprime  staff   1900  4 Oct 16:28 dc5a44bab5929f511e4e03f5f7d8d135fae520

./.git/objects/90:
total 80
 8 -r--r--r--  1 jcprime  staff   853  4 Oct 16:28 00783969cfb8ab60e0b96d5833d9e6882f4a10
 8 -r--r--r--  1 jcprime  staff   145  4 Oct 16:29 314dd04732707076094ee13e47bbcca486b403
 8 -r--r--r--  1 jcprime  staff   737  4 Oct 16:28 399c8c3a6725156590f9c835c921d30d6f7ac9
 8 -r--r--r--  1 jcprime  staff  1552  4 Oct 16:28 5b859fd1de8bd3dd913d57b67ec84567a9f210
16 -r--r--r--  1 jcprime  staff  5887  4 Oct 16:28 db266a32b80ffaa9a4db911d7ab9449dc1b794
 8 -r--r--r--  1 jcprime  staff    76  4 Oct 16:28 db99f4d9bccd94334112cfc752b92f01d32346
 8 -r--r--r--  1 jcprime  staff  3103  4 Oct 16:27 ea20c3f8a9170fc21e0c20ff1e99656d702bb8
16 -r--r--r--  1 jcprime  staff  5462  4 Oct 16:28 ec401330899069739a647b201fcb0ba7f2e5ba

./.git/objects/91:
total 64
8 -r--r--r--  1 jcprime  staff  1792  4 Oct 16:27 1c5ea09dd7ab564f8463d827a40d3535fa9d95
8 -r--r--r--  1 jcprime  staff  1368  4 Oct 16:27 367cc38b4ff07975e751eba7bc61c5fe482ed2
8 -r--r--r--  1 jcprime  staff   295  4 Oct 16:28 3fa0954592f61e8de1c00b70933bc4045462d3
8 -r--r--r--  1 jcprime  staff  2478  4 Oct 16:28 7e456ca7d7df673c8d4cd855b2e8a47a13a993
8 -r--r--r--  1 jcprime  staff  1519  4 Oct 16:28 99dec478e5301e1ea601371a97afa0acb93659
8 -r--r--r--  1 jcprime  staff  2355  4 Oct 16:28 a695ff48968b07e6bcca6b7866b372a97455cb
8 -r--r--r--  1 jcprime  staff   741  4 Oct 16:28 ae32087c8f056b045c39b9b909c92eb62d597b
8 -r--r--r--  1 jcprime  staff   595  4 Oct 16:28 ae81fb4f9819d0d3458e50e830ff8c5bf86f62

./.git/objects/92:
total 1392
  8 -r--r--r--  1 jcprime  staff    3041  4 Oct 16:28 25c74074fff05a6ed3eef4f2b3d7633f8a5f72
856 -r--r--r--  1 jcprime  staff  435294  4 Oct 16:28 604d47ce728e5a30d9f0d7e1154fd0aa6cf49e
  8 -r--r--r--  1 jcprime  staff    1339  4 Oct 16:28 88b4265010e846c9d2bda56beb7dc1120c6b7b
  8 -r--r--r--  1 jcprime  staff    1023  4 Oct 16:28 978cc4d4495d62e1180e75bddfdc0fe1213721
  8 -r--r--r--  1 jcprime  staff     851  4 Oct 16:28 b300c4ce09bb0b3db0c2ff4cd3125ada474006
496 -r--r--r--  1 jcprime  staff  253286  4 Oct 16:28 b7ef828343fcffce7646995f4239c5ce8bf92d
  8 -r--r--r--  1 jcprime  staff     801  4 Oct 16:28 cc9ec947c02060c53d911ba0eda32c334103a2

./.git/objects/93:
total 48
 8 -r--r--r--  1 jcprime  staff   607  4 Oct 16:28 0a81923add200dd0a8074571dab5375007588c
 8 -r--r--r--  1 jcprime  staff   773  4 Oct 16:28 4a97c1510fa59547bd83a83aa39a6849b0d3ea
16 -r--r--r--  1 jcprime  staff  6462  4 Oct 16:28 71f2dc03027a6fb0f5a38743a9db77dfe686bf
 8 -r--r--r--  1 jcprime  staff   113  4 Oct 16:28 d9b958d60e67d9e7bee9e858aec28518e70531
 8 -r--r--r--  1 jcprime  staff  2261  4 Oct 16:28 de582e3207f518ffc9b3ca1268f202aefe303e

./.git/objects/94:
total 56
16 -r--r--r--  1 jcprime  staff  4905  4 Oct 16:28 341a638cf86af58375fefa5300baac6a89ea14
 8 -r--r--r--  1 jcprime  staff  3615  4 Oct 16:28 64d17f2b30c4238d9f1b3c3d88be60f127e62a
 8 -r--r--r--  1 jcprime  staff  1049  4 Oct 16:28 7096d3739cfef7a8f5ef8bc3c26a887954afd1
 8 -r--r--r--  1 jcprime  staff  2305  4 Oct 16:28 974f24c1ff7bf41f019260db64497b5715aa16
 8 -r--r--r--  1 jcprime  staff  1311  4 Oct 16:28 9fd6fae440df8f8a4ccd7f9d1b57c804eaa206
 8 -r--r--r--  1 jcprime  staff  2722  4 Oct 16:28 ab1bc019c079e24093d2678193c307ecd1bfff

./.git/objects/95:
total 8
8 -r--r--r--  1 jcprime  staff  690  4 Oct 16:28 6af7d9f075ebae976c4d2f3072a42e280e32f5

./.git/objects/96:
total 48
8 -r--r--r--  1 jcprime  staff  2314  4 Oct 16:28 0fe9b58eb57b9f01f63e690cb6d386253cb32a
8 -r--r--r--  1 jcprime  staff  2475  4 Oct 16:28 58d6186290033ef280285a776cde5d558e417d
8 -r--r--r--  1 jcprime  staff  1438  4 Oct 16:28 7a0a4291bd0134e9c5bad4a21ca13bba4a9d2c
8 -r--r--r--  1 jcprime  staff    55  4 Oct 16:29 9772323ed9f5adc956b2a1f8db79ae91ce4ad1
8 -r--r--r--  1 jcprime  staff  2301  4 Oct 16:28 c04df680704ec2d0b5503b255f968266f1f187
8 -r--r--r--  1 jcprime  staff  1490  4 Oct 16:28 e18b24dbe3f0b26710cb14a90da633b8bce7c0

./.git/objects/97:
total 176
 8 -r--r--r--  1 jcprime  staff    105  4 Oct 16:28 0ccd4b0b420630841cd195907522ea84c1f847
 8 -r--r--r--  1 jcprime  staff   3850  4 Oct 16:28 36cd5a455efcf3385cdf7febd73ea9181596ad
96 -r--r--r--  1 jcprime  staff  46625  4 Oct 16:28 56b5ddc72fb21d4db5b89d6e6a27c185371a90
 8 -r--r--r--  1 jcprime  staff     92  4 Oct 16:17 6327649172403610a3a3fb5fc41272adea2db3
 8 -r--r--r--  1 jcprime  staff   2341  4 Oct 16:28 83aebcaa1a9de69a907e881373bb7f3c368e03
16 -r--r--r--  1 jcprime  staff   7635  4 Oct 16:28 8bb5b52f570124cfc8ee8cda4a7211b66fc8fe
 8 -r--r--r--  1 jcprime  staff   1308  4 Oct 16:28 b90282c4a8b43bed21865526fa23a407853922
 8 -r--r--r--  1 jcprime  staff   1445  4 Oct 16:28 f561256512c37c47b467f1faa3ea77fad075d9
16 -r--r--r--  1 jcprime  staff   5105  4 Oct 16:28 f8540ec8f01d4102cdff39e6716fcc4b4b87c8

./.git/objects/98:
total 800
  8 -r--r--r--  1 jcprime  staff     389  4 Oct 16:28 5a4c870f9bf2b7666571e4c2eaa266b77c9301
288 -r--r--r--  1 jcprime  staff  145879  4 Oct 16:28 83c51a2f6a90eb063b998ddb21b8fb645f36ee
  8 -r--r--r--  1 jcprime  staff     880  4 Oct 16:28 9480af1b41b6f71889b741fde80fe80dd7ebf0
 16 -r--r--r--  1 jcprime  staff    7103  4 Oct 16:28 a8a029743daacd23435e6186fe176d6eecfdcd
  8 -r--r--r--  1 jcprime  staff      31  4 Oct 16:28 b0a844a8b7c1707e629a72962308e450ef510a
  8 -r--r--r--  1 jcprime  staff    1675  4 Oct 16:27 e8c2d49de219384d5f88cff52dd4a910b2a83d
448 -r--r--r--  1 jcprime  staff  227585  4 Oct 16:28 e8f7f8e4ead9a3cc55c990f1700625b0f6e7f8
  8 -r--r--r--  1 jcprime  staff     744  4 Oct 16:28 e91fbee9be98d3a25993a2f4c61b21f4892abc
  8 -r--r--r--  1 jcprime  staff    3919  4 Oct 16:28 f4154d58adbbdb1a8935f6f5073d74758939a6

./.git/objects/99:
total 16
8 -r--r--r--  1 jcprime  staff  3575  4 Oct 16:28 02b60d79e764b7c417c95e8cf2d63eac195b26
8 -r--r--r--  1 jcprime  staff  1329  4 Oct 16:28 b06b199d64a4a4eb1af02e579bbde54d9e05fa

./.git/objects/9a:
total 2552
   8 -r--r--r--  1 jcprime  staff     2351  4 Oct 16:28 0e0074059e71e5aee53858034c9277e350a8b6
2480 -r--r--r--  1 jcprime  staff  1268663  4 Oct 16:28 5b7616c4df7310485c8797e5e9c30c4ed54ab5
   8 -r--r--r--  1 jcprime  staff     2276  4 Oct 16:28 61b1aec3bd92b5d69cc30722eff1cc1f6c7b2e
   8 -r--r--r--  1 jcprime  staff     1493  4 Oct 16:28 6f539ee546f99118bde7c50638e9e611fa994d
  40 -r--r--r--  1 jcprime  staff    18631  4 Oct 16:28 ba7df2844b971ea3b60f24b4d5f6369623ceaf
   8 -r--r--r--  1 jcprime  staff     1687  4 Oct 16:27 fdf69f6ce84d45a2bc8430c1da6eb9f58d472e

./.git/objects/9b:
total 48
 8 -r--r--r--  1 jcprime  staff  1908  4 Oct 16:28 0673bd802523e024aafd5326a74e87b2b27ca8
 8 -r--r--r--  1 jcprime  staff  1551  4 Oct 16:28 61666545ebfdc759ad8e99f9db642f7e3c4be5
16 -r--r--r--  1 jcprime  staff  4387  4 Oct 16:28 9ad14c7d562b3b6f5f2414720097be472188db
16 -r--r--r--  1 jcprime  staff  4249  4 Oct 16:28 ef37e837db54b24a7b6deb337134e17a7d6b5e

./.git/objects/9c:
total 440
  8 -r--r--r--  1 jcprime  staff    1857  4 Oct 16:27 00e2d45de3406207b776f825c9c56178cae62f
  8 -r--r--r--  1 jcprime  staff     447  4 Oct 16:28 03a7c3ad8eee036ea4ce609ccfbfb7cc01811c
  8 -r--r--r--  1 jcprime  staff    1815  4 Oct 16:27 0555635685f385ef5471c94deaee53c938bf72
  8 -r--r--r--  1 jcprime  staff    1338  4 Oct 16:28 2f984cb2ea1161383263ad816387ab9ede1a01
  8 -r--r--r--  1 jcprime  staff    2652  4 Oct 16:27 3214f7729fc37e363e74789b77ea59062206b5
 32 -r--r--r--  1 jcprime  staff   13326  4 Oct 16:28 3558d8c38f5934b6cb86596780544699825a16
  8 -r--r--r--  1 jcprime  staff    1810  4 Oct 16:27 485cf793d1ec8920c78dbb8161710c798f03b1
 24 -r--r--r--  1 jcprime  staff    9853  4 Oct 16:28 4fc1fa74120b25560f027e491897fa1cbedd40
  8 -r--r--r--  1 jcprime  staff    1158  4 Oct 16:27 616476cd3711ac3e2603eff0c5672601c797a4
  8 -r--r--r--  1 jcprime  staff    3921  4 Oct 16:28 65b4de8dbd39fcd9776ae5d36aa19f89ef1cb5
  8 -r--r--r--  1 jcprime  staff    2201  4 Oct 16:27 7ae2679f416e7c42d435b239505ebc4ed0af66
 64 -r--r--r--  1 jcprime  staff   30300  4 Oct 16:28 8916ea9178ed987f4a1b80f81859728775161b
  8 -r--r--r--  1 jcprime  staff    1532  4 Oct 16:28 9bf581a8166f12be068db070b7f827e7e096ce
232 -r--r--r--  1 jcprime  staff  118607  4 Oct 16:28 a08442ec0487fdcc09bca8364432a04cd966d5
  8 -r--r--r--  1 jcprime  staff    1925  4 Oct 16:27 e1b36bfa2396b4bcb9550b2df71ab89076d52b

./.git/objects/9d:
total 104
 8 -r--r--r--  1 jcprime  staff   1375  4 Oct 16:28 0263f4f538caec717c8b20ef091f1acb221f9d
64 -r--r--r--  1 jcprime  staff  28697  4 Oct 16:28 3dad3bc10f532b44cffcfb6bd8a05824bda119
 8 -r--r--r--  1 jcprime  staff    647  4 Oct 16:28 68cd26f23f5695cfb96597e6157c32667f233f
 8 -r--r--r--  1 jcprime  staff    174  4 Oct 16:29 6bcc563419273e561ddd1a6fe65eb526d98948
 8 -r--r--r--  1 jcprime  staff    448  4 Oct 16:28 89f07b3c8eefca5d0b36d2b0417c8d2712b1f1
 8 -r--r--r--  1 jcprime  staff    569  4 Oct 16:28 f5887babcc32f9f39ca0eab7e1b8d9b66e8fb5

./.git/objects/9e:
total 104
 8 -r--r--r--  1 jcprime  staff    253  4 Oct 16:28 0c92b7786d67b08569a3b3f929f0921225e97c
 8 -r--r--r--  1 jcprime  staff   1302  4 Oct 16:28 41c6effe8f373ed5ec22e4c4e118e67665c7da
16 -r--r--r--  1 jcprime  staff   5089  4 Oct 16:28 473e3b62c6634c462128162477720eff0099aa
 8 -r--r--r--  1 jcprime  staff    622  4 Oct 16:28 5aab04ae32c761cbae20a3b943edb957746ef8
 8 -r--r--r--  1 jcprime  staff   4064  4 Oct 16:27 7a77f614427553335738c9cd7794a833834594
 8 -r--r--r--  1 jcprime  staff    824  4 Oct 16:28 9646b315eed4b07341b7f703a68b1c32c90edc
48 -r--r--r--  1 jcprime  staff  24268  4 Oct 16:28 9efa82983e2e3c5ad40c5f3e28a1cad2ebb408

./.git/objects/9f:
total 88
 8 -r--r--r--  1 jcprime  staff    994  4 Oct 16:28 29d710defb5201195b7e6c6f6683adcfda8836
24 -r--r--r--  1 jcprime  staff   9275  4 Oct 16:28 715ec469e9873580158b69d8c7580176c91760
 8 -r--r--r--  1 jcprime  staff    329  4 Oct 16:19 c42fadd645575b711fd6618e5fa3ebc2c2c900
 8 -r--r--r--  1 jcprime  staff   1152  4 Oct 16:28 f01f3f6cc9831a643ea0592ad497d6ea957f1c
32 -r--r--r--  1 jcprime  staff  14285  4 Oct 16:28 f2b37fba26a0bfa15c33440256313e34eb8d9c
 8 -r--r--r--  1 jcprime  staff   2335  4 Oct 16:28 f8207ecfce04fb37cc8c1a159a645b83243bc1

./.git/objects/a0:
total 232
  8 -r--r--r--  1 jcprime  staff    502  4 Oct 16:29 25df9ffce9afdc0ecc082e7a421597f2082b36
  8 -r--r--r--  1 jcprime  staff   3169  4 Oct 16:27 2d527b2ec980ce1903d1917c21e713f5739cf2
 16 -r--r--r--  1 jcprime  staff   6953  4 Oct 16:28 4296d8de89937085cfc9d6cacfd6f287d0772d
  8 -r--r--r--  1 jcprime  staff   1105  4 Oct 16:28 480b951cc00685f1edfd95189bd3a1fcb57926
  8 -r--r--r--  1 jcprime  staff    172  4 Oct 16:19 9ff8b939c2905c24acf388e5c1fd063be33fa2
176 -r--r--r--  1 jcprime  staff  86921  4 Oct 16:28 a9207d30a2fea487dbd4a2954ff159f1c2d0d8
  8 -r--r--r--  1 jcprime  staff    780  4 Oct 16:28 f62094a02bc924f7a2804a684c5fe655e58b25

./.git/objects/a1:
total 3640
   8 -r--r--r--  1 jcprime  staff      306  4 Oct 16:28 1205e6ebf70ed5088b8c9cecaf08202ca23818
   8 -r--r--r--  1 jcprime  staff     2391  4 Oct 16:28 65e2b53f27bc1c29aa5266701a45e88bcedeaa
   8 -r--r--r--  1 jcprime  staff     3639  4 Oct 16:28 c6e357c454862f579f2077c58116dd2b8eb75a
   8 -r--r--r--  1 jcprime  staff     2324  4 Oct 16:28 d72e0903b9fe7569c185b2c8e417305dad738a
3552 -r--r--r--  1 jcprime  staff  1815933  4 Oct 16:28 d8cbca16daaf4e76e26d709c02526ade55272f
  56 -r--r--r--  1 jcprime  staff    26382  4 Oct 16:28 ebaf2ea598ab38266e56ed2b0c0c444a0e7cc9

./.git/objects/a2:
total 32
8 -r--r--r--  1 jcprime  staff  1324  4 Oct 16:28 22cbc4a4c2cbb08be4da81bb035383bc00f080
8 -r--r--r--  1 jcprime  staff  1781  4 Oct 16:28 69cc5df41358ce1386c2eeff91a12d5ed2d859
8 -r--r--r--  1 jcprime  staff  1132  4 Oct 16:27 7015a9ba16e4217e7c876078db334aca26af8d
8 -r--r--r--  1 jcprime  staff  1669  4 Oct 16:27 fef32749cd5b14408f8644cc6439bfde4a3391

./.git/objects/a3:
total 263344
     8 -r--r--r--  1 jcprime  staff       1469  4 Oct 16:28 097a0470a2f94a52c092974582dbb124831038
   232 -r--r--r--  1 jcprime  staff     117495  4 Oct 16:28 0ff0e9eaefb16e058786de3f624ebe837312f5
     8 -r--r--r--  1 jcprime  staff        924  4 Oct 16:28 1076894faef2facd15b4cd4ab930676ba6647b
     8 -r--r--r--  1 jcprime  staff        751  4 Oct 16:28 3d8fdcae441f43fc98baa9c0f3adc31ea81423
    24 -r--r--r--  1 jcprime  staff       8323  4 Oct 16:27 7e34ef56a2a53566c9fefb4c8065d66ce561f3
     8 -r--r--r--  1 jcprime  staff       1812  4 Oct 16:27 934629b7893182e80474e00425f2f2a1b29182
     8 -r--r--r--  1 jcprime  staff        836  4 Oct 16:28 96e470b2adf5e69d885f221bb2ff82e44498c2
263040 -r--r--r--  1 jcprime  staff  134673481  4 Oct 16:28 a084fad3f7d2f4e53aa32c3703a8642702424d
     8 -r--r--r--  1 jcprime  staff       2713  4 Oct 16:28 e6e4e700a1a7e3ae0f483e8a4b9acf683c40b9

./.git/objects/a4:
total 24
8 -r--r--r--  1 jcprime  staff   120  4 Oct 16:29 4137826e157560b0fcc00d6f7b42d2524db276
8 -r--r--r--  1 jcprime  staff  3287  4 Oct 16:28 78251307cb4ecb5239fe71738ed652f3298186
8 -r--r--r--  1 jcprime  staff  3332  4 Oct 16:28 e099502ec7387ff3f5f814d8974be67134531c

./.git/objects/a5:
total 32
8 -r--r--r--  1 jcprime  staff   149  4 Oct 16:28 0179facd684f8fc28bc58461f7520ec05a38ba
8 -r--r--r--  1 jcprime  staff  1733  4 Oct 16:27 95b8d1d7b939e5c6aac13dc0c7570c6c364162
8 -r--r--r--  1 jcprime  staff   664  4 Oct 16:29 9681877453de46aa0e8fe2451dec03050b8332
8 -r--r--r--  1 jcprime  staff  1790  4 Oct 16:28 dd7e8df27c03ebfc97af8a87a62508f1e86e05

./.git/objects/a6:
total 88
 8 -r--r--r--  1 jcprime  staff   752  4 Oct 16:28 14a7f47f9a62ccabe111589e9d6a48beac4483
16 -r--r--r--  1 jcprime  staff  4984  4 Oct 16:28 1711a78cd0f39cd1a2af9f07cb10797dcda01d
 8 -r--r--r--  1 jcprime  staff    95  4 Oct 16:28 497b5cf2e2d14d70815206fa310417ea3b83b0
 8 -r--r--r--  1 jcprime  staff  2793  4 Oct 16:28 75f7b7f777dae4f6c828a63f3c5fa8027cd8a9
 8 -r--r--r--  1 jcprime  staff   736  4 Oct 16:28 9fdd64ab923486b646d1cf42be2d9fa58a0594
 8 -r--r--r--  1 jcprime  staff   258  4 Oct 16:28 bacc9945d4eab2830d4eaacc3c43c7345ff22e
16 -r--r--r--  1 jcprime  staff  5256  4 Oct 16:28 ca937af3a0df2ee67d0cd434cf8e24e1c3e1a7
 8 -r--r--r--  1 jcprime  staff   145  4 Oct 16:28 deadbac9eebeaf048f8a299336ddb98192a100
 8 -r--r--r--  1 jcprime  staff  1334  4 Oct 16:28 f22192c49042eeadf19e356e5f076f60bada15

./.git/objects/a7:
total 288
264 -r--r--r--  1 jcprime  staff  134607  4 Oct 16:28 431f0973c56140b7565e6e5556a8869cb9b1a6
  8 -r--r--r--  1 jcprime  staff    1136  4 Oct 16:28 78c4ee6851a86fccc5c4879888bbbf7a90c8c6
  8 -r--r--r--  1 jcprime  staff    1778  4 Oct 16:28 96961c3631ac9ca18c88846ebbc0741cdb5a7a
  8 -r--r--r--  1 jcprime  staff    2179  4 Oct 16:28 d0a66430d8e8f7f8c8141a5d4d24e85f3668a2

./.git/objects/a8:
total 56
8 -r--r--r--  1 jcprime  staff  2725  4 Oct 16:27 0c4919846c753594928d86a51a614f4002ac1c
8 -r--r--r--  1 jcprime  staff  2640  4 Oct 16:28 b4cd1c10d21fd0b64ddc62d847834ef0add798
8 -r--r--r--  1 jcprime  staff   985  4 Oct 16:28 d3f6cfafafc95d784bd4220a01bb41aabf3778
8 -r--r--r--  1 jcprime  staff  3225  4 Oct 16:28 dec27a8c10b29c7942c7de2473b1997d9193a2
8 -r--r--r--  1 jcprime  staff  2785  4 Oct 16:27 e45c9de87373ed6461943400f4c8919d08f949
8 -r--r--r--  1 jcprime  staff  1937  4 Oct 16:27 eaa1d501d4f01cf7b9a512f3fc220a84d334f9
8 -r--r--r--  1 jcprime  staff  1238  4 Oct 16:29 effed85b0b3c79009906d6645fbcfd3bb57336

./.git/objects/a9:
total 328
  8 -r--r--r--  1 jcprime  staff   1807  4 Oct 16:28 198e151f7184ba84f915fdc0670326e0c34866
168 -r--r--r--  1 jcprime  staff  84479  4 Oct 16:28 43c4b767ede5db67609c74208cb60e6ede8e28
  8 -r--r--r--  1 jcprime  staff   1302  4 Oct 16:28 490daeb6217bc7e63c11991f2b41a5873a8b88
  8 -r--r--r--  1 jcprime  staff   1714  4 Oct 16:27 583f6212527e5d5a7d3bab1226567c1a5e58f9
  8 -r--r--r--  1 jcprime  staff   1084  4 Oct 16:28 6baf1e9c862eccde6a414172ed1bdfeede37dd
  8 -r--r--r--  1 jcprime  staff   1996  4 Oct 16:27 ae0de05a2f243e8e8b6663a3a6635bbca987a0
112 -r--r--r--  1 jcprime  staff  56960  4 Oct 16:28 e31b4d796a269c7013763735e4733f5b34b4a4
  8 -r--r--r--  1 jcprime  staff    183  4 Oct 16:29 f2c5c01c557ebfd2cc9d17859c69af843f6ac2

./.git/objects/aa:
total 112
56 -r--r--r--  1 jcprime  staff  26345  4 Oct 16:28 1211cd79a5e91c476a4fbc02a563910369aa70
16 -r--r--r--  1 jcprime  staff   5534  4 Oct 16:28 34a90ca796db5df868a97aa84c43e8e3e9b5f2
 8 -r--r--r--  1 jcprime  staff    428  4 Oct 16:28 4d3286798f500e11112a7ee72482fdc2f47e89
24 -r--r--r--  1 jcprime  staff   9807  4 Oct 16:28 ce3a3586bd433ef3340a2670cb838d20db448c
 8 -r--r--r--  1 jcprime  staff   1499  4 Oct 16:28 db9e61bb45c44c74d355e830f5e31c483cf8de

./.git/objects/ab:
total 64
8 -r--r--r--  1 jcprime  staff  1338  4 Oct 16:28 1d031a1b82420da2164a44a33d15969707b636
8 -r--r--r--  1 jcprime  staff  2236  4 Oct 16:27 5ed3c0d1257b4848fab0c23d3ea99e24ed6df1
8 -r--r--r--  1 jcprime  staff  3944  4 Oct 16:28 71de57917f76ec04d1d6dbaae73480cb732693
8 -r--r--r--  1 jcprime  staff  3610  4 Oct 16:28 85ca21c34b08c5fb5b9a85f3e0fc89b064fdb4
8 -r--r--r--  1 jcprime  staff   923  4 Oct 16:28 bb8b07c110ff13adb0e3f60f0dc33f4abd2ff8
8 -r--r--r--  1 jcprime  staff   935  4 Oct 16:28 cc379d79bf7ab40f7b97fd37065e7a8a5a2bc5
8 -r--r--r--  1 jcprime  staff  2463  4 Oct 16:28 d19fd05102d66c5bda7217a4009805afe62a9b
8 -r--r--r--  1 jcprime  staff  1958  4 Oct 16:28 f514b4b58cd8a1796d6a5dd89ceb7c25ee24e4

./.git/objects/ac:
total 3648
  16 -r--r--r--  1 jcprime  staff     5574  4 Oct 16:28 2c0c703f06f4999e12282125e4061a24c7a208
  32 -r--r--r--  1 jcprime  staff    13662  4 Oct 16:28 33fc48a0e285695f9ba563d84d980f20ee5cfb
  16 -r--r--r--  1 jcprime  staff     6133  4 Oct 16:28 42828c85c0d613d3b820993ec204a70715ed76
   8 -r--r--r--  1 jcprime  staff      213  4 Oct 16:29 635c43f3b1d3a05e5357faf6498dd8d27c73b4
   8 -r--r--r--  1 jcprime  staff       98  4 Oct 16:28 801641c8beb857f301bb0683860366781cd766
   8 -r--r--r--  1 jcprime  staff     3544  4 Oct 16:28 9b734fb1b76b70012fa99c42a472d9c7e10717
   8 -r--r--r--  1 jcprime  staff     1819  4 Oct 16:27 ac696a6ac62e437b616bc5a0fafd792549c2bf
3552 -r--r--r--  1 jcprime  staff  1816112  4 Oct 16:28 b780bdbdba4921afff8e810ae7e3dcedc6fcb0

./.git/objects/ad:
total 40
16 -r--r--r--  1 jcprime  staff  5327  4 Oct 16:28 6b72879a3cc8a2abda88ebf1a58db3323072e3
 8 -r--r--r--  1 jcprime  staff  2278  4 Oct 16:28 6e5685491e4bad85dd112b772bd15d1e543291
 8 -r--r--r--  1 jcprime  staff  1588  4 Oct 16:27 6f0d3cd27685a2f2ae3d09beadee0eb274a09e
 8 -r--r--r--  1 jcprime  staff   666  4 Oct 16:28 d789b87b2b72d6d13cd324d8b5101d854c8373

./.git/objects/ae:
total 48
8 -r--r--r--  1 jcprime  staff   728  4 Oct 16:28 100221118e13998442bccdc83259db85a1af7b
8 -r--r--r--  1 jcprime  staff  4075  4 Oct 16:28 1d19157eddd107bf051d7b2d0fb1a4c7396b57
8 -r--r--r--  1 jcprime  staff   744  4 Oct 16:28 2bd8b7ba0c77f7e3dfa825e4d9aad04d4e5954
8 -r--r--r--  1 jcprime  staff  2200  4 Oct 16:27 7a40461e6a3ed4785e5f9ea29d9065e19bc3aa
8 -r--r--r--  1 jcprime  staff   230  4 Oct 16:28 8cc1844f28d0fea584bf9d307da86a3f26aff7
8 -r--r--r--  1 jcprime  staff   409  4 Oct 16:28 9258f47c73ad025278cab7041881228f957c84

./.git/objects/af:
total 24
8 -r--r--r--  1 jcprime  staff   537  4 Oct 16:28 02a1fdf9f890f1e7157d0eb74d0c29c31e27c7
8 -r--r--r--  1 jcprime  staff  3747  4 Oct 16:27 4af9b257f42a246d66f1795d2b89ec699ce117
8 -r--r--r--  1 jcprime  staff  1211  4 Oct 16:28 b98127b6d41897b5860bb8e19f874e110c2b41

./.git/objects/b0:
total 96
16 -r--r--r--  1 jcprime  staff  4195  4 Oct 16:28 0133543b42b2177a61945e48b3883ebe2d6dc4
 8 -r--r--r--  1 jcprime  staff  1508  4 Oct 16:28 142d92d641964e604d186de6c25d73543b172a
 8 -r--r--r--  1 jcprime  staff  2039  4 Oct 16:28 147d15d00c896f37b9a85378ae7cfd523e32b1
 8 -r--r--r--  1 jcprime  staff  2241  4 Oct 16:28 2b9eb97e4735dc673ce72f98139cafb4fc59b9
 8 -r--r--r--  1 jcprime  staff   321  4 Oct 16:28 66d9346cb7fd0d3e6c9eea93bea5367b8c2cb9
 8 -r--r--r--  1 jcprime  staff   823  4 Oct 16:28 77d460bbd8ef8983dac381e97e08e1f818726c
 8 -r--r--r--  1 jcprime  staff  2184  4 Oct 16:28 9910287f117d1d9c060761c6b5086083f64a1d
 8 -r--r--r--  1 jcprime  staff    46  4 Oct 16:29 b3a2a2758cf88b205bef61f59ac4bf1113fb38
16 -r--r--r--  1 jcprime  staff  7932  4 Oct 16:28 dca4cfa038e8ef4994107b082fc1127604aac1
 8 -r--r--r--  1 jcprime  staff  1247  4 Oct 16:28 e08efd4319bfc411b9863d3eefdbf6227c4452

./.git/objects/b1:
total 888
  8 -r--r--r--  1 jcprime  staff      84  4 Oct 16:29 030c81401a7ef186a82e6ff78f27d8f61d7393
 40 -r--r--r--  1 jcprime  staff   19590  4 Oct 16:28 976ce514826d25fbc6772760bd3590d84a1629
  8 -r--r--r--  1 jcprime  staff      46  4 Oct 16:29 aadcc64e8f59aa52fbe50eb1010efcb27d1c87
816 -r--r--r--  1 jcprime  staff  414205  4 Oct 16:28 c3a5e77ccc7dec341d58a796aa379a4aec9517
  8 -r--r--r--  1 jcprime  staff     926  4 Oct 16:28 df7f1bec13108459e30742d1be69423e2f3ef0
  8 -r--r--r--  1 jcprime  staff    1512  4 Oct 16:28 f1a7c5ca06b6227ad1a7c1f5fa258f02203028

./.git/objects/b2:
total 24
8 -r--r--r--  1 jcprime  staff  2174  4 Oct 16:27 6a495a2dac665e2ec4b11521f30d599d1ebb4a
8 -r--r--r--  1 jcprime  staff   576  4 Oct 16:28 722ea90a08007741ba1f2f563d22b511d603b3
8 -r--r--r--  1 jcprime  staff  1302  4 Oct 16:28 b356b414557373f1cc8a4424655e4b826b5789

./.git/objects/b3:
total 40
 8 -r--r--r--  1 jcprime  staff  2339  4 Oct 16:27 2eeb8a33c7c676d4b54a5215bb289804842ff3
 8 -r--r--r--  1 jcprime  staff  1835  4 Oct 16:27 6698127c507a9adc6dac3bb2bfd0f806438c8d
 8 -r--r--r--  1 jcprime  staff   282  4 Oct 16:28 6b3d270368431ff23bb306b13d7ff454a012b5
16 -r--r--r--  1 jcprime  staff  6611  4 Oct 16:27 ff252a274fdf7edde01367545f67dc6e4f25d2

./.git/objects/b4:
total 48
 8 -r--r--r--  1 jcprime  staff  1821  4 Oct 16:28 33cd6b235901fa92fe08067eb573fa4a222e27
 8 -r--r--r--  1 jcprime  staff   175  4 Oct 16:28 3e8383baf09a31643e005123d5699b557142e3
24 -r--r--r--  1 jcprime  staff  9539  4 Oct 16:28 5c5904aa9fa531b9dff570222558e31e3de398
 8 -r--r--r--  1 jcprime  staff  3733  4 Oct 16:27 787a9e743ecfd6104e44fe7c828e85cfdf5e9d

./.git/objects/b5:
total 80
 8 -r--r--r--  1 jcprime  staff   1528  4 Oct 16:28 1b0f2c162dbfc256da77756971c152af10b08c
 8 -r--r--r--  1 jcprime  staff    396  4 Oct 16:28 33ea279f6cae1e5f87d5b15e4d4e02cf02febd
24 -r--r--r--  1 jcprime  staff  11961  4 Oct 16:28 3b054c7db3fb78e72e7bd01c0f70ec07b28340
16 -r--r--r--  1 jcprime  staff   4419  4 Oct 16:28 66897fe64d6c55475d15b5652557e44075b8d0
16 -r--r--r--  1 jcprime  staff   4833  4 Oct 16:28 d4f3e57ce7f4499524319c338b580e0b1eea98
 8 -r--r--r--  1 jcprime  staff   1098  4 Oct 16:28 f71ccde4ac44b0a3d58212ed65f510f16dad8c

./.git/objects/b6:
total 56
 8 -r--r--r--  1 jcprime  staff   587  4 Oct 16:28 3d012b9d084868d58a76936dd0ea158adee043
16 -r--r--r--  1 jcprime  staff  8058  4 Oct 16:28 5b7490a8216c48130211d7e0b624ee980ea842
 8 -r--r--r--  1 jcprime  staff  1453  4 Oct 16:27 7ebaae6e38bccd6e36dac0eb51d7a1af4167ad
 8 -r--r--r--  1 jcprime  staff   639  4 Oct 16:28 a12032033add90f8c558c6add2d11dc5059fdc
 8 -r--r--r--  1 jcprime  staff  1817  4 Oct 16:27 cd61df05aa93db4bebc5dee73c8640e9ff7e20
 8 -r--r--r--  1 jcprime  staff   180  4 Oct 16:28 e7302fb0457894cf621508d7e1fdc3d795abe4

./.git/objects/b7:
total 120
 8 -r--r--r--  1 jcprime  staff    113  4 Oct 16:28 89714dd9dbd16090a5156a061d9523e2a6d0bf
 8 -r--r--r--  1 jcprime  staff    783  4 Oct 16:28 92bab979d9bef5c81b76e731160cca7d02cd90
80 -r--r--r--  1 jcprime  staff  38794  4 Oct 16:28 9cba66b3bfdd5cc4669596e28aa71b3c153f38
 8 -r--r--r--  1 jcprime  staff   1450  4 Oct 16:27 ba4368dffc18e8b72a82730e13c2715eb487c5
16 -r--r--r--  1 jcprime  staff   5627  4 Oct 16:27 fdba123b92dcbc5f4582e4a804ab0ca54a750b

./.git/objects/b8:
total 72
 8 -r--r--r--  1 jcprime  staff  1516  4 Oct 16:28 18f1634c94710364cc72956e0a908fceef6f64
16 -r--r--r--  1 jcprime  staff  5579  4 Oct 16:28 3b75ebbc4903c00b667d4ca5ee71c39d81c21e
24 -r--r--r--  1 jcprime  staff  8347  4 Oct 16:28 3e9ba24bda038b80643326ddd30ab5e0093e62
16 -r--r--r--  1 jcprime  staff  5297  4 Oct 16:28 a59f2c38bbadc73075d3cea074bb15821726de
 8 -r--r--r--  1 jcprime  staff   117  4 Oct 16:29 f2d26e05bb83f42a2dc8d80056f012857b82ef

./.git/objects/b9:
total 720
 24 -r--r--r--  1 jcprime  staff    8506  4 Oct 16:28 119899755871f5fff8d8b9fe2463193589308f
  8 -r--r--r--  1 jcprime  staff    3898  4 Oct 16:28 1a78d7c5bb63b632dded10ad65d71377da9c35
  8 -r--r--r--  1 jcprime  staff    2653  4 Oct 16:28 2793f9ef84c6f5ebb19a5e54df0ad8747c9224
  8 -r--r--r--  1 jcprime  staff     243  4 Oct 16:28 8f12263f5f267ad7449777b353aca2183b6881
560 -r--r--r--  1 jcprime  staff  284749  4 Oct 16:28 a8b71e82bdf9b65c602b04194118764de2c9da
  8 -r--r--r--  1 jcprime  staff    1552  4 Oct 16:28 b44db3c3b7f427405fd188685d68a57188e189
 64 -r--r--r--  1 jcprime  staff   30050  4 Oct 16:28 cce9874634a8486fca789687c0274ec817a0f1
  8 -r--r--r--  1 jcprime  staff     372  4 Oct 16:28 cf85b87f19c8b830edc8d553d01113ead91b23
 16 -r--r--r--  1 jcprime  staff    7032  4 Oct 16:28 d35e10beca3c3a38943cf2d8e5e7a1d73b24a2
  8 -r--r--r--  1 jcprime  staff     840  4 Oct 16:27 d69ba1265d67dca08cd2b89889cba82e72e869
  8 -r--r--r--  1 jcprime  staff    1176  4 Oct 16:27 fb27262a3fc7d93601b87267347e025bca4cb7

./.git/objects/ba:
total 224
  8 -r--r--r--  1 jcprime  staff   2356  4 Oct 16:27 0f68e7db588558df57c8050d672d7de800ade1
  8 -r--r--r--  1 jcprime  staff   4028  4 Oct 16:28 2a5d6c7e40967a251b483d0104dcb74cfbb42d
  8 -r--r--r--  1 jcprime  staff    182  4 Oct 16:29 4e455f7da4aff76cb8c1b3fdd5c95725446ba6
  8 -r--r--r--  1 jcprime  staff   1509  4 Oct 16:28 66e5a3fda69bff56cc94ba035841b3a01023a7
120 -r--r--r--  1 jcprime  staff  59055  4 Oct 16:28 85cb64b9034353b7adce1ff414a4cd42fb2533
 64 -r--r--r--  1 jcprime  staff  31685  4 Oct 16:28 8f730579dc29fead7d0e17bf8db52ba0f483c2
  8 -r--r--r--  1 jcprime  staff   1605  4 Oct 16:28 eae13b0d37448746df7b3331037e4646e6f3d2

./.git/objects/bb:
total 93688
   16 -r--r--r--  1 jcprime  staff      5554  4 Oct 16:28 16b31e6716ec97ae8c701dae48c2185a6f86b2
    8 -r--r--r--  1 jcprime  staff      2350  4 Oct 16:28 29123ab82b80d279bde993d2ed26d06e1f6a5d
    8 -r--r--r--  1 jcprime  staff       123  4 Oct 16:28 51196bb2bb6112a307dac64251773836c0db28
   40 -r--r--r--  1 jcprime  staff     17280  4 Oct 16:28 6163579ae5b506ef2109848935afbb3d289872
    8 -r--r--r--  1 jcprime  staff       924  4 Oct 16:28 7ba59b9c92ca03d07d0e5176d7ee7a41a3a7dc
    8 -r--r--r--  1 jcprime  staff        48  4 Oct 16:29 a165cbce2f45220ff131c4a43f127702ce8376
93592 -r--r--r--  1 jcprime  staff  47918982  4 Oct 16:28 d152eff071536626f24669ac722416eb952f6c
    8 -r--r--r--  1 jcprime  staff      1321  4 Oct 16:28 e8e132d5d6fa3d4aa8f4a3bfab5df2dbe896ce

./.git/objects/bc:
total 56
8 -r--r--r--  1 jcprime  staff  1034  4 Oct 16:28 0ccba06ea015bf35666d20ca6d1814a672a202
8 -r--r--r--  1 jcprime  staff  1683  4 Oct 16:28 12bb3978a56d0f8b9dda99040dfc9652febf3e
8 -r--r--r--  1 jcprime  staff   669  4 Oct 16:28 530224a469d73da6f0f22b5e2d9bb7d3a34c22
8 -r--r--r--  1 jcprime  staff  1927  4 Oct 16:28 751b6730b6b44cf7a6a384d210430f574e95c6
8 -r--r--r--  1 jcprime  staff  2313  4 Oct 16:27 7df4afa15f5f0bf2f5b46cc8690d1ff1b9fd87
8 -r--r--r--  1 jcprime  staff   891  4 Oct 16:29 cdc855e0ddfdfc3010f78146d0c3d1687cd9b9
8 -r--r--r--  1 jcprime  staff  2478  4 Oct 16:28 cf0a1b9f5949c312635cff0069ac3fdf835bac

./.git/objects/bd:
total 120
 8 -r--r--r--  1 jcprime  staff   1800  4 Oct 16:28 554e5b4d7c614011040acf682533309fbe1eb9
32 -r--r--r--  1 jcprime  staff  12535  4 Oct 16:28 8be9a20523e04cade51654be7c884600389e07
16 -r--r--r--  1 jcprime  staff   4371  4 Oct 16:28 910e909840b4049433af53e142e2f329e51c06
16 -r--r--r--  1 jcprime  staff   4460  4 Oct 16:28 9863e33fa6f1fb4a18b9a12d7f45a78468adba
48 -r--r--r--  1 jcprime  staff  24126  4 Oct 16:28 bffbb3e7ac177dfadc8758341e6a6ec12b6893

./.git/objects/be:
total 56
8 -r--r--r--  1 jcprime  staff   122  4 Oct 16:29 06f627e8b28cf466d9c2747747bff5bfffa751
8 -r--r--r--  1 jcprime  staff  2294  4 Oct 16:28 493b76e6f8e628177cbd114ed5e0b10d01a18e
8 -r--r--r--  1 jcprime  staff  1788  4 Oct 16:27 68b04dda8904995d92b5080759849af935bfe8
8 -r--r--r--  1 jcprime  staff   154  4 Oct 16:29 703308f4b815d4700574773cb68430bada1d48
8 -r--r--r--  1 jcprime  staff  1662  4 Oct 16:27 82553547a69efc17b963b5d0241957be916f44
8 -r--r--r--  1 jcprime  staff   431  4 Oct 16:17 eb4d7df6e89380262b72a364bf0df04ed9c3ee
8 -r--r--r--  1 jcprime  staff  2326  4 Oct 16:28 ec659d8cb5104b54bebf8291a0e8360addad51

./.git/objects/bf:
total 272
 56 -r--r--r--  1 jcprime  staff  27069  4 Oct 16:28 05066a1067b9594ffd5a0c2e029649edcb8a29
  8 -r--r--r--  1 jcprime  staff     90  4 Oct 16:29 166afcffbb4f45f0140c8134c9a66abc051844
  8 -r--r--r--  1 jcprime  staff    180  4 Oct 16:28 25402d55cac6c3e4efd9f94ae2d77f821a0bed
  8 -r--r--r--  1 jcprime  staff    836  4 Oct 16:28 2e697acc29287a67189c672504e107febc5e0f
  8 -r--r--r--  1 jcprime  staff    202  4 Oct 16:28 5cd8a7681f6245396a38c8232875a8a73f1262
  8 -r--r--r--  1 jcprime  staff    528  4 Oct 16:28 85f8627b591779d3ce81111f43c1886621d723
 24 -r--r--r--  1 jcprime  staff  10762  4 Oct 16:28 931d737b1e19057158c78faeb593eb765f5e70
144 -r--r--r--  1 jcprime  staff  69673  4 Oct 16:28 cc51a29d884c8786a1f866ab32bfdf266fcc63
  8 -r--r--r--  1 jcprime  staff   2302  4 Oct 16:27 f83bfbf4b27ad8aadba67569cd1542eca5e3ee

./.git/objects/c0:
total 1312
1272 -r--r--r--  1 jcprime  staff  648105  4 Oct 16:28 02e9069f0a9400cd087eaf046893c17b28f727
   8 -r--r--r--  1 jcprime  staff     532  4 Oct 16:28 3c4d08ba3c115217f6fc0fd3f3d62eaea576c1
   8 -r--r--r--  1 jcprime  staff    1129  4 Oct 16:28 a0c44df05ac48dcf9c22ae9bfc0d727a70904e
  16 -r--r--r--  1 jcprime  staff    4882  4 Oct 16:28 cf60d2a053f0634d897ad904f2a174f348153d
   8 -r--r--r--  1 jcprime  staff     641  4 Oct 16:28 e93e09928e1b6e0a3f962e75a7f57c7f034584

./.git/objects/c1:
total 64
16 -r--r--r--  1 jcprime  staff  4105  4 Oct 16:28 45be39b714a0b7ed07827334381385ea8ab494
 8 -r--r--r--  1 jcprime  staff  1969  4 Oct 16:27 5d71ca36705dcae20753b79ba680f09e4f830b
 8 -r--r--r--  1 jcprime  staff  1868  4 Oct 16:27 77b338b78d20e57dc83df8539da8eb9c9b161f
16 -r--r--r--  1 jcprime  staff  5432  4 Oct 16:28 9fc27443f8466d7ff7e25b9dccb33c05b2f325
 8 -r--r--r--  1 jcprime  staff   147  4 Oct 16:28 c6b2f2f784cc9180e39e5a63d89771500797c3
 8 -r--r--r--  1 jcprime  staff  1850  4 Oct 16:28 d7ff22c2a69c73a1d46e6aa19d54ef73ee4265

./.git/objects/c2:
total 48
8 -r--r--r--  1 jcprime  staff  1723  4 Oct 16:27 1621331314b2dcd15d8fda606b751f7d54cc1d
8 -r--r--r--  1 jcprime  staff  1827  4 Oct 16:27 20620fea55cd9b318dc5518b1ab1fd56104d37
8 -r--r--r--  1 jcprime  staff  1445  4 Oct 16:27 2ab1ffb24f06e9fbf852461dd6acbb78440faa
8 -r--r--r--  1 jcprime  staff  3535  4 Oct 16:28 5fb726be54f9bc587b255c73dce517183fddc3
8 -r--r--r--  1 jcprime  staff  1411  4 Oct 16:28 962e99837c62f520adc91ddae67fb85376137e
8 -r--r--r--  1 jcprime  staff   885  4 Oct 16:28 dc4049be38420f0f8ee9bc8b1ac9fa5c9c430f

./.git/objects/c3:
total 152
16 -r--r--r--  1 jcprime  staff   4200  4 Oct 16:28 2ed4235228a4e2ee169ff2bafb23bfdebdf704
 8 -r--r--r--  1 jcprime  staff     86  4 Oct 16:29 4b665b08b2ecd4de0eb3e32d123e4240829db6
 8 -r--r--r--  1 jcprime  staff   1674  4 Oct 16:27 52575c367240b0f887c1a584caec028efde30a
16 -r--r--r--  1 jcprime  staff   6283  4 Oct 16:28 5992676f358e70ada814a28f3e8402b3c07419
48 -r--r--r--  1 jcprime  staff  23581  4 Oct 16:28 7b72a1d46f12fe8c5cefb516d45264f79879b1
 8 -r--r--r--  1 jcprime  staff   1790  4 Oct 16:28 8eccda3c127351582e583ba4d984e8e31f7a71
40 -r--r--r--  1 jcprime  staff  16592  4 Oct 16:28 cfee3e797db75da0dab02179dd116e260721e4
 8 -r--r--r--  1 jcprime  staff    463  4 Oct 16:28 de57dd34b9e82b13eacbae9b9f599787706610

./.git/objects/c4:
total 88
 8 -r--r--r--  1 jcprime  staff    150  4 Oct 16:28 1b7fe07c97be5d855e2aff906a01a01b2187fa
32 -r--r--r--  1 jcprime  staff  15753  4 Oct 16:28 44804ad58b450eb10a939047def413da0a24ac
 8 -r--r--r--  1 jcprime  staff   2309  4 Oct 16:28 4638a4e96e47350cde9fcac7e6d8e461a24f6c
16 -r--r--r--  1 jcprime  staff   5629  4 Oct 16:28 57ca97774b49efe8969693cb53a34797a658be
 8 -r--r--r--  1 jcprime  staff   1837  4 Oct 16:27 7efbff712ba1f4f2d2719afc45c3d663fabc08
 8 -r--r--r--  1 jcprime  staff   2319  4 Oct 16:28 a56db5e8004c3e4e363a5bfed43918d45ec8f7
 8 -r--r--r--  1 jcprime  staff    329  4 Oct 16:29 fe74e8f8a461fdedafc6a053dcb86989b5bc39

./.git/objects/c5:
total 64
 8 -r--r--r--  1 jcprime  staff   1814  4 Oct 16:27 7e661f7758c68142b1926969ae574de67597dc
 8 -r--r--r--  1 jcprime  staff   3471  4 Oct 16:28 a5dba8241b0c9549ab37ac612bbd94e36d2b4c
 8 -r--r--r--  1 jcprime  staff    785  4 Oct 16:28 ecfe52d9dee2e7e51fbb7e04e9020af1d79213
32 -r--r--r--  1 jcprime  staff  15528  4 Oct 16:28 f021df8c45d246a95766f802d268ac648950a3
 8 -r--r--r--  1 jcprime  staff    204  4 Oct 16:28 f665916c9bdb165120da2c2357e25304330231

./.git/objects/c6:
total 64
 8 -r--r--r--  1 jcprime  staff    912  4 Oct 16:28 00ba9b4247beee280ab7705ba4c328669d63ab
 8 -r--r--r--  1 jcprime  staff    959  4 Oct 16:28 0ac69867076f5a325b257bcb7dd042a77c626f
 8 -r--r--r--  1 jcprime  staff    895  4 Oct 16:27 369c891d7105dec259086ca69ecc4358ab02c5
32 -r--r--r--  1 jcprime  staff  13999  4 Oct 16:28 a8693591be578e3bb782b26286a24b7d04124a
 8 -r--r--r--  1 jcprime  staff   1233  4 Oct 16:28 d44bf6ac2cc32c0fd8e6472d299ec5a764d707

./.git/objects/c7:
total 24
8 -r--r--r--  1 jcprime  staff  2345  4 Oct 16:28 2c2982bb57c90a371a5d5ab6ee8d1a914585e5
8 -r--r--r--  1 jcprime  staff  1509  4 Oct 16:28 7cde9f212e39b7f2f0588b8045d6143915888d
8 -r--r--r--  1 jcprime  staff  1777  4 Oct 16:27 8118b8db97ccf9b043f48a04bda9c180d596d2

./.git/objects/c8:
total 64
 8 -r--r--r--  1 jcprime  staff   148  4 Oct 16:17 31fea29c99ed9050a6f88de37804ab0d9c2335
16 -r--r--r--  1 jcprime  staff  6589  4 Oct 16:28 735a031bf04ebe14642d740ae595b1451762c9
 8 -r--r--r--  1 jcprime  staff   115  4 Oct 16:29 93e9a8943095c8f59f9286c9d42d95db614e41
16 -r--r--r--  1 jcprime  staff  7186  4 Oct 16:28 9d412e908ff139993f9af79eec44a2053d4735
 8 -r--r--r--  1 jcprime  staff  1843  4 Oct 16:27 ae69c36e13769f8fb962f219a1c892175adb55
 8 -r--r--r--  1 jcprime  staff  1681  4 Oct 16:27 e580a4b9d2c5debb8588d924a5ff16992f1500

./.git/objects/c9:
total 32
8 -r--r--r--  1 jcprime  staff  1859  4 Oct 16:27 d7f634f8641d86a97f149cf702b358cb050aa3
8 -r--r--r--  1 jcprime  staff   401  4 Oct 16:28 db4591825bd7a918df686ff04aeb3a87d3bda0
8 -r--r--r--  1 jcprime  staff  2031  4 Oct 16:27 e65305660683d8579a731032559be962bcdd5c
8 -r--r--r--  1 jcprime  staff  1640  4 Oct 16:27 f31192b35a275ea6ef075f0412b907144a58de

./.git/objects/ca:
total 32
8 -r--r--r--  1 jcprime  staff  1604  4 Oct 16:28 79fe967c4452dc8e8b0160aa5560cc8cfa509b
8 -r--r--r--  1 jcprime  staff   960  4 Oct 16:28 7aa13eb8b3ba369e1c4bc745f7ac10b7540435
8 -r--r--r--  1 jcprime  staff  1972  4 Oct 16:27 bbf5bc9631ce1905b20c9ab58d00efa3da4b2a
8 -r--r--r--  1 jcprime  staff   792  4 Oct 16:28 e803f3391419e81196f493af07a6547763512e

./.git/objects/cb:
total 88
 8 -r--r--r--  1 jcprime  staff   2890  4 Oct 16:28 0c520e5778cbed04a620e80b7c02771cea7d50
 8 -r--r--r--  1 jcprime  staff   2159  4 Oct 16:28 2a4bf891bc043efc72e15d17ef1904009bb6a0
 8 -r--r--r--  1 jcprime  staff    206  4 Oct 16:29 59897e4fb05c44655e8947578d0c4cd2f81f9d
 8 -r--r--r--  1 jcprime  staff   1409  4 Oct 16:28 a02c17e1927757dca6c973bdb27708123e73f8
 8 -r--r--r--  1 jcprime  staff   3416  4 Oct 16:28 ba57ef4ae568dbd1aa41c23edab8014ecd4f81
 8 -r--r--r--  1 jcprime  staff    111  4 Oct 16:28 cc4428d5b655f6e9cf233d0b1d21578dbe35a6
 8 -r--r--r--  1 jcprime  staff    864  4 Oct 16:28 d473d18f6f560d7cefc167ac5a4527b7e1427f
32 -r--r--r--  1 jcprime  staff  13788  4 Oct 16:28 fc858542fe004191fb53c3a312975905a94a8c

./.git/objects/cc:
total 456
  8 -r--r--r--  1 jcprime  staff    3516  4 Oct 16:28 08e415da0805598c843a23503fb4562f6749f1
  8 -r--r--r--  1 jcprime  staff    1488  4 Oct 16:28 0ec8d945e5ea1003a990806c2838c8e7a43236
 16 -r--r--r--  1 jcprime  staff    4889  4 Oct 16:28 19fb4cfcd52bf05beaf7b106453e884081b324
  8 -r--r--r--  1 jcprime  staff     391  4 Oct 16:28 41fa96c7db4114bf1b9d9398bd8ed23426ab6c
  8 -r--r--r--  1 jcprime  staff    1746  4 Oct 16:27 562e57bb15dfde94fae0e36aafc385fedd3ccc
  8 -r--r--r--  1 jcprime  staff    3289  4 Oct 16:28 69393d7a7fc6b8c57899dfe82798593ed24ec5
  8 -r--r--r--  1 jcprime  staff     724  4 Oct 16:28 789af78cfeabf9e2e6619aa349e88ffcab0248
  8 -r--r--r--  1 jcprime  staff     445  4 Oct 16:28 a3c7321c18ca74e93ec4e684f63e31ba96bd6c
  8 -r--r--r--  1 jcprime  staff    1257  4 Oct 16:28 b61104661b813f211a402b4ed04d67a68d1838
376 -r--r--r--  1 jcprime  staff  189761  4 Oct 16:28 f682b799a9cd09811cbe89106ac962e320d86b

./.git/objects/cd:
total 24
8 -r--r--r--  1 jcprime  staff  3408  4 Oct 16:28 1e6ebfc373c583b59d7867396bf9ac4b405a27
8 -r--r--r--  1 jcprime  staff  1996  4 Oct 16:29 74c7aa4b78fb0a83ce0c069ada40e2debd34e6
8 -r--r--r--  1 jcprime  staff   293  4 Oct 16:28 cfa0c832e480de189c5d267f9117de4f54aa68

./.git/objects/ce:
total 40
 8 -r--r--r--  1 jcprime  staff    908  4 Oct 16:28 203eee470aeeeb2a2113b1e87b8f2ccbcb5b2b
32 -r--r--r--  1 jcprime  staff  12869  4 Oct 16:28 38013afed5b90c60057fdc27064e054a17dfc2

./.git/objects/cf:
total 112
 8 -r--r--r--  1 jcprime  staff   1754  4 Oct 16:27 0fb621bcb97adeb284026f4571f7ca1c39350c
16 -r--r--r--  1 jcprime  staff   6982  4 Oct 16:28 10cb605ecb877eb3cf6976d737b8a8177997d6
16 -r--r--r--  1 jcprime  staff   5728  4 Oct 16:28 579d4cc9bde94f920d9293deb897c66f460976
 8 -r--r--r--  1 jcprime  staff    748  4 Oct 16:28 5ed0b815e0c8de7f19052fc3efe8c0d860a587
 8 -r--r--r--  1 jcprime  staff    243  4 Oct 16:28 6bcf1541dd382d128824b0f7f6c46320ca2765
 8 -r--r--r--  1 jcprime  staff    888  4 Oct 16:28 a1c5a098f480eab7299d370e41185716f8bbd6
 8 -r--r--r--  1 jcprime  staff   1901  4 Oct 16:28 c87f3f1e5a340e32adfdebc9d6a27d72516bf3
32 -r--r--r--  1 jcprime  staff  13282  4 Oct 16:28 cb1b869582825c605512543a2ea553ae0e5788
 8 -r--r--r--  1 jcprime  staff     57  4 Oct 16:29 d42f3b64ddabc4232903e5a329df4096611e9a

./.git/objects/d0:
total 32
 8 -r--r--r--  1 jcprime  staff   613  4 Oct 16:28 666f81d1b4dc4108f8f7a1bfabb1af758c5f74
 8 -r--r--r--  1 jcprime  staff    79  4 Oct 16:29 d3473b917012f498a7cb140730534b486507f0
16 -r--r--r--  1 jcprime  staff  6949  4 Oct 16:28 e50ffe66a1eaae026e329f31e256b50b25c36b

./.git/objects/d1:
total 104
 8 -r--r--r--  1 jcprime  staff  1765  4 Oct 16:27 0d74430e95bb864b76caeac8e94bab9f3b7d4f
 8 -r--r--r--  1 jcprime  staff    86  4 Oct 16:29 48187ad10f7b69f32e99438c8f2995466430ee
 8 -r--r--r--  1 jcprime  staff   454  4 Oct 16:28 6c32f526c2665d55ea06006b1cc71d52f22ad9
 8 -r--r--r--  1 jcprime  staff  1405  4 Oct 16:28 8587e601ab8c5209e6c26b19fb5e326cb57159
 8 -r--r--r--  1 jcprime  staff  1835  4 Oct 16:27 9cd1ec82865132e323cf2ae3906fc51cd04752
 8 -r--r--r--  1 jcprime  staff  1352  4 Oct 16:28 b2e91d981ef400ed9ece0798c3be105911e472
16 -r--r--r--  1 jcprime  staff  4216  4 Oct 16:28 b6c21f220ef7968b9521621a57cb6cfb341be3
 8 -r--r--r--  1 jcprime  staff   617  4 Oct 16:28 ba28d436788573194c7d6935c598dca2dda892
 8 -r--r--r--  1 jcprime  staff  1000  4 Oct 16:28 d5b3e32e5efd08aec6701d2c4e46f1572a6124
24 -r--r--r--  1 jcprime  staff  9384  4 Oct 16:28 ef7895833ba7e420ec1641ac055bf454e38c3d

./.git/objects/d2:
total 56
8 -r--r--r--  1 jcprime  staff   614  4 Oct 16:28 295193b9a8afbcb5a5aed2ab41086361d65bd8
8 -r--r--r--  1 jcprime  staff    54  4 Oct 16:28 2ffd8d0c83c37dfb57dfc684c4595beacc73c0
8 -r--r--r--  1 jcprime  staff  1256  4 Oct 16:28 5e40996f3ad582b3999e8ba8007f90a5d2da3a
8 -r--r--r--  1 jcprime  staff  1744  4 Oct 16:27 68e902a2ec56ef159dc883d9d7a7d6ea300987
8 -r--r--r--  1 jcprime  staff  1287  4 Oct 16:27 6aa6009276a84ec2ceab76a26e0e0d686d3ed0
8 -r--r--r--  1 jcprime  staff  3688  4 Oct 16:28 d4252bf58bd9649e9fa84519d22aab00ae764c
8 -r--r--r--  1 jcprime  staff   537  4 Oct 16:28 e0359ab87a546c5e083bc1ce33ea003f08f777

./.git/objects/d3:
total 48
8 -r--r--r--  1 jcprime  staff  3374  4 Oct 16:28 1bf09817e82685734cc77749851ad3a4d9d72b
8 -r--r--r--  1 jcprime  staff  2303  4 Oct 16:28 6762979bedd5c07c3db1090e293fc9fe32c85c
8 -r--r--r--  1 jcprime  staff  1443  4 Oct 16:28 7a202af56f680413c1fd72796b0005a326d6db
8 -r--r--r--  1 jcprime  staff   772  4 Oct 16:27 949726d8ba42abae3e08ef615636073cadf933
8 -r--r--r--  1 jcprime  staff  2326  4 Oct 16:28 ca0bacf1fb8e18d065155596d64d0085630768
8 -r--r--r--  1 jcprime  staff  3347  4 Oct 16:28 e7374b609780a8f0eb970236d1bb2cf58103d8

./.git/objects/d4:
total 104
24 -r--r--r--  1 jcprime  staff  11716  4 Oct 16:28 11e0abca737dfeecdcaf60d5c8bf2c49f09d6c
 8 -r--r--r--  1 jcprime  staff     87  4 Oct 16:29 297c353773e8f660cb1043b4021ee18cb603b7
 8 -r--r--r--  1 jcprime  staff     51  4 Oct 16:28 404fc45db5ed5b3f7613d91bd5823dfcf38ed9
 8 -r--r--r--  1 jcprime  staff    232  4 Oct 16:17 4fb0c71d67611431cc9dd2658c8d529c5d9f9f
16 -r--r--r--  1 jcprime  staff   6130  4 Oct 16:27 5c8c0c89fe27cbcf3fa3ab90595d1d793df729
16 -r--r--r--  1 jcprime  staff   7695  4 Oct 16:28 6ed7f623801c726889c27dc57a746b3350b68c
 8 -r--r--r--  1 jcprime  staff    176  4 Oct 16:29 865871e1b1bca8056386283b316b2439f1028e
 8 -r--r--r--  1 jcprime  staff    253  4 Oct 16:28 8e31282df19074b63680c5bafa2a49dc8497d2
 8 -r--r--r--  1 jcprime  staff    411  4 Oct 16:28 c5e05b6e4d20f04aded5391723985cf661eecd

./.git/objects/d5:
total 208
16 -r--r--r--  1 jcprime  staff   7719  4 Oct 16:28 11905c1647a1e311e8b20d5930a37a9c2531cd
64 -r--r--r--  1 jcprime  staff  28972  4 Oct 16:28 131d423b8248b8158a6dfd82c52c12d6afb639
16 -r--r--r--  1 jcprime  staff   7590  4 Oct 16:28 17ec774a87a2201ab195ca46371990a3055b01
 8 -r--r--r--  1 jcprime  staff   1822  4 Oct 16:28 2de0899e0d2ba35114351ca612830122fbba30
16 -r--r--r--  1 jcprime  staff   4855  4 Oct 16:28 53b167104548c6a544c1a128190a8335ae3097
 8 -r--r--r--  1 jcprime  staff    189  4 Oct 16:28 91817d6428855b532a703b9413ab913d3e383c
 8 -r--r--r--  1 jcprime  staff     80  4 Oct 16:29 ad11b3767ca40291883083168c7a2b184b291b
72 -r--r--r--  1 jcprime  staff  36347  4 Oct 16:28 c7ab296facb319590ec342702839bbe803cb46

./.git/objects/d6:
total 144
16 -r--r--r--  1 jcprime  staff   5198  4 Oct 16:28 072aec28240746dd0759a15c3bafe93e519e27
24 -r--r--r--  1 jcprime  staff  10268  4 Oct 16:28 134b852cbecdb2120ebcd7def8c2fbab6a54fc
 8 -r--r--r--  1 jcprime  staff   1574  4 Oct 16:27 227427ed6e221b93f12fb336252cb21b780240
 8 -r--r--r--  1 jcprime  staff    911  4 Oct 16:28 22db19dbfcd23eaa3a834c8b4c8c2156f195da
 8 -r--r--r--  1 jcprime  staff    433  4 Oct 16:29 62fdb3bbbd2bdf34f3cd12ced5d2a92753f679
24 -r--r--r--  1 jcprime  staff  11192  4 Oct 16:28 734110e70f8e39d9e116bde9eb82aa5a163bee
 8 -r--r--r--  1 jcprime  staff   2122  4 Oct 16:28 7ccfb9a180c8addbbf9c4bcd32f5c61a4d80d2
 8 -r--r--r--  1 jcprime  staff    834  4 Oct 16:28 997c127ec724143f48bee56ee3637ba4ed224b
 8 -r--r--r--  1 jcprime  staff    593  4 Oct 16:28 9f2e841e488f66f0e625887df0ea7fa783a984
 8 -r--r--r--  1 jcprime  staff   1395  4 Oct 16:27 a9eec9e239575e1e3a1018e14e0fcf7d780637
 8 -r--r--r--  1 jcprime  staff    597  4 Oct 16:28 c8129d90b53df4aa9aefa3286bd44b5ed41af5
16 -r--r--r--  1 jcprime  staff   4860  4 Oct 16:28 f824a28996f73767c4f19e016114674bedd251

./.git/objects/d7:
total 40
8 -r--r--r--  1 jcprime  staff  2179  4 Oct 16:27 14bc5590a499b40693bcf0f9eaeef508f3d69d
8 -r--r--r--  1 jcprime  staff   501  4 Oct 16:28 18005ddf61d40a61fba6982b5bc79f4924e300
8 -r--r--r--  1 jcprime  staff  1764  4 Oct 16:27 2f2c89cb5c79c3be7db9dbd7609a1e081f30d5
8 -r--r--r--  1 jcprime  staff  1506  4 Oct 16:28 5674d5747e50dedb2f5d6eb336f870129c2c10
8 -r--r--r--  1 jcprime  staff  1395  4 Oct 16:28 65ed0b44d30f2a047a6562adad24daf41eed7e

./.git/objects/d8:
total 128
 8 -r--r--r--  1 jcprime  staff   2220  4 Oct 16:27 0238504cb24d6dd8d68e8fecb300342cf9e533
64 -r--r--r--  1 jcprime  staff  29207  4 Oct 16:28 521f49c12f28b6fb89dbd8a0ad47809935b603
 8 -r--r--r--  1 jcprime  staff   2253  4 Oct 16:27 64449983035d60e8a9ad9b93dec823351e604e
 8 -r--r--r--  1 jcprime  staff   1392  4 Oct 16:27 a795537460d884533a79f57e6603054dbb565c
24 -r--r--r--  1 jcprime  staff  10720  4 Oct 16:28 aa6f6d3cd5e72709c8413fd47949d551abd7f7
 8 -r--r--r--  1 jcprime  staff   1932  4 Oct 16:28 b7fd42999745c4623bf7285b5c3dadbb65dca5
 8 -r--r--r--  1 jcprime  staff   2343  4 Oct 16:27 c0200158d22ca62e4ae3269d7180f42064ee1e

./.git/objects/d9:
total 32
8 -r--r--r--  1 jcprime  staff   122  4 Oct 16:28 13ceb8c2bddad1550201a399e9b8fa27925ccc
8 -r--r--r--  1 jcprime  staff   424  4 Oct 16:29 236c4d26249f533cfa7610a7e14a4c506c3642
8 -r--r--r--  1 jcprime  staff  1211  4 Oct 16:27 3c430775a5f30953a5121b552a046045ca404b
8 -r--r--r--  1 jcprime  staff  2293  4 Oct 16:28 4a31acd044d2c2bf49a438f90d39df249488e6

./.git/objects/da:
total 88
16 -r--r--r--  1 jcprime  staff  7428  4 Oct 16:28 1e0772bdc913cf5d306cf9a97498338072ae4f
 8 -r--r--r--  1 jcprime  staff  1665  4 Oct 16:27 1e54046b1b049749a3d858f05931ce37f02bc4
 8 -r--r--r--  1 jcprime  staff  1862  4 Oct 16:28 35a61d0c3588f95115549f60b6b39ba210b6a3
 8 -r--r--r--  1 jcprime  staff  2207  4 Oct 16:28 48c26266e6d9ddcfb39188d3a5fd46652293e0
 8 -r--r--r--  1 jcprime  staff    58  4 Oct 16:29 5c25f331ef2124531b968a9ddea2b17174108e
 8 -r--r--r--  1 jcprime  staff   806  4 Oct 16:28 79a80fa4def9cc8a6680a99289edb2e05e7889
16 -r--r--r--  1 jcprime  staff  6582  4 Oct 16:28 9194f903718124fb6720e65c4fd7d5dd9c8593
16 -r--r--r--  1 jcprime  staff  7881  4 Oct 16:28 a37aea004cab72053c15f5f448475897d197aa

./.git/objects/db:
total 128
16 -r--r--r--  1 jcprime  staff   4810  4 Oct 16:28 010e0904458aa2b02b71efbb90ade0ef2adffb
 8 -r--r--r--  1 jcprime  staff    748  4 Oct 16:28 040601c3406dbfccc373bb3c76399f38f79bfb
 8 -r--r--r--  1 jcprime  staff   1509  4 Oct 16:28 539c90115083504659a279269fe0341d32e395
16 -r--r--r--  1 jcprime  staff   5620  4 Oct 16:28 951776fb890ee1835fdaa5cf8a5c5194d06f9c
16 -r--r--r--  1 jcprime  staff   5041  4 Oct 16:28 b1723de026f564f3cdeebc1967136c22dc6770
 8 -r--r--r--  1 jcprime  staff    227  4 Oct 16:28 bf4dccd43196cc8e033fb4234e2f772bfd0085
56 -r--r--r--  1 jcprime  staff  26460  4 Oct 16:28 f622348201431c89484a009ffc4ac43ea759d6

./.git/objects/dc:
total 128
24 -r--r--r--  1 jcprime  staff  9180  4 Oct 16:28 10e26ae0a81f4702b51f15904f1369f3dec1ac
24 -r--r--r--  1 jcprime  staff  8382  4 Oct 16:28 1a8e2c9614dd91cc1b4460db91ffc0f1a86a23
 8 -r--r--r--  1 jcprime  staff    92  4 Oct 16:27 2a0dd5e64ad5309dc473635201c9c487dac4ab
 8 -r--r--r--  1 jcprime  staff   680  4 Oct 16:28 5a58b58c2d7c045a9459835d3dd4c4bb857ed4
 8 -r--r--r--  1 jcprime  staff  3086  4 Oct 16:27 65b7727f5d5a78415aa8d8720cd9ecad00cbc5
 8 -r--r--r--  1 jcprime  staff  2329  4 Oct 16:28 7a6fbb081582488358579b2534d8110e34efa5
16 -r--r--r--  1 jcprime  staff  7215  4 Oct 16:28 8d91fe599314245b2e8e4d77e0e25ed2e47aa9
 8 -r--r--r--  1 jcprime  staff  1506  4 Oct 16:28 ad57e8ee560a093cf4ea8cc88308e066efb0e0
 8 -r--r--r--  1 jcprime  staff  1370  4 Oct 16:27 d458b6d177553eb2a9ea1df1455cc577526d1f
16 -r--r--r--  1 jcprime  staff  4473  4 Oct 16:28 e90d27bd10353f6a3c17ed2252370e89c71ae9

./.git/objects/dd:
total 96
16 -r--r--r--  1 jcprime  staff  6971  4 Oct 16:28 21ba71d74b423f967ccddf1295d1efe7c564fa
 8 -r--r--r--  1 jcprime  staff  1206  4 Oct 16:28 3ae3e5d41c4981aa1494c5d0351323ff43b737
16 -r--r--r--  1 jcprime  staff  7594  4 Oct 16:28 a78573d3c8788401d95d45ac333ce5b421c376
 8 -r--r--r--  1 jcprime  staff  2182  4 Oct 16:27 aba1d8be7b94b502e605d623371ce234c99486
16 -r--r--r--  1 jcprime  staff  6264  4 Oct 16:28 b5d3b0d46979d9254957e48e6cb52b3bd88a83
16 -r--r--r--  1 jcprime  staff  5108  4 Oct 16:28 cb9ed9a26ca1b01304b5c3253d8a73d1618ab9
16 -r--r--r--  1 jcprime  staff  7549  4 Oct 16:28 f6725e57d091e5336a0b557de697ea9e091da2

./.git/objects/de:
total 792
704 -r--r--r--  1 jcprime  staff  359785  4 Oct 16:28 0767e05f7f8924dcebda8d4a688138efd570f9
 24 -r--r--r--  1 jcprime  staff    8730  4 Oct 16:28 37ac7a8269ebd7df1cb1597d47dc95789f8ad0
  8 -r--r--r--  1 jcprime  staff     126  4 Oct 16:28 3a3d8390eacd0060198490fed089e06c128cfb
  8 -r--r--r--  1 jcprime  staff     172  4 Oct 16:28 4f5f75d7ccd3a5b62bd2ce683ed678a5cb72c2
 24 -r--r--r--  1 jcprime  staff    8730  4 Oct 16:28 58defb273d2dde2693086d2ef5d125c17179d0
  8 -r--r--r--  1 jcprime  staff     942  4 Oct 16:28 6566dab3a0cca0334ac42e898695dc1389a947
  8 -r--r--r--  1 jcprime  staff      89  4 Oct 16:27 d3f26bf934e1750680116968a13439398c4688
  8 -r--r--r--  1 jcprime  staff    1866  4 Oct 16:27 ddbbcc581fdac386669cd144cc4d6a97803d50

./.git/objects/df:
total 632
 40 -r--r--r--  1 jcprime  staff   17466  4 Oct 16:28 16101d9a55effdd79304d53b34689bb441dd3b
  8 -r--r--r--  1 jcprime  staff    1463  4 Oct 16:28 3099cb93b7de9517d8e5791ca420dbfb8a79b4
  8 -r--r--r--  1 jcprime  staff    1162  4 Oct 16:28 70c9ffae142ed40dfbd0cfe796315927e65812
552 -r--r--r--  1 jcprime  staff  282409  4 Oct 16:28 b7ba37b354b37309d7b11824e5af8f8ab73e39
 24 -r--r--r--  1 jcprime  staff   11636  4 Oct 16:28 fb1ec0d50773fc81e4147127fd5a9a8f0cdabb

./.git/objects/e0:
total 56
 8 -r--r--r--  1 jcprime  staff  1524  4 Oct 16:27 055247fa9ec3b0e341bd1bb6e1f5452df6755e
 8 -r--r--r--  1 jcprime  staff   954  4 Oct 16:29 311a51ecc1d912269ad1a47f83428d1f971a7b
16 -r--r--r--  1 jcprime  staff  5709  4 Oct 16:28 58e98294f4ed2d922c8af5903598e5aa87fd00
 8 -r--r--r--  1 jcprime  staff   220  4 Oct 16:29 7c7c6b92935eaf0153cb69e7c059342299b421
 8 -r--r--r--  1 jcprime  staff  3184  4 Oct 16:28 99aadb171fad62fc7b140790e2d64ff8d08e39
 8 -r--r--r--  1 jcprime  staff   344  4 Oct 16:28 eabfc7b9868bfe6256ea50cd74bbac3b600e0d

./.git/objects/e1:
total 192
 16 -r--r--r--  1 jcprime  staff   4394  4 Oct 16:28 18f60806b54b6e5e3c60236e95dc59130b028f
 40 -r--r--r--  1 jcprime  staff  19685  4 Oct 16:28 2613d5e759dce09bed5ae0f89b9a2573f89bac
  8 -r--r--r--  1 jcprime  staff   1739  4 Oct 16:27 339a3b4d6aa4532060ac6a94cdbba52ed227a7
112 -r--r--r--  1 jcprime  staff  54799  4 Oct 16:28 5e63d1e6979a5e4f546520c068c1d79fbc58e5
  8 -r--r--r--  1 jcprime  staff    766  4 Oct 16:28 c8b7b5e02473a62678299e3ea7d905bef0acfa
  8 -r--r--r--  1 jcprime  staff   1475  4 Oct 16:28 fdd566ed51263de2e50734d2c9016d6866f55f

./.git/objects/e2:
total 72
 8 -r--r--r--  1 jcprime  staff   2229  4 Oct 16:27 1b22ef3d802da91c3e9f6886d20e9b2f0dbd76
 8 -r--r--r--  1 jcprime  staff   1540  4 Oct 16:27 1cd2d94e3c221b04885add6336f73cf8426339
 8 -r--r--r--  1 jcprime  staff   4028  4 Oct 16:28 238cc9fd327a7abbbd700aeb6d64f75f29d798
 8 -r--r--r--  1 jcprime  staff     21  4 Oct 16:18 707c08f970e1402617520ebae5f2731db72d9b
 8 -r--r--r--  1 jcprime  staff   2333  4 Oct 16:28 77bf1bdc06878599826d297ccf980acaf4f5eb
32 -r--r--r--  1 jcprime  staff  13891  4 Oct 16:28 86d2ea515dc5e9df38ea3479a1d195aa81427d

./.git/objects/e3:
total 264
  8 -r--r--r--  1 jcprime  staff    108  4 Oct 16:28 0302befb552b84b365466e2c9c251f57647fd3
  8 -r--r--r--  1 jcprime  staff   1118  4 Oct 16:27 0c53aafa92ab5fddc31b8827aee5f7017960c4
  8 -r--r--r--  1 jcprime  staff   2472  4 Oct 16:28 175a1a8c990a34dc55f39c6451c0b5ed400f52
  8 -r--r--r--  1 jcprime  staff   1493  4 Oct 16:28 4967d2798966b5739d491ab58371286c429951
  8 -r--r--r--  1 jcprime  staff    788  4 Oct 16:28 4d781641b0c3c93816f2ccdc5b2349ef64bbcf
  8 -r--r--r--  1 jcprime  staff   1109  4 Oct 16:27 4f1df64c48f8255830e547a7aca14cd2be4427
  8 -r--r--r--  1 jcprime  staff   1297  4 Oct 16:27 72da06f9349c8227555ba6edcc172e8b9b4b2d
168 -r--r--r--  1 jcprime  staff  85108  4 Oct 16:28 91fc9721f7c27d8957a8a7f9b9faaeacf7291a
  8 -r--r--r--  1 jcprime  staff    143  4 Oct 16:18 abe53ed7fbcb16b899bfcad8515f91132dcb95
  8 -r--r--r--  1 jcprime  staff   1265  4 Oct 16:28 ca9e763718718f1358978b6425543b5e959e9d
  8 -r--r--r--  1 jcprime  staff   2096  4 Oct 16:28 cc39dd8ee0a8417a1e0f9a22f40d66fde5e778
  8 -r--r--r--  1 jcprime  staff    588  4 Oct 16:28 e225487e981a9c485ca4e4019dd52f20b9cc10
  8 -r--r--r--  1 jcprime  staff    207  4 Oct 16:28 ef5d253d26c7a8a37dddf56126a8968769a65e

./.git/objects/e4:
total 392
  8 -r--r--r--  1 jcprime  staff     757  4 Oct 16:28 056338134070c0685eff77116e1a37a9d55747
  8 -r--r--r--  1 jcprime  staff    1929  4 Oct 16:28 05afc9a09a6808762fdc8b8e3534f79ec8932c
328 -r--r--r--  1 jcprime  staff  165250  4 Oct 16:28 099b43afdd3ce3b2525df126f03a7f0cad350b
  8 -r--r--r--  1 jcprime  staff    2318  4 Oct 16:28 130332d8ef9ed3d171c29860276193fc13e2ea
  8 -r--r--r--  1 jcprime  staff    1789  4 Oct 16:28 4f46b48b04650dcccfc55bf2f1f388efde5762
  8 -r--r--r--  1 jcprime  staff    1516  4 Oct 16:28 87d132dd2525091f7039c5ab3fb38bbd2ff628
  8 -r--r--r--  1 jcprime  staff    1401  4 Oct 16:28 cd338cabb77d218d3614f67091809eda0854f0
  8 -r--r--r--  1 jcprime  staff     180  4 Oct 16:28 f08775679e669ad2874a9bc602edf566a61a20
  8 -r--r--r--  1 jcprime  staff    1554  4 Oct 16:28 f22b88dee8c4407fe037eee998039ab54245fd

./.git/objects/e5:
total 112
 8 -r--r--r--  1 jcprime  staff   1394  4 Oct 16:28 168aa592daf04f9680cf8c5319895cd0d4ec95
 8 -r--r--r--  1 jcprime  staff   4060  4 Oct 16:27 21ec43146f50caaa6cde3d3890e815f0cfaf31
 8 -r--r--r--  1 jcprime  staff   1488  4 Oct 16:27 2c4dd94189882938f3309125f90ae9aadfc010
 8 -r--r--r--  1 jcprime  staff    864  4 Oct 16:28 691bed59ca6a3b47608b0dea1b54e1942cc14d
24 -r--r--r--  1 jcprime  staff   8564  4 Oct 16:28 98b236ba9446b0fc4ffcabb6bbfcf3d05b1a44
16 -r--r--r--  1 jcprime  staff   6889  4 Oct 16:28 a3d6c9c04db20f564567624ed14205e1224df3
24 -r--r--r--  1 jcprime  staff  10608  4 Oct 16:28 b2bc8a60c22a9bda4623fd8128dd2af0d2b7c5
 8 -r--r--r--  1 jcprime  staff   2635  4 Oct 16:28 e47078ae7886e7658d16a1e28e28c3005ac36f
 8 -r--r--r--  1 jcprime  staff     88  4 Oct 16:29 e83d370f9cfadf8b6861cf712e9cce15f6b64b

./.git/objects/e6:
total 120
 8 -r--r--r--  1 jcprime  staff     56  4 Oct 16:29 37f30a194388cafb0c4dc244fcbe03e99f4055
 8 -r--r--r--  1 jcprime  staff    113  4 Oct 16:29 45a9be1e93aa721875aeaad85b4200e2653439
 8 -r--r--r--  1 jcprime  staff   1858  4 Oct 16:27 4aaa8c462ffcdc8db03b96c765a617c2f2f2a9
 8 -r--r--r--  1 jcprime  staff   1871  4 Oct 16:28 5c20461f17b15bdb2867c2afefb66375127fa0
 8 -r--r--r--  1 jcprime  staff   1148  4 Oct 16:29 89a269e0da8a7682e2499f455e6c3ef5c28c13
32 -r--r--r--  1 jcprime  staff  14536  4 Oct 16:28 89b499897bea380a4f31fd0b89b30667610172
 8 -r--r--r--  1 jcprime  staff     15  4 Oct 16:28 9de29bb2d1d6434b8b29ae775ad8c2e48c5391
 8 -r--r--r--  1 jcprime  staff   2316  4 Oct 16:28 a63bd81ffdefd642e0b1a0abbbf7b0231675c0
 8 -r--r--r--  1 jcprime  staff    817  4 Oct 16:29 b0f73c457705285088108aaec64be02ba85339
 8 -r--r--r--  1 jcprime  staff   1011  4 Oct 16:28 b41343badd38b6317ddf6aa1fa1092250c1f29
 8 -r--r--r--  1 jcprime  staff   3890  4 Oct 16:28 f7d4a0343f7f54db132ecc97835d1620150d88
 8 -r--r--r--  1 jcprime  staff    737  4 Oct 16:28 f8fc08f3b55ccaa6490cac49a8f0c2e6714205

./.git/objects/e7:
total 1480
   8 -r--r--r--  1 jcprime  staff    2455  4 Oct 16:28 38217e27a2997b774bceaf482dbaedf8c1dba2
   8 -r--r--r--  1 jcprime  staff     515  4 Oct 16:28 65c485c49399070eeb32659687f80ba554a10c
   8 -r--r--r--  1 jcprime  staff    1373  4 Oct 16:27 84c6fc75a59e88b361d20f974b94bf91a00caa
   8 -r--r--r--  1 jcprime  staff    1471  4 Oct 16:28 b383d679cafc1179588f23f690cfeb95dcc036
1448 -r--r--r--  1 jcprime  staff  740771  4 Oct 16:28 c45e454627dde05a3b4922c8a4dc438864e828

./.git/objects/e8:
total 48
 8 -r--r--r--  1 jcprime  staff   117  4 Oct 16:29 41f96db0e2561ffe3aca8e4c0b6c1ced5f2b32
24 -r--r--r--  1 jcprime  staff  8878  4 Oct 16:28 72b34f0f9e1ff6587f02e9998498fc6d355110
 8 -r--r--r--  1 jcprime  staff   186  4 Oct 16:28 cfc1d816d88bce8f9d1b77c56974ade3eba40d
 8 -r--r--r--  1 jcprime  staff  3557  4 Oct 16:27 e5af24d77bd9329503b7ed578e73d7ced425d9

./.git/objects/e9:
total 88
 8 -r--r--r--  1 jcprime  staff  1814  4 Oct 16:27 055e0854fa5c0ef0bf6cef837b0c69a3f22ad9
 8 -r--r--r--  1 jcprime  staff  2207  4 Oct 16:27 187c7644e382d24c399e141436696ece3c7be8
16 -r--r--r--  1 jcprime  staff  4644  4 Oct 16:27 3005ae83a767effc0f6b192c99b8c6b12dccb4
 8 -r--r--r--  1 jcprime  staff  1998  4 Oct 16:29 41398b6e274a287b2ba4d3b4148e751873b063
 8 -r--r--r--  1 jcprime  staff  1458  4 Oct 16:28 6a92f01db31a4b4431378fdf3a121a81e1ff43
 8 -r--r--r--  1 jcprime  staff  1526  4 Oct 16:28 6b37fb46a87ddb6f6d624246c477ea3ac8ff3d
16 -r--r--r--  1 jcprime  staff  4822  4 Oct 16:28 e0f30cfd19d2d659b69e15785f866366032811
 8 -r--r--r--  1 jcprime  staff   838  4 Oct 16:28 e969db7818b68f19885de6e1ff06ed4d59e3a1
 8 -r--r--r--  1 jcprime  staff  1434  4 Oct 16:28 ec1371c5e1f1a47b4bb98cd34d7f5aa9b9b0c3

./.git/objects/ea:
total 40
8 -r--r--r--  1 jcprime  staff  2994  4 Oct 16:28 16bb91fea0efd503363282b6728e7a47ee1ed9
8 -r--r--r--  1 jcprime  staff  2710  4 Oct 16:28 9110140a695aad2574af7b75d078563634944a
8 -r--r--r--  1 jcprime  staff   466  4 Oct 16:28 b568b8fa7016f8a6d5a2d5b7c7dccdc65e2391
8 -r--r--r--  1 jcprime  staff  1618  4 Oct 16:28 dc68e4bb65e42e82b22aa0a3c3ff06923b4c2d
8 -r--r--r--  1 jcprime  staff  1475  4 Oct 16:28 ee5b05fc20c66652a57106aa211e7fd08067cd

./.git/objects/eb:
total 64
 8 -r--r--r--  1 jcprime  staff  2152  4 Oct 16:27 3777316657b33c10d7b60b4c295c43a2f2f488
 8 -r--r--r--  1 jcprime  staff  1491  4 Oct 16:28 4017a18f6c0bebde1d9f1bea1589f84202d7d8
 8 -r--r--r--  1 jcprime  staff   128  4 Oct 16:27 b0fa9cddd55b343af0d38d61ba830c9a76ec88
 8 -r--r--r--  1 jcprime  staff  1595  4 Oct 16:28 b2943ae240ad0742918c61dbdcd05095526ddd
 8 -r--r--r--  1 jcprime  staff  1857  4 Oct 16:27 ba0851fbbcc511321f3048d67807e353ef3eff
 8 -r--r--r--  1 jcprime  staff  2209  4 Oct 16:27 ce127c8278e15491cda2c7386371cd7f8c506f
16 -r--r--r--  1 jcprime  staff  4342  4 Oct 16:28 ee59a3e68ab2ce394dbc67b3fc4010d60b04ad

./.git/objects/ec:
total 56
 8 -r--r--r--  1 jcprime  staff    832  4 Oct 16:28 2a824298d2ff1fc30e997074db6a5130253753
40 -r--r--r--  1 jcprime  staff  19180  4 Oct 16:28 8e388efd72e1253dfdb4f813fb2e3a67ad53ce
 8 -r--r--r--  1 jcprime  staff   3595  4 Oct 16:28 9a3e9aa0180d895546b59d42511d7bcf01572d

./.git/objects/ed:
total 120
 8 -r--r--r--  1 jcprime  staff    740  4 Oct 16:28 2c2ee09f135a3c2559cec6f6cc32a92b175d9c
16 -r--r--r--  1 jcprime  staff   6380  4 Oct 16:27 68fce92670a92a326c37a74a9ac4bfb160437f
16 -r--r--r--  1 jcprime  staff   8106  4 Oct 16:28 a877c45a248115cf1ccf6800231097165b649b
 8 -r--r--r--  1 jcprime  staff   2525  4 Oct 16:28 a936b6a977b3711fde1e88eaa509b7d42e1049
16 -r--r--r--  1 jcprime  staff   7397  4 Oct 16:28 b4f09e2678a17ca3d9e79b395bea17acb9daea
 8 -r--r--r--  1 jcprime  staff   1216  4 Oct 16:28 c8a36f3b42840266f5a2e94d679f1ef469fc8a
 8 -r--r--r--  1 jcprime  staff   1335  4 Oct 16:28 d68c2c5adfe78467e10c58d85dcc63f6067d22
32 -r--r--r--  1 jcprime  staff  13860  4 Oct 16:28 daafc16ea5a00ecddf6095d8053fdce757073d
 8 -r--r--r--  1 jcprime  staff     46  4 Oct 16:29 e7bd9cd88667fe89be25647cfe609b0b2888e0

./.git/objects/ee:
total 216
 16 -r--r--r--  1 jcprime  staff   5000  4 Oct 16:28 388dcbba4a3aa5258bb9231545713956002b03
  8 -r--r--r--  1 jcprime  staff    749  4 Oct 16:28 4087b1a9a0338e405de66c20b5a31868166322
  8 -r--r--r--  1 jcprime  staff   1572  4 Oct 16:29 45a85e234f89587acab4ebbd0a4fe0566efd87
128 -r--r--r--  1 jcprime  staff  65465  4 Oct 16:28 75233e18ab945efbd6efd45c44c43278849e4e
  8 -r--r--r--  1 jcprime  staff   1319  4 Oct 16:27 8ca125901b4b6c532468bb5bcbda391f7ae789
 48 -r--r--r--  1 jcprime  staff  22791  4 Oct 16:28 afb7f7ef2f0a18ea97c65d714f198292618c30

./.git/objects/ef:
total 416
248 -r--r--r--  1 jcprime  staff  124761  4 Oct 16:28 3bb83a0aae18fb5ff7170780978541c7ae91b6
  8 -r--r--r--  1 jcprime  staff    1824  4 Oct 16:27 5a39799589193768220dc16ab9ae8f88b961c3
  8 -r--r--r--  1 jcprime  staff     206  4 Oct 16:28 5f8d9a16bb0c39d592c9d4f6f5452a134796a1
  8 -r--r--r--  1 jcprime  staff    2530  4 Oct 16:28 912dd1451a701a435f867850f91ffd76c77076
 24 -r--r--r--  1 jcprime  staff    9303  4 Oct 16:28 91ddfa8b1319a5ee6ba6cc164d77b391b5e3e6
  8 -r--r--r--  1 jcprime  staff    1298  4 Oct 16:28 b296c1de97569157de563157bd4c2d447dec33
  8 -r--r--r--  1 jcprime  staff    1598  4 Oct 16:27 c19a372e350c8fa6d7881c0343dfe85865eb82
 40 -r--r--r--  1 jcprime  staff   19177  4 Oct 16:28 c493370ef22bb2fc2b5b71c66236cac093dd2d
  8 -r--r--r--  1 jcprime  staff     978  4 Oct 16:28 c60ac22a0fea59a87efe4876f7b7511978ffdb
  8 -r--r--r--  1 jcprime  staff    1566  4 Oct 16:28 e4049a8bece0bb7c1765101e52e10fa0ff7912
 24 -r--r--r--  1 jcprime  staff   10247  4 Oct 16:28 ed6401dde0195d8829eec507e1be72ad2a09d0
 16 -r--r--r--  1 jcprime  staff    6096  4 Oct 16:28 edf236cff0106bf7cdc588449e6bb472b0ba40
  8 -r--r--r--  1 jcprime  staff     642  4 Oct 16:28 fd4efd4c9c16886ce3220ceb355e54002f1874

./.git/objects/f0:
total 1144
1104 -r--r--r--  1 jcprime  staff  564449  4 Oct 16:28 0c3f83a81c801719ff5ce54fc8698d6d6cc903
   8 -r--r--r--  1 jcprime  staff    2363  4 Oct 16:27 6f398ad0bfd33d4532c4a2ff2a770aafd9a9f1
   8 -r--r--r--  1 jcprime  staff    2330  4 Oct 16:28 712858a75fe30788c470285f3d559330ea0702
   8 -r--r--r--  1 jcprime  staff     263  4 Oct 16:28 a6991a49d7af735eb660878afc67a22cb2d408
   8 -r--r--r--  1 jcprime  staff     331  4 Oct 16:28 a75e660bc1cae032125ff57e68fc0cf5664520
   8 -r--r--r--  1 jcprime  staff     508  4 Oct 16:28 d7197e01a79379808b8f87feda183891420f21

./.git/objects/f1:
total 1248
   8 -r--r--r--  1 jcprime  staff    2140  4 Oct 16:28 161b16ea8cdb46a3956c9a3814274b13f9e27c
1224 -r--r--r--  1 jcprime  staff  625742  4 Oct 16:28 a22fe380571211d6d58d356d2a4f5d8c1513bb
   8 -r--r--r--  1 jcprime  staff     190  4 Oct 16:29 c9d0be0ff397844a831a42065797ae98043fe8
   8 -r--r--r--  1 jcprime  staff     533  4 Oct 16:28 d18ae17bff159369e2cd935eb6fd6049410c0a

./.git/objects/f2:
total 32
8 -r--r--r--  1 jcprime  staff  1665  4 Oct 16:27 194033c615cf45764623bc7a5143eb040e6411
8 -r--r--r--  1 jcprime  staff   938  4 Oct 16:27 6acd2beb56dfccfedcc8bf61909a6fbbb0900c
8 -r--r--r--  1 jcprime  staff   218  4 Oct 16:29 e705dbd317e2f0c0a61a431523a622e01b0622
8 -r--r--r--  1 jcprime  staff    88  4 Oct 16:28 f6007068049e0664ed865b0028774fce324ce5

./.git/objects/f3:
total 64
 8 -r--r--r--  1 jcprime  staff   455  4 Oct 16:28 272d1f4673221a6d04828e9e1213a082b27a78
 8 -r--r--r--  1 jcprime  staff   365  4 Oct 16:28 3546ab31178795a08902f21501dfb0ae31b357
 8 -r--r--r--  1 jcprime  staff  2425  4 Oct 16:28 46412bbbcd5e894998a6f5f37d40b52329437d
 8 -r--r--r--  1 jcprime  staff   530  4 Oct 16:28 497c1a46fa063eb03b3e0115950c25c00ea7a9
 8 -r--r--r--  1 jcprime  staff    56  4 Oct 16:29 65c860b8696d1560eaf5cddea2ae16ef407b4d
16 -r--r--r--  1 jcprime  staff  6918  4 Oct 16:28 68893a7fd2f3f95a6edb08db7a19056bc4eff9
 8 -r--r--r--  1 jcprime  staff  3634  4 Oct 16:28 e2fe0a485a5d7dcc506ab9e79ac38b36cc72f6

./.git/objects/f4:
total 48
8 -r--r--r--  1 jcprime  staff  1489  4 Oct 16:28 114c72ffe98cb99abe99c8bfbd267786e15e7a
8 -r--r--r--  1 jcprime  staff  2266  4 Oct 16:27 172786b5a42f88ad2d7d4a9593dfa40e569500
8 -r--r--r--  1 jcprime  staff   997  4 Oct 16:28 1836ae87d7f6058f031ef4de711394d2a8c505
8 -r--r--r--  1 jcprime  staff   743  4 Oct 16:28 6bec0b42d6effaf951c86b8d354886fa1b8898
8 -r--r--r--  1 jcprime  staff  2716  4 Oct 16:27 7fa661edd17ec389a7bae9f66fa8208481dd4a
8 -r--r--r--  1 jcprime  staff   809  4 Oct 16:28 b22c4b750d138cb1427ba27021a4e35c1df494

./.git/objects/f5:
total 112
16 -r--r--r--  1 jcprime  staff   5886  4 Oct 16:28 48394668164673474de5f167ac8bf3e080a6e3
24 -r--r--r--  1 jcprime  staff  11172  4 Oct 16:28 587f29c3f50a3158697dd20c07b5cfccef5234
16 -r--r--r--  1 jcprime  staff   6222  4 Oct 16:28 90ea4e3bcba5c09c9f3caa1cdd95059c182321
 8 -r--r--r--  1 jcprime  staff    331  4 Oct 16:28 a9d78bc631077d7f5820f2f145321d569df024
 8 -r--r--r--  1 jcprime  staff   1665  4 Oct 16:28 e7967947f6ec0c44394b2def742152722e7097
24 -r--r--r--  1 jcprime  staff  10439  4 Oct 16:28 e8fa314ebeb713c436279a86abcb736118cd16
16 -r--r--r--  1 jcprime  staff   5880  4 Oct 16:28 fe56afc7a263faafb0362c7508d4809133e933

./.git/objects/f6:
total 56
 8 -r--r--r--  1 jcprime  staff  1200  4 Oct 16:28 163d863a82c8327f2e3615b875a87dc5c622e2
 8 -r--r--r--  1 jcprime  staff  2221  4 Oct 16:28 35662005d153b3c68a94c59dce8d28e10d0a34
16 -r--r--r--  1 jcprime  staff  4523  4 Oct 16:27 6a7e2ae613bb1711b866d986f13cb9c801455d
 8 -r--r--r--  1 jcprime  staff   400  4 Oct 16:28 90c0faea5687cb4831f041c9276196dc3433f4
 8 -r--r--r--  1 jcprime  staff   748  4 Oct 16:28 bf84f695910996fc592a1bdb06f216a0e8d94b
 8 -r--r--r--  1 jcprime  staff   347  4 Oct 16:28 cfdd655e945b231c62e9fbda4cccd53277467b

./.git/objects/f7:
total 80
24 -r--r--r--  1 jcprime  staff  9432  4 Oct 16:28 3476f7dd44dac64151c2e48ac44385d4ccbf51
 8 -r--r--r--  1 jcprime  staff  1395  4 Oct 16:27 3b395b9b63d71746502b1597445370bdb7e325
 8 -r--r--r--  1 jcprime  staff  2905  4 Oct 16:28 6140d925894a681643c28545c20043b41db5e6
 8 -r--r--r--  1 jcprime  staff  3958  4 Oct 16:28 76b3bb1f600f0f4d82b8d41a93358e58ae0b4b
 8 -r--r--r--  1 jcprime  staff  2313  4 Oct 16:28 81d7fbafed4c6add9808afb5775360f35387db
 8 -r--r--r--  1 jcprime  staff  2252  4 Oct 16:28 e31252d555cda1015fe0b36bc198d96d3198cc
16 -r--r--r--  1 jcprime  staff  4223  4 Oct 16:28 eefe8a5a410996527fa726a11ed0527f3c35a4

./.git/objects/f8:
total 32
 8 -r--r--r--  1 jcprime  staff   417  4 Oct 16:28 0a77b39eb21f4fe0bd383f5676e26d33224677
16 -r--r--r--  1 jcprime  staff  6350  4 Oct 16:28 1f11c839db7557d9692f970328cfe9201a6a91
 8 -r--r--r--  1 jcprime  staff  1894  4 Oct 16:28 2609a7e5ab1e2e5c8997e302e9c78bb22c6696

./.git/objects/f9:
total 112
80 -r--r--r--  1 jcprime  staff  39135  4 Oct 16:28 306163c0942345af98ae1b0cab4daffa3554b9
 8 -r--r--r--  1 jcprime  staff    744  4 Oct 16:28 6f4f87e3ac3bda5aba279a3a4a95b13aca4c47
16 -r--r--r--  1 jcprime  staff   5603  4 Oct 16:28 a8051caad817d3ca220faeeedc3af15c5080c1
 8 -r--r--r--  1 jcprime  staff   3914  4 Oct 16:28 fc2dcfefd47fa12fe589e8855df1008259d061

./.git/objects/fa:
total 104
 8 -r--r--r--  1 jcprime  staff  2939  4 Oct 16:28 05c330990531774df041cdf6555eb9db925c4b
 8 -r--r--r--  1 jcprime  staff  1206  4 Oct 16:28 1553ad2ce1612cd690b2b25b70b0c594338723
 8 -r--r--r--  1 jcprime  staff   190  4 Oct 16:28 1d4bd7ce97fbc55a27a1f94f62735f43b01030
 8 -r--r--r--  1 jcprime  staff   174  4 Oct 16:28 1fc0ee7ae8fb6f51eeda75b2c1e9600528f40e
 8 -r--r--r--  1 jcprime  staff  2700  4 Oct 16:28 27831d83c36b3cd307ef50964b8d2640d94e62
 8 -r--r--r--  1 jcprime  staff   194  4 Oct 16:28 5c073ed6815f9e64b6179eb08c4bdd1b3692d3
 8 -r--r--r--  1 jcprime  staff   179  4 Oct 16:28 724ed0d13597e99a29a1cb954fc4c0a82a1410
 8 -r--r--r--  1 jcprime  staff  1206  4 Oct 16:27 7d432a4f732807faf1bee033ff88473f741739
 8 -r--r--r--  1 jcprime  staff   606  4 Oct 16:27 973751d97e99390973fd4e409c4d448e4b470a
16 -r--r--r--  1 jcprime  staff  4542  4 Oct 16:27 9d5b141949e4fdb65ae153f3d03601bd9de650
 8 -r--r--r--  1 jcprime  staff  1263  4 Oct 16:27 cb7caf40b2ce2a4f31812aecc658e37ac24cbe
 8 -r--r--r--  1 jcprime  staff  3385  4 Oct 16:28 e12133ff7870e047ce5192146e5396ef29dfe5

./.git/objects/fb:
total 118608
118568 -r--r--r--  1 jcprime  staff  60706667  4 Oct 16:28 0f8a203cef967962f0f259c0ca3d8d86988a1c
     8 -r--r--r--  1 jcprime  staff        38  4 Oct 16:28 3f2a494a99388efee9670193454a91c2f3b2d5
     8 -r--r--r--  1 jcprime  staff      1253  4 Oct 16:27 668f11f7f39dd63f7e94dd19d3f748f5967379
     8 -r--r--r--  1 jcprime  staff      1448  4 Oct 16:27 ad7ea08d90180cb5e39ed109762d855ad5d651
    16 -r--r--r--  1 jcprime  staff      5101  4 Oct 16:29 cc7ff0e08c6b24609973fe25c6fcede4ca8336

./.git/objects/fc:
total 56
 8 -r--r--r--  1 jcprime  staff   1889  4 Oct 16:28 21c2984e62dc6f395886abb36d87dcf1086c19
24 -r--r--r--  1 jcprime  staff  10799  4 Oct 16:28 2e273a2a92d88e7b36fa70ca3480345001d922
16 -r--r--r--  1 jcprime  staff   6944  4 Oct 16:27 59704a7ae130edbe8be53a944fa75df81865c1
 8 -r--r--r--  1 jcprime  staff    352  4 Oct 16:29 5a37196337ae431899b2b799fa58f92bc546d5

./.git/objects/fd:
total 72
16 -r--r--r--  1 jcprime  staff   4177  4 Oct 16:28 47e4310281be8f99864e321f37118a817b5385
 8 -r--r--r--  1 jcprime  staff    255  4 Oct 16:28 6b70c3ad8f26007e8c52d44293409f1403df88
 8 -r--r--r--  1 jcprime  staff   1354  4 Oct 16:27 7f74bebf07ab3785cee99e3a9e71e6501de154
 8 -r--r--r--  1 jcprime  staff   1698  4 Oct 16:28 8fdf3085332211bc9ff8f192d04e5836c75af5
24 -r--r--r--  1 jcprime  staff  10247  4 Oct 16:28 c0494de416334bf21b13c68d54964a986ea3e2
 8 -r--r--r--  1 jcprime  staff   1297  4 Oct 16:28 f1185fb85d5505e89f3046bc1b361583f1152b

./.git/objects/fe:
total 40
8 -r--r--r--  1 jcprime  staff    92  4 Oct 16:29 166eb20b67668e71b83ce6558e294e0c7deb16
8 -r--r--r--  1 jcprime  staff  1075  4 Oct 16:28 35a68b1062d358caa778b79bbe086e6d5c7f39
8 -r--r--r--  1 jcprime  staff    90  4 Oct 16:29 78799efde36bbc0e2c6e3002b72226d6f44487
8 -r--r--r--  1 jcprime  staff  2069  4 Oct 16:27 cb58c0df987e679be5e23fa8f07bb29b739154
8 -r--r--r--  1 jcprime  staff  1543  4 Oct 16:28 f59f5b4a7a392862861c411395a3aee85c61a3

./.git/objects/ff:
total 24
8 -r--r--r--  1 jcprime  staff  1585  4 Oct 16:28 27ec9a5c7ea5715dedc095e78dd42c27132df6
8 -r--r--r--  1 jcprime  staff   826  4 Oct 16:28 69dc078749319d989defc27dd558cf0e7d519e
8 -r--r--r--  1 jcprime  staff  3573  4 Oct 16:28 859f25dba869c89beb63aa9f8c2059ab642afb

./.git/objects/info:

./.git/objects/pack:

./.git/refs:
total 0
0 drwxr-xr-x  3 jcprime  staff  102  4 Oct 16:29 heads
0 drwxr-xr-x  3 jcprime  staff  102  4 Oct 16:18 remotes
0 drwxr-xr-x  2 jcprime  staff   68  4 Oct 16:16 tags

./.git/refs/heads:
total 8
8 -rw-r--r--  1 jcprime  staff  41  4 Oct 16:29 master

./.git/refs/remotes:
total 0
0 drwxr-xr-x  3 jcprime  staff  102  4 Oct 16:24 origin

./.git/refs/remotes/origin:
total 8
8 -rw-r--r--  1 jcprime  staff  41  4 Oct 16:24 master

./.git/refs/tags:

./bin:
total 1096
 24 -rw-r--r--@  1 jcprime  staff   8196  4 Oct 16:26 .DS_Store
672 -rw-r--r--@  1 jcprime  staff      0  4 Oct 16:26 Icon
  0 drwxr-xr-x@ 17 jcprime  staff    578  4 Oct 16:27 ST3_Portable
 16 -rw-r--r--   1 jcprime  staff   4663  4 Oct 16:26 arch_config.sh
  8 -rw-------   1 jcprime  staff   1632  4 Oct 16:26 autoconfig.sh
 16 -rw-------   1 jcprime  staff   7267  4 Oct 16:26 bashsyntax.sh
  8 -rw-------   1 jcprime  staff   1080  4 Oct 16:26 chroot_setup.sh
  8 -rw-r--r--   1 jcprime  staff   3345  4 Oct 16:26 colour_schemes.css
  8 -rw-r--r--   1 jcprime  staff   3267  4 Oct 16:26 colour_schemes.xml
  8 -rw-r--r--   1 jcprime  staff    105  4 Oct 16:26 colours_hex.md
 16 -rw-r--r--   1 jcprime  staff   7429  4 Oct 16:26 coordinateTransform.py
  0 drwxr-xr-x@ 72 jcprime  staff   2448  4 Oct 16:26 dehydrate_update
  8 -rw-------   1 jcprime  staff    615  4 Oct 16:26 doi_grabber.sh
 16 -rw-------   1 jcprime  staff   4915  4 Oct 16:26 gulp_convert.sh
  8 -rw-------   1 jcprime  staff   2928  4 Oct 16:26 gulp_coords.py
  8 -rw-r--r--   1 jcprime  staff   2257  4 Oct 16:26 makesymlinks.sh
  0 -rw-r--r--   1 jcprime  staff      0  4 Oct 16:26 nat.tmp
 16 -rw-------   1 jcprime  staff   7729  4 Oct 16:26 nat_orig.cart
 16 -rw-------   1 jcprime  staff   8085  4 Oct 16:26 nat_orig.frac
 56 -rwxr-xr-x   1 jcprime  staff  27216  4 Oct 16:26 nat_orig.gin
 56 -rw-r--r--   1 jcprime  staff  26881  4 Oct 16:26 nat_orig.tmp
 16 -rw-------   1 jcprime  staff   7277  4 Oct 16:26 nat_orig.xyz
  8 -rw-------   1 jcprime  staff    570  4 Oct 16:26 packagecontrol_installation.py
  0 drwxr-xr-x@ 15 jcprime  staff    510  4 Oct 16:26 phd
 64 -rw-r--r--   1 jcprime  staff  30635  4 Oct 16:26 precond_tests.md
  8 -rwx--x--x   1 jcprime  staff   2327  4 Oct 16:27 test.sh
 16 -rw-------   1 jcprime  staff   5261  4 Oct 16:27 texmake.sh
  8 -rw-------   1 jcprime  staff    361  4 Oct 16:27 universal_gitconfig
  8 -rw-r--r--   1 jcprime  staff   1812  4 Oct 16:27 usefuls.sh
  0 drwxr-xr-x@ 40 jcprime  staff   1360  4 Oct 16:27 xenon

./bin/ST3_Portable:
total 30544
    0 drwxr-xr-x@   4 jcprime  staff      136  4 Oct 16:26 Data
  672 -rw-r--r--@   1 jcprime  staff        0  4 Oct 16:26 Icon
    0 drwxr-xr-x@  52 jcprime  staff     1768  4 Oct 16:26 Packages
   56 -rw-------    1 jcprime  staff    25425  4 Oct 16:26 changelog.txt
  368 -rw-------    1 jcprime  staff   187504  4 Oct 16:26 crash_reporter.exe
 1624 -rw-------    1 jcprime  staff   829264  4 Oct 16:26 msvcr100.dll
    8 -rw-------    1 jcprime  staff      570  4 Oct 16:26 packagecontrol.txt
 1336 -rw-------    1 jcprime  staff   681984  4 Oct 16:26 plugin_host.exe
    0 drwxr-xr-x@ 182 jcprime  staff     6188  4 Oct 16:27 python3.3
13760 -rw-------    1 jcprime  staff  7042560  4 Oct 16:27 python33.dll
  320 -rw-------    1 jcprime  staff   159856  4 Oct 16:27 subl.exe
   72 -rw-------    1 jcprime  staff    33170  4 Oct 16:27 sublime.py
   48 -rw-------    1 jcprime  staff    21747  4 Oct 16:27 sublime_plugin.py
12040 -rw-------    1 jcprime  staff  6162032  4 Oct 16:27 sublime_text.exe
  240 -rw-------    1 jcprime  staff   119408  4 Oct 16:27 update_installer.exe

./bin/ST3_Portable/Data:
total 672
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:26 Icon
  0 drwxr-xr-x@ 4 jcprime  staff  136  4 Oct 16:26 User

./bin/ST3_Portable/Data/User:
total 672
672 -rw-r--r--@  1 jcprime  staff    0  4 Oct 16:26 Icon
  0 drwxr-xr-x@ 29 jcprime  staff  986  4 Oct 16:26 sublime-windows

./bin/ST3_Portable/Data/User/sublime-windows:
total 1608
  8 -rw-------    1 jcprime  staff     157  4 Oct 16:26 .gitignore
  0 drwxr-xr-x@   9 jcprime  staff     306  4 Oct 16:26 2016-04-22_ARCHIVED_CustomSchemes
  0 drwxr-xr-x@ 135 jcprime  staff    4590  4 Oct 16:26 2016-05-25_ARCHIVED_ColourSchemes
  0 drwxr-xr-x@   5 jcprime  staff     170  4 Oct 16:26 Color Highlighter
 16 -rw-------    1 jcprime  staff    5249  4 Oct 16:26 ColorHighlighter.sublime-settings
  8 -rw-------    1 jcprime  staff     232  4 Oct 16:26 Default (OSX).sublime-keymap
 56 -rw-------    1 jcprime  staff   27607  4 Oct 16:26 Default.sublime-theme
672 -rw-r--r--@   1 jcprime  staff       0  4 Oct 16:26 Icon
  8 -rw-------    1 jcprime  staff     597  4 Oct 16:26 LaTeX.sublime-settings
 32 -rw-------    1 jcprime  staff   12291  4 Oct 16:26 LaTeXTools.sublime-settings
  8 -rw-------    1 jcprime  staff      81  4 Oct 16:26 LaTeXing.sublime-license
 40 -rw-------    1 jcprime  staff   18596  4 Oct 16:26 LaTeXing.sublime-settings
  8 -rw-------    1 jcprime  staff     552  4 Oct 16:26 Markdown.sublime-settings
  8 -rw-------    1 jcprime  staff     743  4 Oct 16:26 MultiMarkdown.sublime-settings
552 -rw-------    1 jcprime  staff  282424  4 Oct 16:26 Package Control.merged-ca-bundle
  8 -rw-------    1 jcprime  staff    2874  4 Oct 16:26 Package Control.sublime-settings
 16 -rw-------    1 jcprime  staff    4225  4 Oct 16:26 Preferences.sublime-settings
  8 -rw-------    1 jcprime  staff      29  4 Oct 16:26 README.md
  8 -rw-------    1 jcprime  staff     121  4 Oct 16:26 Shell-Unix-Generic.sublime-settings
  8 -rw-------    1 jcprime  staff    1557  4 Oct 16:26 Side Bar.sublime-settings
 72 -rw-------    1 jcprime  staff   33974  4 Oct 16:26 Soda Dark 3.sublime-theme
  0 drwxr-xr-x@ 287 jcprime  staff    9758  4 Oct 16:26 SublimeLinter
  8 -rw-------    1 jcprime  staff    3487  4 Oct 16:26 SublimeLinter.sublime-settings
  0 drwxr-xr-x@   8 jcprime  staff     272  4 Oct 16:26 ThemeThings
 48 -rw-------    1 jcprime  staff   21327  4 Oct 16:26 bh_core.sublime-settings
  8 -rw-------    1 jcprime  staff     279  4 Oct 16:26 colorcoder.sublime-settings
  8 -rw-------    1 jcprime  staff    2093  4 Oct 16:26 scope_hunter.sublime-settings

./bin/ST3_Portable/Data/User/sublime-windows/2016-04-22_ARCHIVED_CustomSchemes:
total 1656
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:26 Icon
352 -rw-------  1 jcprime  staff  179866  4 Oct 16:26 MonokaiCopy.tmTheme
 96 -rw-------  1 jcprime  staff   45175  4 Oct 16:26 MonokaiFluffed.tmTheme
 80 -rw-------  1 jcprime  staff   37841  4 Oct 16:26 MonokaiFluffedCopy.tmTheme
 56 -rw-------  1 jcprime  staff   27393  4 Oct 16:26 Writing Color Scheme - Dark.tmTheme
200 -rw-------  1 jcprime  staff  100186  4 Oct 16:26 blah.tmTheme
200 -rw-------  1 jcprime  staff  100186  4 Oct 16:26 blah.xml

./bin/ST3_Portable/Data/User/sublime-windows/2016-05-25_ARCHIVED_ColourSchemes:
total 6552
 24 -rw-------  1 jcprime  staff   11209  4 Oct 16:26 3024 night.tmTheme
 24 -rw-------  1 jcprime  staff    8258  4 Oct 16:26 AarenTheSubtle.tmTheme
 24 -rw-------  1 jcprime  staff   11513  4 Oct 16:26 Abdal Black Hackers 2.tmTheme
 24 -rw-------  1 jcprime  staff   11460  4 Oct 16:26 Abdal Black Hackers.tmTheme
 24 -rw-------  1 jcprime  staff   11224  4 Oct 16:26 Amiga (light).tmTheme
 24 -rw-------  1 jcprime  staff   11251  4 Oct 16:26 Amiga - Rebel palette (dark).tmTheme
 24 -rw-------  1 jcprime  staff   11399  4 Oct 16:26 Anarchist.tmTheme
 16 -rw-------  1 jcprime  staff    7862  4 Oct 16:26 Ansible.tmTheme
 24 -rw-------  1 jcprime  staff    8575  4 Oct 16:26 Antigua.tmTheme
 24 -rw-------  1 jcprime  staff   11215  4 Oct 16:26 Apple II.tmTheme
  8 -rw-------  1 jcprime  staff    3826  4 Oct 16:26 Ascetic Black.tmTheme
 24 -rw-------  1 jcprime  staff   11799  4 Oct 16:26 Asphalt.tmTheme
 48 -rw-------  1 jcprime  staff   22912  4 Oct 16:26 Aurora.tmTheme
 48 -rw-------  1 jcprime  staff   22965  4 Oct 16:26 Autumn.tmTheme
 32 -rw-------  1 jcprime  staff   15397  4 Oct 16:26 Axar.tmTheme
 32 -rw-------  1 jcprime  staff   15384  4 Oct 16:26 Azure.tmTheme
 24 -rw-------  1 jcprime  staff    9939  4 Oct 16:26 BBEdit.tmTheme
 24 -rw-------  1 jcprime  staff   12243  4 Oct 16:26 Base16 3024 Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12250  4 Oct 16:26 Base16 Apathy Dark.tmTheme
 32 -rw-------  1 jcprime  staff   12319  4 Oct 16:26 Base16 Atelier Forest Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12278  4 Oct 16:26 Base16 Atelier Forest Light.tmTheme
 32 -rw-------  1 jcprime  staff   12316  4 Oct 16:26 Base16 Atelier Heath Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12275  4 Oct 16:26 Base16 Atelier Heath Light.tmTheme
 32 -rw-------  1 jcprime  staff   12331  4 Oct 16:26 Base16 Atelier Lakeside Dark.tmTheme
 32 -rw-------  1 jcprime  staff   12290  4 Oct 16:26 Base16 Atelier Lakeside Light.tmTheme
 32 -rw-------  1 jcprime  staff   12328  4 Oct 16:26 Base16 Atelier Seaside Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12251  4 Oct 16:26 Base16 Brewer Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12209  4 Oct 16:26 Base16 Codeschool Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12168  4 Oct 16:26 Base16 Codeschool Light.tmTheme
 24 -rw-------  1 jcprime  staff   12224  4 Oct 16:26 Base16 Colors Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12237  4 Oct 16:26 Base16 Flat Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12196  4 Oct 16:26 Base16 Flat Light.tmTheme
 24 -rw-------  1 jcprime  staff   12238  4 Oct 16:26 Base16 Google Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12264  4 Oct 16:26 Base16 Grayscale Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12223  4 Oct 16:26 Base16 Grayscale Light.tmTheme
 24 -rw-------  1 jcprime  staff   12253  4 Oct 16:26 Base16 Green Screen Dark (1).tmTheme
 24 -rw-------  1 jcprime  staff   12253  4 Oct 16:26 Base16 Green Screen Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12205  4 Oct 16:26 Base16 Isotope Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12243  4 Oct 16:26 Base16 Monokai Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12202  4 Oct 16:26 Base16 Monokai Light.tmTheme
 24 -rw-------  1 jcprime  staff   12268  4 Oct 16:26 Base16 Solarized Dark.tmTheme
 24 -rw-------  1 jcprime  staff   12264  4 Oct 16:26 Base16 Summerfruit Dark.tmTheme
 32 -rw-------  1 jcprime  staff   15300  4 Oct 16:26 Bernat's FrontEnd Deligh.tmTheme
 24 -rw-------  1 jcprime  staff   11191  4 Oct 16:26 Black Pearl II.tmTheme
 24 -rw-------  1 jcprime  staff    9038  4 Oct 16:26 Black Pearl.tmTheme
 24 -rw-------  1 jcprime  staff    8207  4 Oct 16:26 Blackboard Black.tmTheme
 24 -rw-------  1 jcprime  staff    8900  4 Oct 16:26 Blue Thunder.tmTheme
 96 -rw-------  1 jcprime  staff   46462  4 Oct 16:26 Bluloco.tmTheme
 24 -rw-------  1 jcprime  staff   10256  4 Oct 16:26 Blusted.tmTheme
 40 -rw-------  1 jcprime  staff   17362  4 Oct 16:26 Brogrammer.tmTheme
  8 -rw-------  1 jcprime  staff    3779  4 Oct 16:26 Byworded Dark.tmTheme
 32 -rw-------  1 jcprime  staff   15398  4 Oct 16:26 Carbonight Contrast.tmTheme
 32 -rw-------  1 jcprime  staff   15380  4 Oct 16:26 Carbonight.tmTheme
 32 -rw-------  1 jcprime  staff   15378  4 Oct 16:26 Chocolate.tmTheme
 16 -rw-------  1 jcprime  staff    7033  4 Oct 16:26 Citrus-Power.tmTheme
  8 -rw-------  1 jcprime  staff    3045  4 Oct 16:26 Commodore 64.tmTheme
 96 -rw-------  1 jcprime  staff   45475  4 Oct 16:26 Creamy.tmTheme
 32 -rw-------  1 jcprime  staff   16136  4 Oct 16:26 Cube2Media.tmTheme
 40 -rw-------  1 jcprime  staff   19750  4 Oct 16:26 Dark Room - Contrast.tmTheme
 32 -rw-------  1 jcprime  staff   15394  4 Oct 16:26 Darkside Contrast.tmTheme
 24 -rw-------  1 jcprime  staff   11857  4 Oct 16:26 Devastate.tmTheme
 16 -rw-------  1 jcprime  staff    6685  4 Oct 16:26 Donatello.tmTheme
 24 -rw-------  1 jcprime  staff   11205  4 Oct 16:26 EGA.tmTheme
 32 -rw-------  1 jcprime  staff   15396  4 Oct 16:26 Earthsong Contrast.tmTheme
 24 -rw-------  1 jcprime  staff    9239  4 Oct 16:26 Espresso Libre.tmTheme
 24 -rw-------  1 jcprime  staff   10107  4 Oct 16:26 FireCode.tmTheme
136 -rw-------  1 jcprime  staff   66766  4 Oct 16:26 Fizzy.tmTheme
 24 -rw-------  1 jcprime  staff   10174  4 Oct 16:26 Fluidvision.tmTheme
  8 -rw-------  1 jcprime  staff    3538  4 Oct 16:26 Focus (dark).tmTheme
  8 -rw-------  1 jcprime  staff    3542  4 Oct 16:26 Focus (light).tmTheme
 32 -rw-------  1 jcprime  staff   15395  4 Oct 16:26 FreshCut Contrast.tmTheme
 16 -rw-------  1 jcprime  staff    7385  4 Oct 16:26 Frontend Light - Minty Night.tmTheme
 40 -rw-------  1 jcprime  staff   20404  4 Oct 16:26 Future Funk (Blue Codeception).tmTheme
 32 -rw-------  1 jcprime  staff   15394  4 Oct 16:26 Goldfish Contrast.tmTheme
 32 -rw-------  1 jcprime  staff   15376  4 Oct 16:26 Goldfish.tmTheme
 32 -rw-------  1 jcprime  staff   15394  4 Oct 16:26 Halflife Contrast.tmTheme
 32 -rw-------  1 jcprime  staff   15376  4 Oct 16:26 Halflife.tmTheme
 24 -rw-------  1 jcprime  staff   11203  4 Oct 16:26 Harper.tmTheme
 32 -rw-------  1 jcprime  staff   15300  4 Oct 16:26 Hero Dark.tmTheme
 32 -rw-------  1 jcprime  staff   15390  4 Oct 16:26 Hyrule Contrast.tmTheme
 24 -rw-------  1 jcprime  staff    8790  4 Oct 16:26 IceWorld - Dark.tmTheme
 24 -rw-------  1 jcprime  staff    8744  4 Oct 16:26 IceWorld - Night.tmTheme
 32 -rw-------  1 jcprime  staff   15392  4 Oct 16:26 Iceberg Contrast.tmTheme
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:26 Icon
 32 -rw-------  1 jcprime  staff   15666  4 Oct 16:26 Iridis Light.tmTheme
 16 -rw-------  1 jcprime  staff    8140  4 Oct 16:26 Kachun.tmTheme
 32 -rw-------  1 jcprime  staff   14601  4 Oct 16:26 Karel Fox - Dark.tmTheme
 48 -rw-------  1 jcprime  staff   22594  4 Oct 16:26 Kellys (Dark).tmTheme
 32 -rw-------  1 jcprime  staff   15382  4 Oct 16:26 Kiwi.tmTheme
 32 -rw-------  1 jcprime  staff   15487  4 Oct 16:26 Kwrite.tmTheme
 32 -rw-------  1 jcprime  staff   15392  4 Oct 16:26 Laravel Contrast.tmTheme
 32 -rw-------  1 jcprime  staff   15374  4 Oct 16:26 Laravel.tmTheme
 24 -rw-------  1 jcprime  staff   12149  4 Oct 16:26 Lyte-Monokai.tmTheme
 24 -rw-------  1 jcprime  staff   10439  4 Oct 16:26 Mac Classic.tmTheme
 40 -rw-------  1 jcprime  staff   17678  4 Oct 16:26 Mandarin Peacock.tmTheme
 32 -rw-------  1 jcprime  staff   15349  4 Oct 16:26 Material (1).tmTheme
 88 -rw-------  1 jcprime  staff   41508  4 Oct 16:26 Material Seti.tmTheme
 72 -rw-------  1 jcprime  staff   35608  4 Oct 16:26 Material Theme.tmTheme
 32 -rw-------  1 jcprime  staff   15348  4 Oct 16:26 Material.tmTheme
 24 -rw-------  1 jcprime  staff    8246  4 Oct 16:26 Milotic.tmTheme
 32 -rw-------  1 jcprime  staff   13019  4 Oct 16:26 Mirrr.tmTheme
632 -rw-------  1 jcprime  staff  322704  4 Oct 16:26 Monokai (Colorcoded) (SL) (Colorcoded) 2.tmTheme
240 -rw-------  1 jcprime  staff  122138  4 Oct 16:26 Monokai (Colorcoded) (SL) (Colorcoded).tmTheme
136 -rw-------  1 jcprime  staff   66027  4 Oct 16:26 Monokai (Colorcoded) (SL).tmTheme
128 -rw-------  1 jcprime  staff   65084  4 Oct 16:26 Monokai (Colorcoded).tmTheme
136 -rw-------  1 jcprime  staff   65821  4 Oct 16:26 Monokai (SL) (Colorcoded).tmTheme
 24 -rw-------  1 jcprime  staff    9718  4 Oct 16:26 Monokai (SL).tmTheme
 16 -rw-------  1 jcprime  staff    7806  4 Oct 16:26 Monokai Blueberry.tmTheme
200 -rw-------  1 jcprime  staff   98369  4 Oct 16:26 Monokai Extended (Colorcoded) (SL).tmTheme
192 -rw-------  1 jcprime  staff   97664  4 Oct 16:26 Monokai Extended (Colorcoded).tmTheme
 16 -rw-------  1 jcprime  staff    6897  4 Oct 16:26 Monokai SD.tmTheme
 24 -rw-------  1 jcprime  staff   10845  4 Oct 16:26 Monokai inverse.tmTheme
352 -rw-------  1 jcprime  staff  179866  4 Oct 16:26 MonokaiCopy.tmTheme
 80 -rw-------  1 jcprime  staff   40456  4 Oct 16:26 MonokaiFluffed.tmTheme
 32 -rw-------  1 jcprime  staff   15767  4 Oct 16:26 Moon.tmTheme
 16 -rw-------  1 jcprime  staff    6082  4 Oct 16:26 Morrowind.tmTheme
 32 -rw-------  1 jcprime  staff   15384  4 Oct 16:26 Mud Contrast.tmTheme
 56 -rw-------  1 jcprime  staff   27974  4 Oct 16:26 MultiLanger-Dull_Blue.tmTheme
 32 -rw-------  1 jcprime  staff   13201  4 Oct 16:26 Oblivion SodaDark.tmTheme
 32 -rw-------  1 jcprime  staff   13531  4 Oct 16:26 Oblivion.tmTheme
 16 -rw-------  1 jcprime  staff    6556  4 Oct 16:26 Ofer 1.tmTheme
 64 -rw-------  1 jcprime  staff   31141  4 Oct 16:26 Orchid Theme.tmTheme
 24 -rw-------  1 jcprime  staff   11237  4 Oct 16:26 Subway- Barcelona (dark).tmTheme
 24 -rw-------  1 jcprime  staff   11231  4 Oct 16:26 Subway- Berlin (dark).tmTheme
 24 -rw-------  1 jcprime  staff   11220  4 Oct 16:26 Subway- Berlin.tmTheme
 24 -rw-------  1 jcprime  staff   11239  4 Oct 16:26 Subway- Copenhagen (dark).tmTheme
 24 -rw-------  1 jcprime  staff   11231  4 Oct 16:26 Subway- London (dark).tmTheme
 24 -rw-------  1 jcprime  staff   11231  4 Oct 16:26 Subway- Moscow (dark).tmTheme
 24 -rw-------  1 jcprime  staff    8833  4 Oct 16:26 WaterGear.tmTheme
 56 -rw-------  1 jcprime  staff   27391  4 Oct 16:26 Writing Color Scheme - Dark.tmTheme
 32 -rw-------  1 jcprime  staff   13720  4 Oct 16:26 ant.tmTheme
 96 -rw-------  1 jcprime  staff   48086  4 Oct 16:26 ggDark.tmTheme
 40 -rw-------  1 jcprime  staff   16605  4 Oct 16:26 gruvbox.tmTheme

./bin/ST3_Portable/Data/User/sublime-windows/Color Highlighter:
total 672
  0 drwxr-xr-x@ 4 jcprime  staff  136  4 Oct 16:26 ColorPicker
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:26 Icon
  0 drwxr-xr-x@ 8 jcprime  staff  272  4 Oct 16:26 themes

./bin/ST3_Portable/Data/User/sublime-windows/Color Highlighter/ColorPicker:
total 720
 48 -rw-------  1 jcprime  staff  23332  4 Oct 16:26 ColorPicker_osx_x64
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon

./bin/ST3_Portable/Data/User/sublime-windows/Color Highlighter/themes:
total 2272
 672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:26 Icon
1288 -rw-------  1 jcprime  staff  656031  4 Oct 16:26 Monokai (Colorcoded) (SL) (Colorcoded).tmTheme
 136 -rw-------  1 jcprime  staff   69289  4 Oct 16:26 Monokai (SL) (Colorcoded).tmTheme
  24 -rw-------  1 jcprime  staff   10296  4 Oct 16:26 Monokai (SL).tmTheme
  24 -rw-------  1 jcprime  staff    9362  4 Oct 16:26 Monokai.tmTheme
 128 -rw-------  1 jcprime  staff   63543  4 Oct 16:26 MonokaiFluffed.tmTheme

./bin/ST3_Portable/Data/User/sublime-windows/SublimeLinter:
total 10072
 24 -rw-------  1 jcprime  staff  11794  4 Oct 16:26 1337 (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11957  4 Oct 16:26 3024_Day (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11958  4 Oct 16:26 3024_Night (SL).tmTheme
 64 -rw-------  1 jcprime  staff  30231  4 Oct 16:26 8-Colour-Dark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7158  4 Oct 16:26 Abyss (SL).tmTheme
 48 -rw-------  1 jcprime  staff  24547  4 Oct 16:26 Acai (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9719  4 Oct 16:26 Active4D (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7393  4 Oct 16:26 AgolaDark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6941  4 Oct 16:26 All_Hallows_Eve (SL).tmTheme
 16 -rw-------  1 jcprime  staff   4691  4 Oct 16:26 Amber (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13291  4 Oct 16:26 Amy (SL).tmTheme
 32 -rw-------  1 jcprime  staff  14385  4 Oct 16:26 Anarchist (SL).tmTheme
 24 -rw-------  1 jcprime  staff  12115  4 Oct 16:26 AnarchistEighties (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9347  4 Oct 16:26 Array (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10177  4 Oct 16:26 ArtSchool (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7603  4 Oct 16:26 Aurora (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9381  4 Oct 16:26 Batman (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9381  4 Oct 16:26 BatmanLight (SL).tmTheme
 40 -rw-------  1 jcprime  staff  19398  4 Oct 16:26 Better_JS (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11294  4 Oct 16:26 Birds_of_Paradise (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9964  4 Oct 16:26 Bittersweet (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8763  4 Oct 16:26 Blackboard (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5847  4 Oct 16:26 Bliss (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9346  4 Oct 16:26 BlueDawn (SL).tmTheme
 24 -rw-------  1 jcprime  staff  12007  4 Oct 16:26 BlueLover (SL).tmTheme
 16 -rw-------  1 jcprime  staff   4695  4 Oct 16:26 Blueprint (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6073  4 Oct 16:26 Boron (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 BoxUK (SL).tmTheme
136 -rw-------  1 jcprime  staff  69140  4 Oct 16:26 Brilliance_Black (SL).tmTheme
120 -rw-------  1 jcprime  staff  58609  4 Oct 16:26 Brilliance_Dull (SL).tmTheme
 48 -rw-------  1 jcprime  staff  23843  4 Oct 16:26 Brogrammer (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7158  4 Oct 16:26 Brunette (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7583  4 Oct 16:26 Bungler (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7378  4 Oct 16:26 Cake (SL).tmTheme
 40 -rw-------  1 jcprime  staff  17016  4 Oct 16:26 CandyLand (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5983  4 Oct 16:26 Capo-Dark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5984  4 Oct 16:26 Capo-Light (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21984  4 Oct 16:26 Carbonight (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9841  4 Oct 16:26 CarbonightBlue (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9332  4 Oct 16:26 Carbonnight (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9568  4 Oct 16:26 Chameleon (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9473  4 Oct 16:26 Chelevra (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13395  4 Oct 16:26 Choco (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6798  4 Oct 16:26 Chrome_DevTools (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8537  4 Oct 16:26 Clouds (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8848  4 Oct 16:26 Clouds_Midnight (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13493  4 Oct 16:26 Cobalt (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7296  4 Oct 16:26 Code-Lover (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9966  4 Oct 16:26 CodeWarrior (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10093  4 Oct 16:26 Coffee_Dark_Roast (SL).tmTheme
  8 -rw-------  1 jcprime  staff   4022  4 Oct 16:26 Colonoscopy (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9384  4 Oct 16:26 Daguiheso (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9429  4 Oct 16:26 Daily-Grind (SL).tmTheme
 32 -rw-------  1 jcprime  staff  12824  4 Oct 16:26 Dark-Dracula (SL).tmTheme
 64 -rw-------  1 jcprime  staff  32584  4 Oct 16:26 DarkCF (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13365  4 Oct 16:26 DarkMosquito (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22949  4 Oct 16:26 DarkRoomContrast (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22947  4 Oct 16:26 DarkRoomNormal (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10984  4 Oct 16:26 Dark_Krystal (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 Darkside (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10597  4 Oct 16:26 Dawn (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7551  4 Oct 16:26 DeerAntlerDark (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9949  4 Oct 16:26 Dracula (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13718  4 Oct 16:26 Dreamweaver (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7158  4 Oct 16:26 Druid (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21983  4 Oct 16:26 Earthsong (SL).tmTheme
 32 -rw-------  1 jcprime  staff  14776  4 Oct 16:26 Earthworm (SL).tmTheme
 40 -rw-------  1 jcprime  staff  19645  4 Oct 16:26 Easyballs (SL).tmTheme
 40 -rw-------  1 jcprime  staff  18699  4 Oct 16:26 EggplantParm (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10630  4 Oct 16:26 Eiffel (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9838  4 Oct 16:26 Espresso_Libre (SL).tmTheme
 48 -rw-------  1 jcprime  staff  20663  4 Oct 16:26 Facebook (SL).tmTheme
 32 -rw-------  1 jcprime  staff  14706  4 Oct 16:26 FireCode (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7291  4 Oct 16:26 Flat (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7418  4 Oct 16:26 Flat_Dark (SL).tmTheme
 40 -rw-------  1 jcprime  staff  19726  4 Oct 16:26 Flatgrammer (SL).tmTheme
 48 -rw-------  1 jcprime  staff  23828  4 Oct 16:26 Flatland_Black (SL).tmTheme
 48 -rw-------  1 jcprime  staff  20986  4 Oct 16:26 Flatland_Dark (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10551  4 Oct 16:26 Flatland_Monokai (SL).tmTheme
 16 -rw-------  1 jcprime  staff   4244  4 Oct 16:26 Focus_Dark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   4247  4 Oct 16:26 Focus_Light (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13703  4 Oct 16:26 Frank_Sinatra (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 FreshCut (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 Frontier (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9323  4 Oct 16:26 Frozen (SL).tmTheme
 56 -rw-------  1 jcprime  staff  25708  4 Oct 16:26 Future_Funk (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13205  4 Oct 16:26 Gedit (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10854  4 Oct 16:26 GhettoBerryBlast (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9201  4 Oct 16:26 GitHubCleanWhite (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22008  4 Oct 16:26 Github (SL).tmTheme
120 -rw-------  1 jcprime  staff  59770  4 Oct 16:26 Github-v2 (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 Gloom (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 Glowfish (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 Goldfish (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7451  4 Oct 16:26 Gooey (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7465  4 Oct 16:26 GooeyPastel (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5932  4 Oct 16:26 GrandsonOfObsidian (SL).tmTheme
 16 -rw-------  1 jcprime  staff   4691  4 Oct 16:26 Green (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 Grunge (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7357  4 Oct 16:26 Hacker (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 HalfLife (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6067  4 Oct 16:26 Hammer (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11958  4 Oct 16:26 Harper (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 Heroku (SL).tmTheme
 32 -rw-------  1 jcprime  staff  12964  4 Oct 16:26 Hopscotch (SL).tmTheme
 16 -rw-------  1 jcprime  staff   8077  4 Oct 16:26 HotDogStand (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5694  4 Oct 16:26 Humane (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21817  4 Oct 16:26 Humble (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 Hyrule (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6029  4 Oct 16:26 IDLE (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9003  4 Oct 16:26 ILM (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13364  4 Oct 16:26 ILMHackers (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9026  4 Oct 16:26 ILMLight (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6962  4 Oct 16:26 ILMShell (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9105  4 Oct 16:26 ILMStream (SL).tmTheme
 40 -rw-------  1 jcprime  staff  19240  4 Oct 16:26 IR_Black (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21981  4 Oct 16:26 Iceberg (SL).tmTheme
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
 24 -rw-------  1 jcprime  staff   8525  4 Oct 16:26 Into-the-Dark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7462  4 Oct 16:26 IridiumStar (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6661  4 Oct 16:26 Jellybeams (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 Juicy (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13355  4 Oct 16:26 Kabbotta (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6666  4 Oct 16:26 Kane (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11965  4 Oct 16:26 Kimbie_dark (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11967  4 Oct 16:26 Kimbie_light (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6189  4 Oct 16:26 KyzDark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7384  4 Oct 16:26 LAZY (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21981  4 Oct 16:26 Laravel (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9143  4 Oct 16:26 LastNight (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11966  4 Oct 16:26 LastRoom_Dark (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9104  4 Oct 16:26 Lava (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 Lavender (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9448  4 Oct 16:26 LegacyPack (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7291  4 Oct 16:26 LightBulb (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9255  4 Oct 16:26 MagicWB_Amiga (SL).tmTheme
 72 -rw-------  1 jcprime  staff  33288  4 Oct 16:26 MarkdownEditor-Dark (SL).tmTheme
160 -rw-------  1 jcprime  staff  78082  4 Oct 16:26 MarkdownEditor-Focus (Colorcoded) (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13502  4 Oct 16:26 MarsPeacock (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13457  4 Oct 16:26 Mcedit (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 Mellow (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7438  4 Oct 16:26 Merbivore (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7980  4 Oct 16:26 Merbivore_Soft (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7325  4 Oct 16:26 Meteor (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10738  4 Oct 16:26 Metropolis (SL).tmTheme
136 -rw-------  1 jcprime  staff  66027  4 Oct 16:26 Monokai (Colorcoded) (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10513  4 Oct 16:26 Monokai (SL).tmTheme
200 -rw-------  1 jcprime  staff  98369  4 Oct 16:26 Monokai Extended (Colorcoded) (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7478  4 Oct 16:26 Monokai-Aurora (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7348  4 Oct 16:26 Monokai-Cobalt (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9352  4 Oct 16:26 Monokai-Contrast (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9704  4 Oct 16:26 Monokai-Dev (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10336  4 Oct 16:26 Monokai-HighFructose (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9338  4 Oct 16:26 Monokai-Midnight (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7594  4 Oct 16:26 Monokai-Soft (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8609  4 Oct 16:26 Monokai-Soft-MD (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10782  4 Oct 16:26 Monokai-Vivid (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7466  4 Oct 16:26 MonokaiEasyForRetina (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10731  4 Oct 16:26 Mustard (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7505  4 Oct 16:26 Muukii (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9397  4 Oct 16:26 NaturalContrast (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11620  4 Oct 16:26 NetBeans (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7567  4 Oct 16:26 Nette (SL).tmTheme
 40 -rw-------  1 jcprime  staff  17408  4 Oct 16:26 New-Day (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7509  4 Oct 16:26 Nouba (SL).tmTheme
 40 -rw-------  1 jcprime  staff  19665  4 Oct 16:26 Oblivion (SL).tmTheme
 24 -rw-------  1 jcprime  staff  12164  4 Oct 16:26 Ocean-Dark-Soda (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8833  4 Oct 16:26 Oceanic_Next (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11969  4 Oct 16:26 Paraiso_dark (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11971  4 Oct 16:26 Paraiso_light (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 Pastel (SL).tmTheme
 40 -rw-------  1 jcprime  staff  17155  4 Oct 16:26 Pastels_on_Dark (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21981  4 Oct 16:26 Patriot (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21981  4 Oct 16:26 Peacock (SL).tmTheme
 16 -rw-------  1 jcprime  staff   8041  4 Oct 16:26 Poppins (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21983  4 Oct 16:26 Potpourri (SL).tmTheme
 32 -rw-------  1 jcprime  staff  16124  4 Oct 16:26 Poyeyo (SL).tmTheme
 32 -rw-------  1 jcprime  staff  16136  4 Oct 16:26 Poyeyo_Blue (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7093  4 Oct 16:26 PurpleHaze (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7848  4 Oct 16:26 Purrora (SL).tmTheme
 32 -rw-------  1 jcprime  staff  14202  4 Oct 16:26 QuietLight (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7832  4 Oct 16:26 Rebecca (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7849  4 Oct 16:26 Rebecca-dark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7840  4 Oct 16:26 Rebecca-mint (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7839  4 Oct 16:26 Rebecca-sky (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7474  4 Oct 16:26 Resesif (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21984  4 Oct 16:26 Revelation (SL).tmTheme
 56 -rw-------  1 jcprime  staff  25982  4 Oct 16:26 Riot (SL).tmTheme
 32 -rw-------  1 jcprime  staff  12452  4 Oct 16:26 SalmonOnIce (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7178  4 Oct 16:26 Seafoam-Pastel-Dark (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7181  4 Oct 16:26 Seafoam-Pastel-Light (SL).tmTheme
112 -rw-------  1 jcprime  staff  56232  4 Oct 16:26 Seahorse (SL).tmTheme
 32 -rw-------  1 jcprime  staff  14577  4 Oct 16:26 Shadows (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 Slime (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8330  4 Oct 16:26 Slush_and_Poppies (SL).tmTheme
 40 -rw-------  1 jcprime  staff  17791  4 Oct 16:26 Smyck (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 Snappy (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11410  4 Oct 16:26 SolarSooty (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21984  4 Oct 16:26 Solarflare (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7813  4 Oct 16:26 Solarized-dark (SL).tmTheme
 48 -rw-------  1 jcprime  staff  23292  4 Oct 16:26 Solarized-dark-Nitrous.io (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7695  4 Oct 16:26 Solarized-light (SL).tmTheme
 24 -rw-------  1 jcprime  staff  12142  4 Oct 16:26 Solarized-ocean (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 Sourlick (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5499  4 Oct 16:26 SpaceCadet (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11855  4 Oct 16:26 SpaceRave (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21983  4 Oct 16:26 Spearmint (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9157  4 Oct 16:26 Spring (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 Stark (SL).tmTheme
 32 -rw-------  1 jcprime  staff  16099  4 Oct 16:26 Sunburst (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9420  4 Oct 16:26 Sweet_Dreams (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10985  4 Oct 16:26 Symfony (SL).tmTheme
 32 -rw-------  1 jcprime  staff  14935  4 Oct 16:26 ThemebyDarkV1 (SL).tmTheme
 32 -rw-------  1 jcprime  staff  14856  4 Oct 16:26 ThemebyDarkV2 (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7244  4 Oct 16:26 Tireless (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5669  4 Oct 16:26 Tomorrow (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5696  4 Oct 16:26 Tomorrow_Night (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5731  4 Oct 16:26 Tomorrow_Night_Blue (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5705  4 Oct 16:26 Tomorrow_Night_Bright (SL).tmTheme
 16 -rw-------  1 jcprime  staff   5672  4 Oct 16:26 Tomorrow_Night_Eighties (SL).tmTheme
 40 -rw-------  1 jcprime  staff  17017  4 Oct 16:26 ToyMachine (SL).tmTheme
  8 -rw-------  1 jcprime  staff   3499  4 Oct 16:26 Tricolor (SL).tmTheme
 16 -rw-------  1 jcprime  staff   4820  4 Oct 16:26 TricolorLight (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7511  4 Oct 16:26 Trixie (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21978  4 Oct 16:26 Tron (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9347  4 Oct 16:26 TronLegacy (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9334  4 Oct 16:26 TronLight (SL).tmTheme
 32 -rw-------  1 jcprime  staff  13171  4 Oct 16:26 Twilight (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7457  4 Oct 16:26 TwoStones (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21983  4 Oct 16:26 Userscape (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11144  4 Oct 16:26 Vibrant_Ink (SL).tmTheme
 24 -rw-------  1 jcprime  staff  12134  4 Oct 16:26 WarpOS (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9893  4 Oct 16:26 Welded (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9863  4 Oct 16:26 Whiteboard (SL).tmTheme
 16 -rw-------  1 jcprime  staff   6157  4 Oct 16:26 Xcode_default (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21978  4 Oct 16:26 Yule (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8478  4 Oct 16:26 Zenburnesque (SL).tmTheme
 40 -rw-------  1 jcprime  staff  19778  4 Oct 16:26 ant (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22011  4 Oct 16:26 arstotzka (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22007  4 Oct 16:26 azure (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7529  4 Oct 16:26 bashling (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21978  4 Oct 16:26 bold (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21983  4 Oct 16:26 chocolate (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22007  4 Oct 16:26 crisp (SL).tmTheme
120 -rw-------  1 jcprime  staff  57567  4 Oct 16:26 deep_blue_see (SL).tmTheme
 40 -rw-------  1 jcprime  staff  16908  4 Oct 16:26 dimmed (SL).tmTheme
 40 -rw-------  1 jcprime  staff  16904  4 Oct 16:26 dimmed-monokai (SL).tmTheme
 40 -rw-------  1 jcprime  staff  16512  4 Oct 16:26 dimmed-monokai-no-contrast (SL).tmTheme
 40 -rw-------  1 jcprime  staff  16615  4 Oct 16:26 dimmed-night (SL).tmTheme
 32 -rw-------  1 jcprime  staff  16369  4 Oct 16:26 dimmed-storm (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7292  4 Oct 16:26 dropin (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21989  4 Oct 16:26 earthsong-light (SL).tmTheme
 24 -rw-------  1 jcprime  staff   8534  4 Oct 16:26 feel_good (SL).tmTheme
 16 -rw-------  1 jcprime  staff   7101  4 Oct 16:26 iPlastic (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9392  4 Oct 16:26 idleFingers (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21978  4 Oct 16:26 keen (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22006  4 Oct 16:26 kiwi (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22008  4 Oct 16:26 legacy (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21982  4 Oct 16:26 mintchoc (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10480  4 Oct 16:26 monokai-best (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10493  4 Oct 16:26 monokai-inverse (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10589  4 Oct 16:26 monokai_fresh (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21977  4 Oct 16:26 mud (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 otakon (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21991  4 Oct 16:26 peacocks-in-space (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22006  4 Oct 16:26 peel (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22007  4 Oct 16:26 piggy (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22235  4 Oct 16:26 preap (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22009  4 Oct 16:26 rainbow (SL).tmTheme
 24 -rw-------  1 jcprime  staff  10977  4 Oct 16:26 red (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9472  4 Oct 16:26 santojon (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9532  4 Oct 16:26 santojon-dark (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22007  4 Oct 16:26 shrek (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 slate (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21986  4 Oct 16:26 snappy-light (SL).tmTheme
 32 -rw-------  1 jcprime  staff  16365  4 Oct 16:26 spectral (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9378  4 Oct 16:26 sublime_xoria (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 super (SL).tmTheme
 24 -rw-------  1 jcprime  staff  11040  4 Oct 16:26 textmate (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 tonic (SL).tmTheme
 48 -rw-------  1 jcprime  staff  22008  4 Oct 16:26 tribal (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21980  4 Oct 16:26 turnip (SL).tmTheme
 24 -rw-------  1 jcprime  staff   9954  4 Oct 16:26 wild-cherry (SL).tmTheme
 48 -rw-------  1 jcprime  staff  21979  4 Oct 16:26 zacks (SL).tmTheme
 40 -rw-------  1 jcprime  staff  17171  4 Oct 16:26 zenburn (SL).tmTheme

./bin/ST3_Portable/Data/User/sublime-windows/ThemeThings:
total 808
 56 -rw-------   1 jcprime  staff  28124  4 Oct 16:26 Default.sublime-theme
 64 -rw-------   1 jcprime  staff  31986  4 Oct 16:26 Default_2016-05-01.json
672 -rw-r--r--@  1 jcprime  staff      0  4 Oct 16:26 Icon
  0 drwxr-xr-x@ 16 jcprime  staff    544  4 Oct 16:26 SyntaxHighlighting
  8 -rw-------   1 jcprime  staff    225  4 Oct 16:26 Widget.sublime-settings
  8 -rw-------   1 jcprime  staff   2516  4 Oct 16:26 Widgetings.stTheme

./bin/ST3_Portable/Data/User/sublime-windows/ThemeThings/SyntaxHighlighting:
total 2256
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:26 Icon
240 -rw-------  1 jcprime  staff  122138  4 Oct 16:26 Monokai (Colorcoded) (SL) (Colorcoded).tmTheme
136 -rw-------  1 jcprime  staff   66027  4 Oct 16:26 Monokai (Colorcoded) (SL).tmTheme
128 -rw-------  1 jcprime  staff   65084  4 Oct 16:26 Monokai (Colorcoded).tmTheme
136 -rw-------  1 jcprime  staff   65821  4 Oct 16:26 Monokai (SL) (Colorcoded).tmTheme
 24 -rw-------  1 jcprime  staff    9718  4 Oct 16:26 Monokai (SL).tmTheme
 16 -rw-------  1 jcprime  staff    7806  4 Oct 16:26 Monokai Blueberry.tmTheme
200 -rw-------  1 jcprime  staff   98369  4 Oct 16:26 Monokai Extended (Colorcoded) (SL).tmTheme
192 -rw-------  1 jcprime  staff   97664  4 Oct 16:26 Monokai Extended (Colorcoded).tmTheme
 16 -rw-------  1 jcprime  staff    6897  4 Oct 16:26 Monokai SD.tmTheme
 24 -rw-------  1 jcprime  staff   10845  4 Oct 16:26 Monokai inverse.tmTheme
352 -rw-------  1 jcprime  staff  179866  4 Oct 16:26 MonokaiCopy.tmTheme
 96 -rw-------  1 jcprime  staff   46186  4 Oct 16:26 MonokaiFluffed.tmTheme
 24 -rw-------  1 jcprime  staff    9761  4 Oct 16:26 MonokaiOriginal.tmTheme

./bin/ST3_Portable/Packages:
total 9720
  40 -rw-------  1 jcprime  staff    18605  4 Oct 16:26 ASP.sublime-package
  40 -rw-------  1 jcprime  staff    16403  4 Oct 16:26 ActionScript.sublime-package
  72 -rw-------  1 jcprime  staff    33257  4 Oct 16:26 AppleScript.sublime-package
  40 -rw-------  1 jcprime  staff    18417  4 Oct 16:26 Batch File.sublime-package
  56 -rw-------  1 jcprime  staff    25658  4 Oct 16:26 C#.sublime-package
 336 -rw-------  1 jcprime  staff   170821  4 Oct 16:26 C++.sublime-package
 240 -rw-------  1 jcprime  staff   122077  4 Oct 16:26 CSS.sublime-package
 104 -rw-------  1 jcprime  staff    49272  4 Oct 16:26 Clojure.sublime-package
 544 -rw-------  1 jcprime  staff   275925  4 Oct 16:26 Color Scheme - Default.sublime-package
  88 -rw-------  1 jcprime  staff    42509  4 Oct 16:26 D.sublime-package
 720 -rw-------  1 jcprime  staff   368438  4 Oct 16:26 Default.sublime-package
  24 -rw-------  1 jcprime  staff     8299  4 Oct 16:26 Diff.sublime-package
  80 -rw-------  1 jcprime  staff    38504  4 Oct 16:26 Erlang.sublime-package
  72 -rw-------  1 jcprime  staff    36173  4 Oct 16:26 Go.sublime-package
   8 -rw-------  1 jcprime  staff     3157  4 Oct 16:26 Graphviz.sublime-package
 128 -rw-------  1 jcprime  staff    63395  4 Oct 16:26 Groovy.sublime-package
  96 -rw-------  1 jcprime  staff    49039  4 Oct 16:26 HTML.sublime-package
  32 -rw-------  1 jcprime  staff    13826  4 Oct 16:26 Haskell.sublime-package
 672 -rw-r--r--@ 1 jcprime  staff        0  4 Oct 16:26 Icon
  96 -rw-------  1 jcprime  staff    45708  4 Oct 16:26 Java.sublime-package
 160 -rw-------  1 jcprime  staff    80091  4 Oct 16:26 JavaScript.sublime-package
 104 -rw-------  1 jcprime  staff    50830  4 Oct 16:26 LaTeX.sublime-package
2456 -rw-------  1 jcprime  staff  1257072  4 Oct 16:26 Language - English.sublime-package
  16 -rw-------  1 jcprime  staff     5888  4 Oct 16:26 Lisp.sublime-package
  24 -rw-------  1 jcprime  staff     8718  4 Oct 16:26 Lua.sublime-package
  24 -rw-------  1 jcprime  staff    10365  4 Oct 16:26 Makefile.sublime-package
  48 -rw-------  1 jcprime  staff    22590  4 Oct 16:26 Markdown.sublime-package
 168 -rw-------  1 jcprime  staff    83117  4 Oct 16:26 Matlab.sublime-package
 112 -rw-------  1 jcprime  staff    55256  4 Oct 16:26 OCaml.sublime-package
 392 -rw-------  1 jcprime  staff   199770  4 Oct 16:26 Objective-C.sublime-package
1000 -rw-------  1 jcprime  staff   510646  4 Oct 16:26 PHP.sublime-package
  16 -rw-------  1 jcprime  staff     4537  4 Oct 16:26 Pascal.sublime-package
  96 -rw-------  1 jcprime  staff    47270  4 Oct 16:26 Perl.sublime-package
 144 -rw-------  1 jcprime  staff    70116  4 Oct 16:26 Python.sublime-package
  32 -rw-------  1 jcprime  staff    13283  4 Oct 16:26 R.sublime-package
 192 -rw-------  1 jcprime  staff    97054  4 Oct 16:26 Rails.sublime-package
  32 -rw-------  1 jcprime  staff    12348  4 Oct 16:26 Regular Expressions.sublime-package
  24 -rw-------  1 jcprime  staff     8679  4 Oct 16:26 RestructuredText.sublime-package
 200 -rw-------  1 jcprime  staff   101080  4 Oct 16:26 Ruby.sublime-package
 144 -rw-------  1 jcprime  staff    72704  4 Oct 16:26 Rust.sublime-package
  24 -rw-------  1 jcprime  staff    10436  4 Oct 16:26 SQL.sublime-package
  40 -rw-------  1 jcprime  staff    19952  4 Oct 16:26 Scala.sublime-package
  56 -rw-------  1 jcprime  staff    25053  4 Oct 16:26 ShellScript.sublime-package
  48 -rw-------  1 jcprime  staff    20824  4 Oct 16:26 TCL.sublime-package
   8 -rw-------  1 jcprime  staff     1157  4 Oct 16:26 Text.sublime-package
  24 -rw-------  1 jcprime  staff    10850  4 Oct 16:26 Textile.sublime-package
 304 -rw-------  1 jcprime  staff   153879  4 Oct 16:26 Theme - Default.sublime-package
 216 -rw-------  1 jcprime  staff   109410  4 Oct 16:26 Vintage.sublime-package
  40 -rw-------  1 jcprime  staff    16600  4 Oct 16:26 XML.sublime-package
  88 -rw-------  1 jcprime  staff    43395  4 Oct 16:26 YAML.sublime-package

./bin/ST3_Portable/python3.3:
total 8312
672 -rw-r--r--@   1 jcprime  staff       0  4 Oct 16:26 Icon
 16 -rw-------    1 jcprime  staff    4879  4 Oct 16:26 __future__.pyo
  8 -rw-------    1 jcprime  staff     124  4 Oct 16:26 __phello__.foo.pyo
 16 -rw-------    1 jcprime  staff    5440  4 Oct 16:26 _compat_pickle.pyo
 16 -rw-------    1 jcprime  staff    5763  4 Oct 16:26 _dummy_thread.pyo
 24 -rw-------    1 jcprime  staff   10866  4 Oct 16:26 _markupbase.pyo
 32 -rw-------    1 jcprime  staff   13438  4 Oct 16:26 _osx_support.pyo
160 -rw-------    1 jcprime  staff   81443  4 Oct 16:26 _pyio.pyo
 40 -rw-------    1 jcprime  staff   19077  4 Oct 16:26 _strptime.pyo
 48 -rw-------    1 jcprime  staff   22995  4 Oct 16:26 _sysconfigdata.pyo
 24 -rw-------    1 jcprime  staff    8439  4 Oct 16:26 _threading_local.pyo
 24 -rw-------    1 jcprime  staff   12274  4 Oct 16:26 _weakrefset.pyo
 24 -rw-------    1 jcprime  staff    9219  4 Oct 16:26 abc.pyo
 72 -rw-------    1 jcprime  staff   34510  4 Oct 16:26 aifc.pyo
  8 -rw-------    1 jcprime  staff    1003  4 Oct 16:26 antigravity.pyo
176 -rw-------    1 jcprime  staff   89882  4 Oct 16:26 argparse.pyo
 32 -rw-------    1 jcprime  staff   14837  4 Oct 16:26 ast.pyo
 24 -rw-------    1 jcprime  staff   10681  4 Oct 16:26 asynchat.pyo
 48 -rw-------    1 jcprime  staff   24131  4 Oct 16:26 asyncore.pyo
 32 -rw-------    1 jcprime  staff   14714  4 Oct 16:26 base64.pyo
 56 -rw-------    1 jcprime  staff   24604  4 Oct 16:26 bdb.pyo
 40 -rw-------    1 jcprime  staff   18046  4 Oct 16:26 binhex.pyo
  8 -rw-------    1 jcprime  staff    3258  4 Oct 16:26 bisect.pyo
 40 -rw-------    1 jcprime  staff   18552  4 Oct 16:26 bz2.pyo
 16 -rw-------    1 jcprime  staff    6797  4 Oct 16:26 cProfile.pyo
 72 -rw-------    1 jcprime  staff   36771  4 Oct 16:26 calendar.pyo
 72 -rw-------    1 jcprime  staff   35782  4 Oct 16:26 cgi.pyo
 32 -rw-------    1 jcprime  staff   13393  4 Oct 16:26 cgitb.pyo
 16 -rw-------    1 jcprime  staff    6180  4 Oct 16:26 chunk.pyo
 32 -rw-------    1 jcprime  staff   15568  4 Oct 16:26 cmd.pyo
 24 -rw-------    1 jcprime  staff   11403  4 Oct 16:26 code.pyo
 88 -rw-------    1 jcprime  staff   44348  4 Oct 16:26 codecs.pyo
 16 -rw-------    1 jcprime  staff    7439  4 Oct 16:26 codeop.pyo
  0 drwxr-xr-x@   6 jcprime  staff     204  4 Oct 16:26 collections
 16 -rw-------    1 jcprime  staff    4195  4 Oct 16:26 colorsys.pyo
 24 -rw-------    1 jcprime  staff    8677  4 Oct 16:26 compileall.pyo
  0 drwxr-xr-x@   5 jcprime  staff     170  4 Oct 16:26 concurrent
120 -rw-------    1 jcprime  staff   58530  4 Oct 16:26 configparser.pyo
 24 -rw-------    1 jcprime  staff   10876  4 Oct 16:26 contextlib.pyo
 24 -rw-------    1 jcprime  staff    9618  4 Oct 16:26 copy.pyo
 16 -rw-------    1 jcprime  staff    5462  4 Oct 16:26 copyreg.pyo
  8 -rw-------    1 jcprime  staff    2946  4 Oct 16:26 crypt.pyo
 40 -rw-------    1 jcprime  staff   17114  4 Oct 16:26 csv.pyo
  0 drwxr-xr-x@   8 jcprime  staff     272  4 Oct 16:26 ctypes
  0 drwxr-xr-x@   8 jcprime  staff     272  4 Oct 16:26 curses
144 -rw-------    1 jcprime  staff   71917  4 Oct 16:26 datetime.pyo
  0 drwxr-xr-x@   7 jcprime  staff     238  4 Oct 16:26 dbm
408 -rw-------    1 jcprime  staff  206819  4 Oct 16:26 decimal.pyo
136 -rw-------    1 jcprime  staff   68469  4 Oct 16:26 difflib.pyo
 24 -rw-------    1 jcprime  staff   10925  4 Oct 16:26 dis.pyo
  0 drwxr-xr-x@  32 jcprime  staff    1088  4 Oct 16:26 distutils
192 -rw-------    1 jcprime  staff   95413  4 Oct 16:26 doctest.pyo
  8 -rw-------    1 jcprime  staff    1341  4 Oct 16:26 dummy_threading.pyo
  0 drwxr-xr-x@  23 jcprime  staff     782  4 Oct 16:26 email
  0 drwxr-xr-x@ 123 jcprime  staff    4182  4 Oct 16:26 encodings
 24 -rw-------    1 jcprime  staff   10891  4 Oct 16:26 filecmp.pyo
 40 -rw-------    1 jcprime  staff   17062  4 Oct 16:26 fileinput.pyo
  8 -rw-------    1 jcprime  staff    3689  4 Oct 16:26 fnmatch.pyo
 56 -rw-------    1 jcprime  staff   25686  4 Oct 16:26 formatter.pyo
 48 -rw-------    1 jcprime  staff   23289  4 Oct 16:26 fractions.pyo
 88 -rw-------    1 jcprime  staff   43274  4 Oct 16:26 ftplib.pyo
 32 -rw-------    1 jcprime  staff   14897  4 Oct 16:26 functools.pyo
  8 -rw-------    1 jcprime  staff    3545  4 Oct 16:26 genericpath.pyo
 16 -rw-------    1 jcprime  staff    7804  4 Oct 16:26 getopt.pyo
 16 -rw-------    1 jcprime  staff    5402  4 Oct 16:26 getpass.pyo
 40 -rw-------    1 jcprime  staff   17024  4 Oct 16:26 gettext.pyo
  8 -rw-------    1 jcprime  staff    3117  4 Oct 16:26 glob.pyo
 48 -rw-------    1 jcprime  staff   23914  4 Oct 16:26 gzip.pyo
 16 -rw-------    1 jcprime  staff    5153  4 Oct 16:26 hashlib.pyo
 32 -rw-------    1 jcprime  staff   15773  4 Oct 16:26 heapq.pyo
 16 -rw-------    1 jcprime  staff    5553  4 Oct 16:26 hmac.pyo
  0 drwxr-xr-x@   6 jcprime  staff     204  4 Oct 16:26 html
  0 drwxr-xr-x@   8 jcprime  staff     272  4 Oct 16:26 http
104 -rw-------    1 jcprime  staff   51680  4 Oct 16:26 imaplib.pyo
 16 -rw-------    1 jcprime  staff    5200  4 Oct 16:26 imghdr.pyo
 24 -rw-------    1 jcprime  staff   12115  4 Oct 16:26 imp.pyo
  0 drwxr-xr-x@   8 jcprime  staff     272  4 Oct 16:26 importlib
160 -rw-------    1 jcprime  staff   78917  4 Oct 16:26 inspect.pyo
 16 -rw-------    1 jcprime  staff    4173  4 Oct 16:26 io.pyo
160 -rw-------    1 jcprime  staff   78564  4 Oct 16:26 ipaddress.pyo
  0 drwxr-xr-x@   8 jcprime  staff     272  4 Oct 16:26 json
  8 -rw-------    1 jcprime  staff    2166  4 Oct 16:26 keyword.pyo
  0 drwxr-xr-x@  16 jcprime  staff     544  4 Oct 16:27 lib2to3
  8 -rw-------    1 jcprime  staff    3716  4 Oct 16:27 linecache.pyo
104 -rw-------    1 jcprime  staff   53202  4 Oct 16:27 locale.pyo
  0 drwxr-xr-x@   6 jcprime  staff     204  4 Oct 16:27 logging
 40 -rw-------    1 jcprime  staff   18091  4 Oct 16:27 lzma.pyo
 16 -rw-------    1 jcprime  staff    7603  4 Oct 16:27 macpath.pyo
  8 -rw-------    1 jcprime  staff    2473  4 Oct 16:27 macurl2path.pyo
192 -rw-------    1 jcprime  staff   94486  4 Oct 16:27 mailbox.pyo
 16 -rw-------    1 jcprime  staff    7829  4 Oct 16:27 mailcap.pyo
 40 -rw-------    1 jcprime  staff   19830  4 Oct 16:27 mimetypes.pyo
 48 -rw-------    1 jcprime  staff   22022  4 Oct 16:27 modulefinder.pyo
  0 drwxr-xr-x@  16 jcprime  staff     544  4 Oct 16:27 multiprocessing
 16 -rw-------    1 jcprime  staff    5324  4 Oct 16:27 netrc.pyo
 96 -rw-------    1 jcprime  staff   45312  4 Oct 16:27 nntplib.pyo
 40 -rw-------    1 jcprime  staff   16932  4 Oct 16:27 ntpath.pyo
  8 -rw-------    1 jcprime  staff    2017  4 Oct 16:27 nturl2path.pyo
 40 -rw-------    1 jcprime  staff   17473  4 Oct 16:27 numbers.pyo
 16 -rw-------    1 jcprime  staff    5861  4 Oct 16:27 opcode.pyo
136 -rw-------    1 jcprime  staff   67510  4 Oct 16:27 optparse.pyo
 80 -rw-------    1 jcprime  staff   37001  4 Oct 16:27 os.pyo
 16 -rw-------    1 jcprime  staff    5040  4 Oct 16:27 os2emxpath.pyo
120 -rw-------    1 jcprime  staff   60736  4 Oct 16:27 pdb.pyo
104 -rw-------    1 jcprime  staff   50587  4 Oct 16:27 pickle.pyo
136 -rw-------    1 jcprime  staff   65565  4 Oct 16:27 pickletools.pyo
 24 -rw-------    1 jcprime  staff    9817  4 Oct 16:27 pipes.pyo
 48 -rw-------    1 jcprime  staff   22606  4 Oct 16:27 pkgutil.pyo
  0 drwxr-xr-x@   4 jcprime  staff     136  4 Oct 16:27 plat-darwin
 80 -rw-------    1 jcprime  staff   39507  4 Oct 16:27 platform.pyo
 48 -rw-------    1 jcprime  staff   22195  4 Oct 16:27 plistlib.pyo
 32 -rw-------    1 jcprime  staff   13921  4 Oct 16:27 poplib.pyo
 32 -rw-------    1 jcprime  staff   13117  4 Oct 16:27 posixpath.pyo
 32 -rw-------    1 jcprime  staff   12639  4 Oct 16:27 pprint.pyo
 40 -rw-------    1 jcprime  staff   17741  4 Oct 16:27 profile.pyo
 64 -rw-------    1 jcprime  staff   30937  4 Oct 16:27 pstats.pyo
 16 -rw-------    1 jcprime  staff    5575  4 Oct 16:27 pty.pyo
 16 -rw-------    1 jcprime  staff    7545  4 Oct 16:27 py_compile.pyo
 24 -rw-------    1 jcprime  staff   10759  4 Oct 16:27 pyclbr.pyo
232 -rw-------    1 jcprime  staff  117970  4 Oct 16:27 pydoc.pyo
  0 drwxr-xr-x@   5 jcprime  staff     170  4 Oct 16:27 pydoc_data
 24 -rw-------    1 jcprime  staff   11422  4 Oct 16:27 queue.pyo
 16 -rw-------    1 jcprime  staff    7445  4 Oct 16:27 quopri.pyo
 48 -rw-------    1 jcprime  staff   23026  4 Oct 16:27 random.pyo
 32 -rw-------    1 jcprime  staff   15990  4 Oct 16:27 re.pyo
 16 -rw-------    1 jcprime  staff    7852  4 Oct 16:27 reprlib.pyo
 16 -rw-------    1 jcprime  staff    6300  4 Oct 16:27 rlcompleter.pyo
 24 -rw-------    1 jcprime  staff   10089  4 Oct 16:27 runpy.pyo
 16 -rw-------    1 jcprime  staff    7895  4 Oct 16:27 sched.pyo
 24 -rw-------    1 jcprime  staff   12190  4 Oct 16:27 shelve.pyo
 24 -rw-------    1 jcprime  staff    9086  4 Oct 16:27 shlex.pyo
 80 -rw-------    1 jcprime  staff   40098  4 Oct 16:27 shutil.pyo
 56 -rw-------    1 jcprime  staff   24720  4 Oct 16:27 site.pyo
 72 -rw-------    1 jcprime  staff   32963  4 Oct 16:27 smtpd.pyo
 80 -rw-------    1 jcprime  staff   39740  4 Oct 16:27 smtplib.pyo
 16 -rw-------    1 jcprime  staff    8174  4 Oct 16:27 sndhdr.pyo
 40 -rw-------    1 jcprime  staff   17722  4 Oct 16:27 socket.pyo
 64 -rw-------    1 jcprime  staff   30077  4 Oct 16:27 socketserver.pyo
  0 drwxr-xr-x@   6 jcprime  staff     204  4 Oct 16:27 sqlite3
 24 -rw-------    1 jcprime  staff   11852  4 Oct 16:27 sre_compile.pyo
 16 -rw-------    1 jcprime  staff    6382  4 Oct 16:27 sre_constants.pyo
 56 -rw-------    1 jcprime  staff   24681  4 Oct 16:27 sre_parse.pyo
 56 -rw-------    1 jcprime  staff   27095  4 Oct 16:27 ssl.pyo
 16 -rw-------    1 jcprime  staff    4341  4 Oct 16:27 stat.pyo
 24 -rw-------    1 jcprime  staff    9851  4 Oct 16:27 string.pyo
 32 -rw-------    1 jcprime  staff   15569  4 Oct 16:27 stringprep.pyo
  8 -rw-------    1 jcprime  staff     385  4 Oct 16:27 struct.pyo
112 -rw-------    1 jcprime  staff   54597  4 Oct 16:27 subprocess.pyo
 48 -rw-------    1 jcprime  staff   22621  4 Oct 16:27 sunau.pyo
  8 -rw-------    1 jcprime  staff    3005  4 Oct 16:27 symbol.pyo
 32 -rw-------    1 jcprime  staff   16024  4 Oct 16:27 symtable.pyo
 48 -rw-------    1 jcprime  staff   21558  4 Oct 16:27 sysconfig.pyo
 24 -rw-------    1 jcprime  staff    9742  4 Oct 16:27 tabnanny.pyo
168 -rw-------    1 jcprime  staff   85006  4 Oct 16:27 tarfile.pyo
 56 -rw-------    1 jcprime  staff   26397  4 Oct 16:27 telnetlib.pyo
 64 -rw-------    1 jcprime  staff   29354  4 Oct 16:27 tempfile.pyo
 32 -rw-------    1 jcprime  staff   13709  4 Oct 16:27 textwrap.pyo
  8 -rw-------    1 jcprime  staff    1385  4 Oct 16:27 this.pyo
 96 -rw-------    1 jcprime  staff   46680  4 Oct 16:27 threading.pyo
 32 -rw-------    1 jcprime  staff   13309  4 Oct 16:27 timeit.pyo
 16 -rw-------    1 jcprime  staff    4265  4 Oct 16:27 token.pyo
 48 -rw-------    1 jcprime  staff   23970  4 Oct 16:27 tokenize.pyo
 64 -rw-------    1 jcprime  staff   30500  4 Oct 16:27 trace.pyo
 32 -rw-------    1 jcprime  staff   14073  4 Oct 16:27 traceback.pyo
  8 -rw-------    1 jcprime  staff    1413  4 Oct 16:27 tty.pyo
  8 -rw-------    1 jcprime  staff    3571  4 Oct 16:27 types.pyo
  0 drwxr-xr-x@  14 jcprime  staff     476  4 Oct 16:27 unittest
  0 drwxr-xr-x@   9 jcprime  staff     306  4 Oct 16:27 urllib
 16 -rw-------    1 jcprime  staff    4766  4 Oct 16:27 uu.pyo
 56 -rw-------    1 jcprime  staff   24849  4 Oct 16:27 uuid.pyo
  0 drwxr-xr-x@   5 jcprime  staff     170  4 Oct 16:27 venv
 32 -rw-------    1 jcprime  staff   14375  4 Oct 16:27 warnings.pyo
 48 -rw-------    1 jcprime  staff   23695  4 Oct 16:27 wave.pyo
 40 -rw-------    1 jcprime  staff   17408  4 Oct 16:27 weakref.pyo
 56 -rw-------    1 jcprime  staff   24966  4 Oct 16:27 webbrowser.pyo
  0 drwxr-xr-x@   9 jcprime  staff     306  4 Oct 16:27 wsgiref
 24 -rw-------    1 jcprime  staff   11643  4 Oct 16:27 xdrlib.pyo
  0 drwxr-xr-x@   8 jcprime  staff     272  4 Oct 16:27 xml
  0 drwxr-xr-x@   6 jcprime  staff     204  4 Oct 16:27 xmlrpc
112 -rw-------    1 jcprime  staff   57295  4 Oct 16:27 zipfile.pyo

./bin/ST3_Portable/python3.3/collections:
total 872
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
120 -rw-------  1 jcprime  staff  60806  4 Oct 16:26 __init__.pyo
  8 -rw-------  1 jcprime  staff   2254  4 Oct 16:26 __main__.pyo
 72 -rw-------  1 jcprime  staff  33147  4 Oct 16:26 abc.pyo

./bin/ST3_Portable/python3.3/concurrent:
total 680
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff  129  4 Oct 16:26 __init__.pyo
  0 drwxr-xr-x@ 7 jcprime  staff  238  4 Oct 16:26 futures

./bin/ST3_Portable/python3.3/concurrent/futures:
total 792
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff    879  4 Oct 16:26 __init__.pyo
 56 -rw-------  1 jcprime  staff  28399  4 Oct 16:26 _base.pyo
 40 -rw-------  1 jcprime  staff  16753  4 Oct 16:26 process.pyo
 16 -rw-------  1 jcprime  staff   5274  4 Oct 16:26 thread.pyo

./bin/ST3_Portable/python3.3/ctypes:
total 776
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
 56 -rw-------  1 jcprime  staff  25834  4 Oct 16:26 __init__.pyo
  8 -rw-------  1 jcprime  staff   2802  4 Oct 16:26 _endian.pyo
  0 drwxr-xr-x@ 7 jcprime  staff    238  4 Oct 16:26 macholib
 24 -rw-------  1 jcprime  staff   9115  4 Oct 16:26 util.pyo
 16 -rw-------  1 jcprime  staff   7840  4 Oct 16:26 wintypes.pyo

./bin/ST3_Portable/python3.3/ctypes/macholib:
total 712
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff   315  4 Oct 16:26 __init__.pyo
 16 -rw-------  1 jcprime  staff  6510  4 Oct 16:26 dyld.pyo
  8 -rw-------  1 jcprime  staff  1859  4 Oct 16:26 dylib.pyo
  8 -rw-------  1 jcprime  staff  2032  4 Oct 16:26 framework.pyo

./bin/ST3_Portable/python3.3/curses:
total 736
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff  2376  4 Oct 16:26 __init__.pyo
 16 -rw-------  1 jcprime  staff  5662  4 Oct 16:26 ascii.pyo
 16 -rw-------  1 jcprime  staff  5997  4 Oct 16:26 has_key.pyo
  8 -rw-------  1 jcprime  staff   242  4 Oct 16:26 panel.pyo
 16 -rw-------  1 jcprime  staff  7525  4 Oct 16:26 textpad.pyo

./bin/ST3_Portable/python3.3/dbm:
total 728
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:26 Icon
 16 -rw-------  1 jcprime  staff  5050  4 Oct 16:26 __init__.pyo
 24 -rw-------  1 jcprime  staff  8623  4 Oct 16:26 dumb.pyo
  8 -rw-------  1 jcprime  staff   222  4 Oct 16:26 gnu.pyo
  8 -rw-------  1 jcprime  staff   221  4 Oct 16:26 ndbm.pyo

./bin/ST3_Portable/python3.3/distutils:
total 1488
672 -rw-r--r--@  1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-------   1 jcprime  staff    350  4 Oct 16:26 __init__.pyo
 16 -rw-------   1 jcprime  staff   6464  4 Oct 16:26 archive_util.pyo
 24 -rw-------   1 jcprime  staff   9407  4 Oct 16:26 bcppcompiler.pyo
 88 -rw-------   1 jcprime  staff  42468  4 Oct 16:26 ccompiler.pyo
 40 -rw-------   1 jcprime  staff  19824  4 Oct 16:26 cmd.pyo
  0 drwxr-xr-x@ 26 jcprime  staff    884  4 Oct 16:26 command
 16 -rw-------   1 jcprime  staff   4906  4 Oct 16:26 config.pyo
 24 -rw-------   1 jcprime  staff   8319  4 Oct 16:26 core.pyo
 24 -rw-------   1 jcprime  staff  11851  4 Oct 16:26 cygwinccompiler.pyo
  8 -rw-------   1 jcprime  staff    219  4 Oct 16:26 debug.pyo
  8 -rw-------   1 jcprime  staff   3283  4 Oct 16:26 dep_util.pyo
 16 -rw-------   1 jcprime  staff   7375  4 Oct 16:26 dir_util.pyo
 96 -rw-------   1 jcprime  staff  47387  4 Oct 16:26 dist.pyo
 24 -rw-------   1 jcprime  staff   8377  4 Oct 16:26 emxccompiler.pyo
 24 -rw-------   1 jcprime  staff   8574  4 Oct 16:26 errors.pyo
 24 -rw-------   1 jcprime  staff   8406  4 Oct 16:26 extension.pyo
 32 -rw-------   1 jcprime  staff  14305  4 Oct 16:26 fancy_getopt.pyo
 16 -rw-------   1 jcprime  staff   7222  4 Oct 16:26 file_util.pyo
 32 -rw-------   1 jcprime  staff  12647  4 Oct 16:26 filelist.pyo
  8 -rw-------   1 jcprime  staff   3422  4 Oct 16:26 log.pyo
 56 -rw-------   1 jcprime  staff  24864  4 Oct 16:26 msvc9compiler.pyo
 48 -rw-------   1 jcprime  staff  20591  4 Oct 16:26 msvccompiler.pyo
 16 -rw-------   1 jcprime  staff   7517  4 Oct 16:26 spawn.pyo
 40 -rw-------   1 jcprime  staff  17667  4 Oct 16:26 sysconfig.pyo
 24 -rw-------   1 jcprime  staff  10122  4 Oct 16:26 text_file.pyo
 24 -rw-------   1 jcprime  staff   9155  4 Oct 16:26 unixccompiler.pyo
 40 -rw-------   1 jcprime  staff  19975  4 Oct 16:26 util.pyo
 24 -rw-------   1 jcprime  staff   9771  4 Oct 16:26 version.pyo
 16 -rw-------   1 jcprime  staff   6439  4 Oct 16:26 versionpredicate.pyo

./bin/ST3_Portable/python3.3/distutils/command:
total 1200
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff    630  4 Oct 16:26 __init__.pyo
 16 -rw-------  1 jcprime  staff   5200  4 Oct 16:26 bdist.pyo
 16 -rw-------  1 jcprime  staff   5088  4 Oct 16:26 bdist_dumb.pyo
 56 -rw-------  1 jcprime  staff  26377  4 Oct 16:26 bdist_msi.pyo
 40 -rw-------  1 jcprime  staff  20010  4 Oct 16:26 bdist_rpm.pyo
 24 -rw-------  1 jcprime  staff  12053  4 Oct 16:26 bdist_wininst.pyo
 16 -rw-------  1 jcprime  staff   5861  4 Oct 16:26 build.pyo
 16 -rw-------  1 jcprime  staff   7432  4 Oct 16:26 build_clib.pyo
 48 -rw-------  1 jcprime  staff  22804  4 Oct 16:26 build_ext.pyo
 32 -rw-------  1 jcprime  staff  15902  4 Oct 16:26 build_py.pyo
 16 -rw-------  1 jcprime  staff   6332  4 Oct 16:26 build_scripts.pyo
 16 -rw-------  1 jcprime  staff   7062  4 Oct 16:26 check.pyo
  8 -rw-------  1 jcprime  staff   3471  4 Oct 16:26 clean.pyo
 32 -rw-------  1 jcprime  staff  14849  4 Oct 16:26 config.pyo
 48 -rw-------  1 jcprime  staff  21149  4 Oct 16:26 install.pyo
  8 -rw-------  1 jcprime  staff   3579  4 Oct 16:26 install_data.pyo
 16 -rw-------  1 jcprime  staff   4319  4 Oct 16:26 install_egg_info.pyo
  8 -rw-------  1 jcprime  staff   2676  4 Oct 16:26 install_headers.pyo
 16 -rw-------  1 jcprime  staff   7991  4 Oct 16:26 install_lib.pyo
  8 -rw-------  1 jcprime  staff   3449  4 Oct 16:26 install_scripts.pyo
 24 -rw-------  1 jcprime  staff  11509  4 Oct 16:26 register.pyo
 40 -rw-------  1 jcprime  staff  17999  4 Oct 16:26 sdist.pyo
 16 -rw-------  1 jcprime  staff   7291  4 Oct 16:26 upload.pyo

./bin/ST3_Portable/python3.3/email:
total 1448
672 -rw-r--r--@  1 jcprime  staff       0  4 Oct 16:26 Icon
  8 -rw-------   1 jcprime  staff    2360  4 Oct 16:26 __init__.pyo
 16 -rw-------   1 jcprime  staff    7499  4 Oct 16:26 _encoded_words.pyo
240 -rw-------   1 jcprime  staff  120008  4 Oct 16:26 _header_value_parser.pyo
 40 -rw-------   1 jcprime  staff   17141  4 Oct 16:26 _parseaddr.pyo
 40 -rw-------   1 jcprime  staff   17808  4 Oct 16:26 _policybase.pyo
  8 -rw-------   1 jcprime  staff    3912  4 Oct 16:26 base64mime.pyo
 32 -rw-------   1 jcprime  staff   14205  4 Oct 16:26 charset.pyo
  8 -rw-------   1 jcprime  staff    2588  4 Oct 16:26 encoders.pyo
 24 -rw-------   1 jcprime  staff   10614  4 Oct 16:26 errors.pyo
 32 -rw-------   1 jcprime  staff   14527  4 Oct 16:26 feedparser.pyo
 40 -rw-------   1 jcprime  staff   17476  4 Oct 16:26 generator.pyo
 48 -rw-------   1 jcprime  staff   22135  4 Oct 16:26 header.pyo
 64 -rw-------   1 jcprime  staff   31605  4 Oct 16:26 headerregistry.pyo
  8 -rw-------   1 jcprime  staff    2628  4 Oct 16:26 iterators.pyo
 72 -rw-------   1 jcprime  staff   36224  4 Oct 16:26 message.pyo
  0 drwxr-xr-x@ 12 jcprime  staff     408  4 Oct 16:26 mime
 16 -rw-------   1 jcprime  staff    7544  4 Oct 16:26 parser.pyo
 24 -rw-------   1 jcprime  staff    9671  4 Oct 16:26 policy.pyo
 24 -rw-------   1 jcprime  staff    9655  4 Oct 16:26 quoprimime.pyo
 32 -rw-------   1 jcprime  staff   13246  4 Oct 16:26 utils.pyo

./bin/ST3_Portable/python3.3/email/mime:
total 744
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff   129  4 Oct 16:26 __init__.pyo
  8 -rw-------  1 jcprime  staff  1771  4 Oct 16:26 application.pyo
  8 -rw-------  1 jcprime  staff  3139  4 Oct 16:26 audio.pyo
  8 -rw-------  1 jcprime  staff  1248  4 Oct 16:26 base.pyo
  8 -rw-------  1 jcprime  staff  2221  4 Oct 16:26 image.pyo
  8 -rw-------  1 jcprime  staff  1624  4 Oct 16:26 message.pyo
  8 -rw-------  1 jcprime  staff  1815  4 Oct 16:26 multipart.pyo
  8 -rw-------  1 jcprime  staff  1037  4 Oct 16:26 nonmultipart.pyo
  8 -rw-------  1 jcprime  staff  1604  4 Oct 16:26 text.pyo

./bin/ST3_Portable/python3.3/encodings:
total 2024
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:26 Icon
 16 -rw-------  1 jcprime  staff  4455  4 Oct 16:26 __init__.pyo
 24 -rw-------  1 jcprime  staff  8585  4 Oct 16:26 aliases.pyo
  8 -rw-------  1 jcprime  staff  3191  4 Oct 16:26 ascii.pyo
  8 -rw-------  1 jcprime  staff  3883  4 Oct 16:26 base64_codec.pyo
  8 -rw-------  1 jcprime  staff  2410  4 Oct 16:26 big5.pyo
  8 -rw-------  1 jcprime  staff  2455  4 Oct 16:26 big5hkscs.pyo
 16 -rw-------  1 jcprime  staff  5114  4 Oct 16:26 bz2_codec.pyo
 16 -rw-------  1 jcprime  staff  4816  4 Oct 16:26 charmap.pyo
  8 -rw-------  1 jcprime  staff  3760  4 Oct 16:26 cp037.pyo
  8 -rw-------  1 jcprime  staff  3843  4 Oct 16:26 cp1006.pyo
  8 -rw-------  1 jcprime  staff  3771  4 Oct 16:26 cp1026.pyo
  8 -rw-------  1 jcprime  staff  3757  4 Oct 16:26 cp1140.pyo
  8 -rw-------  1 jcprime  staff  3794  4 Oct 16:26 cp1250.pyo
  8 -rw-------  1 jcprime  staff  3791  4 Oct 16:26 cp1251.pyo
  8 -rw-------  1 jcprime  staff  3794  4 Oct 16:26 cp1252.pyo
  8 -rw-------  1 jcprime  staff  3807  4 Oct 16:26 cp1253.pyo
  8 -rw-------  1 jcprime  staff  3796  4 Oct 16:26 cp1254.pyo
  8 -rw-------  1 jcprime  staff  3815  4 Oct 16:26 cp1255.pyo
  8 -rw-------  1 jcprime  staff  3793  4 Oct 16:26 cp1256.pyo
  8 -rw-------  1 jcprime  staff  3801  4 Oct 16:26 cp1257.pyo
  8 -rw-------  1 jcprime  staff  3799  4 Oct 16:26 cp1258.pyo
  8 -rw-------  1 jcprime  staff  3787  4 Oct 16:26 cp424.pyo
 24 -rw-------  1 jcprime  staff  8983  4 Oct 16:26 cp437.pyo
  8 -rw-------  1 jcprime  staff  3760  4 Oct 16:26 cp500.pyo
  8 -rw-------  1 jcprime  staff  2492  4 Oct 16:26 cp65001.pyo
  8 -rw-------  1 jcprime  staff  3854  4 Oct 16:26 cp720.pyo
 24 -rw-------  1 jcprime  staff  9211  4 Oct 16:26 cp737.pyo
 24 -rw-------  1 jcprime  staff  8997  4 Oct 16:26 cp775.pyo
 24 -rw-------  1 jcprime  staff  8730  4 Oct 16:26 cp850.pyo
 24 -rw-------  1 jcprime  staff  8999  4 Oct 16:26 cp852.pyo
 24 -rw-------  1 jcprime  staff  9180  4 Oct 16:26 cp855.pyo
  8 -rw-------  1 jcprime  staff  3819  4 Oct 16:26 cp856.pyo
 24 -rw-------  1 jcprime  staff  8720  4 Oct 16:26 cp857.pyo
 24 -rw-------  1 jcprime  staff  8700  4 Oct 16:26 cp858.pyo
 24 -rw-------  1 jcprime  staff  8966  4 Oct 16:26 cp860.pyo
 24 -rw-------  1 jcprime  staff  8977  4 Oct 16:26 cp861.pyo
 24 -rw-------  1 jcprime  staff  9112  4 Oct 16:26 cp862.pyo
 24 -rw-------  1 jcprime  staff  8977  4 Oct 16:26 cp863.pyo
 24 -rw-------  1 jcprime  staff  9108  4 Oct 16:26 cp864.pyo
 24 -rw-------  1 jcprime  staff  8977  4 Oct 16:26 cp865.pyo
 24 -rw-------  1 jcprime  staff  9212  4 Oct 16:26 cp866.pyo
 24 -rw-------  1 jcprime  staff  9024  4 Oct 16:26 cp869.pyo
  8 -rw-------  1 jcprime  staff  3885  4 Oct 16:26 cp874.pyo
  8 -rw-------  1 jcprime  staff  3754  4 Oct 16:26 cp875.pyo
  8 -rw-------  1 jcprime  staff  2419  4 Oct 16:26 cp932.pyo
  8 -rw-------  1 jcprime  staff  2419  4 Oct 16:26 cp949.pyo
  8 -rw-------  1 jcprime  staff  2419  4 Oct 16:26 cp950.pyo
  8 -rw-------  1 jcprime  staff  2482  4 Oct 16:26 euc_jis_2004.pyo
  8 -rw-------  1 jcprime  staff  2482  4 Oct 16:26 euc_jisx0213.pyo
  8 -rw-------  1 jcprime  staff  2428  4 Oct 16:26 euc_jp.pyo
  8 -rw-------  1 jcprime  staff  2428  4 Oct 16:26 euc_kr.pyo
  8 -rw-------  1 jcprime  staff  2437  4 Oct 16:26 gb18030.pyo
  8 -rw-------  1 jcprime  staff  2428  4 Oct 16:26 gb2312.pyo
  8 -rw-------  1 jcprime  staff  2401  4 Oct 16:26 gbk.pyo
  8 -rw-------  1 jcprime  staff  3807  4 Oct 16:26 hex_codec.pyo
  8 -rw-------  1 jcprime  staff  3967  4 Oct 16:26 hp_roman8.pyo
  8 -rw-------  1 jcprime  staff  2392  4 Oct 16:26 hz.pyo
 24 -rw-------  1 jcprime  staff  8642  4 Oct 16:26 idna.pyo
  8 -rw-------  1 jcprime  staff  2469  4 Oct 16:26 iso2022_jp.pyo
  8 -rw-------  1 jcprime  staff  2487  4 Oct 16:26 iso2022_jp_1.pyo
  8 -rw-------  1 jcprime  staff  2487  4 Oct 16:26 iso2022_jp_2.pyo
  8 -rw-------  1 jcprime  staff  2514  4 Oct 16:26 iso2022_jp_2004.pyo
  8 -rw-------  1 jcprime  staff  2487  4 Oct 16:26 iso2022_jp_3.pyo
  8 -rw-------  1 jcprime  staff  2505  4 Oct 16:26 iso2022_jp_ext.pyo
  8 -rw-------  1 jcprime  staff  2469  4 Oct 16:26 iso2022_kr.pyo
  8 -rw-------  1 jcprime  staff  3799  4 Oct 16:26 iso8859_1.pyo
  8 -rw-------  1 jcprime  staff  3811  4 Oct 16:26 iso8859_10.pyo
  8 -rw-------  1 jcprime  staff  3905  4 Oct 16:26 iso8859_11.pyo
  8 -rw-------  1 jcprime  staff  3814  4 Oct 16:26 iso8859_13.pyo
  8 -rw-------  1 jcprime  staff  3832  4 Oct 16:26 iso8859_14.pyo
  8 -rw-------  1 jcprime  staff  3811  4 Oct 16:26 iso8859_15.pyo
  8 -rw-------  1 jcprime  staff  3813  4 Oct 16:26 iso8859_16.pyo
  8 -rw-------  1 jcprime  staff  3796  4 Oct 16:26 iso8859_2.pyo
  8 -rw-------  1 jcprime  staff  3803  4 Oct 16:26 iso8859_3.pyo
  8 -rw-------  1 jcprime  staff  3796  4 Oct 16:26 iso8859_4.pyo
  8 -rw-------  1 jcprime  staff  3797  4 Oct 16:26 iso8859_5.pyo
  8 -rw-------  1 jcprime  staff  3841  4 Oct 16:26 iso8859_6.pyo
  8 -rw-------  1 jcprime  staff  3804  4 Oct 16:26 iso8859_7.pyo
  8 -rw-------  1 jcprime  staff  3835  4 Oct 16:26 iso8859_8.pyo
  8 -rw-------  1 jcprime  staff  3796  4 Oct 16:26 iso8859_9.pyo
  8 -rw-------  1 jcprime  staff  2419  4 Oct 16:26 johab.pyo
  8 -rw-------  1 jcprime  staff  3818  4 Oct 16:26 koi8_r.pyo
  8 -rw-------  1 jcprime  staff  3804  4 Oct 16:26 koi8_u.pyo
  8 -rw-------  1 jcprime  staff  3229  4 Oct 16:26 latin_1.pyo
 24 -rw-------  1 jcprime  staff  8933  4 Oct 16:26 mac_arabic.pyo
  8 -rw-------  1 jcprime  staff  3865  4 Oct 16:26 mac_centeuro.pyo
  8 -rw-------  1 jcprime  staff  3873  4 Oct 16:26 mac_croatian.pyo
  8 -rw-------  1 jcprime  staff  3863  4 Oct 16:26 mac_cyrillic.pyo
  8 -rw-------  1 jcprime  staff  3777  4 Oct 16:26 mac_farsi.pyo
  8 -rw-------  1 jcprime  staff  3817  4 Oct 16:26 mac_greek.pyo
  8 -rw-------  1 jcprime  staff  3856  4 Oct 16:26 mac_iceland.pyo
  8 -rw-------  1 jcprime  staff  3984  4 Oct 16:26 mac_latin2.pyo
  8 -rw-------  1 jcprime  staff  3834  4 Oct 16:26 mac_roman.pyo
  8 -rw-------  1 jcprime  staff  3874  4 Oct 16:26 mac_romanian.pyo
  8 -rw-------  1 jcprime  staff  3857  4 Oct 16:26 mac_turkish.pyo
  8 -rw-------  1 jcprime  staff  2728  4 Oct 16:26 mbcs.pyo
  8 -rw-------  1 jcprime  staff  3794  4 Oct 16:26 palmos.pyo
  8 -rw-------  1 jcprime  staff  3898  4 Oct 16:26 ptcp154.pyo
 24 -rw-------  1 jcprime  staff  9905  4 Oct 16:26 punycode.pyo
  8 -rw-------  1 jcprime  staff  4040  4 Oct 16:26 quopri_codec.pyo
  8 -rw-------  1 jcprime  staff  3014  4 Oct 16:26 raw_unicode_escape.pyo
 16 -rw-------  1 jcprime  staff  4417  4 Oct 16:26 rot_13.pyo
  8 -rw-------  1 jcprime  staff  2455  4 Oct 16:26 shift_jis.pyo
  8 -rw-------  1 jcprime  staff  2500  4 Oct 16:26 shift_jis_2004.pyo
  8 -rw-------  1 jcprime  staff  2500  4 Oct 16:26 shift_jisx0213.pyo
  8 -rw-------  1 jcprime  staff  3866  4 Oct 16:26 tis_620.pyo
  8 -rw-------  1 jcprime  staff  3433  4 Oct 16:26 undefined.pyo
  8 -rw-------  1 jcprime  staff  2954  4 Oct 16:26 unicode_escape.pyo
  8 -rw-------  1 jcprime  staff  2984  4 Oct 16:26 unicode_internal.pyo
 16 -rw-------  1 jcprime  staff  7941  4 Oct 16:26 utf_16.pyo
  8 -rw-------  1 jcprime  staff  2716  4 Oct 16:26 utf_16_be.pyo
  8 -rw-------  1 jcprime  staff  2716  4 Oct 16:26 utf_16_le.pyo
 16 -rw-------  1 jcprime  staff  7834  4 Oct 16:26 utf_32.pyo
  8 -rw-------  1 jcprime  staff  2609  4 Oct 16:26 utf_32_be.pyo
  8 -rw-------  1 jcprime  staff  2609  4 Oct 16:26 utf_32_le.pyo
  8 -rw-------  1 jcprime  staff  2593  4 Oct 16:26 utf_7.pyo
  8 -rw-------  1 jcprime  staff  2652  4 Oct 16:26 utf_8.pyo
 16 -rw-------  1 jcprime  staff  7225  4 Oct 16:26 utf_8_sig.pyo
 16 -rw-------  1 jcprime  staff  4932  4 Oct 16:26 uu_codec.pyo
 16 -rw-------  1 jcprime  staff  4956  4 Oct 16:26 zlib_codec.pyo

./bin/ST3_Portable/python3.3/html:
total 848
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff    943  4 Oct 16:26 __init__.pyo
128 -rw-------  1 jcprime  staff  65146  4 Oct 16:26 entities.pyo
 40 -rw-------  1 jcprime  staff  17898  4 Oct 16:26 parser.pyo

./bin/ST3_Portable/python3.3/http:
total 1064
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff    123  4 Oct 16:26 __init__.pyo
 88 -rw-------  1 jcprime  staff  42349  4 Oct 16:26 client.pyo
152 -rw-------  1 jcprime  staff  75376  4 Oct 16:26 cookiejar.pyo
 48 -rw-------  1 jcprime  staff  21355  4 Oct 16:26 cookies.pyo
 96 -rw-------  1 jcprime  staff  46708  4 Oct 16:26 server.pyo

./bin/ST3_Portable/python3.3/importlib:
total 880
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-------  1 jcprime  staff   3016  4 Oct 16:26 __init__.pyo
144 -rw-------  1 jcprime  staff  71396  4 Oct 16:26 _bootstrap.pyo
 40 -rw-------  1 jcprime  staff  19042  4 Oct 16:26 abc.pyo
  8 -rw-------  1 jcprime  staff   1381  4 Oct 16:26 machinery.pyo
  8 -rw-------  1 jcprime  staff    976  4 Oct 16:26 util.pyo

./bin/ST3_Portable/python3.3/json:
total 784
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
 32 -rw-------  1 jcprime  staff  13252  4 Oct 16:26 __init__.pyo
 32 -rw-------  1 jcprime  staff  12533  4 Oct 16:26 decoder.pyo
 32 -rw-------  1 jcprime  staff  14797  4 Oct 16:26 encoder.pyo
  8 -rw-------  1 jcprime  staff   2874  4 Oct 16:26 scanner.pyo
  8 -rw-------  1 jcprime  staff   1329  4 Oct 16:26 tool.pyo

./bin/ST3_Portable/python3.3/lib2to3:
total 976
672 -rw-r--r--@  1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-------   1 jcprime  staff    126  4 Oct 16:26 __init__.pyo
  8 -rw-------   1 jcprime  staff    245  4 Oct 16:26 __main__.pyo
 16 -rw-------   1 jcprime  staff   6647  4 Oct 16:26 btm_matcher.pyo
 24 -rw-------   1 jcprime  staff   8501  4 Oct 16:26 btm_utils.pyo
 24 -rw-------   1 jcprime  staff   8300  4 Oct 16:26 fixer_base.pyo
 40 -rw-------   1 jcprime  staff  16697  4 Oct 16:26 fixer_util.pyo
  0 drwxr-xr-x@ 55 jcprime  staff   1870  4 Oct 16:27 fixes
 24 -rw-------   1 jcprime  staff  10904  4 Oct 16:27 main.pyo
 16 -rw-------   1 jcprime  staff   8070  4 Oct 16:27 patcomp.pyo
  0 drwxr-xr-x@ 12 jcprime  staff    408  4 Oct 16:27 pgen2
  8 -rw-------   1 jcprime  staff   1519  4 Oct 16:27 pygram.pyo
 72 -rw-------   1 jcprime  staff  34130  4 Oct 16:27 pytree.pyo
 64 -rw-------   1 jcprime  staff  29986  4 Oct 16:27 refactor.pyo

./bin/ST3_Portable/python3.3/lib2to3/fixes:
total 1152
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff   132  4 Oct 16:26 __init__.pyo
  8 -rw-------  1 jcprime  staff  2061  4 Oct 16:26 fix_apply.pyo
  8 -rw-------  1 jcprime  staff   950  4 Oct 16:26 fix_basestring.pyo
  8 -rw-------  1 jcprime  staff  1095  4 Oct 16:26 fix_buffer.pyo
  8 -rw-------  1 jcprime  staff  1702  4 Oct 16:26 fix_callable.pyo
 16 -rw-------  1 jcprime  staff  4349  4 Oct 16:26 fix_dict.pyo
  8 -rw-------  1 jcprime  staff  3832  4 Oct 16:26 fix_except.pyo
  8 -rw-------  1 jcprime  staff  1549  4 Oct 16:26 fix_exec.pyo
  8 -rw-------  1 jcprime  staff  2296  4 Oct 16:26 fix_execfile.pyo
  8 -rw-------  1 jcprime  staff  3146  4 Oct 16:26 fix_exitfunc.pyo
  8 -rw-------  1 jcprime  staff  2456  4 Oct 16:26 fix_filter.pyo
  8 -rw-------  1 jcprime  staff  1277  4 Oct 16:26 fix_funcattrs.pyo
  8 -rw-------  1 jcprime  staff  1064  4 Oct 16:26 fix_future.pyo
  8 -rw-------  1 jcprime  staff  1075  4 Oct 16:26 fix_getcwdu.pyo
  8 -rw-------  1 jcprime  staff  3731  4 Oct 16:26 fix_has_key.pyo
 16 -rw-------  1 jcprime  staff  4995  4 Oct 16:26 fix_idioms.pyo
  8 -rw-------  1 jcprime  staff  3928  4 Oct 16:26 fix_import.pyo
 16 -rw-------  1 jcprime  staff  6234  4 Oct 16:26 fix_imports.pyo
  8 -rw-------  1 jcprime  staff   729  4 Oct 16:26 fix_imports2.pyo
  8 -rw-------  1 jcprime  staff  1293  4 Oct 16:26 fix_input.pyo
  8 -rw-------  1 jcprime  staff  1994  4 Oct 16:26 fix_intern.pyo
  8 -rw-------  1 jcprime  staff  2009  4 Oct 16:26 fix_isinstance.pyo
  8 -rw-------  1 jcprime  staff  1965  4 Oct 16:26 fix_itertools.pyo
  8 -rw-------  1 jcprime  staff  2208  4 Oct 16:26 fix_itertools_imports.pyo
  8 -rw-------  1 jcprime  staff   998  4 Oct 16:26 fix_long.pyo
  8 -rw-------  1 jcprime  staff  3262  4 Oct 16:26 fix_map.pyo
 16 -rw-------  1 jcprime  staff  7506  4 Oct 16:26 fix_metaclass.pyo
  8 -rw-------  1 jcprime  staff  1288  4 Oct 16:26 fix_methodattrs.pyo
  8 -rw-------  1 jcprime  staff  1165  4 Oct 16:26 fix_ne.pyo
 16 -rw-------  1 jcprime  staff  4416  4 Oct 16:26 fix_next.pyo
  8 -rw-------  1 jcprime  staff  1239  4 Oct 16:26 fix_nonzero.pyo
  8 -rw-------  1 jcprime  staff  1498  4 Oct 16:26 fix_numliterals.pyo
 16 -rw-------  1 jcprime  staff  6202  4 Oct 16:26 fix_operator.pyo
  8 -rw-------  1 jcprime  staff  1696  4 Oct 16:26 fix_paren.pyo
  8 -rw-------  1 jcprime  staff  3139  4 Oct 16:26 fix_print.pyo
  8 -rw-------  1 jcprime  staff  2914  4 Oct 16:26 fix_raise.pyo
  8 -rw-------  1 jcprime  staff  1089  4 Oct 16:26 fix_raw_input.pyo
  8 -rw-------  1 jcprime  staff  1413  4 Oct 16:26 fix_reduce.pyo
  8 -rw-------  1 jcprime  staff  2874  4 Oct 16:26 fix_renames.pyo
  8 -rw-------  1 jcprime  staff  1185  4 Oct 16:26 fix_repr.pyo
  8 -rw-------  1 jcprime  staff  2235  4 Oct 16:26 fix_set_literal.pyo
  8 -rw-------  1 jcprime  staff  1022  4 Oct 16:27 fix_standarderror.pyo
  8 -rw-------  1 jcprime  staff  1945  4 Oct 16:27 fix_sys_exc.pyo
  8 -rw-------  1 jcprime  staff  2407  4 Oct 16:27 fix_throw.pyo
 16 -rw-------  1 jcprime  staff  6791  4 Oct 16:27 fix_tuple_params.pyo
  8 -rw-------  1 jcprime  staff  2485  4 Oct 16:27 fix_types.pyo
  8 -rw-------  1 jcprime  staff  2186  4 Oct 16:27 fix_unicode.pyo
 16 -rw-------  1 jcprime  staff  8155  4 Oct 16:27 fix_urllib.pyo
  8 -rw-------  1 jcprime  staff  1551  4 Oct 16:27 fix_ws_comma.pyo
  8 -rw-------  1 jcprime  staff  3742  4 Oct 16:27 fix_xrange.pyo
  8 -rw-------  1 jcprime  staff  1512  4 Oct 16:27 fix_xreadlines.pyo
  8 -rw-------  1 jcprime  staff  1513  4 Oct 16:27 fix_zip.pyo

./bin/ST3_Portable/python3.3/lib2to3/pgen2:
total 832
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff    173  4 Oct 16:27 __init__.pyo
 16 -rw-------  1 jcprime  staff   7559  4 Oct 16:27 conv.pyo
 16 -rw-------  1 jcprime  staff   6070  4 Oct 16:27 driver.pyo
 16 -rw-------  1 jcprime  staff   6496  4 Oct 16:27 grammar.pyo
  8 -rw-------  1 jcprime  staff   1903  4 Oct 16:27 literals.pyo
 16 -rw-------  1 jcprime  staff   8049  4 Oct 16:27 parse.pyo
 32 -rw-------  1 jcprime  staff  14202  4 Oct 16:27 pgen.pyo
  8 -rw-------  1 jcprime  staff   2386  4 Oct 16:27 token.pyo
 40 -rw-------  1 jcprime  staff  18588  4 Oct 16:27 tokenize.pyo

./bin/ST3_Portable/python3.3/logging:
total 1000
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
152 -rw-------  1 jcprime  staff  77194  4 Oct 16:27 __init__.pyo
 64 -rw-------  1 jcprime  staff  32749  4 Oct 16:27 config.pyo
112 -rw-------  1 jcprime  staff  55772  4 Oct 16:27 handlers.pyo

./bin/ST3_Portable/python3.3/multiprocessing:
total 1184
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
 24 -rw-------  1 jcprime  staff  10928  4 Oct 16:27 __init__.pyo
 80 -rw-------  1 jcprime  staff  37771  4 Oct 16:27 connection.pyo
  0 drwxr-xr-x@ 5 jcprime  staff    170  4 Oct 16:27 dummy
 40 -rw-------  1 jcprime  staff  16683  4 Oct 16:27 forking.pyo
 16 -rw-------  1 jcprime  staff   7877  4 Oct 16:27 heap.pyo
104 -rw-------  1 jcprime  staff  50747  4 Oct 16:27 managers.pyo
 64 -rw-------  1 jcprime  staff  29196  4 Oct 16:27 pool.pyo
 24 -rw-------  1 jcprime  staff  11178  4 Oct 16:27 process.pyo
 32 -rw-------  1 jcprime  staff  14825  4 Oct 16:27 queues.pyo
 32 -rw-------  1 jcprime  staff  14447  4 Oct 16:27 reduction.pyo
 24 -rw-------  1 jcprime  staff  10445  4 Oct 16:27 sharedctypes.pyo
 40 -rw-------  1 jcprime  staff  16574  4 Oct 16:27 synchronize.pyo
 32 -rw-------  1 jcprime  staff  12367  4 Oct 16:27 util.pyo

./bin/ST3_Portable/python3.3/multiprocessing/dummy:
total 704
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
 16 -rw-------  1 jcprime  staff  6250  4 Oct 16:27 __init__.pyo
 16 -rw-------  1 jcprime  staff  4187  4 Oct 16:27 connection.pyo

./bin/ST3_Portable/python3.3/plat-darwin:
total 720
 48 -rw-------  1 jcprime  staff  24539  4 Oct 16:27 IN.pyo
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon

./bin/ST3_Portable/python3.3/pydoc_data:
total 1376
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff     129  4 Oct 16:27 __init__.pyo
696 -rw-------  1 jcprime  staff  353339  4 Oct 16:27 topics.pyo

./bin/ST3_Portable/python3.3/sqlite3:
total 696
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff   171  4 Oct 16:27 __init__.pyo
  8 -rw-------  1 jcprime  staff  3524  4 Oct 16:27 dbapi2.pyo
  8 -rw-------  1 jcprime  staff  2318  4 Oct 16:27 dump.pyo

./bin/ST3_Portable/python3.3/unittest:
total 1120
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff   3460  4 Oct 16:27 __init__.pyo
  8 -rw-------  1 jcprime  staff    607  4 Oct 16:27 __main__.pyo
112 -rw-------  1 jcprime  staff  55886  4 Oct 16:27 case.pyo
 32 -rw-------  1 jcprime  staff  14486  4 Oct 16:27 loader.pyo
 24 -rw-------  1 jcprime  staff  10659  4 Oct 16:27 main.pyo
160 -rw-------  1 jcprime  staff  80742  4 Oct 16:27 mock.pyo
 24 -rw-------  1 jcprime  staff   9657  4 Oct 16:27 result.pyo
 24 -rw-------  1 jcprime  staff  10365  4 Oct 16:27 runner.pyo
  8 -rw-------  1 jcprime  staff   3438  4 Oct 16:27 signals.pyo
 32 -rw-------  1 jcprime  staff  13744  4 Oct 16:27 suite.pyo
 16 -rw-------  1 jcprime  staff   4958  4 Oct 16:27 util.pyo

./bin/ST3_Portable/python3.3/urllib:
total 1016
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff     125  4 Oct 16:27 __init__.pyo
  8 -rw-------  1 jcprime  staff    3496  4 Oct 16:27 error.pyo
 80 -rw-------  1 jcprime  staff   39613  4 Oct 16:27 parse.pyo
208 -rw-------  1 jcprime  staff  103239  4 Oct 16:27 request.pyo
 16 -rw-------  1 jcprime  staff    5547  4 Oct 16:27 response.pyo
 24 -rw-------  1 jcprime  staff    8927  4 Oct 16:27 robotparser.pyo

./bin/ST3_Portable/python3.3/venv:
total 720
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
 40 -rw-------  1 jcprime  staff  17214  4 Oct 16:27 __init__.pyo
  8 -rw-------  1 jcprime  staff    392  4 Oct 16:27 __main__.pyo

./bin/ST3_Portable/python3.3/wsgiref:
total 824
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff    728  4 Oct 16:27 __init__.pyo
 48 -rw-------  1 jcprime  staff  21821  4 Oct 16:27 handlers.pyo
 24 -rw-------  1 jcprime  staff  10039  4 Oct 16:27 headers.pyo
 16 -rw-------  1 jcprime  staff   7553  4 Oct 16:27 simple_server.pyo
 16 -rw-------  1 jcprime  staff   7024  4 Oct 16:27 util.pyo
 40 -rw-------  1 jcprime  staff  20151  4 Oct 16:27 validate.pyo

./bin/ST3_Portable/python3.3/xml:
total 680
672 -rw-r--r--@  1 jcprime  staff    0  4 Oct 16:27 Icon
  8 -rw-------   1 jcprime  staff  718  4 Oct 16:27 __init__.pyo
  0 drwxr-xr-x@ 11 jcprime  staff  374  4 Oct 16:27 dom
  0 drwxr-xr-x@  8 jcprime  staff  272  4 Oct 16:27 etree
  0 drwxr-xr-x@  5 jcprime  staff  170  4 Oct 16:27 parsers
  0 drwxr-xr-x@  9 jcprime  staff  306  4 Oct 16:27 sax

./bin/ST3_Portable/python3.3/xml/dom:
total 1088
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff   1229  4 Oct 16:27 NodeFilter.pyo
 24 -rw-------  1 jcprime  staff   9025  4 Oct 16:27 __init__.pyo
  8 -rw-------  1 jcprime  staff   3574  4 Oct 16:27 domreg.pyo
 88 -rw-------  1 jcprime  staff  43010  4 Oct 16:27 expatbuilder.pyo
 16 -rw-------  1 jcprime  staff   4221  4 Oct 16:27 minicompat.pyo
184 -rw-------  1 jcprime  staff  91946  4 Oct 16:27 minidom.pyo
 40 -rw-------  1 jcprime  staff  16581  4 Oct 16:27 pulldom.pyo
 48 -rw-------  1 jcprime  staff  21080  4 Oct 16:27 xmlbuilder.pyo

./bin/ST3_Portable/python3.3/xml/etree:
total 816
  8 -rw-------  1 jcprime  staff   2299  4 Oct 16:27 ElementInclude.pyo
 24 -rw-------  1 jcprime  staff   9187  4 Oct 16:27 ElementPath.pyo
 96 -rw-------  1 jcprime  staff  46931  4 Oct 16:27 ElementTree.pyo
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff    128  4 Oct 16:27 __init__.pyo
  8 -rw-------  1 jcprime  staff    184  4 Oct 16:27 cElementTree.pyo

./bin/ST3_Portable/python3.3/xml/parsers:
total 688
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff  313  4 Oct 16:27 __init__.pyo
  8 -rw-------  1 jcprime  staff  381  4 Oct 16:27 expat.pyo

./bin/ST3_Portable/python3.3/xml/sax:
total 864
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
 16 -rw-------  1 jcprime  staff   4168  4 Oct 16:27 __init__.pyo
 16 -rw-------  1 jcprime  staff   7505  4 Oct 16:27 _exceptions.pyo
 40 -rw-------  1 jcprime  staff  18657  4 Oct 16:27 expatreader.pyo
 32 -rw-------  1 jcprime  staff  14474  4 Oct 16:27 handler.pyo
 40 -rw-------  1 jcprime  staff  19508  4 Oct 16:27 saxutils.pyo
 48 -rw-------  1 jcprime  staff  23400  4 Oct 16:27 xmlreader.pyo

./bin/ST3_Portable/python3.3/xmlrpc:
total 864
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-------  1 jcprime  staff    125  4 Oct 16:27 __init__.pyo
104 -rw-------  1 jcprime  staff  49964  4 Oct 16:27 client.pyo
 80 -rw-------  1 jcprime  staff  40047  4 Oct 16:27 server.pyo

./bin/dehydrate_update:
total 1568
 24 -rw-r--r--@ 1 jcprime  staff  10244  4 Oct 16:26 .DS_Store
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-r--r--  1 jcprime  staff    142  4 Oct 16:26 README
  8 -rw-r--r--  1 jcprime  staff   2520  4 Oct 16:26 X_pos.c
  8 -rw-r--r--  1 jcprime  staff   3624  4 Oct 16:26 X_pos.o
  0 drwxr-xr-x@ 5 jcprime  staff    170  4 Oct 16:26 archive
  8 -rw-r--r--  1 jcprime  staff   1314  4 Oct 16:26 atom_separation_squared.c
  8 -rw-r--r--  1 jcprime  staff   1856  4 Oct 16:26 atom_separation_squared.o
 16 -rw-r--r--  1 jcprime  staff   7830  4 Oct 16:26 cart_latt_vecs.c
 16 -rw-r--r--  1 jcprime  staff   4840  4 Oct 16:26 cart_latt_vecs.o
  8 -rw-r--r--  1 jcprime  staff   1174  4 Oct 16:26 cart_to_fract.c
  8 -rw-r--r--  1 jcprime  staff   1416  4 Oct 16:26 cart_to_fract.o
  8 -rw-r--r--  1 jcprime  staff    362  4 Oct 16:26 compare_strings.c
  8 -rw-r--r--  1 jcprime  staff   1392  4 Oct 16:26 compare_strings.o
  8 -rw-r--r--  1 jcprime  staff    149  4 Oct 16:26 constants.h
  8 -rw-r--r--  1 jcprime  staff     47  4 Oct 16:26 control
 16 -rw-r--r--  1 jcprime  staff   7806  4 Oct 16:26 data.h
 80 -rw-r--r--  1 jcprime  staff  40824  4 Oct 16:26 dehydrate
 40 -rw-r--r--  1 jcprime  staff  20114  4 Oct 16:26 dehydrate.c
 48 -rw-r--r--  1 jcprime  staff  20920  4 Oct 16:26 dehydrate.o
  8 -rw-r--r--  1 jcprime  staff    801  4 Oct 16:26 ewald.h
  8 -rw-r--r--  1 jcprime  staff   1112  4 Oct 16:26 fract_to_cart.c
  8 -rw-r--r--  1 jcprime  staff   1416  4 Oct 16:26 fract_to_cart.o
  8 -rw-r--r--  1 jcprime  staff   2776  4 Oct 16:26 generate_neighbours.c
  8 -rw-r--r--  1 jcprime  staff   2584  4 Oct 16:26 generate_neighbours.o
  8 -rw-r--r--  1 jcprime  staff   3521  4 Oct 16:26 global_values.h
  8 -rw-r--r--  1 jcprime  staff   2326  4 Oct 16:26 h_pos.c
  8 -rw-r--r--  1 jcprime  staff   3624  4 Oct 16:26 h_pos.o
  8 -rw-r--r--  1 jcprime  staff   2618  4 Oct 16:26 header.h
  8 -rw-r--r--  1 jcprime  staff   1152  4 Oct 16:26 index_sort.c
  8 -rw-r--r--  1 jcprime  staff   2336  4 Oct 16:26 index_sort.o
  8 -rw-r--r--  1 jcprime  staff    869  4 Oct 16:26 locate_string.c
  8 -rw-r--r--  1 jcprime  staff   1384  4 Oct 16:26 locate_string.o
  8 -rw-r--r--  1 jcprime  staff   2272  4 Oct 16:26 makefile
  8 -rw-r--r--  1 jcprime  staff    995  4 Oct 16:26 maxima.h
  8 -rw-r--r--  1 jcprime  staff   1946  4 Oct 16:26 min_image.c
  8 -rw-r--r--  1 jcprime  staff   2272  4 Oct 16:26 min_image.o
 56 -rw-r--r--  1 jcprime  staff  25364  4 Oct 16:26 move_file
  8 -rw-r--r--  1 jcprime  staff   1522  4 Oct 16:26 move_file.c
  8 -rw-r--r--  1 jcprime  staff    797  4 Oct 16:26 orig_atom_separation_squared.c
  8 -rw-r--r--  1 jcprime  staff    702  4 Oct 16:26 own_maths.h
  0 -rw-r--r--  1 jcprime  staff      0  4 Oct 16:26 poo
  8 -rw-r--r--  1 jcprime  staff   2042  4 Oct 16:26 potent.c
  8 -rw-r--r--  1 jcprime  staff   2616  4 Oct 16:26 potent.o
  8 -rw-r--r--  1 jcprime  staff    709  4 Oct 16:26 print_molecule.c
  8 -rw-r--r--  1 jcprime  staff   1976  4 Oct 16:26 print_molecule.o
  8 -rw-r--r--  1 jcprime  staff   1068  4 Oct 16:26 print_neighbours.c
  8 -rw-r--r--  1 jcprime  staff   2080  4 Oct 16:26 print_neighbours.o
 40 -rw-r--r--  1 jcprime  staff  16561  4 Oct 16:26 reader.c
  8 -rw-r--r--  1 jcprime  staff   1132  4 Oct 16:26 reader.h
 24 -rw-r--r--  1 jcprime  staff  10560  4 Oct 16:26 reader.o
  8 -rw-r--r--  1 jcprime  staff   1599  4 Oct 16:26 real_random.c
  8 -rw-r--r--  1 jcprime  staff   2072  4 Oct 16:26 real_random.o
  8 -rw-r--r--  1 jcprime  staff    778  4 Oct 16:26 save_config.c
  8 -rw-r--r--  1 jcprime  staff   1904  4 Oct 16:26 save_config.o
  8 -rw-r--r--  1 jcprime  staff   2329  4 Oct 16:26 save_gulpfile.c
  8 -rw-r--r--  1 jcprime  staff   4088  4 Oct 16:26 save_gulpfile.o
  8 -rw-r--r--  1 jcprime  staff   1263  4 Oct 16:26 save_xtlfile.c
  8 -rw-r--r--  1 jcprime  staff   2776  4 Oct 16:26 save_xtlfile.o
  8 -rw-r--r--  1 jcprime  staff   2551  4 Oct 16:26 set_elem.c
  8 -rw-r--r--  1 jcprime  staff   2896  4 Oct 16:26 set_elem.o
  8 -rw-r--r--  1 jcprime  staff    887  4 Oct 16:26 standard_bond.c
  8 -rw-r--r--  1 jcprime  staff   2080  4 Oct 16:26 standard_bond.o
  8 -rw-r--r--  1 jcprime  staff   2388  4 Oct 16:26 structures.h
  8 -rw-r--r--  1 jcprime  staff    340  4 Oct 16:26 vec_cross.c
  8 -rw-r--r--  1 jcprime  staff   1392  4 Oct 16:26 vec_cross.o
  8 -rw-r--r--  1 jcprime  staff    262  4 Oct 16:26 vec_dot.c
  8 -rw-r--r--  1 jcprime  staff   1336  4 Oct 16:26 vec_dot.o
 56 -rw-r--r--  1 jcprime  staff  26122  4 Oct 16:26 working.c
 40 -rw-r--r--  1 jcprime  staff  18850  4 Oct 16:26 working_dehydrate.c

./bin/dehydrate_update/archive:
total 728
 16 -rw-r--r--@ 1 jcprime  staff   6148  4 Oct 16:26 .DS_Store
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
 40 -rw-r--r--  1 jcprime  staff  18852  4 Oct 16:26 bodge_dehydrate.c

./bin/phd:
total 744
 24 -rw-r--r--@  1 jcprime  staff  8196  4 Oct 16:26 .DS_Store
  8 -rw-r--r--   1 jcprime  staff   351  4 Oct 16:26 0_toc.txt
672 -rw-r--r--@  1 jcprime  staff     0  4 Oct 16:26 Icon
  0 drwxr-xr-x  25 jcprime  staff   850  4 Oct 16:26 basic_blocks
  8 -rw-r--r--   1 jcprime  staff   722  4 Oct 16:26 dehydrate.sh
  0 drwxr-xr-x@  4 jcprime  staff   136  4 Oct 16:26 dehydrate_cpp
  0 drwxr-xr-x@ 72 jcprime  staff  2448  4 Oct 16:26 dehydrate_update
  8 -rw-r--r--   1 jcprime  staff  2083  4 Oct 16:26 general_info.pl
  0 drwxr-xr-x@  6 jcprime  staff   204  4 Oct 16:26 manpages
  8 -rw-r--r--   1 jcprime  staff  1132  4 Oct 16:26 musings.sh
  8 -rw-r--r--   1 jcprime  staff    40  4 Oct 16:26 readme.txt
  8 -rw-r--r--   1 jcprime  staff   103  4 Oct 16:26 template.sh
  0 drwxr-xr-x@  5 jcprime  staff   170  4 Oct 16:26 tips

./bin/phd/basic_blocks:
total 184
8 -rw-r--r--  1 jcprime  staff   562  4 Oct 16:26 0_toc.txt
8 -rw-r--r--  1 jcprime  staff   282  4 Oct 16:26 argument_testing.txt
8 -rw-r--r--  1 jcprime  staff  1849  4 Oct 16:26 arrays.txt
8 -rw-r--r--  1 jcprime  staff  1306  4 Oct 16:26 brace_expansions.txt
8 -rw-r--r--  1 jcprime  staff  1115  4 Oct 16:26 bracketing.txt
8 -rw-r--r--  1 jcprime  staff   936  4 Oct 16:26 case_statements.txt
8 -rw-r--r--  1 jcprime  staff  1562  4 Oct 16:26 comparison_operators.txt
8 -rw-r--r--  1 jcprime  staff   369  4 Oct 16:26 conditional_expressions.txt
8 -rw-r--r--  1 jcprime  staff   180  4 Oct 16:26 copy_from_clipboard.txt
8 -rw-r--r--  1 jcprime  staff  1737  4 Oct 16:26 cron.txt
8 -rw-r--r--  1 jcprime  staff  2108  4 Oct 16:26 exit_status.txt
8 -rw-r--r--  1 jcprime  staff  1522  4 Oct 16:26 for_loops.txt
8 -rw-r--r--  1 jcprime  staff   701  4 Oct 16:26 functions.txt
8 -rw-r--r--  1 jcprime  staff   192  4 Oct 16:26 if_elif_fi.txt
8 -rw-r--r--  1 jcprime  staff   387  4 Oct 16:26 looping_by_line.txt
8 -rw-r--r--  1 jcprime  staff   473  4 Oct 16:26 select_statements.txt
8 -rw-r--r--  1 jcprime  staff  1033  4 Oct 16:26 special_characters.txt
8 -rw-r--r--  1 jcprime  staff   739  4 Oct 16:26 strings.txt
8 -rw-r--r--  1 jcprime  staff   940  4 Oct 16:26 tilde_expansion.txt
8 -rw-r--r--  1 jcprime  staff    78  4 Oct 16:26 until_loops.txt
8 -rw-r--r--  1 jcprime  staff   253  4 Oct 16:26 useful_commands.txt
8 -rw-r--r--  1 jcprime  staff   968  4 Oct 16:26 variables.txt
8 -rw-r--r--  1 jcprime  staff   331  4 Oct 16:26 while_loops.txt

./bin/phd/dehydrate_cpp:
total 712
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
 40 -rw-r--r--  1 jcprime  staff  18849  4 Oct 16:26 dehydrate.cpp

./bin/phd/dehydrate_update:
total 1568
 24 -rw-r--r--@ 1 jcprime  staff  10244  4 Oct 16:26 .DS_Store
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
  8 -rw-r--r--  1 jcprime  staff    142  4 Oct 16:26 README
  8 -rw-r--r--  1 jcprime  staff   2520  4 Oct 16:26 X_pos.c
  8 -rw-r--r--  1 jcprime  staff   3624  4 Oct 16:26 X_pos.o
  0 drwxr-xr-x@ 5 jcprime  staff    170  4 Oct 16:26 archive
  8 -rw-r--r--  1 jcprime  staff   1314  4 Oct 16:26 atom_separation_squared.c
  8 -rw-r--r--  1 jcprime  staff   1856  4 Oct 16:26 atom_separation_squared.o
 16 -rw-r--r--  1 jcprime  staff   7830  4 Oct 16:26 cart_latt_vecs.c
 16 -rw-r--r--  1 jcprime  staff   4840  4 Oct 16:26 cart_latt_vecs.o
  8 -rw-r--r--  1 jcprime  staff   1174  4 Oct 16:26 cart_to_fract.c
  8 -rw-r--r--  1 jcprime  staff   1416  4 Oct 16:26 cart_to_fract.o
  8 -rw-r--r--  1 jcprime  staff    362  4 Oct 16:26 compare_strings.c
  8 -rw-r--r--  1 jcprime  staff   1392  4 Oct 16:26 compare_strings.o
  8 -rw-r--r--  1 jcprime  staff    149  4 Oct 16:26 constants.h
  8 -rw-r--r--  1 jcprime  staff     47  4 Oct 16:26 control
 16 -rw-r--r--  1 jcprime  staff   8162  4 Oct 16:26 data.h
 80 -rw-r--r--  1 jcprime  staff  40824  4 Oct 16:26 dehydrate
 40 -rw-r--r--  1 jcprime  staff  18848  4 Oct 16:26 dehydrate.c
 48 -rw-r--r--  1 jcprime  staff  20920  4 Oct 16:26 dehydrate.o
  8 -rw-r--r--  1 jcprime  staff    801  4 Oct 16:26 ewald.h
  8 -rw-r--r--  1 jcprime  staff   1112  4 Oct 16:26 fract_to_cart.c
  8 -rw-r--r--  1 jcprime  staff   1416  4 Oct 16:26 fract_to_cart.o
  8 -rw-r--r--  1 jcprime  staff   2776  4 Oct 16:26 generate_neighbours.c
  8 -rw-r--r--  1 jcprime  staff   2584  4 Oct 16:26 generate_neighbours.o
  8 -rw-r--r--  1 jcprime  staff   3513  4 Oct 16:26 global_values.h
  8 -rw-r--r--  1 jcprime  staff   2326  4 Oct 16:26 h_pos.c
  8 -rw-r--r--  1 jcprime  staff   3624  4 Oct 16:26 h_pos.o
  8 -rw-r--r--  1 jcprime  staff   2570  4 Oct 16:26 header.h
  8 -rw-r--r--  1 jcprime  staff   1152  4 Oct 16:26 index_sort.c
  8 -rw-r--r--  1 jcprime  staff   2336  4 Oct 16:26 index_sort.o
  8 -rw-r--r--  1 jcprime  staff    869  4 Oct 16:26 locate_string.c
  8 -rw-r--r--  1 jcprime  staff   1384  4 Oct 16:26 locate_string.o
  8 -rw-r--r--  1 jcprime  staff   2285  4 Oct 16:26 makefile
  8 -rw-r--r--  1 jcprime  staff    955  4 Oct 16:26 maxima.h
  8 -rw-r--r--  1 jcprime  staff   1946  4 Oct 16:26 min_image.c
  8 -rw-r--r--  1 jcprime  staff   2272  4 Oct 16:26 min_image.o
 56 -rw-r--r--  1 jcprime  staff  25364  4 Oct 16:26 move_file
  8 -rw-r--r--  1 jcprime  staff   1522  4 Oct 16:26 move_file.c
  8 -rw-r--r--  1 jcprime  staff    797  4 Oct 16:26 orig_atom_separation_squared.c
  8 -rw-r--r--  1 jcprime  staff    702  4 Oct 16:26 own_maths.h
  0 -rw-r--r--  1 jcprime  staff      0  4 Oct 16:26 poo
  8 -rw-r--r--  1 jcprime  staff   2042  4 Oct 16:26 potent.c
  8 -rw-r--r--  1 jcprime  staff   2616  4 Oct 16:26 potent.o
  8 -rw-r--r--  1 jcprime  staff    709  4 Oct 16:26 print_molecule.c
  8 -rw-r--r--  1 jcprime  staff   1976  4 Oct 16:26 print_molecule.o
  8 -rw-r--r--  1 jcprime  staff   1068  4 Oct 16:26 print_neighbours.c
  8 -rw-r--r--  1 jcprime  staff   2080  4 Oct 16:26 print_neighbours.o
 40 -rw-r--r--  1 jcprime  staff  16621  4 Oct 16:26 reader.c
  8 -rw-r--r--  1 jcprime  staff   1132  4 Oct 16:26 reader.h
 24 -rw-r--r--  1 jcprime  staff  10560  4 Oct 16:26 reader.o
  8 -rw-r--r--  1 jcprime  staff   1599  4 Oct 16:26 real_random.c
  8 -rw-r--r--  1 jcprime  staff   2072  4 Oct 16:26 real_random.o
  8 -rw-r--r--  1 jcprime  staff    778  4 Oct 16:26 save_config.c
  8 -rw-r--r--  1 jcprime  staff   1904  4 Oct 16:26 save_config.o
  8 -rw-r--r--  1 jcprime  staff   2329  4 Oct 16:26 save_gulpfile.c
  8 -rw-r--r--  1 jcprime  staff   4088  4 Oct 16:26 save_gulpfile.o
  8 -rw-r--r--  1 jcprime  staff   1263  4 Oct 16:26 save_xtlfile.c
  8 -rw-r--r--  1 jcprime  staff   2776  4 Oct 16:26 save_xtlfile.o
  8 -rw-r--r--  1 jcprime  staff   2551  4 Oct 16:26 set_elem.c
  8 -rw-r--r--  1 jcprime  staff   2896  4 Oct 16:26 set_elem.o
  8 -rw-r--r--  1 jcprime  staff    887  4 Oct 16:26 standard_bond.c
  8 -rw-r--r--  1 jcprime  staff   2080  4 Oct 16:26 standard_bond.o
  8 -rw-r--r--  1 jcprime  staff   2174  4 Oct 16:26 structures.h
  8 -rw-r--r--  1 jcprime  staff    340  4 Oct 16:26 vec_cross.c
  8 -rw-r--r--  1 jcprime  staff   1392  4 Oct 16:26 vec_cross.o
  8 -rw-r--r--  1 jcprime  staff    262  4 Oct 16:26 vec_dot.c
  8 -rw-r--r--  1 jcprime  staff   1336  4 Oct 16:26 vec_dot.o
 56 -rw-r--r--  1 jcprime  staff  26122  4 Oct 16:26 working.c
 40 -rw-r--r--  1 jcprime  staff  18850  4 Oct 16:26 working_dehydrate.c

./bin/phd/dehydrate_update/archive:
total 728
 16 -rw-r--r--@ 1 jcprime  staff   6148  4 Oct 16:26 .DS_Store
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:26 Icon
 40 -rw-r--r--  1 jcprime  staff  18852  4 Oct 16:26 bodge_dehydrate.c

./bin/phd/manpages:
total 1768
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:26 Icon
672 -rw-r--r--  1 jcprime  staff  340514  4 Oct 16:26 bash.txt
 24 -rw-r--r--  1 jcprime  staff   10728  4 Oct 16:26 cron.txt
400 -rw-r--r--  1 jcprime  staff  200781  4 Oct 16:26 rsync.txt

./bin/phd/tips:
total 688
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:26 Icon
  8 -rw-r--r--  1 jcprime  staff  519  4 Oct 16:26 install_certificates_ubuntu.txt
  8 -rw-r--r--  1 jcprime  staff   45  4 Oct 16:26 install_packages_from_deb.txt

./bin/xenon:
total 19496
   16 -rw-r--r--@  1 jcprime  staff     6148  4 Oct 16:27 .DS_Store
    8 -rw-r--r--   1 jcprime  staff     2150  4 Oct 16:27 0_toc.txt
  672 -rw-r--r--@  1 jcprime  staff        0  4 Oct 16:27 Icon
    8 -rw-r--r--   1 jcprime  staff      112  4 Oct 16:27 awking.awk
    0 drwxr-xr-x@  8 jcprime  staff      272  4 Oct 16:27 backups
    0 drwxr-xr-x@ 26 jcprime  staff      884  4 Oct 16:27 basic_blocks
    8 -rw-r--r--   1 jcprime  staff      776  4 Oct 16:27 car_to_xyz.sh
    8 -rw-r--r--   1 jcprime  staff     1172  4 Oct 16:27 cp2k
    0 drwxr-xr-x   3 jcprime  staff      102  4 Oct 16:27 dehydrate
    8 -rw-r--r--   1 jcprime  staff      722  4 Oct 16:27 dehydrate.sh
    0 drwxr-xr-x@  7 jcprime  staff      238  4 Oct 16:27 deprecated
    0 drwxr-xr-x@ 14 jcprime  staff      476  4 Oct 16:27 editing
    8 -rw-r--r--   1 jcprime  staff      610  4 Oct 16:27 erm.sh
    8 -rw-r--r--   1 jcprime  staff       15  4 Oct 16:27 erm_cp2k.sh
   16 -rw-r--r--@  1 jcprime  staff     6932  4 Oct 16:27 faff.sh
    0 -rw-r--r--   1 jcprime  staff        0  4 Oct 16:27 file.txt
    8 -rw-r--r--   1 jcprime  staff      788  4 Oct 16:27 find_min.sh
    8 -rw-r--r--   1 jcprime  staff      202  4 Oct 16:27 ganal
    0 drwxr-xr-x@ 13 jcprime  staff      442  4 Oct 16:27 gdis
    8 -rw-r--r--   1 jcprime  staff     2085  4 Oct 16:27 general_info.pl
    8 -rw-r--r--   1 jcprime  staff       70  4 Oct 16:27 gulp
    8 -rw-r--r--   1 jcprime  staff      303  4 Oct 16:27 gulp_batch.sh
    0 -rw-r--r--   1 jcprime  staff        0  4 Oct 16:27 gulptmp_4_1
    0 drwxr-xr-x@ 18 jcprime  staff      612  4 Oct 16:34 jmol
    8 -rw-r--r--   1 jcprime  staff      108  4 Oct 16:27 jmol_export.spt
    0 drwxr-xr-x@ 12 jcprime  staff      408  4 Oct 16:27 jw
 8192 -rw-r--r--   1 jcprime  staff  4192849  4 Oct 16:27 klmc
    8 -rw-r--r--   1 jcprime  staff     4012  4 Oct 16:27 qgulp
    8 -rw-r--r--   1 jcprime  staff     4010  4 Oct 16:27 qgulp_working.bak
    8 -rw-r--r--   1 jcprime  staff     4012  4 Oct 16:27 qstep
    8 -rw-r--r--   1 jcprime  staff       40  4 Oct 16:27 readme.txt
    8 -rw-r--r--   1 jcprime  staff     2182  4 Oct 16:27 rgulp
    8 -rw-r--r--   1 jcprime  staff     2182  4 Oct 16:27 rstep
    0 drwxr-xr-x@  7 jcprime  staff      238  4 Oct 16:27 scott
    8 -rw-r--r--   1 jcprime  staff      103  4 Oct 16:27 template.sh
    0 drwxr-xr-x@ 24 jcprime  staff      816  4 Oct 16:27 testdisk-7.0
10432 -rw-r--r--   1 jcprime  staff  5337375  4 Oct 16:27 unison
    8 -rw-r--r--   1 jcprime  staff     2169  4 Oct 16:27 xyz_dehydrate.sh

./bin/xenon/backups:
total 11200
   32 -rw-r--r--  1 jcprime  staff    16384  4 Oct 16:27 .qgulp.bak
  672 -rw-r--r--@ 1 jcprime  staff        0  4 Oct 16:27 Icon
    8 -rw-r--r--  1 jcprime  staff     4010  4 Oct 16:27 qgulp_working.bak
    8 -rw-r--r--  1 jcprime  staff     2384  4 Oct 16:27 scol.inp.bak
10440 -rw-r--r--  1 jcprime  staff  5345280  4 Oct 16:27 unison-2.40.63_centos5.7x86_64.tar
   40 -rw-r--r--  1 jcprime  staff    17832  4 Oct 16:27 viminfo

./bin/xenon/basic_blocks:
total 856
  8 -rw-r--r--  1 jcprime  staff   562  4 Oct 16:27 0_toc.txt
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff   282  4 Oct 16:27 argument_testing.txt
  8 -rw-r--r--  1 jcprime  staff  1849  4 Oct 16:27 arrays.txt
  8 -rw-r--r--  1 jcprime  staff  1306  4 Oct 16:27 brace_expansions.txt
  8 -rw-r--r--  1 jcprime  staff  1115  4 Oct 16:27 bracketing.txt
  8 -rw-r--r--  1 jcprime  staff   936  4 Oct 16:27 case_statements.txt
  8 -rw-r--r--  1 jcprime  staff  1562  4 Oct 16:27 comparison_operators.txt
  8 -rw-r--r--  1 jcprime  staff   369  4 Oct 16:27 conditional_expressions.txt
  8 -rw-r--r--  1 jcprime  staff   180  4 Oct 16:27 copy_from_clipboard.txt
  8 -rw-r--r--  1 jcprime  staff  1737  4 Oct 16:27 cron.txt
  8 -rw-r--r--  1 jcprime  staff  2108  4 Oct 16:27 exit_status.txt
  8 -rw-r--r--  1 jcprime  staff  1522  4 Oct 16:27 for_loops.txt
  8 -rw-r--r--  1 jcprime  staff   701  4 Oct 16:27 functions.txt
  8 -rw-r--r--  1 jcprime  staff   192  4 Oct 16:27 if_elif_fi.txt
  8 -rw-r--r--  1 jcprime  staff   387  4 Oct 16:27 looping_by_line.txt
  8 -rw-r--r--  1 jcprime  staff   473  4 Oct 16:27 select_statements.txt
  8 -rw-r--r--  1 jcprime  staff  1033  4 Oct 16:27 special_characters.txt
  8 -rw-r--r--  1 jcprime  staff   739  4 Oct 16:27 strings.txt
  8 -rw-r--r--  1 jcprime  staff   940  4 Oct 16:27 tilde_expansion.txt
  8 -rw-r--r--  1 jcprime  staff    78  4 Oct 16:27 until_loops.txt
  8 -rw-r--r--  1 jcprime  staff   253  4 Oct 16:27 useful_commands.txt
  8 -rw-r--r--  1 jcprime  staff   968  4 Oct 16:27 variables.txt
  8 -rw-r--r--  1 jcprime  staff   331  4 Oct 16:27 while_loops.txt

./bin/xenon/dehydrate:
total 40
40 -rw-r--r--  1 jcprime  staff  18849  4 Oct 16:27 dehydrate.c

./bin/xenon/deprecated:
total 704
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff   722  4 Oct 16:27 dehydrate.sh
  8 -rw-r--r--  1 jcprime  staff  2002  4 Oct 16:27 erm_copy.sh
  8 -rw-r--r--  1 jcprime  staff   788  4 Oct 16:27 find_min.sh
  8 -rw-r--r--  1 jcprime  staff   103  4 Oct 16:27 template.sh

./bin/xenon/editing:
total 800
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff   1735  4 Oct 16:27 datamine_runjob_edit.sh
  8 -rw-r--r--  1 jcprime  staff    610  4 Oct 16:27 erm.sh
 16 -rw-r--r--  1 jcprime  staff   5359  4 Oct 16:27 erm_copy_v2.sh
  8 -rw-r--r--  1 jcprime  staff     15  4 Oct 16:27 erm_cp2k.sh
 16 -rw-r--r--  1 jcprime  staff   4229  4 Oct 16:27 export_images.sh
  8 -rw-r--r--  1 jcprime  staff    166  4 Oct 16:27 gin_to_cp2k.sh
 32 -rw-r--r--  1 jcprime  staff  13412  4 Oct 16:27 lm_gatherings.sh
  8 -rw-r--r--  1 jcprime  staff    364  4 Oct 16:27 structure_splodge.sh
  8 -rw-r--r--  1 jcprime  staff   1467  4 Oct 16:27 tabulate_energies.sh
  8 -rw-r--r--  1 jcprime  staff   1234  4 Oct 16:27 tex_edit.sh
  8 -rw-r--r--  1 jcprime  staff    237  4 Oct 16:27 tex_to_txt.sh

./bin/xenon/gdis:
total 7568
  96 -rw-r--r--   1 jcprime  staff    47765  4 Oct 16:27 GDIS.icns
 672 -rw-r--r--@  1 jcprime  staff        0  4 Oct 16:27 Icon
   0 drwxr-xr-x@  5 jcprime  staff      170  4 Oct 16:27 case_studies
   0 drwxr-xr-x@  5 jcprime  staff      170  4 Oct 16:27 etc
   8 -rw-r--r--   1 jcprime  staff       62  4 Oct 16:27 gdis.bat
  80 -rw-r--r--   1 jcprime  staff    40505  4 Oct 16:27 gdis.elements
6600 -rw-r--r--   1 jcprime  staff  3378802  4 Oct 16:27 gdis.exe
  48 -rw-r--r--   1 jcprime  staff    23441  4 Oct 16:27 gdis.library
  64 -rw-r--r--   1 jcprime  staff    32401  4 Oct 16:27 gdis.manual
   0 drwxr-xr-x@ 60 jcprime  staff     2040  4 Oct 16:27 lib
   0 drwxr-xr-x@ 94 jcprime  staff     3196  4 Oct 16:27 models

./bin/xenon/gdis/case_studies:
total 1584
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:27 Icon
904 -rw-r--r--  1 jcprime  staff  460904  4 Oct 16:27 si_bulk.RHO
  8 -rw-r--r--  1 jcprime  staff    1224  4 Oct 16:27 si_bulk.fdf

./bin/xenon/gdis/etc:
total 672
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon
  0 drwxr-xr-x@ 6 jcprime  staff  204  4 Oct 16:27 gtk-2.0
  0 drwxr-xr-x@ 4 jcprime  staff  136  4 Oct 16:27 pango

./bin/xenon/gdis/etc/gtk-2.0:
total 696
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff  3614  4 Oct 16:27 gdk-pixbuf.loaders
  8 -rw-r--r--  1 jcprime  staff    37  4 Oct 16:27 gtk.immodules
  8 -rw-r--r--  1 jcprime  staff   890  4 Oct 16:27 im-multipress.conf

./bin/xenon/gdis/etc/pango:
total 680
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff  163  4 Oct 16:27 pango.modules

./bin/xenon/gdis/lib:
total 68808
   16 -rw-r--r--  1 jcprime  staff      8011  4 Oct 16:27 COLORS.INC
    8 -rw-r--r--  1 jcprime  staff      1363  4 Oct 16:27 FINISH.INC
    8 -rw-r--r--  1 jcprime  staff      2979  4 Oct 16:27 GLASS.INC
   24 -rw-r--r--  1 jcprime  staff      8855  4 Oct 16:27 GOLDS.INC
  672 -rw-r--r--@ 1 jcprime  staff         0  4 Oct 16:27 Icon
 3056 -rw-r--r--  1 jcprime  staff   1564672  4 Oct 16:27 POVRAY.EXE
    8 -rw-r--r--  1 jcprime  staff      1214  4 Oct 16:27 README
   64 -rw-r--r--  1 jcprime  staff     30665  4 Oct 16:27 TEXTURES.INC
    0 drwxr-xr-x@ 5 jcprime  staff       170  4 Oct 16:27 certificates
 2536 -rw-r--r--  1 jcprime  staff   1295582  4 Oct 16:27 cygwin1.dll
  824 -rw-r--r--  1 jcprime  staff    418304  4 Oct 16:27 exchndl.dll
   56 -rw-r--r--  1 jcprime  staff     24906  4 Oct 16:27 gdk-pixbuf-query-loaders.exe
   48 -rw-r--r--  1 jcprime  staff     24264  4 Oct 16:27 gspawn-win32-helper-console.exe
   56 -rw-r--r--  1 jcprime  staff     25718  4 Oct 16:27 gspawn-win32-helper.exe
    0 drwxr-xr-x@ 5 jcprime  staff       170  4 Oct 16:27 gtk-2.0
   56 -rw-r--r--  1 jcprime  staff     26199  4 Oct 16:27 gtk-query-immodules-2.0.exe
25744 -rw-r--r--  1 jcprime  staff  13178783  4 Oct 16:27 init.jar
  208 -rw-r--r--  1 jcprime  staff    104861  4 Oct 16:27 intl.dll
 1208 -rw-r--r--  1 jcprime  staff    616580  4 Oct 16:27 libasprintf-0.dll
  304 -rw-r--r--  1 jcprime  staff    151812  4 Oct 16:27 libatk-1.0-0.dll
 1552 -rw-r--r--  1 jcprime  staff    794572  4 Oct 16:27 libcairo-2.dll
 2304 -rw-r--r--  1 jcprime  staff   1177600  4 Oct 16:27 libeay32.dll
  360 -rw-r--r--  1 jcprime  staff    181333  4 Oct 16:27 libfontconfig-1.dll
 1632 -rw-r--r--  1 jcprime  staff    833006  4 Oct 16:27 libgdk-win32-2.0-0.dll
  272 -rw-r--r--  1 jcprime  staff    138714  4 Oct 16:27 libgdk_pixbuf-2.0-0.dll
 3744 -rw-r--r--  1 jcprime  staff   1914599  4 Oct 16:27 libgdkglext-win32-1.0-0.dll
 2600 -rw-r--r--  1 jcprime  staff   1330175  4 Oct 16:27 libgettextlib-0-17.dll
  608 -rw-r--r--  1 jcprime  staff    310280  4 Oct 16:27 libgettextpo-0.dll
  552 -rw-r--r--  1 jcprime  staff    281320  4 Oct 16:27 libgettextsrc-0-17.dll
  920 -rw-r--r--  1 jcprime  staff    469447  4 Oct 16:27 libgio-2.0-0.dll
 2016 -rw-r--r--  1 jcprime  staff   1030127  4 Oct 16:27 libglib-2.0-0.dll
   64 -rw-r--r--  1 jcprime  staff     30762  4 Oct 16:27 libgmodule-2.0-0.dll
  584 -rw-r--r--  1 jcprime  staff    295457  4 Oct 16:27 libgobject-2.0-0.dll
   80 -rw-r--r--  1 jcprime  staff     39316  4 Oct 16:27 libgthread-2.0-0.dll
 9464 -rw-r--r--  1 jcprime  staff   4844696  4 Oct 16:27 libgtk-win32-2.0-0.dll
  464 -rw-r--r--  1 jcprime  staff    235362  4 Oct 16:27 libgtkglext-win32-1.0-0.dll
 1760 -rw-r--r--  1 jcprime  staff    898048  4 Oct 16:27 libiconv2.dll
  200 -rw-r--r--  1 jcprime  staff    101888  4 Oct 16:27 libintl3.dll
  632 -rw-r--r--  1 jcprime  staff    321494  4 Oct 16:27 libpango-1.0-0.dll
  160 -rw-r--r--  1 jcprime  staff     80812  4 Oct 16:27 libpangocairo-1.0-0.dll
  552 -rw-r--r--  1 jcprime  staff    278683  4 Oct 16:27 libpangoft2-1.0-0.dll
  192 -rw-r--r--  1 jcprime  staff     97985  4 Oct 16:27 libpangowin32-1.0-0.dll
  400 -rw-r--r--  1 jcprime  staff    202923  4 Oct 16:27 libpng12-0.dll
  456 -rw-r--r--  1 jcprime  staff    232960  4 Oct 16:27 libssl32.dll
  736 -rw-r--r--  1 jcprime  staff    376832  4 Oct 16:27 libtiff3.dll
  144 -rw-r--r--  1 jcprime  staff     73728  4 Oct 16:27 libz.dll
   32 -rw-r--r--  1 jcprime  staff     13032  4 Oct 16:27 metals.inc
   48 -rw-r--r--  1 jcprime  staff     23407  4 Oct 16:27 pango-arabic-ft2.dll
   40 -rw-r--r--  1 jcprime  staff     18405  4 Oct 16:27 pango-basic-ft2.dll
   72 -rw-r--r--  1 jcprime  staff     32797  4 Oct 16:27 pango-basic-win32.dll
   48 -rw-r--r--  1 jcprime  staff     21211  4 Oct 16:27 pango-hebrew-ft2.dll
   72 -rw-r--r--  1 jcprime  staff     36417  4 Oct 16:27 pango-indic-ft2.dll
   56 -rw-r--r--  1 jcprime  staff     26537  4 Oct 16:27 pango-querymodules.exe
   56 -rw-r--r--  1 jcprime  staff     25577  4 Oct 16:27 pango-thai-ft2.dll
  528 -rw-r--r--  1 jcprime  staff    270329  4 Oct 16:27 sggc.jar
    8 -rw-r--r--  1 jcprime  staff       362  4 Oct 16:27 slcs-client.properties
  392 -rw-r--r--  1 jcprime  staff    200704  4 Oct 16:27 ssleay32.dll
  112 -rw-r--r--  1 jcprime  staff     55808  4 Oct 16:27 zlib1.dll

./bin/xenon/gdis/lib/certificates:
total 688
  8 -rw-r--r--  1 jcprime  staff  2594  4 Oct 16:27 1e12d831.0
  8 -rw-r--r--  1 jcprime  staff   600  4 Oct 16:27 1e12d831.signing_policy
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon

./bin/xenon/gdis/lib/gtk-2.0:
total 672
  0 drwxr-xr-x@ 5 jcprime  staff  170  4 Oct 16:27 2.10.0
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon
  0 drwxr-xr-x@ 4 jcprime  staff  136  4 Oct 16:27 modules

./bin/xenon/gdis/lib/gtk-2.0/2.10.0:
total 672
672 -rw-r--r--@  1 jcprime  staff    0  4 Oct 16:27 Icon
  0 drwxr-xr-x@  5 jcprime  staff  170  4 Oct 16:27 engines
  0 drwxr-xr-x@ 18 jcprime  staff  612  4 Oct 16:27 loaders

./bin/xenon/gdis/lib/gtk-2.0/2.10.0/engines:
total 960
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
112 -rw-r--r--  1 jcprime  staff  53537  4 Oct 16:27 libpixmap.dll
176 -rw-r--r--  1 jcprime  staff  88617  4 Oct 16:27 libwimp.dll

./bin/xenon/gdis/lib/gtk-2.0/2.10.0/loaders:
total 1536
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
 56 -rw-r--r--  1 jcprime  staff  28560  4 Oct 16:27 libpixbufloader-ani.dll
 56 -rw-r--r--  1 jcprime  staff  27492  4 Oct 16:27 libpixbufloader-bmp.dll
 88 -rw-r--r--  1 jcprime  staff  41827  4 Oct 16:27 libpixbufloader-gif.dll
 48 -rw-r--r--  1 jcprime  staff  20750  4 Oct 16:27 libpixbufloader-icns.dll
 56 -rw-r--r--  1 jcprime  staff  27004  4 Oct 16:27 libpixbufloader-ico.dll
 72 -rw-r--r--  1 jcprime  staff  33364  4 Oct 16:27 libpixbufloader-jpeg.dll
 48 -rw-r--r--  1 jcprime  staff  21329  4 Oct 16:27 libpixbufloader-pcx.dll
 72 -rw-r--r--  1 jcprime  staff  35326  4 Oct 16:27 libpixbufloader-png.dll
 48 -rw-r--r--  1 jcprime  staff  23528  4 Oct 16:27 libpixbufloader-pnm.dll
 40 -rw-r--r--  1 jcprime  staff  18354  4 Oct 16:27 libpixbufloader-ras.dll
 48 -rw-r--r--  1 jcprime  staff  23858  4 Oct 16:27 libpixbufloader-tga.dll
 56 -rw-r--r--  1 jcprime  staff  28334  4 Oct 16:27 libpixbufloader-tiff.dll
 40 -rw-r--r--  1 jcprime  staff  17895  4 Oct 16:27 libpixbufloader-wbmp.dll
 48 -rw-r--r--  1 jcprime  staff  23851  4 Oct 16:27 libpixbufloader-xbm.dll
 88 -rw-r--r--  1 jcprime  staff  41060  4 Oct 16:27 libpixbufloader-xpm.dll

./bin/xenon/gdis/lib/gtk-2.0/modules:
total 1568
672 -rw-r--r--@ 1 jcprime  staff       0  4 Oct 16:27 Icon
896 -rw-r--r--  1 jcprime  staff  456663  4 Oct 16:27 libgail.dll

./bin/xenon/gdis/models:
total 6144
  64 -rw-r--r--  1 jcprime  staff    29484  4 Oct 16:27 1FUF.gin
  16 -rw-r--r--  1 jcprime  staff     4563  4 Oct 16:27 37230-ICSD.dmol
 672 -rw-r--r--@ 1 jcprime  staff        0  4 Oct 16:27 Icon
   8 -rw-r--r--  1 jcprime  staff     2375  4 Oct 16:27 adp1.cif
   8 -rw-r--r--  1 jcprime  staff     2264  4 Oct 16:27 adp2.cif
   8 -rw-r--r--  1 jcprime  staff      564  4 Oct 16:27 al2o3_BFDH.gmf
   8 -rw-r--r--  1 jcprime  staff     1200  4 Oct 16:27 aloh4-.car
   8 -rw-r--r--  1 jcprime  staff      795  4 Oct 16:27 aloh4-.fdf
   8 -rw-r--r--  1 jcprime  staff      609  4 Oct 16:27 arag.gin
   8 -rw-r--r--  1 jcprime  staff     1189  4 Oct 16:27 arag_full
  80 -rw-r--r--  1 jcprime  staff    37174  4 Oct 16:27 baso4-011.mvnout
   8 -rw-r--r--  1 jcprime  staff     1320  4 Oct 16:27 baso4.gin
   8 -rw-r--r--  1 jcprime  staff      326  4 Oct 16:27 box.gin
   8 -rw-r--r--  1 jcprime  staff     2020  4 Oct 16:27 burk1.cif
   8 -rw-r--r--  1 jcprime  staff     2020  4 Oct 16:27 burk2.cif
   8 -rw-r--r--  1 jcprime  staff     2020  4 Oct 16:27 burk3.cif
   8 -rw-r--r--  1 jcprime  staff     3732  4 Oct 16:27 burkeite_p1.xtl
   8 -rw-r--r--  1 jcprime  staff     2013  4 Oct 16:27 butane.gin
   8 -rw-r--r--  1 jcprime  staff     2098  4 Oct 16:27 butanol.gin
 152 -rw-r--r--  1 jcprime  staff    76235  4 Oct 16:27 caco3-5116.mvnout
  32 -rw-r--r--  1 jcprime  staff    15163  4 Oct 16:27 caco3-5116.xtl
   8 -rw-r--r--  1 jcprime  staff     1132  4 Oct 16:27 caco3.gin
   8 -rw-r--r--  1 jcprime  staff      271  4 Oct 16:27 calc2d.gin
   8 -rw-r--r--  1 jcprime  staff      596  4 Oct 16:27 calc2d.xtl
   8 -rw-r--r--  1 jcprime  staff     1644  4 Oct 16:27 calcite.fdf
   8 -rw-r--r--  1 jcprime  staff     2029  4 Oct 16:27 calcite.gin
  64 -rw-r--r--  1 jcprime  staff    28885  4 Oct 16:27 calcite_-114_0.0000.gin
 128 -rw-r--r--  1 jcprime  staff    61485  4 Oct 16:27 calcite_-114_0.0000.got
  72 -rw-r--r--  1 jcprime  staff    33174  4 Oct 16:27 calcite_-114_0.0000.res
  24 -rw-r--r--  1 jcprime  staff     8542  4 Oct 16:27 calcite_0-14_0.5000.gin
  48 -rw-r--r--  1 jcprime  staff    23251  4 Oct 16:27 calcite_0-14_0.5000.got
  24 -rw-r--r--  1 jcprime  staff     9846  4 Oct 16:27 calcite_0-14_0.5000.res
  64 -rw-r--r--  1 jcprime  staff    28881  4 Oct 16:27 calcite_104_0.0000.gin
 128 -rw-r--r--  1 jcprime  staff    61485  4 Oct 16:27 calcite_104_0.0000.got
  72 -rw-r--r--  1 jcprime  staff    33174  4 Oct 16:27 calcite_104_0.0000.res
   8 -rw-r--r--  1 jcprime  staff     2530  4 Oct 16:27 canamgk.gin
  32 -rw-r--r--  1 jcprime  staff    13993  4 Oct 16:27 caox.cif
   8 -rw-r--r--  1 jcprime  staff     3265  4 Oct 16:27 caox.gin
   8 -rw-r--r--  1 jcprime  staff      225  4 Oct 16:27 carbon.xtl
   8 -rw-r--r--  1 jcprime  staff      264  4 Oct 16:27 carbonate.aims
   8 -rw-r--r--  1 jcprime  staff      401  4 Oct 16:27 carbonate.car
   8 -rw-r--r--  1 jcprime  staff      293  4 Oct 16:27 carbonate.gin
   8 -rw-r--r--  1 jcprime  staff      807  4 Oct 16:27 caso4.gin
   8 -rw-r--r--  1 jcprime  staff     1757  4 Oct 16:27 caso4_DH.gin
   8 -rw-r--r--  1 jcprime  staff     2031  4 Oct 16:27 caso4_HH.gin
   8 -rw-r--r--  1 jcprime  staff      273  4 Oct 16:27 damo.xml
  64 -rw-r--r--  1 jcprime  staff    28839  4 Oct 16:27 dummy.gin
  96 -rw-r--r--  1 jcprime  staff    46321  4 Oct 16:27 dummy.got
  40 -rw-r--r--  1 jcprime  staff    20395  4 Oct 16:27 dummy_0.gin
   8 -rw-r--r--  1 jcprime  staff     1443  4 Oct 16:27 ethane.gin
   8 -rw-r--r--  1 jcprime  staff     2334  4 Oct 16:27 full.car
  16 -rw-r--r--  1 jcprime  staff     4714  4 Oct 16:27 gibb.car
   8 -rw-r--r--  1 jcprime  staff     1776  4 Oct 16:27 gibb.gin
  16 -rw-r--r--  1 jcprime  staff     4439  4 Oct 16:27 gibb_001.gin
   8 -rw-r--r--  1 jcprime  staff     1664  4 Oct 16:27 gibb_best3.gin
  80 -rw-r--r--  1 jcprime  staff    38857  4 Oct 16:27 gibb_best3.got
   8 -rw-r--r--  1 jcprime  staff     2882  4 Oct 16:27 gibb_opt.res
 184 -rw-r--r--  1 jcprime  staff    93925  4 Oct 16:27 gibb_opt_bulk.arc
   8 -rw-r--r--  1 jcprime  staff      900  4 Oct 16:27 gibb_re.xtl
   8 -rw-r--r--  1 jcprime  staff      656  4 Oct 16:27 gibbsite_BFDH.gmf
 528 -rw-r--r--  1 jcprime  staff   267783  4 Oct 16:27 gmon.out
  16 -rw-r--r--  1 jcprime  staff     6563  4 Oct 16:27 la_rdefe_clus.car
   8 -rw-r--r--  1 jcprime  staff     1468  4 Oct 16:27 liox.gin
   8 -rw-r--r--  1 jcprime  staff     1255  4 Oct 16:27 methane.gin
   8 -rw-r--r--  1 jcprime  staff     1387  4 Oct 16:27 mgo.gin
   8 -rw-r--r--  1 jcprime  staff      735  4 Oct 16:27 model_0.gin
  24 -rw-r--r--  1 jcprime  staff     8944  4 Oct 16:27 model_0.got
   8 -rw-r--r--  1 jcprime  staff     1292  4 Oct 16:27 model_0.res
   8 -rw-r--r--  1 jcprime  staff     3620  4 Oct 16:27 multi12.gin
   8 -rw-r--r--  1 jcprime  staff     1046  4 Oct 16:27 naox.gin
   8 -rw-r--r--  1 jcprime  staff     1163  4 Oct 16:27 napth.xtl
   8 -rw-r--r--  1 jcprime  staff     3751  4 Oct 16:27 napth_full.gin
   8 -rw-r--r--  1 jcprime  staff     1491  4 Oct 16:27 napth_re.gin
   8 -rw-r--r--  1 jcprime  staff     1142  4 Oct 16:27 natrite.gin
   8 -rw-r--r--  1 jcprime  staff     2673  4 Oct 16:27 natrite_full8.gin
  16 -rw-r--r--  1 jcprime  staff     4992  4 Oct 16:27 natrite_gulpfull.gin
   8 -rw-r--r--  1 jcprime  staff     1515  4 Oct 16:27 new_file.fdf
  16 -rw-r--r--  1 jcprime  staff     5358  4 Oct 16:27 oxalate.gin
2848 -rw-r--r--  1 jcprime  staff  1456776  4 Oct 16:27 si_cubic3_tzp.RHO.cube
   8 -rw-r--r--  1 jcprime  staff      910  4 Oct 16:27 sio2.res
   8 -rw-r--r--  1 jcprime  staff      697  4 Oct 16:27 so4_iso.gin
   8 -rw-r--r--  1 jcprime  staff     1354  4 Oct 16:27 spectrum.txt
   8 -rw-r--r--  1 jcprime  staff     1239  4 Oct 16:27 test1.gin
   8 -rw-r--r--  1 jcprime  staff     1195  4 Oct 16:27 test2.cif
   8 -rw-r--r--  1 jcprime  staff     1150  4 Oct 16:27 test2.gin
   8 -rw-r--r--  1 jcprime  staff     1772  4 Oct 16:27 test2.pdb
   8 -rw-r--r--  1 jcprime  staff     1671  4 Oct 16:27 trona.gin
   8 -rw-r--r--  1 jcprime  staff     1476  4 Oct 16:27 urea.gin
  32 -rw-r--r--  1 jcprime  staff    14556  4 Oct 16:27 urea_001_cut1.gin
   8 -rw-r--r--  1 jcprime  staff      674  4 Oct 16:27 water.car
   8 -rw-r--r--  1 jcprime  staff     1201  4 Oct 16:27 zircon.gin
   8 -rw-r--r--  1 jcprime  staff      481  4 Oct 16:27 zircon_BFDH.gmf

./bin/xenon/jmol:
total 121424
  776 -rw-r--r--  1 jcprime  staff    396847  4 Oct 16:27 CHANGES.txt
    8 -rw-r--r--  1 jcprime  staff      2841  4 Oct 16:27 COPYRIGHT.txt
  672 -rw-r--r--@ 1 jcprime  staff         0  4 Oct 16:27 Icon
 1544 -rw-r--r--  1 jcprime  staff    788817  4 Oct 16:27 JSpecView.jar
11056 -rw-r--r--  1 jcprime  staff   5658194  4 Oct 16:27 Jmol.jar
 8408 -rw-r--r--  1 jcprime  staff   4303489  4 Oct 16:27 JmolData.jar
 4768 -rw-r--r--  1 jcprime  staff   2437151  4 Oct 16:27 JmolLib.jar
   32 -rw-r--r--  1 jcprime  staff     12651  4 Oct 16:27 LEAME.txt
   56 -rw-r--r--  1 jcprime  staff     26936  4 Oct 16:27 LICENSE.txt
   24 -rw-r--r--  1 jcprime  staff     10868  4 Oct 16:27 README.txt
    8 -rw-r--r--  1 jcprime  staff      1151  4 Oct 16:27 build.README.txt
    8 -rw-r--r--  1 jcprime  staff       566  4 Oct 16:27 jmol
    8 -rw-r--r--  1 jcprime  staff       183  4 Oct 16:27 jmol.bat
    8 -rw-r--r--  1 jcprime  staff      2935  4 Oct 16:27 jmol.mac
    8 -rw-r--r--  1 jcprime  staff       849  4 Oct 16:27 jmol.sh
94040 -rw-r--r--  1 jcprime  staff  48145600  4 Oct 16:27 jsmol.zip

./bin/xenon/jw:
total 760
  8 -rw-r--r--  1 jcprime  staff   1247  4 Oct 16:27 .alias
  8 -rw-r--r--  1 jcprime  staff    220  4 Oct 16:27 .bash_logout
  8 -rw-r--r--  1 jcprime  staff   3184  4 Oct 16:27 .bashrc
  0 drwxr-xr-x@ 4 jcprime  staff    136  4 Oct 16:27 .fontconfig
 48 -rw-r--r--  1 jcprime  staff  23345  4 Oct 16:27 .gdisrc
  8 -rw-r--r--  1 jcprime  staff    683  4 Oct 16:27 .login
  8 -rw-r--r--  1 jcprime  staff    675  4 Oct 16:27 .profile
  0 drwxr-xr-x@ 3 jcprime  staff    102  4 Oct 16:27 .ssh
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  0 drwxr-xr-x@ 4 jcprime  staff    136  4 Oct 16:27 MCSubs

./bin/xenon/jw/.fontconfig:
total 680
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff  104  4 Oct 16:27 cabbd14511b9e8a55e92af97fb3a0461-le64.cache-3

./bin/xenon/jw/.ssh:
total 672
672 -rw-r--r--@ 1 jcprime  staff  0  4 Oct 16:27 Icon

./bin/xenon/jw/MCSubs:
total 672
  0 drwxr-xr-x@ 4 jcprime  staff  136  4 Oct 16:27 Code
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon

./bin/xenon/jw/MCSubs/Code:
total 672
672 -rw-r--r--@  1 jcprime  staff     0  4 Oct 16:27 Icon
  0 drwxr-xr-x@ 41 jcprime  staff  1394  4 Oct 16:27 Update

./bin/xenon/jw/MCSubs/Code/Update:
total 1280
672 -rw-r--r--@  1 jcprime  staff      0  4 Oct 16:27 Icon
  0 drwxr-xr-x@ 38 jcprime  staff   1292  4 Oct 16:27 Work_01_01_2004
  0 drwxr-xr-x@ 38 jcprime  staff   1292  4 Oct 16:27 Work_05_01_2004
  8 -rw-r--r--   1 jcprime  staff   2545  4 Oct 16:27 X_pos.c
  8 -rw-r--r--   1 jcprime  staff   1314  4 Oct 16:27 atom_separation_squared.c
 24 -rw-r--r--   1 jcprime  staff   9041  4 Oct 16:27 cart_latt_vecs.c
  8 -rw-r--r--   1 jcprime  staff   1174  4 Oct 16:27 cart_to_fract.c
  8 -rw-r--r--   1 jcprime  staff    362  4 Oct 16:27 compare_strings.c
  8 -rw-r--r--   1 jcprime  staff    149  4 Oct 16:27 constants.h
 24 -rw-r--r--   1 jcprime  staff   8200  4 Oct 16:27 data.h
  8 -rw-r--r--   1 jcprime  staff    801  4 Oct 16:27 ewald.h
  8 -rw-r--r--   1 jcprime  staff   1112  4 Oct 16:27 fract_to_cart.c
  8 -rw-r--r--   1 jcprime  staff   2776  4 Oct 16:27 generate_neighbours.c
  8 -rw-r--r--   1 jcprime  staff   3454  4 Oct 16:27 global_values.h
  8 -rw-r--r--   1 jcprime  staff   2360  4 Oct 16:27 h_pos.c
  8 -rw-r--r--   1 jcprime  staff   2329  4 Oct 16:27 header.h
  8 -rw-r--r--   1 jcprime  staff   1152  4 Oct 16:27 index_sort.c
  8 -rw-r--r--   1 jcprime  staff    869  4 Oct 16:27 locate_string.c
  8 -rw-r--r--   1 jcprime  staff   2278  4 Oct 16:27 makefile
  8 -rw-r--r--   1 jcprime  staff    875  4 Oct 16:27 maxima.h
144 -rw-r--r--   1 jcprime  staff  69903  4 Oct 16:27 mc_subs
120 -rw-r--r--   1 jcprime  staff  59664  4 Oct 16:27 mc_subs.c
  8 -rw-r--r--   1 jcprime  staff   1946  4 Oct 16:27 min_image.c
  8 -rw-r--r--   1 jcprime  staff    797  4 Oct 16:27 orig_atom_separation_squared.c
  8 -rw-r--r--   1 jcprime  staff    702  4 Oct 16:27 own_maths.h
  8 -rw-r--r--   1 jcprime  staff   2042  4 Oct 16:27 potent.c
  8 -rw-r--r--   1 jcprime  staff    709  4 Oct 16:27 print_molecule.c
  8 -rw-r--r--   1 jcprime  staff   1050  4 Oct 16:27 print_neighbours.c
 48 -rw-r--r--   1 jcprime  staff  23601  4 Oct 16:27 reader.c
  8 -rw-r--r--   1 jcprime  staff   2312  4 Oct 16:27 reader.h
  8 -rw-r--r--   1 jcprime  staff   1599  4 Oct 16:27 real_random.c
  8 -rw-r--r--   1 jcprime  staff   1111  4 Oct 16:27 save_config.c
  8 -rw-r--r--   1 jcprime  staff   2293  4 Oct 16:27 save_gulpfile.c
  8 -rw-r--r--   1 jcprime  staff   1228  4 Oct 16:27 save_xtlfile.c
  8 -rw-r--r--   1 jcprime  staff   2455  4 Oct 16:27 set_elem.c
  8 -rw-r--r--   1 jcprime  staff    887  4 Oct 16:27 standard_bond.c
  8 -rw-r--r--   1 jcprime  staff   2088  4 Oct 16:27 structures.h
  8 -rw-r--r--   1 jcprime  staff    340  4 Oct 16:27 vec_cross.c
  8 -rw-r--r--   1 jcprime  staff    262  4 Oct 16:27 vec_dot.c

./bin/xenon/jw/MCSubs/Code/Update/Work_01_01_2004:
total 1112
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff   2545  4 Oct 16:27 X_pos.c
  8 -rw-r--r--  1 jcprime  staff   1314  4 Oct 16:27 atom_separation_squared.c
 24 -rw-r--r--  1 jcprime  staff   9041  4 Oct 16:27 cart_latt_vecs.c
  8 -rw-r--r--  1 jcprime  staff   1174  4 Oct 16:27 cart_to_fract.c
  8 -rw-r--r--  1 jcprime  staff    362  4 Oct 16:27 compare_strings.c
  8 -rw-r--r--  1 jcprime  staff    149  4 Oct 16:27 constants.h
 16 -rw-r--r--  1 jcprime  staff   8051  4 Oct 16:27 data.h
  8 -rw-r--r--  1 jcprime  staff    801  4 Oct 16:27 ewald.h
  8 -rw-r--r--  1 jcprime  staff   1112  4 Oct 16:27 fract_to_cart.c
  8 -rw-r--r--  1 jcprime  staff   2776  4 Oct 16:27 generate_neighbours.c
  8 -rw-r--r--  1 jcprime  staff   3454  4 Oct 16:27 global_values.h
  8 -rw-r--r--  1 jcprime  staff   2360  4 Oct 16:27 h_pos.c
  8 -rw-r--r--  1 jcprime  staff   2269  4 Oct 16:27 header.h
  8 -rw-r--r--  1 jcprime  staff   1152  4 Oct 16:27 index_sort.c
  8 -rw-r--r--  1 jcprime  staff    869  4 Oct 16:27 locate_string.c
  8 -rw-r--r--  1 jcprime  staff   2278  4 Oct 16:27 makefile
  8 -rw-r--r--  1 jcprime  staff    875  4 Oct 16:27 maxima.h
104 -rw-r--r--  1 jcprime  staff  53108  4 Oct 16:27 mc_subs.c
  8 -rw-r--r--  1 jcprime  staff   1946  4 Oct 16:27 min_image.c
  8 -rw-r--r--  1 jcprime  staff    797  4 Oct 16:27 orig_atom_separation_squared.c
  8 -rw-r--r--  1 jcprime  staff    702  4 Oct 16:27 own_maths.h
  8 -rw-r--r--  1 jcprime  staff   2042  4 Oct 16:27 potent.c
  8 -rw-r--r--  1 jcprime  staff    709  4 Oct 16:27 print_molecule.c
  8 -rw-r--r--  1 jcprime  staff   1050  4 Oct 16:27 print_neighbours.c
 48 -rw-r--r--  1 jcprime  staff  22967  4 Oct 16:27 reader.c
  8 -rw-r--r--  1 jcprime  staff   2162  4 Oct 16:27 reader.h
  8 -rw-r--r--  1 jcprime  staff   1599  4 Oct 16:27 real_random.c
  8 -rw-r--r--  1 jcprime  staff   1111  4 Oct 16:27 save_config.c
  8 -rw-r--r--  1 jcprime  staff   2293  4 Oct 16:27 save_gulpfile.c
  8 -rw-r--r--  1 jcprime  staff   1228  4 Oct 16:27 save_xtlfile.c
  8 -rw-r--r--  1 jcprime  staff   2455  4 Oct 16:27 set_elem.c
  8 -rw-r--r--  1 jcprime  staff    887  4 Oct 16:27 standard_bond.c
  8 -rw-r--r--  1 jcprime  staff   2088  4 Oct 16:27 structures.h
  8 -rw-r--r--  1 jcprime  staff    340  4 Oct 16:27 vec_cross.c
  8 -rw-r--r--  1 jcprime  staff    262  4 Oct 16:27 vec_dot.c

./bin/xenon/jw/MCSubs/Code/Update/Work_05_01_2004:
total 1128
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff   2545  4 Oct 16:27 X_pos.c
  8 -rw-r--r--  1 jcprime  staff   1314  4 Oct 16:27 atom_separation_squared.c
 24 -rw-r--r--  1 jcprime  staff   9041  4 Oct 16:27 cart_latt_vecs.c
  8 -rw-r--r--  1 jcprime  staff   1174  4 Oct 16:27 cart_to_fract.c
  8 -rw-r--r--  1 jcprime  staff    362  4 Oct 16:27 compare_strings.c
  8 -rw-r--r--  1 jcprime  staff    149  4 Oct 16:27 constants.h
 16 -rw-r--r--  1 jcprime  staff   8137  4 Oct 16:27 data.h
  8 -rw-r--r--  1 jcprime  staff    801  4 Oct 16:27 ewald.h
  8 -rw-r--r--  1 jcprime  staff   1112  4 Oct 16:27 fract_to_cart.c
  8 -rw-r--r--  1 jcprime  staff   2776  4 Oct 16:27 generate_neighbours.c
  8 -rw-r--r--  1 jcprime  staff   3454  4 Oct 16:27 global_values.h
  8 -rw-r--r--  1 jcprime  staff   2360  4 Oct 16:27 h_pos.c
  8 -rw-r--r--  1 jcprime  staff   2329  4 Oct 16:27 header.h
  8 -rw-r--r--  1 jcprime  staff   1152  4 Oct 16:27 index_sort.c
  8 -rw-r--r--  1 jcprime  staff    869  4 Oct 16:27 locate_string.c
  8 -rw-r--r--  1 jcprime  staff   2278  4 Oct 16:27 makefile
  8 -rw-r--r--  1 jcprime  staff    875  4 Oct 16:27 maxima.h
120 -rw-r--r--  1 jcprime  staff  57857  4 Oct 16:27 mc_subs.c
  8 -rw-r--r--  1 jcprime  staff   1946  4 Oct 16:27 min_image.c
  8 -rw-r--r--  1 jcprime  staff    797  4 Oct 16:27 orig_atom_separation_squared.c
  8 -rw-r--r--  1 jcprime  staff    702  4 Oct 16:27 own_maths.h
  8 -rw-r--r--  1 jcprime  staff   2042  4 Oct 16:27 potent.c
  8 -rw-r--r--  1 jcprime  staff    709  4 Oct 16:27 print_molecule.c
  8 -rw-r--r--  1 jcprime  staff   1050  4 Oct 16:27 print_neighbours.c
 48 -rw-r--r--  1 jcprime  staff  23601  4 Oct 16:27 reader.c
  8 -rw-r--r--  1 jcprime  staff   2312  4 Oct 16:27 reader.h
  8 -rw-r--r--  1 jcprime  staff   1599  4 Oct 16:27 real_random.c
  8 -rw-r--r--  1 jcprime  staff   1111  4 Oct 16:27 save_config.c
  8 -rw-r--r--  1 jcprime  staff   2293  4 Oct 16:27 save_gulpfile.c
  8 -rw-r--r--  1 jcprime  staff   1228  4 Oct 16:27 save_xtlfile.c
  8 -rw-r--r--  1 jcprime  staff   2455  4 Oct 16:27 set_elem.c
  8 -rw-r--r--  1 jcprime  staff    887  4 Oct 16:27 standard_bond.c
  8 -rw-r--r--  1 jcprime  staff   2088  4 Oct 16:27 structures.h
  8 -rw-r--r--  1 jcprime  staff    340  4 Oct 16:27 vec_cross.c
  8 -rw-r--r--  1 jcprime  staff    262  4 Oct 16:27 vec_dot.c

./bin/xenon/scott:
total 696
672 -rw-r--r--@  1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-r--r--   1 jcprime  staff   114  4 Oct 16:27 dmol3.exe
  0 drwxr-xr-x@ 11 jcprime  staff   374  4 Oct 16:27 template
  8 -rw-r--r--   1 jcprime  staff  1166  4 Oct 16:27 tobedatamined.sh
  8 -rw-r--r--   1 jcprime  staff   864  4 Oct 16:27 top10_for_datamining.sh

./bin/xenon/scott/template:
total 800
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff     31  4 Oct 16:27 KLMC.err
 80 -rw-r--r--  1 jcprime  staff  38326  4 Oct 16:27 KLMC.out
  0 drwxr-xr-x@ 9 jcprime  staff    306  4 Oct 16:27 data
  8 -rw-r--r--  1 jcprime  staff   1800  4 Oct 16:27 job.error
 24 -rw-r--r--  1 jcprime  staff  11038  4 Oct 16:27 job.output
  8 -rw-r--r--  1 jcprime  staff    351  4 Oct 16:27 jobSubmit.sh
  0 drwxr-xr-x@ 3 jcprime  staff    102  4 Oct 16:27 restart
  0 drwxr-xr-x@ 3 jcprime  staff    102  4 Oct 16:27 run

./bin/xenon/scott/template/data:
total 736
 24 -rw-r--r--  1 jcprime  staff  12288  4 Oct 16:27 .run.job.swp
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff   1047  4 Oct 16:27 Master.dmol3
  8 -rw-r--r--  1 jcprime  staff   1318  4 Oct 16:27 Master.gin
  8 -rw-r--r--  1 jcprime  staff   3056  4 Oct 16:27 atoms.in
  8 -rw-r--r--  1 jcprime  staff      8  4 Oct 16:27 jobs
  8 -rw-r--r--  1 jcprime  staff   2278  4 Oct 16:27 run.job

./bin/xenon/scott/template/restart:
total 672
672 -rw-r--r--@ 1 jcprime  staff  0  4 Oct 16:27 Icon

./bin/xenon/scott/template/run:
total 672
672 -rw-r--r--@ 1 jcprime  staff  0  4 Oct 16:27 Icon

./bin/xenon/testdisk-7.0:
total 14688
   8 -rw-r--r--  1 jcprime  staff      216  4 Oct 16:27 AUTHORS
   8 -rw-r--r--  1 jcprime  staff     2049  4 Oct 16:27 Android.mk
  40 -rw-r--r--  1 jcprime  staff    17987  4 Oct 16:27 COPYING
 368 -rw-r--r--  1 jcprime  staff   188377  4 Oct 16:27 ChangeLog
   8 -rw-r--r--  1 jcprime  staff      117  4 Oct 16:27 INFO
 672 -rw-r--r--@ 1 jcprime  staff        0  4 Oct 16:27 Icon
  40 -rw-r--r--  1 jcprime  staff    18573  4 Oct 16:27 NEWS
   8 -rw-r--r--  1 jcprime  staff     2085  4 Oct 16:27 README
   8 -rw-r--r--  1 jcprime  staff      344  4 Oct 16:27 THANKS
   8 -rw-r--r--  1 jcprime  staff       34  4 Oct 16:27 VERSION
   8 -rw-r--r--  1 jcprime  staff      312  4 Oct 16:27 documentation.html
   8 -rw-r--r--  1 jcprime  staff      898  4 Oct 16:27 fidentify.8
2600 -rw-r--r--  1 jcprime  staff  1328008  4 Oct 16:27 fidentify_static
   0 drwxr-xr-x@ 9 jcprime  staff      306  4 Oct 16:27 icons
   0 drwxr-xr-x@ 4 jcprime  staff      136  4 Oct 16:27 jni
   0 drwxr-xr-x@ 4 jcprime  staff      136  4 Oct 16:27 l
   8 -rw-r--r--  1 jcprime  staff     1165  4 Oct 16:27 photorec.8
5688 -rw-r--r--  1 jcprime  staff  2909784  4 Oct 16:27 photorec_static
   8 -rw-r--r--  1 jcprime  staff     1021  4 Oct 16:27 qphotorec.8
   8 -rw-r--r--  1 jcprime  staff      299  4 Oct 16:27 readme.txt
   8 -rw-r--r--  1 jcprime  staff     1750  4 Oct 16:27 testdisk.8
5184 -rw-r--r--  1 jcprime  staff  2651160  4 Oct 16:27 testdisk_static

./bin/xenon/testdisk-7.0/icons:
total 736
  0 drwxr-xr-x@ 4 jcprime  staff    136  4 Oct 16:27 48x48
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff    604  4 Oct 16:27 Makefile.am
 32 -rw-r--r--  1 jcprime  staff  15257  4 Oct 16:27 Makefile.in
  8 -rw-r--r--  1 jcprime  staff   2238  4 Oct 16:27 photorec.ico
  0 drwxr-xr-x@ 4 jcprime  staff    136  4 Oct 16:27 scalable
 16 -rw-r--r--  1 jcprime  staff   7406  4 Oct 16:27 testdisk.ico

./bin/xenon/testdisk-7.0/icons/48x48:
total 672
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon
  0 drwxr-xr-x@ 4 jcprime  staff  136  4 Oct 16:27 apps

./bin/xenon/testdisk-7.0/icons/48x48/apps:
total 680
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff  3492  4 Oct 16:27 qphotorec.png

./bin/xenon/testdisk-7.0/icons/scalable:
total 672
672 -rw-r--r--@ 1 jcprime  staff    0  4 Oct 16:27 Icon
  0 drwxr-xr-x@ 4 jcprime  staff  136  4 Oct 16:27 apps

./bin/xenon/testdisk-7.0/icons/scalable/apps:
total 696
672 -rw-r--r--@ 1 jcprime  staff      0  4 Oct 16:27 Icon
 24 -rw-r--r--  1 jcprime  staff  10617  4 Oct 16:27 qphotorec.svg

./bin/xenon/testdisk-7.0/jni:
total 680
  8 -rw-r--r--  1 jcprime  staff  2392  4 Oct 16:27 Android.mk
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon

./bin/xenon/testdisk-7.0/l:
total 680
672 -rw-r--r--@ 1 jcprime  staff     0  4 Oct 16:27 Icon
  8 -rw-r--r--  1 jcprime  staff  1794  4 Oct 16:27 linux
