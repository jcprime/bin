#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
howsoftisit.py

Maps softness levels over a given shape (the grid of which has been manually compared for softness)
"""

