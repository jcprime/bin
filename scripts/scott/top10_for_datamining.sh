#!/bin/bash
# top_10_for_datamining.sh
# Finds the top 10 structures for each MX (as below) from the r1000 run
# Copies them into the dmol/datamine/ directory to be datamined
# NEED TO STILL DO SIMILARLY FOR OUTPUT DMOL STRUCTURES!

path="james_p_rsync/dmol"
path2="Desktop/structures"

MX=("LiF" "NaCl" "KF" "RbBr")

argc=$#
if [[ "$argc" == 0 ]]; then
 a="0"
 while [[ "$a" -lt "4" ]]; do #Loops over MX
  for i in {1..24}; do
   cd ~/"$path"/"${MX[a]}"_"$i"/top_structures/ #Move to directory containing the statistics file, with structure ID in order of rank
   file=./statistics #File with ranked structures and energies in order
   for rank in {1..10}; do
    cp ~/"$path2"/"${MX[a]}"_"$i"_lm"$rank"_r1000.xyz ~/"$path"/datamine/"${MX[a]}"_"$i"_lm"$rank".xyz
   done
  done
  a=$((a+1))
 done
else
 echo "Something's gone wrong.  Sorry."
fi
echo "Finished."
