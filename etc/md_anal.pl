#!/usr/bin/perl -w

# script for getting useful stuff out of .gout files
# includes Pressure*volume info for pressure calcs
# but not average psi calculation.
# command line is: general_info.pl

@Files = glob "*.gout";

print "output filename?\n";
chomp ($txtfile = <STDIN>);
open TXTFILE, ">$txtfile" or die "Can't create $txtfile: $!\n";

printf TXTFILE "%20s%4s%16s%8s%12s%12s%12s%12s%12s%12s\n",
  "Filename", "Temperature", "Energy", "A", "B", "C", "Alpha",
  "Beta", "Gamma","Vol";


foreach $Files (@Files) {
	open OUTPUT, "<$Files" or die "Can't open $Files: $!\n";

	while (<OUTPUT>) {

	  if (/(Energy =\s+) (.\d+.\d+)/) {
	     $energy = $2;
	     $pcalc = 3;
		$pv = 0.0;
	     printf TXTFILE "%16.8f", $energy;
	  } 
	}


	  if (/(       A         \s+) (\d+.\d+)/) {
	     $A = $2;
	     printf TXTFILE "%12.6f",$A;
	  }

	  if (/(       b         \s+) (\d+.\d+)/) {
	     $B = $2;
	     printf TXTFILE "%12.6f", $B;
	  }

	  if (/(       c         \s+) (\d+.\d+)/) {
	     $C = $2;
	     printf TXTFILE "%12.6f", $C;
	  }

	  if (/(       alpha  \s+) (\d+.\d+)/) {
	     $alpha = $2;
	     printf TXTFILE "%12.6f", $alpha;
	  }

	  if (/(       beta   \s+) (\d+.\d+)/) {
	     $beta = $2;
	     printf TXTFILE "%12.6f", $beta;
	  }

	  if (/(       gamma  \s+) (\d+.\d+)/) {
	     $gamma = $2;
	     printf TXTFILE "%12.6f", $gamma;
	  }

	  if (/(Non-primitive cell volume =\s+) (\d+.\d+)/) {
	     $vol = $2;
	     printf TXTFILE "%14.6f", $vol;
	  }
	}
	close OUTPUT;

	printf TXTFILE "%16.8f", $pv;
	
print "Done $Files\n";
}

