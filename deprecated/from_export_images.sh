#!/bin/bash
# This might come in handy later on, so I'm just dumping it here

# In the case that a single argument (filename) is given:
elif [[ "$argc" -eq "1" ]]; then
    # Alters zeo according to zeolite (ie. sco directory leads to scol... files)
    if [[ "$zeolite" == "sco" ]]; then
        zeo="scol"
    else
        zeo="$zeolite"
    fi
# In the case that more than one argument (filename) is given:
elif [[ "argc" -gt "1" ]]; then
    zeolite=("$@")
    if [[ "${zeolite[loop]}" == "sco" ]]; then
        zeo="scol"
    else
        zeo="${zeolite[loop]}"
    fi
