import sys
from pymol import cmd, stored

def shiny(filename):
    """
    DESCRIPTION

    Makes rendering look like how I got 'em looking in Jmol
    """
    # Load filename
    cmd.load(filename)
    # Turn everything to sticks (except sodium)
    #cmd.show_as("sticks")
    #cmd.show_as("nb_spheres", "name Na")
    #cmd.show_as("nb_spheres", "name Ca")
    #cmd.show_as("nb_spheres", "name K")
    # Attempting to replicate a Jmol-y version of PyMOL preset.ball_and_stick(selection='all', mode=1)
    #cmd.set_bond("stick_color", "atomic", "all", "all")
    cmd.set_bond("stick_radius", 0.20, "all", "all")
    cmd.set("sphere_scale", 0.25, "all")
    cmd.show("sticks", "all")
    cmd.show("spheres", "all")
    # Turn off perspective (by turning orthoscopic on)
    cmd.set("orthoscopic", 1)
    # Fit all atoms in frame
    cmd.zoom(complete=1, buffer=2)
    # Axes off
    # Unit cell display off
    # Background white
    cmd.bg_color("white")
    # Make sure all Al-O bonds and Si-O bonds have been created
    # Delete all bonds between Al and water-O
    # Delete all bonds between Si and water-O
    # Set colours
    cmd.set_color("uclgrey", [140, 130, 121])
    cmd.set_color("uclyellow", [246, 190, 0])
    cmd.set_color("ucldblue", [0, 40, 85])
    cmd.set_color("uclgreen", [181, 189, 0])
    cmd.set_color("ucldred", [147, 39, 44])
    # Colour H black (note that hydrogen is a built-in of PyMOL)
    cmd.color("black", "hydrogen")
    # Colour O red
    cmd.color("red", "name O2")
    # Colour Al [x8C8279] (uclgrey)
    cmd.color("uclgrey", "name Al")
    # Colour Si [xF6BE00] (uclyellow)
    cmd.color("uclyellow", "name Si")
    # Colour Na purple
    cmd.color("purple", "name Na")
    # Colour K [x002855] (ucldblue)
    cmd.color("ucldblue", "name K")
    # Colour Ca [xB5BD00] (uclgreen)
    cmd.color("uclgreen", "name Ca")
    # Colour water-O [x93272C] (ucldred)
    cmd.color("ucldred", "name O1")
    # Write POVRAY
    #cmd.ray()
    cmd.set("ray_opaque_background", 1)
    # Set antialiasing to highest (for best-quality images)
    cmd.set("antialias", 2)
    # Write PNG
    cmd.png(filename[:-4] + '.png', width="20cm", dpi=300, ray=1)
    # Quit
    cmd.quit()

cmd.extend("shiny", shiny);
shiny(sys.argv[1])
