// Start of header guard
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

// Defining all functions here because I'm a lazy arse
int reader(char *file_to_read);                                           // Input file reader
double atom_separation_squared(atom *p_A, atom *p_B, int pbc);            // Gives vector square of atomic separation, with minimum image if pbc flag is set
simple_atom h_pos(simple_atom *si1, simple_atom *si2, simple_atom *o);    // Calculates position of H from 2T coordinates and O coordinate (both Cartesian)
simple_atom X_pos(simple_atom *si1, simple_atom *si2, simple_atom *o);    // Calculates position of cation from 2 O coordinates and one T coordinate (both Cartesian)
// NB: "FILE" is a struct in standard library (i.e. not in user-defined headers)
// save_config presumably saves your configuration!
void save_config(FILE *file_fp, int config,
                                int num_replace_atoms,
                                double final_energy,
                                int *p_replaceable_index,
                                int *p_replaceable_done);
// Generates neighbours
void generate_neighbours(atom *p_molecule, int num_atoms,
                                                atom_number *p_types, int *p_num_types,
                                                int use_pbc);             // Searches molecule for neighbour list
void print_neighbours(atom *p_molecule, int num_atoms, FILE *fp);         // Prints neighbour list for molecule
void print_molecule(atom *p_molecule, int num_atoms, FILE *output_fp);    // Prints molecule in .car format
void set_elem(atom *p_molecule, int num_atoms);                           // Sets up element name from GULP input; mode of calculation determined by input file
void steric_setup(atom *p_molecule, int num_atoms);                       // Defined in potent.c; assigns vdW radii and calculates non-bonding parameters
double real_random(int done);                                             // Random number generator, gives a random double 0.0 -> 1.0 using standard library routines random and srandom, with optional seeding on the current calendar time
void index_sort(int n, double *p_arrin, int *p_indx);                     // Sorts array index on the contents of arrin
void save_xtlfile(FILE *file_fp, atom *p_mol,
                                     int num_atoms, char *p_spacegroup);  // Writes .xtl file
void save_gulpfile(FILE *file_fp, char *p_dump_name, atom *p_mol,
                                     int num_atoms, char *p_spacegroup,
                                     char *p_xtlname, char *p_pots_file); // Writes .gin file

// End of header guard
#endif
