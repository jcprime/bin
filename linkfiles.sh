#!/bin/bash

# function dir_links() {
#     from="$1"
#     to="$2"
#     for file in "$from/"*; do
#         if [ -f "$file" ]; then
#             echo "Linking $file" "$to"/"${file#$from/}"
#             #ln -sf "$file" "$to"/"${file#$from/}"
#         fi
#         if [ -d "$file" ]; then
#             echo "Making directory $to"/"${file#$from/}"
#             #mkdir -p "$to"/"${file#$from/}"
#             #cd "$to"/"${file#$from/}"
#             #ln -sf "$file/"* "$to"/"${file#$from/}"/
#             echo 'Calling dir_links' "$file" "$to"/"${file#$from/}"
#             dir_links "$file" "$to"/"${file#$from/}"
#         fi
#     done
#     return 0
# }

function recurse_link() {
    if [ "$#" -eq "2" ]; then
        from="$1"
        to="$2"
    else
        from="$HOME/.dotfiles/arch/.conky"
        to="$HOME/.conky"
    fi
    if [ -f "$file" ]; then
        echo "Linking $file" "$to"/"${file#$from/}"
        #ln -sf "$file" "$to"/"${file#$from/}"
    fi
    if [ -d "$file" ]; then
        while [[ "$?" == "1" ]]; do
            echo "Making directory $to/${file#$from/}"
            #mkdir -p "$to"/"${file#$from/}"
            #cd "$to"/"${file#$from/}"
            #ln -sf "$file/"* "$to"/"${file#$from/}"/
            echo 'Calling recurse_link' "$file" "$to"/"${file#$from/}"
            recurse_link "$file" "$to"/"${file#$from/}"
        done
    fi
    return 0
}

#for file in "$HOME/.dotfiles/arch/.conky/"*; do
#   if [ -d "$file" ]; then
#       mkdir -p "$HOME/.conky/${file#*.conky/}"
#       cd "$HOME/.conky/${file#*.conky/}"
#           ln -sf "$file/"* "$HOME/.conky/${file#*.conky/}"/
#   fi
#   if [ -f "$file" ]; then
#       ln -sf "$file" "$HOME/.conky/${file#*.conky/}"
#   fi
#done

from="$HOME/.dotfiles/arch/.conky"
to="$HOME/.conky"

for file in "$from/"*; do
    recurse_link "$from" "$to"
done
