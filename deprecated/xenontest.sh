#!/bin/bash
# xenontest.sh (based on test.sh, which is itself based on gulpconvert.sh)
#  - Takes cell parameters and fractional coordinates from *SINGLE* GULP input file, formatted with a single space separating them in the string
#  - Reformats them as XYZ-type coordinates using Python script (gulp_coords.py)

# WORKING VERSION, BUT FOR SINGLE FILES ONLY!

# If "-h" or "--help" is given when running the script as only argument, print usage information
if [[ ( "$#" -eq "1" ) && ( ( "$1" == "-h" ) || ( "$1" == "--help" ) ) ]]; then
    echo "Usage: xenontest.sh [coordinate_file] [input_file]"
fi

# Grab input filename via command-line arguments or user input
if [[ "$#" -ge "2" ]]; then
    file="$2" # Was $3 until 17/06/2017
else
    read -p "File to convert please: " file
fi

# Set working environment
work_dir="$(pwd)"
cd "${work_dir}" || exit

# Loop over existing GULP coordinates (in .gin files)
base="${file%%.*}"

# Set output-coordinate file to first argument, if given, or else taken as same basename as original .gin file
if [[ "$#" -ge "1" ]]; then
    coordinate_file="$1"
elif [[ "$#" -eq "0" ]]; then
    coordinate_file="${base}.cp2k"
fi

# Set cp2k-input file to second argument, if given, or else taken as same basename as original .gin file
if [[ "$#" -ge "1" ]]; then
    input_file="$2"
elif [[ "$#" -eq "0" ]]; then
    input_file="${base}.inp"
fi

# Grab cell parameters into a new variable (separated by SINGLE space)
cell=$(grep -A1 'cell' "${file}" | tail -n 1 | tr -s ' ')
echo "Cell parameters: ${cell}"

# Create temporary file
temp_file="${base}.tmp"
> "${temp_file}"

# THIS IS SPECIFIC TO UNIX/LINUX SYSTEMS, AND WON'T WORK ON MAC OS X:
# Output everything including "fractional" and "totalenergy" OR "species" (whichever comes first) to a temporary file for processing in the while loop to follow
sed -n "/^fractional/,/^\(space\|totalenergy\|species\)/p" "${file}" >> "${temp_file}"

frac_file="${base}.frac"
if [ ! -e "${frac_file}" ]; then
    touch "${frac_file}"
elif [ -e "${frac_file}" ]; then
    > "${frac_file}"
fi

# # Remove first and last lines using sed
# sed '1d;$d' "${temp_file}" >> "${frac_file}"
# cat "${frac_file}"

# Grab coordinate lines for CORES ONLY and process them with awk, assuming the form below:
awk ' { if ($1 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } else if ($1 ~ /^[A-Za-z].*/ && $2 ~ "core") { print $1, $3, $4, $5; next } } ' "${temp_file}" >> "${frac_file}"

# Actually do the conversion, via gulp_coords.py
python2.7 ~/bin/gulp_coords.py "${cell}" "${frac_file}"
cart_file="${base}.cart"

# Copy lines with format * number number number (on the assumption that that's all there'll be) into "$coordinate_file"
awk ' { if ($2 ~ /^[0-9].*/ && $3 ~ /^[0-9].*/ && $4 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } } ' "${cart_file}" > "${coordinate_file}"

mkdir "${base}"
# cp "${base}".* "${base}"/
mv "${base}".* "${base}"/
