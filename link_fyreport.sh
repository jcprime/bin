#!/bin/bash
# link_fyreport.sh

for file in gulp dehydrate cp2k gulp_parameters introduction literature_review methodology original_contribution plan_timetable; do
	ln -f ~/Google\ Drive/"$file".tex ~/Desktop/PhD/writings_mac/fyreport/tex_plain/"$file".tex
	chmod go-w ~/Desktop/PhD/writings_mac/fyreport/tex_plain/"$file".tex
done

# From link_overleaf.sh

ln -f ~/Google\ Drive/introduction.tex ~/Desktop/PhD/writings_mac/fyreport/tex_thesis/01_Introduction.tex
ln -f ~/Google\ Drive/literature_review.tex ~/Desktop/PhD/writings_mac/fyreport/tex_thesis/02_Chapter2.tex
ln -f ~/Google\ Drive/methodology.tex ~/Desktop/PhD/writings_mac/fyreport/tex_thesis/03_Chapter3.tex
ln -f ~/Google\ Drive/original_contribution.tex ~/Desktop/PhD/writings_mac/fyreport/tex_thesis/04_Chapter4.tex
ln -f ~/Google\ Drive/plan_timetable.tex ~/Desktop/PhD/writings_mac/fyreport/tex_thesis/05_Chapter5.tex
