for (initialisation, different_initialisation; condition; increase, different_increase)
{
	statement;
	more_statements;
}

for (declaration: range) // eg. declaration could be "char c" and range could be a pre-initialised string "str" (and yes, that should be a colon!)
{
	stuff;
	other_stuff;
}

while (expression)
{
	statement;
	another_statement;
}

do
{
	stuff;
	different_stuff;
} while (expression);

switch (expression)
{
	case constant1:
		group;
		of;
		statements;
		break; // MUST HAVE THIS FOR EVERY CASE PLEASE, TO SAVE THE COMPUTER'S RESOURCES!
	case constant2:
		different;
		group;
		of;
		statements;
		break;
	case and_so_on:
		blah;
		break;
	default:
		default_group;
		of;
		statements;
		break;
}

// Define "prototype" functions before main(), and give actual code after main()
type_of_return_value function_name (type_of_parameter name_of_parameter=default_value_if_no_argument_given, type_of_another_parameter name_of_another_parameter); // Parameters are LOCAL to their function

// Now for the actual functions (assume main() has been and gone)
type_of_return_value function_name (type_of_parameter name_of_parameter=default_value_if_no_argument_given, type_of_another_parameter name_of_another_parameter) // Parameters are LOCAL to their function
{
	statement;
	another_statement;
	more_statements;
	return 0; // Return 0 or EXIT_SUCCESS if completed successfully, and return EXIT_FAILURE if it did not
}
// NOTE: You can pass arguments by value (ie. duplicates) or by reference (in which case the function's
// parameters must be eg, "type_of_parameter& name_of_parameter", with the ampersand indicating "reference")

// NOTE: Arguments passed by reference do not require a copy, and is thus less computationally taxing, so if
// you need to access arguments actively without modifying them and also spare the computer the resources used
// in making copies, use "const type_param& name_param", which will ensure the parameters aren't changed but
// can still be used in the function without requiring any duplication

// NOTE: If you want to pass an array as an argument to a function, THAT'S where you need to use a pointer, as
// passing the address of the array's location in memory as the actual argument is more efficient than passing
// the array itself

inline type_of_return_value inline_function_name (type_param name_param, ...)
{
	// Inline functions specify "execute this code when called, don't process beforehand" to save on efficiency
	// As before
	stuff;
	extra_stuff;
	return 0;
}

// Function template
template <template_parameters> // "template <typename SomeType>" and "template <class SomeType>" are equivalent in this context
function_type function_name (type_param name_param, ...) // Function declaration follows
{
	do_some_things;
  return 0;
}

// Template instantiation - this calls function_name with the required template arguments as well as its specific function arguments
function_name <template_arguments> (function_arguments)

// Namespace declaration (somewhere in between global and local scope; useful for avoiding name collisions)
namespace namespace_identifier
{
	variable_type variable_name;
	type type_name;
	function_type function_name(function_parameters)
}
// NOTE: To access the specific things contained in namespace "namespace_identifier" you need to specify the namespace with them:
// eg. namespace_identifier::variable_name, etc...
// Or the namespaces must be specifically included using "using"!
// eg. "using namespace namespace_identifier", or "using namespace_identifier::variable_name" for a specific variable, which you
// can call "variable_name" instead of "namespace_identifier::variable_name" from then on within that scope

// Arrays
element_type array_name [number_of_elements] = {}; // The curly brackets are optional but best/safest to leave them in as they initialise all elements to zero
element_type array_name [number_of_elements] {}; // Equivalent to above
// Pseudomultidimensional arrays can be achieved using multi[x][y] = pseudomulti[x*y]!  Saves a lot of memory too!

// Address-of operator "&"
variable_name = &variable_whose_address_is_being_passed;
// So now variable_name holds the actual value of the address in memory of variable_whose_address_is_being_passed
// Say variable_whose_address_is_being_passed = "Cheese" and is assigned the memory location 1234
// variable_name = 1234 and NOT "Cheese"!

// Pointers are variables which hold the memory address of other variables, eg. variable_name above is a pointer
type_of_value_pointed_to * pointer_name;

// Dereference operator "*"
variable_fluff = *variable_name; // Assigns [the value pointed to by the pointer variable_name] to variable_fluff
// * means "value pointed to by"
// So, using the assignments as above, variable_fluff = "Cheese"
// And if, say, variable_soft = variable_name, then variable_soft = 1234

// The following indirectly assigns 10 to value_fluff
mypointer = &value_fluff;
*mypointer = 10;