#!/bin/bash
# setting up the environment for SGE:
#$ -cwd -V
#$ -m e
#$ -M james@master.xenon.local
#$ -pe smp scol.inp 
#$ -N -c
#$ -e -c.error
#$ -o -c.output

/opt/openmpi/gfortran/1.6.5/bin/mpirun --mca btl ^tcp -n scol.inp /usr/local/cp2k/2.6.0/bin/cp2k.popt  > .log

