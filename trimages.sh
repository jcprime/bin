#!/bin/bash
# trimages.sh
# One-liner (requires ImageMagick to be properly installed) to
#   - Take JPG and PNG files in the directory from which it is called
#   - Trim them (to first instance of non-white pixels from outer edges)
#   - Save them, overwriting the original files in the process

for file in *.{jpg,png}; do magick "$file" -trim "$file"; done
