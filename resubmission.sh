#!/bin/bash
# resubmission.sh
# Takes contents of all separate files and squidges them into one (for ease of reading/editing/stuff)

bigfile="0x_ResubmissionFULL.tex"
cd ~/words/ || exit
> "$bigfile"
for file in "0x-01_Expt.tex" "0x-02_Hypothesis.tex" "0x-03_Methodology.tex" "0x-04_ResultsDiscussion.tex"; do
    cat "$file" >> "$bigfile"
    echo >> "$bigfile"
done
