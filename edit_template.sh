#!/bin/bash
# edit_template.sh
# Copies edited-in-place (given) CP2K template file to (given) named file; needs in-script editing to change zeolite, etc.
# REQUIRES file-containing directory to be provided as first argument (for ease of command-line for-looping)

filedir="$1"
gulp_path="$(realpath "$filedir")" # Ensures you get full absolute path (when $1 isn't guaranteed to!)

zeo="mnat"
path="/home/james/test/cp2k/$zeo"
template_file="/home/james/test/cp2k/$zeo/template.inp"

# Set all "constants"
scf_guess="ATOMIC"
converge_to="1.0E-006"
inner_convergence="1.0E-003"
outer_convergence="1.0E-004"
minimiser="CG"
preconditioner="FULL_SINGLE_INVERSE"
xc="PBE"
max_inner_cycles="1000"
max_outer_cycles="5000"
#cutoff="750"
#rel_cutoff="60"
extrapolation="ASPC" # Or something
run_type="CELL_OPT"

# Move to file-containing directory
cd "$gulp_path" || (echo "Cannot enter $filedir directory!" && exit)

# Grab filename from directory name (which needs to be obtained using basename instead of dirname, oddly enough)
project_name="$(basename $filedir)"
coord_file="${project_name}.cp2k"
restart_file="${project_name}-1.restart"

gin_file="${project_name}.gin" # Gives e.g. lau03_N12.gin
cell=$(grep -A1 'cell' "${gin_file}" | tail -n 1 | tr -s ' ' | awk '{$1=$1};1') # NB: don't need tr -s ' ' when awk snippet's there, but I've left 'em both in for future reference

# Grab cell parameters again from .gin file in given directory
abc="$(echo "$cell" | tr -s ' ' | cut -d ' ' -f 1-3)"
angles="$(echo "$cell" | tr -s ' ' | cut -d ' ' -f 4-6)"

# Make All The Replacements
sed -e "s/PH_project_name/${project_name}/g" \
    -e "s/PH_coord_file/${coord_file}/g" \
    -e "s/PH_abc/${abc}/g" \
    -e "s/PH_angles/${angles}/g" \
    -e "s/PH_restart_file/${restart_file}/g" \
    -e "s/PH_scf_guess/${scf_guess}/g" \
    -e "s/PH_converge_to/${converge_to}/g" \
    -e "s/PH_inner_convergence/${inner_convergence}/g" \
    -e "s/PH_outer_convergence/${outer_convergence}/g" \
    -e "s/PH_minimiser/${minimiser}/g" \
    -e "s/PH_preconditioner/${preconditioner}/g" \
    -e "s/PH_xc/${xc}/g" \
    -e "s/PH_max_inner_cycles/${max_inner_cycles}/g" \
    -e "s/PH_max_outer_cycles/${max_outer_cycles}/g" \
    -e "s/PH_extrapolation/${extrapolation}/g" \
    -e "s/PH_run_type/${run_type}/g" \
    $template_file > "${project_name}.inp"

# REMOVED THE BELOW BECAUSE NOT CONSTANT ACROSS THE ZEOLITES
    #-e "s/PH_cutoff/${cutoff}/g" \
    #-e "s/PH_rel_cutoff/${rel_cutoff}/g" \
