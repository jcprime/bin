#!/bin/bash
# lastpdbstep.sh
# Extracts last step from CP2K-output PDB file
# (assuming it's printed every step in the same file, one after the other)
# REQUIRES first argument to be filename of original PDB file

orig_file="$1"
output_file="${orig_file%%-*}-final.pdb"

lineno=$(awk '/REMARK/ {var=NR} END{print var}' "$orig_file")
awk -v lineno="$lineno" 'NR>=lineno' "$orig_file" > "$output_file"
