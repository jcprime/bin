#!/bin/bash
# Adapted from ~/bin/editing/faff.sh (to replace one atom with another in a given input file)
# AS YET UNFINISHED!
set -o nounset # Exits script if it comes across an uninitialised variable
set -o errexit # Exits if any statements return non-true value

echo
echo
echo "****************************************************"
echo "**** Script to set up directories and input     ****"
echo "**** files for CP2K, using GULP input files as  ****"
echo "**** starting points.                           ****"
echo "****                                            ****"
echo "**** Takes path to GULP input files as argument ****"
echo "**** and grabs the coordinates in fractional    ****"
echo "**** format, converts them to .xyz format for   ****"
echo "**** CP2K-ing.                                  ****"
echo "****                                            ****"
echo "**** Creates a directory for every GULP file it ****"
echo "**** finds, copies the template CP2K input file ****"
echo "**** (currently semi-hard-coded) into the given ****"
echo "**** directory, copies the .xyz-format          ****"
echo "**** Cartesian coordinate file across as well,  ****"
echo "**** and replaces template variables in the     ****"
echo "**** CP2K input file with those it finds from   ****"
echo "**** the GULP input file (or otherwise user-    ****"
echo "**** specified).                                ****"
echo "****                                            ****"
echo "**** Written by James Prime November 2016       ****"
echo "****************************************************"
echo
echo

#Below is a command to find all directories (including those with the same exact paths), for potential use to navigate with more ease??
#find / -type d -name "nacl*test?" 2> /dev/null

# Check for arguments, and assign path to GULP files accordingly
if [[ "$#" -eq "1" ]]; then
    pass
fi

# Find GULP input files, either given as argument or hard-coded in gulp_path
let i=$4-$3-1
echo "$i directories, coming right up."
for i in ~/klmc/; do
    echo "Creating directories for $1$2 for n = $3 , ..., $4 ."
    mkdir "$1$2_$i"
    cp -r ~/klmc/template/ ~/klmc/"$1$2_$i"/
    #Using an in-place text editor to search and replace the "old" input file with the "new" input file in the run.job file so KLMC knows which file to work with
    echo "%s/NaCl.gin/$1$2_$i.gin/g
    w
    q
    " | ex ~/klmc/"$1$2_$i"/data/run.job
done
#OK, this is fine for what we want it to do - no extraneous directories or overwriting what we've just created, as far as I can tell...
###############################################################################

###############################################################################
#See ~/bin/ginputs.sh
#Copy .gin files from each template/data/ directory into new separate n_range directory, rename as "$1$2_$?.gin", and these will be the templates for the first lot of tests (pre-data-mining)
#------------------------------------------------------------------------------
#(DONE ALREADY - doesn't need doing more than once!) mkdir ~/klmc/gulp_input/
for i in ~/klmc/template/NaCl.gin; do
    echo "Copying across .gin file for n = '$i'."
    cp ~/klmc/template/data/NaCl.gin ~/klmc/gulp_input/tempfile.gin
    mv ~/klmc/gulp_input/tempfile.gin ~/klmc/gulp_input/"$1$2_$i".gin
    match="Na  shel 0.0 0.0 0.0  1.0 1.0 0.0\rCl  shel 0.0 0.0 0.0 -1.0 1.0 0.0\rspecies"
    insert="$1  shel 0.0 0.0 0.0  1.0 1.0 0.0\n$2  shel 0.0 0.0 0.0 -1.0 1.0 0.0\n$1  shel 0.0 0.0 0.0  1.0 1.0 0.0\n$2  shel 0.0 0.0 0.0 -1.0 1.0 0.0"
    file=~"/klmc/gulp_input/$1$2_$i.gin"
    count=1
    while [ "$count" -le "$i" ]; do
        printf "%s/'$match'/'$insert'\nspecies/g
        w
        q
        " | ex ~/klmc/gulp_input/"$1$2_$i".gin
        let count=count+1
    done
done
###############################################################################

###############################################################################
#See ~/bin/all_the_input.sh
#Now creating all the other .gin files from the NaCl ones, to go into the ~/klmc/gulp_input folder
#------------------------------------------------------------------------------
for i in ~/klmc/gulp_input/nacl_*.gin; do
    cp ~/klmc/gulp_input/nacl_"$i".gin ~/klmc/gulp_input/"$1$2_$i".gin
    cp -r ~/klmc/template/ ~/klmc/gulp_input/"$1$2_$i"/
    #Using an in-place text editor to search and replace the "old" input file with the "new" input file in the run.job file so KLMC knows which file to use
    echo "%s/NaCl.gin/$1$2_$i/g
    w
    q
    " | ex ~/klmc/"$1$2_$i"/data/run.job
done
###############################################################################

###############################################################################
#See ~/bin/find_energies.sh
#Run through gulp over all those directories, and grep for "Total energy" in the .gout files to a .txt file or something
#------------------------------------------------------------------------------
for i in $(~/klmc/gulp_input/*.gin); do
    gulp ~/klmc/gulp_input/"$1$2_$i".gin 3.4 1 "$1$2_$i"
done
###############################################################################

###############################################################################
#See ~/bin/datamining_input.sh
#Make .gin files of top 3 structures and rename according to rank (need .gout files for this purpose, or use "dump" when running original .gin files to output the actual optimised structure as a named file of your own)
#------------------------------------------------------------------------------
code
###############################################################################

###############################################################################
#See ~/bin/datamining_atom_replacement.sh
#Replaces all instances of $1 (first argument) with $2 (second argument)
#------------------------------------------------------------------------------
argc=$#
if [ "$argc" == 2 ]; then
    echo Replacing "$1" atoms with "$2" atoms
    for i in ./*.gin; do
        echo Processing "$i"
        cp "$i" "$i".orig
        sed "s/'$1'/'$2'/g" "$i" > tmp
        mv tmp "$i"
    done
else
    echo "Usage: datamine.sh <Atom to replace> <Replacement atom>"
    echo "e.g.: datamine.sh Na K"
fi
echo "Done!"
###############################################################################

#------------------------------------------------------------------------------
#SNIPPETS AND CONFUSIONS
#------------------------------------------------------------------------------
#Apparently printf is better at acknowledging escape characters than echo is, hence the change, but I ended up looping over i anyway
#printf "%s/$1  shel 0.0 0.0 0.0  1.0 1.0 0.0\r$2  shel 0.0 0.0 0.0 -1.0 1.0 0.0\rspecies/$2  shel 0.0 0.0 0.0 -1.0 1.0 0.0\n$1  shel 0.0 0.0 0.0 -1.0 1.0 0.0\n$2  shel 0.0 0.0 0.0  1.0 1.0 0.0\nspecies/g

#Thought I was doing one thing, actually doing another...
#cp -r ~/klmc/template/ ~/klmc/gulp_input/"$1$2_$i"/

#Matt Farrow's original code for data-mining
#Replaces all instances of $1 (first argument) with $2 (second argument)
argc=$#
if [ "$argc" == 2 ]; then
    echo Replacing "$1" atoms with "$2" atoms
    for i in ./*.gin; do
        echo Processing "$i"
        cp "$i" "$i".orig
        sed "s/'$1'/'$2'/g" "$i" > tmp
        mv tmp "$i"
    done
else
    echo "Usage: datamine.sh <Atom to replace> <Replacement atom>"
    echo "e.g.: datamine.sh Na K"
fi
echo Done!
