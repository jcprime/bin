#!/bin/bash
# tex_edit.sh
# Edits .tex files accordingly if referenced image filenames are changed

#textype=("mc100" "mc1000" "r100" "r1000" "dmol")

replace=("]{L" "]{N" "]{K" "]{R")
with=("]{../structures/L" "]{../structures/N" "]{../structures/K" "]{../structures/R")

replace_end=("_lm1}" "_lm2}" "_lm3}" "_lm4}" "_lm5}")
with_end=("_lm1" "_lm2" "_lm3" "_lm4" "_lm5")

#To do all the main replacements of text for species
#i="1"
#while [[ "$i" -le "24" ]]; do
# a="0"
# while [[ "$a" -le "3" ]]; do
#  b="0"
#  while [[ "$b" -le "3" ]]; do
   c="0"
   while [[ "$c" -le "3" ]]; do
    cd /home/cirrusprime/Desktop/tex
    for i in tables_*.tex; do
     part1="${i%%.tex}"
     part2="${i#*.}"
     method="${part1##*_*_*_}"
     cp -f /home/cirrusprime/Desktop/tex/"$i" /home/cirrusprime/Desktop/tex/"$i".orig
     match1="${replace[c]}"
     replace1="${with[c]}"
     echo "%s/${match1}/${replace1}/g
     w
     q
     " | ex /home/cirrusprime/Desktop/tex/"$i"
     match2="${replace_end[c]}"
     replace2="${with_end[c]}_$method"
     echo "%s/${match2}/${replace2}/g
     w
     q
     " | ex /home/cirrusprime/Desktop/tex/"$i"
    done
    c=$((c+1))
   done
#   b=$((b+1))
#  done
#  a=$((a+1))
# done
# i=$((i+1))
#done
