# xyz_dehydrate.sh
# Takes xyz-style coordinate file, identifies water molecules, outputs new xyz-style
# coordinate files with each water molecule in turn commented out

set counter to 1
# While loop (while not EOF)
	# Read in input file and note the line numbers and coordinates of the component atoms of the water molecules
	# TODO: Add functionality for command-line input of the specific atoms to check for and the molecule to be constructed
	if line starts with a hash
		ignore line (do not store in VECTOR but do store in buffer)
		increment counter by 1
	else
		read line and store in buffer-equivalent
		save atom symbol and coordinates in line_n vector (or array) where n is line number (== counter)
		increment counter by 1
	end if
# End while loop

set new_counter to 1
set water_counter to 0
# While loop (while there remain unmatched atoms of the required type)
	if new_counter-th vector is already matched
		increment new_counter by 1
		skip the rest of the loooop
	else
		# Calculate which atoms combine to form which water molecule
		Pass subroutine (POINTERS TO????) coordinates of new_counter-th vector (ie. elements 1, 2, and 3)
		Subroutine returns vectors of atoms within bonding distance of said first vector atom
		# Take details of which atoms have been matched
		Take first element (element 0: line number) of original and returned vectors
		Copy them into new water_x vector where x == new_counter
		Increment water_counter by 1
		Add new BOOLEAN element to each original vector (element 4) where true means matched, and either false or absent element 4 means not yet matched
		Increment new_counter by 1
	end if
# End while loop

# Count water molecules (ie. equivalent to number of vectors produced)
calculate highest x of water_x vectors, which would be value of water_counter by the end of the previous loop
set water_total = water_counter

# While loop
	# Write out new files, one per vector, line by line, commenting out lines in given vector's elements
	Stuff
	# Create copies of original .inp file, numbering them accordingly, and editing the coordinate file marker
	# to match that of the new coordinate file
	Stuff
# End while loop
