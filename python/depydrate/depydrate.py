#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 21:58:42 2017

@author: james
"""

import math
import glob
import os

# My modules
import elements
import bondlengths as bls
import definitions as dfns
import mymaths

# CLASSES #########################################################################################

# Classes from structures.h: energy, internal_energy, stats, atom, simple_atom, bond, atom_number, links, symm_ops, list_partition, types, hot_list

class Configuration(object):
    """docstring for Configuration"""
    def __init__(self, name):
        super(Configuration, self).__init__()
        self.name = name

    def function():
        pass

"""
Files output by CP2K:
 - {basename}-1.restart: contains input and coordinate stuff in one file
 - {basename}-RESTART.wfn: BINARY
 - {basename}-trajectory-pos-1.pdb:
"""

# FUNCTIONS #######################################################################################

def check_files(path):
    """
    Checks files in given path to find the relevant ones needed for processing

    Where:
        - The restart file is ends in ".restart", and will be read in and written out with a new name and very few edits (only number of atoms and suchlike)
        - The xyz file, if there is one, which may be used as a starting coordinate file, if desired
        - The pdb file, if there is one, which may be used as a starting coordinate file, if desired
        - The cp2k file, which is the constantly-updated coordinate file, so if used, would only need copying from original location to new location and the relevant dehydrated molecule removed
    """
    for file in glob.glob(*):
        ext = os.path.extsep(file).lower()
        filepath = os.path.abspath(file)
        if ext == "xyz":
            xyzfile = filepath
        elif ext == "pdb":
            pdbfile = filepath
        elif ext == "restart":
            restartfile = filepath
        elif ext == "cp2k":
            coordfile = filepath
        return xyzfile, pdbfile, restartfile, coordfile

# MAIN CONTENT ####################################################################################

with open('filename') as file:
    pass
