#!/bin/bash
# find_min.sh finds the lowest-energy configuration in a given set
# of GULP output files, then creates a GULP input file from that
# configuration, generates the script to run them all, and recursively
# generates the same process until the global minimum is reached

# Sorts final energies numerically and outputs filenames with them
grep -l "Final ene" *out | sort -n

# Takes last result filename (ie. lowest-energy) as variable to find
# and use as input for the next H2O extraction run
next=$(grep -l "Final ene" *out | sort -n)

# Generate input files from lowest-energy output file, with one less H2O

# Reduce number of H2Os expected in the number of [BLAH]_N... files
# Call new script with the arguments informing the range of files to be submitted

# Erm.  Fluff.
