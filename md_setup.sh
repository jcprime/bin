#!/bin/bash
# md_setup.sh
# Sets up files for MD simulations

pressure="0.100"
zeo="lau"

for temp in 050 075 100 125 150 175 200 225 250 275 300 325 350 375 400 425 450 475 500; do
    #cp ./template.gin ./"${zeo}_${temp}.gin"
    temperature="$(printf '%.3f\n' "$((10#$temp))")"
    #echo "$temperature"
    file="${zeo}00"
    #file="${zeo}_${temp}" # If in main directory
    # Make All The Replacements
    sed -e "s/PH_TEMP/${temperature}/g" \
        -e "s/PH_PRESSURE/${pressure}/g" \
        -e "s/PH_FILE/${file}/g" \
        template.gin > "${temp}/${zeo}00.gin"
done
