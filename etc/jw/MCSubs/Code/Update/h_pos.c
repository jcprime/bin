/****h_pos calc position of H from 2T coords and O coord****/
/****hacked from Rob version****/
/*****CARTESIAN****/
#include <stdio.h>
#include <math.h>
#include "structures.h"
#include "header.h"
#define OH_BONDLENGTH 1.0


simple_atom h_pos(simple_atom *si1, simple_atom *si2, simple_atom *o)
{
   simple_atom h,h_alt;
   double si_si[4],si1_o[4],si2_o[4],norm[4],h_vec[4],bit_vec[4];
   double dist_si1o,dist_si2o,mag_hvec;
   double dx,dy,dz,dist1,dist2;
   int i;
/*
   strcpy (h.atyp,"H   ");
   strcpy (h.styp,"CORE");
   strcpy (h_alt.atyp,"H   ");
   strcpy (h_alt.styp,"CORE");
   h_alt.us_ind = 0;
   h.us_ind = 0;
*/


   
   for (i=1 ; i<=3 ; i++)
      si_si[i] = si1->coord[i]-si2->coord[i];

   for (i=1 ; i<=3 ; i++)
      si1_o[i] = si1->coord[i]-o->coord[i];
   dist_si1o = sqrt (si1_o[1]*si1_o[1] +
                     si1_o[2]*si1_o[2] +
                     si1_o[3]*si1_o[3]);

   for (i=1 ; i<=3 ; i++)
      si2_o[i] = si2->coord[i]-o->coord[i];
   dist_si2o = sqrt (si2_o[1]*si2_o[1] +
                     si2_o[2]*si2_o[2] +
                     si2_o[3]*si2_o[3]);

   norm[1] = si_si[2]*si1_o[3] - si_si[3]*si1_o[2];
   norm[2] = si_si[3]*si1_o[1] - si_si[1]*si1_o[3];
   norm[3] = si_si[1]*si1_o[2] - si_si[2]*si1_o[1];

   h_vec[1] = norm[2]*si_si[3] - norm[3]*si_si[2];
   h_vec[2] = norm[3]*si_si[1] - norm[1]*si_si[3];
   h_vec[3] = norm[1]*si_si[2] - norm[2]*si_si[1];

   mag_hvec = sqrt (h_vec[1]*h_vec[1] + h_vec[2]*h_vec[2] + h_vec[3]*h_vec[3]);
   for (i=1 ; i<=3 ; i++)
   {
      bit_vec[i] = h_vec[i] * -(OH_BONDLENGTH/mag_hvec) ;
      h.coord[i] = o->coord[i] + bit_vec[i]; 
      h_alt.coord[i] = o->coord[i] - bit_vec[i]; 
   }

   dx = h.coord[1] - si1->coord[1];
   dy = h.coord[2] - si1->coord[2];
   dz = h.coord[3] - si1->coord[3];

   dist1 = (sqrt (dx*dx + dy*dy + dz*dz));

   dx = h_alt.coord[1] - si1->coord[1];
   dy = h_alt.coord[2] - si1->coord[2];
   dz = h_alt.coord[3] - si1->coord[3];

   dist2 = (sqrt (dx*dx + dy*dy + dz*dz));

/*DEBUG**********
   printf("Atom T1 %f %f %f\n", si1->coord[1], si1->coord[2], si1->coord[3]);
   printf("Atom T2 %f %f %f\n", si2->coord[1], si2->coord[2], si2->coord[3]);
   printf("Atom  O %f %f %f\n", o->coord[1], o->coord[2], o->coord[3]);
   printf("Atom  H %f %f %f\n", h_alt.coord[1], h_alt.coord[2], h_alt.coord[3]);
********DEBUG*/

   return h_alt;
}
