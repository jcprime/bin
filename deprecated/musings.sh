#!/bin/bash
# musings.sh
# A script to remind me of the current things that I need to do

installs=("Eclipse (and TeXlipse and all that stuff)"
"Mendeley"
"LaTeX, BibTeX, and TeXstudio"
"RStudio"
"FocusWriter"
"Pandoc"
"MultiMarkdown"
"Some kind of markdown editor with live preview"
"Unison for Linux"
"Evernote"
"Dropbox"
"Sumatra PDF viewer")
i="0"

echo

# Prints out the things that need installing from the install array above
echo "Install the things please; that would be:"
until [[  ! "${installs[i]}" ]]; do
	echo "- ${installs[i]}"
	i=$((i+1))
done

individuals=("Grab ethernet cable from somewhere"
"Do the sodding updates and get compilers and gdebi and stuff when connected to the internet"
"Sort out Scrivener's Proper Install please"
"Set up cron jobs for syncing and backing up via ssh, amongst other things"
"Sort out static IP"
"De-stick mouse scroll wheel")
j="0"

# Prints array of individual tasks
echo 
echo "Not to mention:"
until [[ ! "${individuals[j]}" ]]; do
	echo "- ${individuals[j]}"
	j=$((j+1))
done

echo
echo "Once you've got all that done, you're laughing.  So prepare for a life of misery."
echo
