#!/bin/bash
# checkstatus.sh
# Reads off output of git status in all directories in checklist.txt (so I know if I've forgotten to commit something before I turn my machine off! NB: Originally looked in dirlist.txt, but was taking too long to run so removed some directories from it --> checklist.txt)

if [ "$#" -eq "1" ]; then
	dirlist_file="$1"
elif [ "$#" -eq "0" ]; then
	dirlist_file="$HOME/bin/checklist.txt"
fi

#for directory in "bin" ".dotfiles" "words" "test" "writings" "dotfiles-universal" "ice" "webstuff" "mfarmer" "flp" "wallpapers" ".config/sublime-text-3/Packages/User" "Library/Application Support/Sublime Text 3/Packages/User"; do
while read -r directory; do
    if [ -d "$HOME/$directory" ]; then
        cd "$HOME/$directory" || continue
        echo
        echo "$directory":
        git status
        git log origin/master..HEAD
        #echo
        cd "$HOME"
    fi
done < "$dirlist_file"
