#
# This is the default standard .login provided to csh users.
# They are expected to edit it to meet their own needs.
#
# The commands in this file are executed when a csh user first
# logs in.  This file is processed after .cshrc.
#
# $Revision: 1.6 $
#


if ($term == "vt100") then
	stty  erase  kill 
endif
if ($term == "vt220") then
	stty  erase  kill 
endif
if ($term == "vt320") then
	stty  erase  kill 
endif

if ($term == "iris-ansi-net") then
		stty  intr 
		unset autologout
	endif

if ($term == "xterm") then
	stty  intr 
	unset autologout
	endif

	tset -Q
# Set the default X server.
#setenv DISPLAY :0

#add user bin to path
setenv PATH ${PATH}:/home/$USER/bin
