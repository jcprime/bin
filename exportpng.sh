#!/bin/bash
# exportpng.sh
# Takes path as argument

path="$1"
cd "$path" || exit

for file in *{.xyz,-final.pdb}; do
    pymol -r ~/bin/pymol_test.py -- "$file"
done
