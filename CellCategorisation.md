Cell type | a=b=c | a!=b!=c | alpha=90 | beta=90 | gamma=90 | equal angles | unequal angles | one angle 120
----------|-------|---------|----------|---------|----------|--------------|----------------|---------------
Cubic     | true  | false   | true     | true    | true     | true         | false          | false
Rhombohedral | true | false | false    | false   | false    | true         | false          | false
Tetragonal | false | false  | true     | true    | true     | true         | false          | false
Hexagonal | false | false   | true     | true    | false    | false        | false          | true
Monoclinic | false | false  | true     | false   | true     | false        | false          | false
Orthorhombic | false | true | true     | true    | true     | true         | false          | false
Triclinic | ~~~~~~~~~~~~~~~~~~~~~~ any remaining cases ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~