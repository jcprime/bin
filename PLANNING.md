# Planning Programming Projects

## Diagrams and Graphs

### Sequence Diagrams

These describe interactions among classes in terms of an exchange of messages over time.  Predicts behaviour.

### UML Diagrams

UML = Unified Modelling Language.

Lots of types of UML diagram (sequence diagrams are one of them).  More likely than not, it'll be the class diagram and/or activity diagram that gets used.

## Software Design Principles

### Don't Repeat Yourself (DRY)

Your code shouldn't have lots of repeated statements that aren't function calls.

### Separation of Concerns

One module for each concern.  For example: HTML deals with page structure, CSS deals with page styling, and JavaScript deals with page behaviour.  Although these are actually distinct languages and as such are not really module-per-concern, they are good illustrations of what this means.

### Don't Make Me Think

Write your code so that anyone can understand it.  Don't make it unnecessarily short, because less-experienced coders could look at it and not have a clue what you've done.  Don't sacrifice readability for fewer lines of code.

### Keep It Simple, Stupid (KISS)

Do this by adding comments, and by not trying to be too intellectually superior over anyone else who might come across your code.  Your goal is *not* to confuse the hell out of them.

## General Points

* Keep your variables in lists/arrays/whatever, and use functions for everything else; pass the variables to the function in all cases, so the function can be generalised for any variable of the same type