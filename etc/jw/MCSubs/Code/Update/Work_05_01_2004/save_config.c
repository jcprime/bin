
#include <stdio.h>
#include "structures.h"
#include "header.h"
void save_config(FILE *file_fp, int config, int num_replace_atoms, 
                 double final_energy, double final_gnorm,
                   char *p_gulp_achieved_flag,
                 int *p_replaceable_index,int *p_replaceable_done, atom *whereisthecation)
                   
{
int i, iloop;
int *index;
int *done;



             fprintf(file_fp, "%s %10.6f %10.6f %s %d ",insert_ele, final_energy,
                         final_gnorm, p_gulp_achieved_flag, config);
             for (i=0;i<=num_replace_atoms;i++)
                  {
                  done = p_replaceable_done+i;
                  if (*done == 1)
                     {
                      index = p_replaceable_index+i;
/*aileen changed this so that the na positions wouold be recorded to the configs file */ 
                      fprintf(file_fp, "%d %lf %lf %lf ", *index, whereisthecation[i].x, whereisthecation[i].y, whereisthecation[i].z);
                     }
                  }
             fprintf(file_fp, "\n");
             
             
return;
}

