#!/bin/bash
# erm_copy_v2.sh (Edited to do some mini-features of dehydrate which it suddenly decided to stop doing 01/2016)
# The Recursive Version (deprecated in favour of the step-at-a-time dehydrated.sh)

# Move into directory specified in $path
if [[ "$#" -eq "1" ]]; then
	path="$(dirname "$1")/$(basename "$1")"
elif [[ "$#" -eq "0" ]]; then
	path="$(pwd)"
fi
cd "$path" || exit

function dehydration_check() {
	# Looks at listed files in current directory and takes the word-only common part of the filename(s) as a variable
	file_list=$(find . "*.gout" | cut -d'_' -f1)
	zeolite="${file_list%%[0-9]*}" # gives you eg. "scol"
	step_full=$(echo "$file_list" | awk '{print $2}') # gives you eg. "scol03"
	step_number="${step_full##[A-Za-z]*[^0-9]}" # gives you eg. "03"

	# Resets step_number to the re-padded version of the first argument, if given
	if [[ "$#" -eq "1" ]]; then
		step_number="$(printf '%02d' "$1")"
	fi

	# Obtain x (stage of dehydration - ie. number of water molecules missing in the next step)
	x_new=$((step_number+1)) # where x_new is the step for which files need to be created still (NB: removes leading zero if it exists)
	x=$(printf '%02d' "$x_new") # where x = x_new with padding, so you get the same number of digits for ease of sorting!

	#no_fail=0
	fail_check=$(grep -n 'fail' "*out")
	#no_error=0
	error_check=$(grep -n 'ERROR' "*out")
	#no_min=0
	min_check=$(grep 'Conditions for a minimum have not been satisfied' "*out")
	ginfile_count=$(find . -maxdepth 1 -type f -name "$zeolite$step_number\_N*.gin" -exec printf x \; | wc -c)
	dumpfile_count=$(find . -maxdepth 1 -type f -name "$zeolite$step_number\_N*.dump" -exec printf x \; | wc -c)

	if [[ "$fail_check" == "" ]]; then
		#no_fail=0
		echo "No obvious failures so far!"
	else
		#no_fail=1
		echo
		echo "We've got a few failures to sort out, sorry!  Here they come:"
		echo "$fail_check"
		echo "Erm, we'll just be over here in the corner while you sort 'em out, okay?"
		echo
	fi

	if [[ "$error_check" == "" ]]; then
		#no_error=0
		echo "No obvious errors so far!"
	else
		#no_error=1
		echo
		echo "We've got a few errors to sort out, sorry!  Here they come:"
		echo "$error_check"
		echo "Erm, we'll just be over here in the corner while you sort 'em out, okay?"
		echo
	fi

	if [[ "$min_check" == "" ]]; then
		#no_min=0
		echo "No obvious minimisation troubles so far!"
	else
		#no_min=1
		echo
		echo "We've got a few minimisation troubles to sort out, sorry!  Here they come:"
		echo "$min_check"
		echo "Erm, we'll just be over here in the corner while you sort 'em out, okay?"
		echo
	fi

	if [[ "$fail_check" == "" && "$error_check" == "" && "$min_check" == "" && "$dumpfile_count" == $((ginfile_count-1)) ]]; then
		echo
		echo "Looks like no obvious errors, and all your dumpfiles have appeared as expected, so huzzah!"
		echo
		# Print the output energies to terminal and output them to a file
		grep 'Final en' "*out" | sort -n -k5
		grep 'Final en' "*out" | sort -n -k5 > ene/"$step_full".txt
		# Find lowest energy structure for the last dehydration step (where -k5 means sort by the value in the 5th column, with whitespace as the default delimiter, and where n is numerical sort)
		top_gout=$(head -n1 ene/"$step_full".txt | cut -d':' -f1) # Gives eg. scol03_N18.gout
		top_full="${top_gout%.*}" # Gives eg. scol03_N18
		echo "The lowest-eenrgy configuration for dehydration step $step_number is $top_full." # Without extension, for submitting to dehydrate!
		echo
		echo "Copying $top_full.dump to $zeolite$x.gin"
		cp "$top_full.dump" "$zeolite$x.gin"
		echo
		echo "Copying all $top_full files into ground-state directory"
		cp "$top_full".* gs
		echo
		echo "Moving all $step_full files into $step_number directory"
		mv "$step_full"* "$step_number"
		# Generate the next set of test configurations by calling dehydrate
		echo
		echo "Calling dehydrate for $zeolite$x"
		dehydrate "$zeolite$x"

		# TESTING TESTING TESTING STILL
		# Check how many files there are starting with "$zeolite$x\_N"
		file_number=$(find . -maxdepth 1 -type f -name "$zeolite$step_number\_N*.gin" -exec printf x \; | wc -c)
		echo
		echo "Submitting the newly-dehydrated .gin files to The Queue"
		i="0"
		while [[ "$i" -le "$file_number" ]]; do
			qgulp "$zeolite$x\_N$i".gin
			i=$((i+1))
		done

		# Strip out $x from start of $top and replace it with ${x+1}
		next="${top_full/${step_number#0}/$x}"
		# Copy dumpfile for the lowest-energy structure to the equivalent next-step incremented input file
		cp "$top_full".dump "$next".gin
	fi
}

# Run dehydration_check once, with intention to make recursive and include waiting for checks that the runs have finished at each stage!
while [[ "${step_number#0}" -ge "0" ]]; do
	dehydration_check "$step_number"
done

# Check all .gout files have actually achieved optimisation where they're supposed to at the end!
for file in *.gout; do
	opti_check=$(grep 'achi' "$file")
	if [[ "$opti_check" != "" ]]; then
		pass
	fi
done

# printf "%02d " {0..9} # This "pads" the numbers 0 to 9, each with a leading zero
# ls na[0-9]* | sort -rn | awk '{FS="_"; printf "%03d_new_file.php\n",$1+1;exit}' # Using awk to do leading zeroes now

#------------------------------------------------------------------------------------------
# SUPERCEDED
#------------------------------------------------------------------------------------------
# Obtain x (stage of dehydration - ie. number of water molecules missing in the next step)
#throwaway=$(find *.gout | sort -rn) # Superceded by file_list
#x_throwaway="${throwaway#zeolite}" # No longer necessary (would lose the zeolite to give eg. 03_N3.whatever)
#x_prev="${x_throwaway%%_N*.gout}" # where x_prev is the step for which the files are to be analysed # SUPERCEDED BY STEP_NUMBER
#top_number=$(head -n1 "$step_full".txt | cut -d'_' -f1-${top_full#0}) # No longer needed

# Call gulp_batch.sh with the arguments it requires (no longer needed because dehydrate is working again)
#"$HOME/bin/gulp_batch.sh $water_total $prefix"
# Below is gulp_batch.sh, which has just been called by this script
#argc="$#"
#water_total="$1"
#prefix="$2"
#for i in {0.."$water_total"}; do
#    /usr/local/gulp/gulp-4.2.mpi < "$prefix"_N"$i".gin > "$prefix"_N"$i".gout
#done
