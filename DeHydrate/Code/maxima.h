#define MAX_LABELS 10 /*max no of different water molecules*/
#define MAX_ATOMS 1000
#define MAX_LINE_LEN 256
#define BUFFER  256
#define PORE_GROUP pore
#define END_CAR "end"
#define NUM_POTENTIALS 28
#define NUM_ELEMENTS 104  /* update when more found we found deuterium! */

#define MAXATOM 8000 /* used as max size of pore - will get round to malloc */
#define MAXTEMPLATE 1000 /* used as max size of template*/
#define MAXFRAGMENTS 50 /* num of diff fragments in library */
#define FILELEN_MAX 128

/****************************************************************/
/***** Maximum array dimension for users of ends in searches ****/
/****************************************************************/

#define MAX_ENDS 20

/****************************************************************/
/***** Maximum list length for pair interaction lists ***********/
/****************************************************************/

#define MAX_PAIR_LIST 100
