#!/bin/bash
# Creates monthly backups of bash history file
# Requires HISTSIZE to be set to a large number to ensure all commands within a month are retained
# Keeps last 200 commands when it rotates history file every month
#
# Typical usage in .bash_profile
# HISTSIZE=90000
# source ~/bin/history_backup.sh
#
# And to search whole history use
# grep xyz -h --color ~/.history/.bash_history.*

#KEEP=200
#BASH_HIST="$HOME/.history/bash_history"
now="$(date +%Y-%m)"
#BACKUP="$HOME/.history/bash_history.${now}"

# History file is newer than backup
#if [ -s "$BASH_HIST" -a "$BASH_HIST" -nt "$BACKUP" ]; then
    # There is already a backup
    #if [[ -f "${HOME}/.history/bash_history.${now}" ]]; then
        #echo "Blanking out the ${HOME}/.history/bash_history.${now} file we found"
        #> "${HOME}/.history/bash_history.${now}"
    #else
        # Create new backup, leave last few commands and reinitialise
        #mv -f "$BASH_HIST" "$BACKUP"
        #tail -n"$KEEP" "$BACKUP" > "$BASH_HIST"
        #history -r
    #fi
    #echo "Aaaand grabbing the ${now} history and squidging it into ${HOME}/.history/bash_history.${now}"
    history | grep "${now}" >> "${HOME}/.history/bash_history.${now}"
#fi
