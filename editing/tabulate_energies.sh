#!/bin/bash
# tabulate_energies.sh
# Creates or overwrites file for energies, and tabulates them in that file using AWK

echo
echo "Starting."
echo

change="dmol"
path="james_p_rsync/$change"
output_path="james_p_rsync/energies"

if [[ ! -d ~/"$output_path" ]]; then
 mkdir ~/"$output_path"
fi

if [[ ! -e ~/"$output_path"/energies_lm1_"$change".txt ]]; then
 touch ~/"$output_path"/energies_lm1_"$change".txt
else
 > ~/"$output_path"/energies_lm1_"$change".txt
fi

M=("Li" "Na" "K" "Rb")
X=("F" "Cl" "Br" "I")

a="0"
while [[ "$a" -lt "4" ]]; do
 b="0"
 while [[ "$b" -lt "4" ]]; do
  echo "${M[a]}${X[b]}" >> ~/"$output_path"/energies_lm1_"$change".txt
  for i in {1..24}; do #Loops over n (preset to avoid taking arguments)
   cd ~/"$path"/"${M[a]}${X[b]}"_"$i"/top_structures/ #Move to directory containing the statistics file
   file=./statistics #File with ranked structures and energies in order
#   to_copy=$(
   awk -v rankawk="3" -v n="$i" -v tab="\t" '$1 == rankawk { print n, tab, $3 }' $file >> ~/"$output_path"/energies_lm1_"$change".txt
#   echo >> ~/"$output_path"/energies_lm1_"$change".txt
#   awk -v rankawk="x" 'NR > 1 && $2 != rankawk { print $1, $2, $3, $4 }' OFS="\t" $file >> ~/"$output_path"/energies_lm1_"$change".txt
#   echo >> ~/"$output_path"/energies_lm1_"$change".txt
  done
  echo >> ~/"$output_path"/energies_lm1_"$change".txt
  b=$((b+1))
 done
 echo >> ~/"$output_path"/energies_lm1_"$change".txt
 a=$((a+1))
done
echo "Finished."
